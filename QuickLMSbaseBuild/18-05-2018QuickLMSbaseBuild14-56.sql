# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.5.5-10.1.25-MariaDB)
# Database: QuickLMSbaseBuild
# Generation Time: 2018-05-18 13:56:14 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table canneds
# ------------------------------------------------------------

DROP TABLE IF EXISTS `canneds`;

CREATE TABLE `canneds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL,
  `tid` int(11) NOT NULL,
  `response` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table classrooms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `classrooms`;

CREATE TABLE `classrooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trainer_id` int(11) NOT NULL,
  `trainee_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `course_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table course_student
# ------------------------------------------------------------

DROP TABLE IF EXISTS `course_student`;

CREATE TABLE `course_student` (
  `course_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `rating` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `course_student_course_id_foreign` (`course_id`),
  KEY `course_student_user_id_foreign` (`user_id`),
  CONSTRAINT `course_student_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE,
  CONSTRAINT `course_student_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `course_student` WRITE;
/*!40000 ALTER TABLE `course_student` DISABLE KEYS */;

INSERT INTO `course_student` (`course_id`, `user_id`, `rating`, `created_at`, `updated_at`)
VALUES
	(10,1,0,'2018-05-13 10:21:56','2018-05-13 10:21:56'),
	(11,1,0,'2018-05-13 10:26:01','2018-05-13 10:26:01'),
	(12,1,0,'2018-05-13 10:26:21','2018-05-13 10:26:21'),
	(13,1,0,'2018-05-13 10:26:45','2018-05-13 10:26:45'),
	(6,9,0,'2018-05-13 10:56:45','2018-05-13 10:56:45'),
	(7,10,0,'2018-05-13 10:56:53','2018-05-13 10:56:53'),
	(8,11,0,'2018-05-13 10:57:02','2018-05-13 10:57:02'),
	(9,12,0,'2018-05-13 10:57:12','2018-05-13 10:57:12'),
	(14,9,0,'2018-05-13 10:58:41','2018-05-13 10:58:41'),
	(15,10,0,'2018-05-13 13:20:12','2018-05-13 13:20:12'),
	(16,11,0,'2018-05-13 13:20:29','2018-05-13 13:20:29'),
	(17,12,0,'2018-05-13 13:20:52','2018-05-13 13:20:52'),
	(18,11,0,'2018-05-17 14:28:48','2018-05-17 14:28:48');

/*!40000 ALTER TABLE `course_student` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table course_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `course_user`;

CREATE TABLE `course_user` (
  `course_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  KEY `fk_p_54418_54417_user_cou_596eece522b73` (`course_id`),
  KEY `fk_p_54417_54418_course_u_596eece522bee` (`user_id`),
  CONSTRAINT `fk_p_54417_54418_course_u_596eece522bee` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_p_54418_54417_user_cou_596eece522b73` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `course_user` WRITE;
/*!40000 ALTER TABLE `course_user` DISABLE KEYS */;

INSERT INTO `course_user` (`course_id`, `user_id`)
VALUES
	(1,1),
	(2,1),
	(3,1),
	(4,1),
	(5,1),
	(6,5),
	(7,6),
	(8,7),
	(9,8),
	(14,5),
	(15,5),
	(16,5),
	(17,5),
	(18,8);

/*!40000 ALTER TABLE `course_user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table courses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `courses`;

CREATE TABLE `courses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `price` decimal(15,2) DEFAULT NULL,
  `course_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `published` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `courses_deleted_at_index` (`deleted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;

INSERT INTO `courses` (`id`, `title`, `slug`, `description`, `price`, `course_image`, `start_date`, `published`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'Mr. Emory Ryan','mr-emory-ryan','Quia doloribus libero facere consectetur ea quas et. Soluta asperiores fugiat explicabo id vitae est non. Sit accusamus temporibus vero modi accusamus.',107.84,NULL,NULL,1,'2018-05-11 14:19:23','2018-05-13 10:15:35','2018-05-13 10:15:35'),
	(2,'Ward Franecki','ward-franecki','Debitis omnis voluptas quos molestias id voluptatem hic. Non commodi minima est enim omnis nam. Excepturi ratione culpa hic debitis adipisci consequuntur maiores est.',5.77,NULL,NULL,1,'2018-05-11 14:19:23','2018-05-13 10:15:38','2018-05-13 10:15:38'),
	(3,'Kiel Bashirian','kiel-bashirian','Occaecati quisquam doloribus ducimus est possimus eius corporis. Iusto cumque commodi a voluptatem est. Quam cupiditate optio rerum nisi sed officia laborum. Recusandae quia cum aut nihil.',22.04,NULL,NULL,1,'2018-05-11 14:19:23','2018-05-13 10:15:41','2018-05-13 10:15:41'),
	(4,'Mr. Stanley Goyette','mr-stanley-goyette','Soluta nisi occaecati in. Libero dolores iusto qui corrupti inventore. Facilis expedita autem expedita.',2.07,NULL,NULL,1,'2018-05-11 14:19:23','2018-05-13 10:15:45','2018-05-13 10:15:45'),
	(5,'Mr. Jadon Hartmann','mr-jadon-hartmann','Vel autem a dicta et in reiciendis est. Debitis totam quo saepe odio eveniet. Nesciunt porro facilis aut.',173.77,NULL,NULL,1,'2018-05-11 14:19:23','2018-05-13 10:15:48','2018-05-13 10:15:48'),
	(6,'120 Hour TESOL','120 Hour TESOL','120 Hour TESOL Course',123.00,NULL,'2018-05-28',0,'2018-05-13 10:16:21','2018-05-13 10:16:21',NULL),
	(7,'140 Hour TESOL','140 Hour TESOL','140 Hour TESOL Course',123.00,NULL,'2018-05-30',0,'2018-05-13 10:16:46','2018-05-13 10:16:46',NULL),
	(8,'190 Hour TESOL','190 Hour TESOL','190 Hour TESOL Course',123.00,NULL,'2018-05-29',0,'2018-05-13 10:17:23','2018-05-13 10:17:23',NULL),
	(9,'IELTS','IELTS','IELTS Course',123.00,NULL,'2018-05-30',0,'2018-05-13 10:17:45','2018-05-13 10:17:45',NULL),
	(10,NULL,NULL,NULL,NULL,NULL,NULL,0,'2018-05-13 10:21:56','2018-05-13 10:57:28','2018-05-13 10:57:28'),
	(11,NULL,NULL,NULL,NULL,NULL,NULL,0,'2018-05-13 10:26:01','2018-05-13 10:57:31','2018-05-13 10:57:31'),
	(12,NULL,NULL,NULL,NULL,NULL,NULL,0,'2018-05-13 10:26:21','2018-05-13 10:57:35','2018-05-13 10:57:35'),
	(13,NULL,NULL,NULL,NULL,NULL,NULL,0,'2018-05-13 10:26:45','2018-05-13 10:57:38','2018-05-13 10:57:38'),
	(14,'120 Hour Special Course','120 Hour Special Course','120 Hour Special Course',333.00,NULL,'2018-05-31',0,'2018-05-13 10:58:41','2018-05-13 10:58:41',NULL),
	(15,'120 Hour TESOL','120 Hour TESOL','120 Hour TESOL',NULL,NULL,'2018-05-30',0,'2018-05-13 13:20:12','2018-05-13 13:20:12',NULL),
	(16,'120 Hour TESOL','120 Hour TESOL','120 Hour TESOL',NULL,NULL,'2018-05-31',0,'2018-05-13 13:20:29','2018-05-13 13:20:29',NULL),
	(17,'120 Hour TESOL','120 Hour TESOL','120 Hour TESOL',NULL,NULL,'2018-05-09',0,'2018-05-13 13:20:52','2018-05-13 13:20:52',NULL),
	(18,'140 Hour Course','1605 test app','1605 test app',NULL,NULL,'2018-05-29',0,'2018-05-17 14:28:48','2018-05-17 14:28:48',NULL);

/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u1m1s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u1m1s1s`;

CREATE TABLE `int120u1m1s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `u1m1s1q1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q3` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q4` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q5` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q6` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q7` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u1m1s1q8` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q9` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q10` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q11` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q12` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q13` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q14` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q15` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q16` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q17` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q18` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q19` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q20` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q21` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q22` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q23` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q24` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q25` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q26` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q27` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q28` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q29` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q30` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q31` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u1m1s1s` WRITE;
/*!40000 ALTER TABLE `int120u1m1s1s` DISABLE KEYS */;

INSERT INTO `int120u1m1s1s` (`id`, `user_id`, `u1m1s1q1`, `u1m1s1q2`, `u1m1s1q3`, `u1m1s1q4`, `u1m1s1q5`, `u1m1s1q6`, `u1m1s1q7`, `created_at`, `updated_at`, `u1m1s1q8`, `u1m1s1q9`, `u1m1s1q10`, `u1m1s1q11`, `u1m1s1q12`, `u1m1s1q13`, `u1m1s1q14`, `u1m1s1q15`, `u1m1s1q16`, `u1m1s1q17`, `u1m1s1q18`, `u1m1s1q19`, `u1m1s1q20`, `u1m1s1q21`, `u1m1s1q22`, `u1m1s1q23`, `u1m1s1q24`, `u1m1s1q25`, `u1m1s1q26`, `u1m1s1q27`, `u1m1s1q28`, `u1m1s1q29`, `u1m1s1q30`, `u1m1s1q31`, `text`, `status`)
VALUES
	(1,0,'Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL,''),
	(2,0,'Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL,''),
	(3,0,'Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL,''),
	(4,0,'test answer version','test answer version','test answer version','test answer version','test answer version','test answer version','test answer version',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL,''),
	(5,0,'fbGqG4BWWJAC3c7BBp1DFbUclpUEwwwV','fbGqG4BWWJAC3c7BBp1DFbUclpUEwwwV','fbGqG4BWWJAC3c7BBp1DFbUclpUEwwwV','fbGqG4BWWJAC3c7BBp1DFbUclpUEwwwV','fbGqG4BWWJAC3c7BBp1DFbUclpUEwwwV','fbGqG4BWWJAC3c7BBp1DFbUclpUEwwwV','fbGqG4BWWJAC3c7BBp1DFbUclpUEwwwV',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL,''),
	(6,0,'nullable test','nullable test','nullable test','nullable test','nullable test','nullable test','nullable test',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL,''),
	(7,0,'random test','random test','random test','random test','random test','random test','random test',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL,''),
	(8,0,'qwerty','qwerty','qwerty','qwerty','qwerty','qwerty','qwerty',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL,''),
	(9,0,'new data test','new data test','new data test','new data test','new data test','new data test','new data test',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL,''),
	(10,0,'new data test','new data test','new data test','new data test','new data test','new data test','new data test',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL,''),
	(11,4,'','','','','','','',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL,''),
	(12,0,'user id db test','user id db test','user id db test','user id db test','user id db test','user id db test','user id db test',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL,''),
	(13,4,'','','','','','','',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL,''),
	(14,0,'metaData test','metaData test','metaData test','metaData test','metaData test','metaData test','metaData test',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL,''),
	(15,4,'','','','','','','',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL,''),
	(16,0,'hidden id test','hidden id test','hidden id test','hidden id test','hidden id test','hidden id test','hidden id test',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL,''),
	(17,4,'hidden user name 2','hidden user name 2','hidden user name 2','hidden user name 2','hidden user name 2','hidden user name 2','hidden user name 2',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL,''),
	(18,6,'neo user 120','neo user 120','neo user 120','neo user 120','neo user 120','neo user 120','neo user 120',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL,''),
	(19,9,'Jay 120 test','Jay 120 test','Jay 120 test','Jay 120 test','Jay 120 test','Jay 120 test','Jay 120 test',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL,''),
	(20,6,'pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL,''),
	(21,6,'','','','','','','',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','pbysovceatn;ovsuaebriv;',''),
	(22,4,'gsghvdbvxbcvnf,dsj','gsghvdbvxbcvnf,dsj','gsghvdbvxbcvnf,dsj','gsghvdbvxbcvnf,dsj','gsghvdbvxbcvnf,dsj','gsghvdbvxbcvnf,dsj','gsghvdbvxbcvnf,dsj',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL,'');

/*!40000 ALTER TABLE `int120u1m1s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u1m1s1t1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u1m1s1t1s`;

CREATE TABLE `int120u1m1s1t1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `u1m1s1q1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q3` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q4` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q5` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q6` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q7` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u1m1s1t1s` WRITE;
/*!40000 ALTER TABLE `int120u1m1s1t1s` DISABLE KEYS */;

INSERT INTO `int120u1m1s1t1s` (`id`, `user_id`, `u1m1s1q1`, `u1m1s1q2`, `u1m1s1q3`, `u1m1s1q4`, `u1m1s1q5`, `u1m1s1q6`, `u1m1s1q7`, `created_at`, `updated_at`, `text`, `status`)
VALUES
	(1,0,'Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146',NULL,NULL,NULL,NULL),
	(2,0,'Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146',NULL,NULL,NULL,NULL),
	(3,0,'Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146',NULL,NULL,NULL,NULL),
	(4,0,'test answer version','test answer version','test answer version','test answer version','test answer version','test answer version','test answer version',NULL,NULL,NULL,NULL),
	(5,0,'fbGqG4BWWJAC3c7BBp1DFbUclpUEwwwV','fbGqG4BWWJAC3c7BBp1DFbUclpUEwwwV','fbGqG4BWWJAC3c7BBp1DFbUclpUEwwwV','fbGqG4BWWJAC3c7BBp1DFbUclpUEwwwV','fbGqG4BWWJAC3c7BBp1DFbUclpUEwwwV','fbGqG4BWWJAC3c7BBp1DFbUclpUEwwwV','fbGqG4BWWJAC3c7BBp1DFbUclpUEwwwV',NULL,NULL,NULL,NULL),
	(6,0,'nullable test','nullable test','nullable test','nullable test','nullable test','nullable test','nullable test',NULL,NULL,NULL,NULL),
	(7,0,'random test','random test','random test','random test','random test','random test','random test',NULL,NULL,NULL,NULL),
	(8,0,'qwerty','qwerty','qwerty','qwerty','qwerty','qwerty','qwerty',NULL,NULL,NULL,NULL),
	(9,0,'new data test','new data test','new data test','new data test','new data test','new data test','new data test',NULL,NULL,NULL,NULL),
	(10,0,'new data test','new data test','new data test','new data test','new data test','new data test','new data test',NULL,NULL,NULL,NULL),
	(11,4,'','','','','','','',NULL,NULL,NULL,NULL),
	(12,0,'user id db test','user id db test','user id db test','user id db test','user id db test','user id db test','user id db test',NULL,NULL,NULL,NULL),
	(13,4,'','','','','','','',NULL,NULL,NULL,NULL),
	(14,0,'metaData test','metaData test','metaData test','metaData test','metaData test','metaData test','metaData test',NULL,NULL,NULL,NULL),
	(15,4,'','','','','','','',NULL,NULL,NULL,NULL),
	(16,0,'hidden id test','hidden id test','hidden id test','hidden id test','hidden id test','hidden id test','hidden id test',NULL,NULL,NULL,NULL),
	(17,4,'hidden user name 2','hidden user name 2','hidden user name 2','hidden user name 2','hidden user name 2','hidden user name 2','hidden user name 2',NULL,NULL,NULL,NULL),
	(18,6,'neo user 120','neo user 120','neo user 120','neo user 120','neo user 120','neo user 120','neo user 120',NULL,NULL,NULL,NULL),
	(19,9,'Jay 120 test','Jay 120 test','Jay 120 test','Jay 120 test','Jay 120 test','Jay 120 test','Jay 120 test',NULL,NULL,NULL,NULL),
	(20,6,'pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;',NULL,NULL,NULL,NULL),
	(22,4,'gsghvdbvxbcvnf,dsj','gsghvdbvxbcvnf,dsj','gsghvdbvxbcvnf,dsj','gsghvdbvxbcvnf,dsj','gsghvdbvxbcvnf,dsj','gsghvdbvxbcvnf,dsj','gsghvdbvxbcvnf,dsj',NULL,NULL,NULL,NULL),
	(23,4,'0105 test input','0105 test input','0105 test input','0105 test input','0105 test input','0105 test input','0105 test input',NULL,NULL,NULL,NULL),
	(24,4,'0105 test input task 2','0105 test input task 2','0105 test input task 2','0105 test input task 2','0105 test input task 2','0105 test input task 2','0105 test input task 2',NULL,NULL,NULL,NULL),
	(25,4,'0105 input test task 1','0105 input test task 1','0105 input test task 1','0105 input test task 1','0105 input test task 1','0105 input test task 1','0105 input test task 1',NULL,NULL,NULL,NULL),
	(26,4,'test again 0105 Task 1','test again 0105 Task 1','test again 0105 Task 1','test again 0105 Task 1','test again 0105 Task 1','test again 0105 Task 1','test again 0105 Task 1',NULL,NULL,NULL,NULL),
	(27,4,'!!!!!!!!!!!!!!!!!! Input Task 1','!!!!!!!!!!!!!!!!!! Input Task 1','!!!!!!!!!!!!!!!!!! Input Task 1','!!!!!!!!!!!!!!!!!! Input Task 1','!!!!!!!!!!!!!!!!!! Input Task 1','!!!!!!!!!!!!!!!!!! Input Task 1','!!!!!!!!!!!!!!!!!! Input Task 1',NULL,NULL,NULL,NULL),
	(28,4,'!!!!!!!!!!!!!!!!!! Input Task 01','!!!!!!!!!!!!!!!!!! Input Task 01','!!!!!!!!!!!!!!!!!! Input Task 01','!!!!!!!!!!!!!!!!!! Input Task 01','!!!!!!!!!!!!!!!!!! Input Task 01','!!!!!!!!!!!!!!!!!! Input Task 01','!!!!!!!!!!!!!!!!!! Input Task 01',NULL,NULL,NULL,NULL),
	(29,4,'What does motivate you?','What does motivate you?','What does motivate you?','What does motivate you?','What does motivate you?','What does motivate you?','What does motivate you?',NULL,NULL,NULL,NULL),
	(30,4,'task1Form','task1Form','task1Form','task1Form','task1Form','task1Form','task1Form',NULL,NULL,NULL,NULL),
	(31,4,'task1Form','task1Form','task1Form','task1Form','task1Form','task1Form','task1Form',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `int120u1m1s1t1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u1m1s1t2s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u1m1s1t2s`;

CREATE TABLE `int120u1m1s1t2s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u1m1s1q8` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q9` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q10` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q11` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q12` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q13` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u1m1s1t2s` WRITE;
/*!40000 ALTER TABLE `int120u1m1s1t2s` DISABLE KEYS */;

INSERT INTO `int120u1m1s1t2s` (`id`, `user_id`, `created_at`, `updated_at`, `u1m1s1q8`, `u1m1s1q9`, `u1m1s1q10`, `u1m1s1q11`, `u1m1s1q12`, `u1m1s1q13`, `text`, `status`)
VALUES
	(1,0,NULL,NULL,'','','','','','',NULL,''),
	(2,0,NULL,NULL,'','','','','','',NULL,''),
	(3,0,NULL,NULL,'','','','','','',NULL,''),
	(4,0,NULL,NULL,'','','','','','',NULL,''),
	(5,0,NULL,NULL,'','','','','','',NULL,''),
	(6,0,NULL,NULL,'','','','','','',NULL,''),
	(7,0,NULL,NULL,'','','','','','',NULL,''),
	(8,0,NULL,NULL,'','','','','','',NULL,''),
	(9,0,NULL,NULL,'','','','','','',NULL,''),
	(10,0,NULL,NULL,'','','','','','',NULL,''),
	(11,4,NULL,NULL,'','','','','','',NULL,''),
	(12,0,NULL,NULL,'','','','','','',NULL,''),
	(13,4,NULL,NULL,'','','','','','',NULL,''),
	(14,0,NULL,NULL,'','','','','','',NULL,''),
	(15,4,NULL,NULL,'','','','','','',NULL,''),
	(16,0,NULL,NULL,'','','','','','',NULL,''),
	(17,4,NULL,NULL,'','','','','','',NULL,''),
	(18,6,NULL,NULL,'','','','','','',NULL,''),
	(19,9,NULL,NULL,'','','','','','',NULL,''),
	(20,6,NULL,NULL,'','','','','','',NULL,''),
	(21,6,NULL,NULL,'','','','','','','pbysovceatn;ovsuaebriv;',''),
	(22,4,NULL,NULL,'','','','','','','0105 test input task 2',''),
	(23,4,NULL,NULL,'0105 input test task 2','0105 input test task 2','0105 input test task 2','0105 input test task 2','0105 input test task 2','0105 input test task 2',NULL,''),
	(24,4,NULL,NULL,'test again 0105 Task 2','test again 0105 Task 2','test again 0105 Task 2','test again 0105 Task 2','test again 0105 Task 2','test again 0105 Task 2',NULL,''),
	(25,4,NULL,NULL,'!!!!!!!!!!!!!!!!!! Input Task 2','!!!!!!!!!!!!!!!!!! Input Task 2','!!!!!!!!!!!!!!!!!! Input Task 2','!!!!!!!!!!!!!!!!!! Input Task 2','!!!!!!!!!!!!!!!!!! Input Task 2','!!!!!!!!!!!!!!!!!! Input Task 2',NULL,'');

/*!40000 ALTER TABLE `int120u1m1s1t2s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u1m1s1t3s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u1m1s1t3s`;

CREATE TABLE `int120u1m1s1t3s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u1m1s1q14` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q15` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q16` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q17` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q18` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q19` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q20` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q21` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q22` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q23` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q24` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q25` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q26` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q27` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q28` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u1m1s1t3s` WRITE;
/*!40000 ALTER TABLE `int120u1m1s1t3s` DISABLE KEYS */;

INSERT INTO `int120u1m1s1t3s` (`id`, `user_id`, `created_at`, `updated_at`, `u1m1s1q14`, `u1m1s1q15`, `u1m1s1q16`, `u1m1s1q17`, `u1m1s1q18`, `u1m1s1q19`, `u1m1s1q20`, `u1m1s1q21`, `u1m1s1q22`, `u1m1s1q23`, `u1m1s1q24`, `u1m1s1q25`, `u1m1s1q26`, `u1m1s1q27`, `u1m1s1q28`, `text`, `status`)
VALUES
	(1,0,NULL,NULL,'','','','','','','','','','','','','','','',NULL,''),
	(2,0,NULL,NULL,'','','','','','','','','','','','','','','',NULL,''),
	(3,0,NULL,NULL,'','','','','','','','','','','','','','','',NULL,''),
	(4,0,NULL,NULL,'','','','','','','','','','','','','','','',NULL,''),
	(5,0,NULL,NULL,'','','','','','','','','','','','','','','',NULL,''),
	(6,0,NULL,NULL,'','','','','','','','','','','','','','','',NULL,''),
	(7,0,NULL,NULL,'','','','','','','','','','','','','','','',NULL,''),
	(8,0,NULL,NULL,'','','','','','','','','','','','','','','',NULL,''),
	(9,0,NULL,NULL,'','','','','','','','','','','','','','','',NULL,''),
	(10,0,NULL,NULL,'','','','','','','','','','','','','','','',NULL,''),
	(11,4,NULL,NULL,'','','','','','','','','','','','','','','',NULL,''),
	(12,0,NULL,NULL,'','','','','','','','','','','','','','','',NULL,''),
	(13,4,NULL,NULL,'','','','','','','','','','','','','','','',NULL,''),
	(14,0,NULL,NULL,'','','','','','','','','','','','','','','',NULL,''),
	(15,4,NULL,NULL,'','','','','','','','','','','','','','','',NULL,''),
	(16,0,NULL,NULL,'','','','','','','','','','','','','','','',NULL,''),
	(17,4,NULL,NULL,'','','','','','','','','','','','','','','',NULL,''),
	(18,6,NULL,NULL,'','','','','','','','','','','','','','','',NULL,''),
	(19,9,NULL,NULL,'','','','','','','','','','','','','','','',NULL,''),
	(20,6,NULL,NULL,'','','','','','','','','','','','','','','',NULL,''),
	(21,6,NULL,NULL,'','','','','','','','','','','','','','','','pbysovceatn;ovsuaebriv;',''),
	(22,4,NULL,NULL,'test again 0105 Task 3','test again 0105 Task 3','test again 0105 Task 3','test again 0105 Task 3','test again 0105 Task 3','test again 0105 Task 3','test again 0105 Task 3','test again 0105 Task 3','test again 0105 Task 3','test again 0105 Task 3','test again 0105 Task 3','test again 0105 Task 3','test again 0105 Task 3','test again 0105 Task 3','test again 0105 Task 3',NULL,''),
	(23,4,NULL,NULL,'!!!!!!!!!!!!!!!!!! Input Task 3','!!!!!!!!!!!!!!!!!! Input Task 3','!!!!!!!!!!!!!!!!!! Input Task 3','!!!!!!!!!!!!!!!!!! Input Task 3','!!!!!!!!!!!!!!!!!! Input Task 3','!!!!!!!!!!!!!!!!!! Input Task 3','!!!!!!!!!!!!!!!!!! Input Task 3','!!!!!!!!!!!!!!!!!! Input Task 3','!!!!!!!!!!!!!!!!!! Input Task 3','!!!!!!!!!!!!!!!!!! Input Task 3','!!!!!!!!!!!!!!!!!! Input Task 3','!!!!!!!!!!!!!!!!!! Input Task 3','!!!!!!!!!!!!!!!!!! Input Task 3','!!!!!!!!!!!!!!!!!! Input Task 3','!!!!!!!!!!!!!!!!!! Input Task 3',NULL,'');

/*!40000 ALTER TABLE `int120u1m1s1t3s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u1m1s1t4s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u1m1s1t4s`;

CREATE TABLE `int120u1m1s1t4s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u1m1s1q29` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q30` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q31` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u1m1s1t4s` WRITE;
/*!40000 ALTER TABLE `int120u1m1s1t4s` DISABLE KEYS */;

INSERT INTO `int120u1m1s1t4s` (`id`, `user_id`, `created_at`, `updated_at`, `u1m1s1q29`, `u1m1s1q30`, `u1m1s1q31`, `text`, `status`)
VALUES
	(1,0,NULL,NULL,'','','',NULL,''),
	(2,0,NULL,NULL,'','','',NULL,''),
	(3,0,NULL,NULL,'','','',NULL,''),
	(4,0,NULL,NULL,'','','',NULL,''),
	(5,0,NULL,NULL,'','','',NULL,''),
	(6,0,NULL,NULL,'','','',NULL,''),
	(7,0,NULL,NULL,'','','',NULL,''),
	(8,0,NULL,NULL,'','','',NULL,''),
	(9,0,NULL,NULL,'','','',NULL,''),
	(10,0,NULL,NULL,'','','',NULL,''),
	(11,4,NULL,NULL,'','','',NULL,''),
	(12,0,NULL,NULL,'','','',NULL,''),
	(13,4,NULL,NULL,'','','',NULL,''),
	(14,0,NULL,NULL,'','','',NULL,''),
	(15,4,NULL,NULL,'','','',NULL,''),
	(16,0,NULL,NULL,'','','',NULL,''),
	(17,4,NULL,NULL,'','','',NULL,''),
	(18,6,NULL,NULL,'','','',NULL,''),
	(19,9,NULL,NULL,'','','',NULL,''),
	(20,6,NULL,NULL,'','','',NULL,''),
	(21,6,NULL,NULL,'','','','pbysovceatn;ovsuaebriv;',''),
	(22,4,NULL,NULL,'test again 0105 Task 4','test again 0105 Task 4','test again 0105 Task 4',NULL,''),
	(23,4,NULL,NULL,'!!!!!!!!!!!!!!!!!! Input Task 4','!!!!!!!!!!!!!!!!!! Input Task 4','!!!!!!!!!!!!!!!!!! Input Task 4',NULL,'');

/*!40000 ALTER TABLE `int120u1m1s1t4s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u2m1s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u2m1s1s`;

CREATE TABLE `int120u2m1s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `u2m1s1q1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u2m1s1q2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q3` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q4` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q5` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q6` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q7` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q8` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q9` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q10` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q11` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q12` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q13` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q14` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q15` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q16` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q17` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q18` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q19` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q20` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q21` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q23` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q22` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q24` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q25` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q26` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q27` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q28` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q29` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q30` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q31` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q32` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q33` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q34` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q35` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q36` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q37` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q38` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q39` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q40` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q41` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q42` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q43` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q44` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q45` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q46` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q47` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q48` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q49` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q50` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q51` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q52` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q53` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q54` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q55` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q56` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q57` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q58` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q59` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q60` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q61` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q62` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q63` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q64` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u2m1s1s` WRITE;
/*!40000 ALTER TABLE `int120u2m1s1s` DISABLE KEYS */;

INSERT INTO `int120u2m1s1s` (`id`, `user_id`, `u2m1s1q1`, `created_at`, `updated_at`, `u2m1s1q2`, `u2m1s1q3`, `u2m1s1q4`, `u2m1s1q5`, `u2m1s1q6`, `u2m1s1q7`, `u2m1s1q8`, `u2m1s1q9`, `u2m1s1q10`, `u2m1s1q11`, `u2m1s1q12`, `u2m1s1q13`, `u2m1s1q14`, `u2m1s1q15`, `u2m1s1q16`, `u2m1s1q17`, `u2m1s1q18`, `u2m1s1q19`, `u2m1s1q20`, `u2m1s1q21`, `u2m1s1q23`, `u2m1s1q22`, `u2m1s1q24`, `u2m1s1q25`, `u2m1s1q26`, `u2m1s1q27`, `u2m1s1q28`, `u2m1s1q29`, `u2m1s1q30`, `u2m1s1q31`, `u2m1s1q32`, `u2m1s1q33`, `u2m1s1q34`, `u2m1s1q35`, `u2m1s1q36`, `u2m1s1q37`, `u2m1s1q38`, `u2m1s1q39`, `u2m1s1q40`, `u2m1s1q41`, `u2m1s1q42`, `u2m1s1q43`, `u2m1s1q44`, `u2m1s1q45`, `u2m1s1q46`, `u2m1s1q47`, `u2m1s1q48`, `u2m1s1q49`, `u2m1s1q50`, `u2m1s1q51`, `u2m1s1q52`, `u2m1s1q53`, `u2m1s1q54`, `u2m1s1q55`, `u2m1s1q56`, `u2m1s1q57`, `u2m1s1q58`, `u2m1s1q59`, `u2m1s1q60`, `u2m1s1q61`, `u2m1s1q62`, `u2m1s1q63`, `u2m1s1q64`, `status`)
VALUES
	(1,6,'test answer',NULL,NULL,'test answer','test answer','test answer','test answer','test answer','test answer','test answer','test answer','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(2,6,'',NULL,NULL,'','','','','','','','','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(3,6,'Unit 2 | The Study of EnglishUnit 2 | The Study of English',NULL,NULL,'Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(4,6,'',NULL,NULL,'','','','','','','','','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(5,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(6,6,'self check test',NULL,NULL,'self check test','self check test','self check test','self check test','self check test','self check test','self check test','self check test','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(7,6,'',NULL,NULL,'','','','','','','','','task 1 test','task 1 test','task 1 test','task 1 test','task 1 test','task 1 test','task 1 test','task 1 test','task 1 test','task 1 test','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(8,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','task 2 test','task 2 test','task 2 test','task 2 test','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(9,6,'Unit 2 | The Study of English',NULL,NULL,'Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(10,6,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(11,6,'',NULL,NULL,'','','','','','','','','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(12,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(13,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','','','','','','','','',''),
	(14,6,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(15,6,'',NULL,NULL,'','','','','','','','','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(16,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(17,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','','','','','','','','',''),
	(18,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','','','','','',''),
	(19,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',''),
	(20,6,'vbhskghersvgnlstilbh',NULL,NULL,'vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(21,6,'',NULL,NULL,'','','','','','','','','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(22,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(23,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','','','','','','','','','',''),
	(24,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','','','','','','',''),
	(25,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh',''),
	(26,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','asdfgfhjklghsdlhfsdlkh','asdfgfhjklghsdlhfsdlkh','asdfgfhjklghsdlhfsdlkh','asdfgfhjklghsdlhfsdlkh','asdfgfhjklghsdlhfsdlkh','asdfgfhjklghsdlhfsdlkh',''),
	(27,4,'asdfgfhjklghsdlhfsdlkh',NULL,NULL,'asdfgfhjklghsdlhfsdlkh','asdfgfhjklghsdlhfsdlkh','asdfgfhjklghsdlhfsdlkh','asdfgfhjklghsdlhfsdlkh','asdfgfhjklghsdlhfsdlkh','asdfgfhjklghsdlhfsdlkh','asdfgfhjklghsdlhfsdlkh','asdfgfhjklghsdlhfsdlkh','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(28,4,'0105 test data',NULL,NULL,'0105 test data','0105 test data','0105 test data','0105 test data','0105 test data','0105 test data','0105 test data','0105 test data','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(29,4,'',NULL,NULL,'','','','','','','','','0105 test data','0105 test data','0105 test data','0105 test data','0105 test data','0105 test data','0105 test data','0105 test data','0105 test data','0105 test data','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(30,4,'// task 1',NULL,NULL,'// task 1','// task 1','// task 1','// task 1','// task 1','// task 1','// task 1','// task 1','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','');

/*!40000 ALTER TABLE `int120u2m1s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u2m1s1t1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u2m1s1t1s`;

CREATE TABLE `int120u2m1s1t1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `u2m1s1q1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u2m1s1q2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q3` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q4` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q5` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q6` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q7` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q8` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q9` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u2m1s1t1s` WRITE;
/*!40000 ALTER TABLE `int120u2m1s1t1s` DISABLE KEYS */;

INSERT INTO `int120u2m1s1t1s` (`id`, `user_id`, `u2m1s1q1`, `created_at`, `updated_at`, `u2m1s1q2`, `u2m1s1q3`, `u2m1s1q4`, `u2m1s1q5`, `u2m1s1q6`, `u2m1s1q7`, `u2m1s1q8`, `u2m1s1q9`, `status`)
VALUES
	(1,6,'test answer',NULL,NULL,'test answer','test answer','test answer','test answer','test answer','test answer','test answer','test answer',''),
	(2,6,'',NULL,NULL,'','','','','','','','',''),
	(3,6,'Unit 2 | The Study of EnglishUnit 2 | The Study of English',NULL,NULL,'Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English',''),
	(4,6,'',NULL,NULL,'','','','','','','','',''),
	(5,6,'',NULL,NULL,'','','','','','','','',''),
	(6,6,'self check test',NULL,NULL,'self check test','self check test','self check test','self check test','self check test','self check test','self check test','self check test',''),
	(7,6,'',NULL,NULL,'','','','','','','','',''),
	(8,6,'',NULL,NULL,'','','','','','','','',''),
	(9,6,'Unit 2 | The Study of English',NULL,NULL,'Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English',''),
	(10,6,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',''),
	(11,6,'',NULL,NULL,'','','','','','','','',''),
	(12,6,'',NULL,NULL,'','','','','','','','',''),
	(13,6,'',NULL,NULL,'','','','','','','','',''),
	(14,6,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',''),
	(15,6,'',NULL,NULL,'','','','','','','','',''),
	(16,6,'',NULL,NULL,'','','','','','','','',''),
	(17,6,'',NULL,NULL,'','','','','','','','',''),
	(18,6,'',NULL,NULL,'','','','','','','','',''),
	(19,6,'',NULL,NULL,'','','','','','','','',''),
	(20,6,'vbhskghersvgnlstilbh',NULL,NULL,'vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh',''),
	(21,6,'',NULL,NULL,'','','','','','','','',''),
	(22,6,'',NULL,NULL,'','','','','','','','',''),
	(23,6,'',NULL,NULL,'','','','','','','','',''),
	(24,6,'',NULL,NULL,'','','','','','','','',''),
	(25,6,'',NULL,NULL,'','','','','','','','',''),
	(26,4,'u2m1 input test',NULL,NULL,'u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','');

/*!40000 ALTER TABLE `int120u2m1s1t1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u2m1s1t2s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u2m1s1t2s`;

CREATE TABLE `int120u2m1s1t2s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u2m1s1q10` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q11` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q12` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q13` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q14` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q15` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q16` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q17` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q18` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q19` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u2m1s1t2s` WRITE;
/*!40000 ALTER TABLE `int120u2m1s1t2s` DISABLE KEYS */;

INSERT INTO `int120u2m1s1t2s` (`id`, `user_id`, `created_at`, `updated_at`, `u2m1s1q10`, `u2m1s1q11`, `u2m1s1q12`, `u2m1s1q13`, `u2m1s1q14`, `u2m1s1q15`, `u2m1s1q16`, `u2m1s1q17`, `u2m1s1q18`, `u2m1s1q19`, `status`)
VALUES
	(1,6,NULL,NULL,'','','','','','','','','','',''),
	(2,6,NULL,NULL,'Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.',''),
	(3,6,NULL,NULL,'','','','','','','','','','',''),
	(4,6,NULL,NULL,'Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English',''),
	(5,6,NULL,NULL,'','','','','','','','','','',''),
	(6,6,NULL,NULL,'','','','','','','','','','',''),
	(7,6,NULL,NULL,'task 1 test','task 1 test','task 1 test','task 1 test','task 1 test','task 1 test','task 1 test','task 1 test','task 1 test','task 1 test',''),
	(8,6,NULL,NULL,'','','','','','','','','','',''),
	(9,6,NULL,NULL,'','','','','','','','','','',''),
	(10,6,NULL,NULL,'','','','','','','','','','',''),
	(11,6,NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',''),
	(12,6,NULL,NULL,'','','','','','','','','','',''),
	(13,6,NULL,NULL,'','','','','','','','','','',''),
	(14,6,NULL,NULL,'','','','','','','','','','',''),
	(15,6,NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',''),
	(16,6,NULL,NULL,'','','','','','','','','','',''),
	(17,6,NULL,NULL,'','','','','','','','','','',''),
	(18,6,NULL,NULL,'','','','','','','','','','',''),
	(19,6,NULL,NULL,'','','','','','','','','','',''),
	(20,6,NULL,NULL,'','','','','','','','','','',''),
	(21,6,NULL,NULL,'vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh',''),
	(22,6,NULL,NULL,'','','','','','','','','','',''),
	(23,6,NULL,NULL,'','','','','','','','','','',''),
	(24,6,NULL,NULL,'','','','','','','','','','',''),
	(25,6,NULL,NULL,'','','','','','','','','','',''),
	(26,4,NULL,NULL,'u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','');

/*!40000 ALTER TABLE `int120u2m1s1t2s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u2m1s1t3s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u2m1s1t3s`;

CREATE TABLE `int120u2m1s1t3s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u2m1s1q20` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q21` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q23` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q22` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u2m1s1t3s` WRITE;
/*!40000 ALTER TABLE `int120u2m1s1t3s` DISABLE KEYS */;

INSERT INTO `int120u2m1s1t3s` (`id`, `user_id`, `created_at`, `updated_at`, `u2m1s1q20`, `u2m1s1q21`, `u2m1s1q23`, `u2m1s1q22`, `status`)
VALUES
	(1,6,NULL,NULL,'','','','',''),
	(2,6,NULL,NULL,'','','','',''),
	(3,6,NULL,NULL,'','','','',''),
	(4,6,NULL,NULL,'','','','',''),
	(5,6,NULL,NULL,'Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English',''),
	(6,6,NULL,NULL,'','','','',''),
	(7,6,NULL,NULL,'','','','',''),
	(8,6,NULL,NULL,'task 2 test','task 2 test','task 2 test','task 2 test',''),
	(9,6,NULL,NULL,'','','','',''),
	(10,6,NULL,NULL,'','','','',''),
	(11,6,NULL,NULL,'','','','',''),
	(12,6,NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',''),
	(13,6,NULL,NULL,'','','','',''),
	(14,6,NULL,NULL,'','','','',''),
	(15,6,NULL,NULL,'','','','',''),
	(16,6,NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',''),
	(17,6,NULL,NULL,'','','','',''),
	(18,6,NULL,NULL,'','','','',''),
	(19,6,NULL,NULL,'','','','',''),
	(20,6,NULL,NULL,'','','','',''),
	(21,6,NULL,NULL,'','','','',''),
	(22,6,NULL,NULL,'vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh',''),
	(23,6,NULL,NULL,'','','','',''),
	(24,6,NULL,NULL,'','','','',''),
	(25,6,NULL,NULL,'','','','',''),
	(26,4,NULL,NULL,'u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','');

/*!40000 ALTER TABLE `int120u2m1s1t3s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u2m1s1t4s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u2m1s1t4s`;

CREATE TABLE `int120u2m1s1t4s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u2m1s1q24` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q25` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q26` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q27` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q28` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q29` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q30` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q31` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q32` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q33` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q34` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q35` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q36` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q37` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q38` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q39` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q40` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q41` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q42` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q43` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q44` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q45` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q46` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q47` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q48` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q49` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q50` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q51` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q52` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q53` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q54` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q55` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u2m1s1t4s` WRITE;
/*!40000 ALTER TABLE `int120u2m1s1t4s` DISABLE KEYS */;

INSERT INTO `int120u2m1s1t4s` (`id`, `user_id`, `created_at`, `updated_at`, `u2m1s1q24`, `u2m1s1q25`, `u2m1s1q26`, `u2m1s1q27`, `u2m1s1q28`, `u2m1s1q29`, `u2m1s1q30`, `u2m1s1q31`, `u2m1s1q32`, `u2m1s1q33`, `u2m1s1q34`, `u2m1s1q35`, `u2m1s1q36`, `u2m1s1q37`, `u2m1s1q38`, `u2m1s1q39`, `u2m1s1q40`, `u2m1s1q41`, `u2m1s1q42`, `u2m1s1q43`, `u2m1s1q44`, `u2m1s1q45`, `u2m1s1q46`, `u2m1s1q47`, `u2m1s1q48`, `u2m1s1q49`, `u2m1s1q50`, `u2m1s1q51`, `u2m1s1q52`, `u2m1s1q53`, `u2m1s1q54`, `u2m1s1q55`, `status`)
VALUES
	(1,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(2,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(3,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(4,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(5,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(6,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(7,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(8,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(9,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(10,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(11,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(12,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(13,6,NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',''),
	(14,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(15,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(16,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(17,6,NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',''),
	(18,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(19,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(20,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(21,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(22,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(23,6,NULL,NULL,'vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh',''),
	(24,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(25,6,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(26,4,NULL,NULL,'u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','');

/*!40000 ALTER TABLE `int120u2m1s1t4s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u2m1s1t5s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u2m1s1t5s`;

CREATE TABLE `int120u2m1s1t5s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u2m1s1q56` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q57` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q58` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u2m1s1t5s` WRITE;
/*!40000 ALTER TABLE `int120u2m1s1t5s` DISABLE KEYS */;

INSERT INTO `int120u2m1s1t5s` (`id`, `user_id`, `created_at`, `updated_at`, `u2m1s1q56`, `u2m1s1q57`, `u2m1s1q58`, `status`)
VALUES
	(1,6,NULL,NULL,'','','',''),
	(2,6,NULL,NULL,'','','',''),
	(3,6,NULL,NULL,'','','',''),
	(4,6,NULL,NULL,'','','',''),
	(5,6,NULL,NULL,'','','',''),
	(6,6,NULL,NULL,'','','',''),
	(7,6,NULL,NULL,'','','',''),
	(8,6,NULL,NULL,'','','',''),
	(9,6,NULL,NULL,'','','',''),
	(10,6,NULL,NULL,'','','',''),
	(11,6,NULL,NULL,'','','',''),
	(12,6,NULL,NULL,'','','',''),
	(13,6,NULL,NULL,'','','',''),
	(14,6,NULL,NULL,'','','',''),
	(15,6,NULL,NULL,'','','',''),
	(16,6,NULL,NULL,'','','',''),
	(17,6,NULL,NULL,'','','',''),
	(18,6,NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',''),
	(19,6,NULL,NULL,'','','',''),
	(20,6,NULL,NULL,'','','',''),
	(21,6,NULL,NULL,'','','',''),
	(22,6,NULL,NULL,'','','',''),
	(23,6,NULL,NULL,'','','',''),
	(24,6,NULL,NULL,'vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh',''),
	(25,6,NULL,NULL,'','','',''),
	(26,4,NULL,NULL,'u2m1 input test','u2m1 input test','u2m1 input test','');

/*!40000 ALTER TABLE `int120u2m1s1t5s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u2m1s1t6s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u2m1s1t6s`;

CREATE TABLE `int120u2m1s1t6s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u2m1s1q59` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q60` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q61` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q62` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q63` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q64` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u2m1s1t6s` WRITE;
/*!40000 ALTER TABLE `int120u2m1s1t6s` DISABLE KEYS */;

INSERT INTO `int120u2m1s1t6s` (`id`, `user_id`, `created_at`, `updated_at`, `u2m1s1q59`, `u2m1s1q60`, `u2m1s1q61`, `u2m1s1q62`, `u2m1s1q63`, `u2m1s1q64`, `status`)
VALUES
	(1,6,NULL,NULL,'','','','','','',''),
	(2,6,NULL,NULL,'','','','','','',''),
	(3,6,NULL,NULL,'','','','','','',''),
	(4,6,NULL,NULL,'','','','','','',''),
	(5,6,NULL,NULL,'','','','','','',''),
	(6,6,NULL,NULL,'','','','','','',''),
	(7,6,NULL,NULL,'','','','','','',''),
	(8,6,NULL,NULL,'','','','','','',''),
	(9,6,NULL,NULL,'','','','','','',''),
	(10,6,NULL,NULL,'','','','','','',''),
	(11,6,NULL,NULL,'','','','','','',''),
	(12,6,NULL,NULL,'','','','','','',''),
	(13,6,NULL,NULL,'','','','','','',''),
	(14,6,NULL,NULL,'','','','','','',''),
	(15,6,NULL,NULL,'','','','','','',''),
	(16,6,NULL,NULL,'','','','','','',''),
	(17,6,NULL,NULL,'','','','','','',''),
	(18,6,NULL,NULL,'','','','','','',''),
	(19,6,NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',''),
	(20,6,NULL,NULL,'','','','','','',''),
	(21,6,NULL,NULL,'','','','','','',''),
	(22,6,NULL,NULL,'','','','','','',''),
	(23,6,NULL,NULL,'','','','','','',''),
	(24,6,NULL,NULL,'','','','','','',''),
	(25,6,NULL,NULL,'vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh',''),
	(26,4,NULL,NULL,'u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','u2m1 input test','');

/*!40000 ALTER TABLE `int120u2m1s1t6s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u2m2s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u2m2s1s`;

CREATE TABLE `int120u2m2s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `u2m2s1q1` int(11) DEFAULT NULL,
  `u2m2s1q2` int(11) DEFAULT NULL,
  `u2m2s1q3` int(11) DEFAULT NULL,
  `u2m2s1q4` int(11) DEFAULT NULL,
  `u2m2s1q5` int(11) DEFAULT NULL,
  `u2m2s1q6` int(11) DEFAULT NULL,
  `u2m2s1q7` int(11) DEFAULT NULL,
  `u2m2s1q8` int(11) DEFAULT NULL,
  `u2m2s1q9` int(11) DEFAULT NULL,
  `u2m2s1q10` int(11) DEFAULT NULL,
  `u2m2s1q11` int(11) DEFAULT NULL,
  `u2m2s1q12` int(11) DEFAULT NULL,
  `u2m2s1q13` int(11) DEFAULT NULL,
  `u2m2s1q14` int(11) DEFAULT NULL,
  `u2m2s1q15` int(11) DEFAULT NULL,
  `u2m2s1q16` int(11) DEFAULT NULL,
  `u2m2s1q17` int(11) DEFAULT NULL,
  `u2m2s1q18` int(11) DEFAULT NULL,
  `u2m2s1q19` int(11) DEFAULT NULL,
  `u2m2s1q20` int(11) DEFAULT NULL,
  `u2m2s1q21` int(11) DEFAULT NULL,
  `u2m2s1q22` int(11) DEFAULT NULL,
  `u2m2s1q23` int(11) DEFAULT NULL,
  `u2m2s1q24` int(11) DEFAULT NULL,
  `u2m2s1q25` int(11) DEFAULT NULL,
  `u2m2s1q26` int(11) DEFAULT NULL,
  `u2m2s1q27` int(11) DEFAULT NULL,
  `u2m2s1q28` int(11) DEFAULT NULL,
  `u2m2s1q29` int(11) DEFAULT NULL,
  `u2m2s1q30` int(11) DEFAULT NULL,
  `u2m2s1q31` int(11) DEFAULT NULL,
  `u2m2s1q32` int(11) DEFAULT NULL,
  `u2m2s1q33` int(11) DEFAULT NULL,
  `u2m2s1q34` int(11) DEFAULT NULL,
  `u2m2s1q35` int(11) DEFAULT NULL,
  `u2m2s1q36` int(11) DEFAULT NULL,
  `u2m2s1q37` int(11) DEFAULT NULL,
  `u2m2s1q38` int(11) DEFAULT NULL,
  `u2m2s1q39` int(11) DEFAULT NULL,
  `u2m2s1q40` int(11) DEFAULT NULL,
  `u2m2s1q41` int(11) DEFAULT NULL,
  `u2m2s1q42` int(11) DEFAULT NULL,
  `u2m2s1q43` int(11) DEFAULT NULL,
  `u2m2s1q44` int(11) DEFAULT NULL,
  `u2m2s1q45` int(11) DEFAULT NULL,
  `u2m2s1q46` int(11) DEFAULT NULL,
  `u2m2s1q47` int(11) DEFAULT NULL,
  `u2m2s1q48` int(11) DEFAULT NULL,
  `u2m2s1q49` int(11) DEFAULT NULL,
  `u2m2s1q50` int(11) DEFAULT NULL,
  `u2m2s1q51` int(11) DEFAULT NULL,
  `u2m2s1q52` int(11) DEFAULT NULL,
  `u2m2s1q53` int(11) DEFAULT NULL,
  `u2m2s1q54` int(11) DEFAULT NULL,
  `u2m2s1q55` int(11) DEFAULT NULL,
  `u2m2s1q56` int(11) DEFAULT NULL,
  `u2m2s1q57` int(11) DEFAULT NULL,
  `u2m2s1q58` int(11) DEFAULT NULL,
  `u2m2s1q59` int(11) DEFAULT NULL,
  `u2m2s1q60` int(11) DEFAULT NULL,
  `u2m2s1q61` int(11) DEFAULT NULL,
  `u2m2s1q62` int(11) DEFAULT NULL,
  `u2m2s1q63` int(11) DEFAULT NULL,
  `u2m2s1q64` int(11) DEFAULT NULL,
  `u2m2s1q65` int(11) DEFAULT NULL,
  `u2m2s1q66` int(11) DEFAULT NULL,
  `u2m2s1q67` int(11) DEFAULT NULL,
  `u2m2s1q68` int(11) DEFAULT NULL,
  `u2m2s1q69` int(11) DEFAULT NULL,
  `u2m2s1q70` int(11) DEFAULT NULL,
  `u2m2s1q71` int(11) DEFAULT NULL,
  `u2m2s1q72` int(11) DEFAULT NULL,
  `u2m2s1q73` int(11) DEFAULT NULL,
  `u2m2s1q74` int(11) DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u2m2s1s` WRITE;
/*!40000 ALTER TABLE `int120u2m2s1s` DISABLE KEYS */;

INSERT INTO `int120u2m2s1s` (`id`, `created_at`, `updated_at`, `user_id`, `u2m2s1q1`, `u2m2s1q2`, `u2m2s1q3`, `u2m2s1q4`, `u2m2s1q5`, `u2m2s1q6`, `u2m2s1q7`, `u2m2s1q8`, `u2m2s1q9`, `u2m2s1q10`, `u2m2s1q11`, `u2m2s1q12`, `u2m2s1q13`, `u2m2s1q14`, `u2m2s1q15`, `u2m2s1q16`, `u2m2s1q17`, `u2m2s1q18`, `u2m2s1q19`, `u2m2s1q20`, `u2m2s1q21`, `u2m2s1q22`, `u2m2s1q23`, `u2m2s1q24`, `u2m2s1q25`, `u2m2s1q26`, `u2m2s1q27`, `u2m2s1q28`, `u2m2s1q29`, `u2m2s1q30`, `u2m2s1q31`, `u2m2s1q32`, `u2m2s1q33`, `u2m2s1q34`, `u2m2s1q35`, `u2m2s1q36`, `u2m2s1q37`, `u2m2s1q38`, `u2m2s1q39`, `u2m2s1q40`, `u2m2s1q41`, `u2m2s1q42`, `u2m2s1q43`, `u2m2s1q44`, `u2m2s1q45`, `u2m2s1q46`, `u2m2s1q47`, `u2m2s1q48`, `u2m2s1q49`, `u2m2s1q50`, `u2m2s1q51`, `u2m2s1q52`, `u2m2s1q53`, `u2m2s1q54`, `u2m2s1q55`, `u2m2s1q56`, `u2m2s1q57`, `u2m2s1q58`, `u2m2s1q59`, `u2m2s1q60`, `u2m2s1q61`, `u2m2s1q62`, `u2m2s1q63`, `u2m2s1q64`, `u2m2s1q65`, `u2m2s1q66`, `u2m2s1q67`, `u2m2s1q68`, `u2m2s1q69`, `u2m2s1q70`, `u2m2s1q71`, `u2m2s1q72`, `u2m2s1q73`, `u2m2s1q74`, `text`, `status`)
VALUES
	(1,NULL,NULL,6,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,NULL,NULL,6,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,NULL,NULL,6,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(4,NULL,NULL,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'hsdvglb',NULL);

/*!40000 ALTER TABLE `int120u2m2s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u2m2s1t1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u2m2s1t1s`;

CREATE TABLE `int120u2m2s1t1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `u2m2s1q1` int(11) DEFAULT NULL,
  `u2m2s1q2` int(11) DEFAULT NULL,
  `u2m2s1q3` int(11) DEFAULT NULL,
  `u2m2s1q4` int(11) DEFAULT NULL,
  `u2m2s1q5` int(11) DEFAULT NULL,
  `u2m2s1q6` int(11) DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u2m2s1t1s` WRITE;
/*!40000 ALTER TABLE `int120u2m2s1t1s` DISABLE KEYS */;

INSERT INTO `int120u2m2s1t1s` (`id`, `created_at`, `updated_at`, `user_id`, `u2m2s1q1`, `u2m2s1q2`, `u2m2s1q3`, `u2m2s1q4`, `u2m2s1q5`, `u2m2s1q6`, `text`, `status`)
VALUES
	(1,NULL,NULL,6,0,0,0,0,0,0,NULL,NULL),
	(2,NULL,NULL,6,0,0,0,0,0,0,NULL,NULL),
	(3,NULL,NULL,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(4,NULL,NULL,6,NULL,NULL,NULL,NULL,NULL,NULL,'hsdvglb',NULL),
	(5,NULL,NULL,4,0,0,0,0,0,0,NULL,NULL);

/*!40000 ALTER TABLE `int120u2m2s1t1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u2m2s1t2s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u2m2s1t2s`;

CREATE TABLE `int120u2m2s1t2s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `u2m2s1q7` int(11) DEFAULT NULL,
  `u2m2s1q8` int(11) DEFAULT NULL,
  `u2m2s1q9` int(11) DEFAULT NULL,
  `u2m2s1q10` int(11) DEFAULT NULL,
  `u2m2s1q11` int(11) DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u2m2s1t2s` WRITE;
/*!40000 ALTER TABLE `int120u2m2s1t2s` DISABLE KEYS */;

INSERT INTO `int120u2m2s1t2s` (`id`, `created_at`, `updated_at`, `user_id`, `u2m2s1q7`, `u2m2s1q8`, `u2m2s1q9`, `u2m2s1q10`, `u2m2s1q11`, `text`, `status`)
VALUES
	(1,NULL,NULL,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,NULL,NULL,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,NULL,NULL,6,0,0,0,0,0,NULL,NULL),
	(4,NULL,NULL,6,NULL,NULL,NULL,NULL,NULL,'hsdvglb',NULL),
	(5,NULL,NULL,4,0,0,0,0,0,NULL,NULL),
	(6,NULL,NULL,4,0,0,0,0,0,NULL,NULL),
	(7,NULL,NULL,4,0,0,0,0,0,NULL,NULL);

/*!40000 ALTER TABLE `int120u2m2s1t2s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u2m2s1t3s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u2m2s1t3s`;

CREATE TABLE `int120u2m2s1t3s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `u2m2s1q12` int(11) DEFAULT NULL,
  `u2m2s1q13` int(11) DEFAULT NULL,
  `u2m2s1q14` int(11) DEFAULT NULL,
  `u2m2s1q15` int(11) DEFAULT NULL,
  `u2m2s1q16` int(11) DEFAULT NULL,
  `u2m2s1q17` int(11) DEFAULT NULL,
  `u2m2s1q18` int(11) DEFAULT NULL,
  `u2m2s1q19` int(11) DEFAULT NULL,
  `u2m2s1q20` int(11) DEFAULT NULL,
  `u2m2s1q21` int(11) DEFAULT NULL,
  `u2m2s1q22` int(11) DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u2m2s1t3s` WRITE;
/*!40000 ALTER TABLE `int120u2m2s1t3s` DISABLE KEYS */;

INSERT INTO `int120u2m2s1t3s` (`id`, `created_at`, `updated_at`, `user_id`, `u2m2s1q12`, `u2m2s1q13`, `u2m2s1q14`, `u2m2s1q15`, `u2m2s1q16`, `u2m2s1q17`, `u2m2s1q18`, `u2m2s1q19`, `u2m2s1q20`, `u2m2s1q21`, `u2m2s1q22`, `text`, `status`)
VALUES
	(1,NULL,NULL,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,NULL,NULL,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,NULL,NULL,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(4,NULL,NULL,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'hsdvglb',NULL),
	(5,NULL,NULL,4,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL);

/*!40000 ALTER TABLE `int120u2m2s1t3s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u2m2s1t4s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u2m2s1t4s`;

CREATE TABLE `int120u2m2s1t4s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `u2m2s1q23` int(11) DEFAULT NULL,
  `u2m2s1q24` int(11) DEFAULT NULL,
  `u2m2s1q25` int(11) DEFAULT NULL,
  `u2m2s1q26` int(11) DEFAULT NULL,
  `u2m2s1q27` int(11) DEFAULT NULL,
  `u2m2s1q28` int(11) DEFAULT NULL,
  `u2m2s1q29` int(11) DEFAULT NULL,
  `u2m2s1q30` int(11) DEFAULT NULL,
  `u2m2s1q31` int(11) DEFAULT NULL,
  `u2m2s1q32` int(11) DEFAULT NULL,
  `u2m2s1q33` int(11) DEFAULT NULL,
  `u2m2s1q34` int(11) DEFAULT NULL,
  `u2m2s1q35` int(11) DEFAULT NULL,
  `u2m2s1q36` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q37` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q38` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q39` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q40` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q41` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q42` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q43` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q44` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q45` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q46` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q47` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q48` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q49` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q50` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q51` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q52` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q53` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q54` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q55` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q56` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q57` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q58` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q59` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q60` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q61` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q62` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q63` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q64` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q65` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q66` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q67` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q68` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q69` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q70` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q71` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q72` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q73` text COLLATE utf8mb4_unicode_ci,
  `u2m2s1q74` text COLLATE utf8mb4_unicode_ci,
  `text` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u2m2s1t4s` WRITE;
/*!40000 ALTER TABLE `int120u2m2s1t4s` DISABLE KEYS */;

INSERT INTO `int120u2m2s1t4s` (`id`, `created_at`, `updated_at`, `user_id`, `u2m2s1q23`, `u2m2s1q24`, `u2m2s1q25`, `u2m2s1q26`, `u2m2s1q27`, `u2m2s1q28`, `u2m2s1q29`, `u2m2s1q30`, `u2m2s1q31`, `u2m2s1q32`, `u2m2s1q33`, `u2m2s1q34`, `u2m2s1q35`, `u2m2s1q36`, `u2m2s1q37`, `u2m2s1q38`, `u2m2s1q39`, `u2m2s1q40`, `u2m2s1q41`, `u2m2s1q42`, `u2m2s1q43`, `u2m2s1q44`, `u2m2s1q45`, `u2m2s1q46`, `u2m2s1q47`, `u2m2s1q48`, `u2m2s1q49`, `u2m2s1q50`, `u2m2s1q51`, `u2m2s1q52`, `u2m2s1q53`, `u2m2s1q54`, `u2m2s1q55`, `u2m2s1q56`, `u2m2s1q57`, `u2m2s1q58`, `u2m2s1q59`, `u2m2s1q60`, `u2m2s1q61`, `u2m2s1q62`, `u2m2s1q63`, `u2m2s1q64`, `u2m2s1q65`, `u2m2s1q66`, `u2m2s1q67`, `u2m2s1q68`, `u2m2s1q69`, `u2m2s1q70`, `u2m2s1q71`, `u2m2s1q72`, `u2m2s1q73`, `u2m2s1q74`, `text`, `status`)
VALUES
	(1,NULL,NULL,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,NULL,NULL,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,NULL,NULL,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(4,NULL,NULL,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'hsdvglb',NULL);

/*!40000 ALTER TABLE `int120u2m2s1t4s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u2m3s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u2m3s1s`;

CREATE TABLE `int120u2m3s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u2m3s1q1` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q2` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q3` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q4` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q5` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q6` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q7` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q8` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q9` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q10` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q11` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q12` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q13` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q14` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q15` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q16` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q17` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q18` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q19` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q20` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q21` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q22` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q23` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q24` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q25` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q26` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q27` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q28` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q29` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q30` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q31` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q32` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q33` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q34` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q35` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q36` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q37` text COLLATE utf8mb4_unicode_ci,
  `text` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u2m3s1s` WRITE;
/*!40000 ALTER TABLE `int120u2m3s1s` DISABLE KEYS */;

INSERT INTO `int120u2m3s1s` (`id`, `user_id`, `created_at`, `updated_at`, `u2m3s1q1`, `u2m3s1q2`, `u2m3s1q3`, `u2m3s1q4`, `u2m3s1q5`, `u2m3s1q6`, `u2m3s1q7`, `u2m3s1q8`, `u2m3s1q9`, `u2m3s1q10`, `u2m3s1q11`, `u2m3s1q12`, `u2m3s1q13`, `u2m3s1q14`, `u2m3s1q15`, `u2m3s1q16`, `u2m3s1q17`, `u2m3s1q18`, `u2m3s1q19`, `u2m3s1q20`, `u2m3s1q21`, `u2m3s1q22`, `u2m3s1q23`, `u2m3s1q24`, `u2m3s1q25`, `u2m3s1q26`, `u2m3s1q27`, `u2m3s1q28`, `u2m3s1q29`, `u2m3s1q30`, `u2m3s1q31`, `u2m3s1q32`, `u2m3s1q33`, `u2m3s1q34`, `u2m3s1q35`, `u2m3s1q36`, `u2m3s1q37`, `text`, `status`)
VALUES
	(1,6,NULL,NULL,'ghfdshsf','gfsdhsfg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,6,NULL,NULL,'null, [\'class\' => \'form-control\'])','null, [\'class\' => \'form-control\'])','null, [\'class\' => \'form-control\'])','null, [\'class\' => \'form-control\'])','null, [\'class\' => \'form-control\'])',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'null, [\'class\' => \'form-control\'])','null, [\'class\' => \'form-control\'])',NULL),
	(3,6,NULL,NULL,'pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;',NULL),
	(4,6,NULL,NULL,'byvxkus;bglsvyzg','byvxkus;bglsvyzg','byvxkus;bglsvyzg','byvxkus;bglsvyzg','byvxkus;bglsvyzg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'byvxkus;bglsvyzg','byvxkus;bglsvyzg',NULL),
	(5,4,NULL,NULL,'rando input test','rando input test','rando input test','rando input test','rando input test',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'rando input test','rando input test',NULL),
	(6,4,NULL,NULL,'test u2m3 input','test u2m3 input','test u2m3 input','test u2m3 input','test u2m3 input',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'test u2m3 input','test u2m3 input',NULL),
	(7,4,NULL,NULL,'input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc','input test  hgcfiegkusbd>>kveolc',NULL,NULL),
	(8,4,NULL,NULL,'Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them',NULL,NULL);

/*!40000 ALTER TABLE `int120u2m3s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u3m1s1bakup
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u3m1s1bakup`;

CREATE TABLE `int120u3m1s1bakup` (
  `bakup` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table int120u3m1s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u3m1s1s`;

CREATE TABLE `int120u3m1s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `u3m1s1q1` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u3m1s1q2` text COLLATE utf8mb4_unicode_ci,
  `u3m1s1q3` text COLLATE utf8mb4_unicode_ci,
  `u3m1s1q4` text COLLATE utf8mb4_unicode_ci,
  `u3m1s1q5` tinyint(4) DEFAULT NULL,
  `u3m1s1q6` tinyint(4) DEFAULT NULL,
  `u3m1s1q7` tinyint(4) DEFAULT NULL,
  `u3m1s1q8` tinyint(4) DEFAULT NULL,
  `u3m1s1q9` tinyint(4) DEFAULT NULL,
  `u3m1s1q10` tinyint(4) DEFAULT NULL,
  `u3m1s1q11` tinyint(4) DEFAULT NULL,
  `u3m1s1q12` tinyint(4) DEFAULT NULL,
  `u3m1s1q13` tinyint(4) DEFAULT NULL,
  `u3m1s1q14` tinyint(4) DEFAULT NULL,
  `u3m1s1q15` tinyint(4) DEFAULT NULL,
  `u3m1s1q16` tinyint(4) DEFAULT NULL,
  `u3m1s1q17` tinyint(4) DEFAULT NULL,
  `u3m1s1q18` tinyint(4) DEFAULT NULL,
  `u3m1s1q19` tinyint(4) DEFAULT NULL,
  `u3m1s1q20` tinyint(4) DEFAULT NULL,
  `u3m1s1q21` tinyint(4) DEFAULT NULL,
  `u3m1s1q22` tinyint(4) DEFAULT NULL,
  `u3m1s1q23` tinyint(4) DEFAULT NULL,
  `u3m1s1q24` tinyint(4) DEFAULT NULL,
  `u3m1s1q25` tinyint(4) DEFAULT NULL,
  `u3m1s1q26` tinyint(4) DEFAULT NULL,
  `u3m1s1q27` tinyint(4) DEFAULT NULL,
  `u3m1s1q28` tinyint(4) DEFAULT NULL,
  `u3m1s1q29` tinyint(4) DEFAULT NULL,
  `u3m1s1q30` tinyint(4) DEFAULT NULL,
  `u3m1s1q31` tinyint(4) DEFAULT NULL,
  `u3m1s1q32` tinyint(4) DEFAULT NULL,
  `u3m1s1q33` tinyint(4) DEFAULT NULL,
  `u3m1s1q34` tinyint(4) DEFAULT NULL,
  `u3m1s1q35` tinyint(4) DEFAULT NULL,
  `u3m1s1q36` tinyint(4) DEFAULT NULL,
  `u3m1s1q37` tinyint(4) DEFAULT NULL,
  `status` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u3m1s1s` WRITE;
/*!40000 ALTER TABLE `int120u3m1s1s` DISABLE KEYS */;

INSERT INTO `int120u3m1s1s` (`id`, `user_id`, `u3m1s1q1`, `created_at`, `updated_at`, `u3m1s1q2`, `u3m1s1q3`, `u3m1s1q4`, `u3m1s1q5`, `u3m1s1q6`, `u3m1s1q7`, `u3m1s1q8`, `u3m1s1q9`, `u3m1s1q10`, `u3m1s1q11`, `u3m1s1q12`, `u3m1s1q13`, `u3m1s1q14`, `u3m1s1q15`, `u3m1s1q16`, `u3m1s1q17`, `u3m1s1q18`, `u3m1s1q19`, `u3m1s1q20`, `u3m1s1q21`, `u3m1s1q22`, `u3m1s1q23`, `u3m1s1q24`, `u3m1s1q25`, `u3m1s1q26`, `u3m1s1q27`, `u3m1s1q28`, `u3m1s1q29`, `u3m1s1q30`, `u3m1s1q31`, `u3m1s1q32`, `u3m1s1q33`, `u3m1s1q34`, `u3m1s1q35`, `u3m1s1q36`, `u3m1s1q37`, `status`)
VALUES
	(1,6,'fdssd',NULL,NULL,'kfjdhtsg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,6,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,0,NULL,NULL,NULL,0,NULL,0,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `int120u3m1s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u3m1s1t1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u3m1s1t1s`;

CREATE TABLE `int120u3m1s1t1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `u3m1s1q1` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u3m1s1q2` text COLLATE utf8mb4_unicode_ci,
  `u3m1s1q3` text COLLATE utf8mb4_unicode_ci,
  `u3m1s1q4` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u3m1s1t1s` WRITE;
/*!40000 ALTER TABLE `int120u3m1s1t1s` DISABLE KEYS */;

INSERT INTO `int120u3m1s1t1s` (`id`, `user_id`, `u3m1s1q1`, `created_at`, `updated_at`, `u3m1s1q2`, `u3m1s1q3`, `u3m1s1q4`, `status`)
VALUES
	(1,6,'fdssd',NULL,NULL,'kfjdhtsg',NULL,NULL,NULL),
	(2,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,4,'unit 3 module one input test',NULL,NULL,'unit 3 module one input test','unit 3 module one input test','unit 3 module one input test',NULL),
	(4,4,'Name the forms and demonstrate them',NULL,NULL,'Name the forms and demonstrate them','Name the forms and demonstrate them','Name the forms and demonstrate them',NULL);

/*!40000 ALTER TABLE `int120u3m1s1t1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u3m1s1t2s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u3m1s1t2s`;

CREATE TABLE `int120u3m1s1t2s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u3m1s1q5` tinyint(4) DEFAULT NULL,
  `u3m1s1q6` tinyint(4) DEFAULT NULL,
  `u3m1s1q7` tinyint(4) DEFAULT NULL,
  `u3m1s1q8` tinyint(4) DEFAULT NULL,
  `u3m1s1q9` tinyint(4) DEFAULT NULL,
  `u3m1s1q10` tinyint(4) DEFAULT NULL,
  `u3m1s1q11` tinyint(4) DEFAULT NULL,
  `u3m1s1q12` tinyint(4) DEFAULT NULL,
  `u3m1s1q13` tinyint(4) DEFAULT NULL,
  `u3m1s1q14` tinyint(4) DEFAULT NULL,
  `u3m1s1q15` tinyint(4) DEFAULT NULL,
  `u3m1s1q16` tinyint(4) DEFAULT NULL,
  `u3m1s1q17` tinyint(4) DEFAULT NULL,
  `u3m1s1q18` tinyint(4) DEFAULT NULL,
  `u3m1s1q19` tinyint(4) DEFAULT NULL,
  `u3m1s1q20` tinyint(4) DEFAULT NULL,
  `u3m1s1q21` tinyint(4) DEFAULT NULL,
  `u3m1s1q22` tinyint(4) DEFAULT NULL,
  `u3m1s1q23` tinyint(4) DEFAULT NULL,
  `u3m1s1q24` tinyint(4) DEFAULT NULL,
  `u3m1s1q25` tinyint(4) DEFAULT NULL,
  `u3m1s1q26` tinyint(4) DEFAULT NULL,
  `u3m1s1q27` tinyint(4) DEFAULT NULL,
  `u3m1s1q28` tinyint(4) DEFAULT NULL,
  `u3m1s1q29` tinyint(4) DEFAULT NULL,
  `u3m1s1q30` tinyint(4) DEFAULT NULL,
  `u3m1s1q31` tinyint(4) DEFAULT NULL,
  `u3m1s1q32` tinyint(4) DEFAULT NULL,
  `u3m1s1q33` tinyint(4) DEFAULT NULL,
  `u3m1s1q34` tinyint(4) DEFAULT NULL,
  `u3m1s1q35` tinyint(4) DEFAULT NULL,
  `u3m1s1q36` tinyint(4) DEFAULT NULL,
  `u3m1s1q37` tinyint(4) DEFAULT NULL,
  `status` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u3m1s1t2s` WRITE;
/*!40000 ALTER TABLE `int120u3m1s1t2s` DISABLE KEYS */;

INSERT INTO `int120u3m1s1t2s` (`id`, `user_id`, `created_at`, `updated_at`, `u3m1s1q5`, `u3m1s1q6`, `u3m1s1q7`, `u3m1s1q8`, `u3m1s1q9`, `u3m1s1q10`, `u3m1s1q11`, `u3m1s1q12`, `u3m1s1q13`, `u3m1s1q14`, `u3m1s1q15`, `u3m1s1q16`, `u3m1s1q17`, `u3m1s1q18`, `u3m1s1q19`, `u3m1s1q20`, `u3m1s1q21`, `u3m1s1q22`, `u3m1s1q23`, `u3m1s1q24`, `u3m1s1q25`, `u3m1s1q26`, `u3m1s1q27`, `u3m1s1q28`, `u3m1s1q29`, `u3m1s1q30`, `u3m1s1q31`, `u3m1s1q32`, `u3m1s1q33`, `u3m1s1q34`, `u3m1s1q35`, `u3m1s1q36`, `u3m1s1q37`, `status`)
VALUES
	(1,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,6,NULL,NULL,0,NULL,NULL,NULL,0,NULL,NULL,NULL,0,NULL,0,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL),
	(3,4,NULL,NULL,0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,NULL,NULL,NULL,0,NULL,NULL,NULL,0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL),
	(4,4,NULL,NULL,0,NULL,NULL,NULL,0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `int120u3m1s1t2s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u3m2s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u3m2s1s`;

CREATE TABLE `int120u3m2s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u3m2s1q1` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q2` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q3` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q4` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q5` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q6` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q7` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q8` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q9` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q10` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q11` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q12` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q13` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q14` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q15` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q16` tinyint(4) DEFAULT NULL,
  `u3m2s1q17` tinyint(4) DEFAULT NULL,
  `u3m2s1q18` tinyint(4) DEFAULT NULL,
  `u3m2s1q19` tinyint(4) DEFAULT NULL,
  `u3m2s1q20` tinyint(4) DEFAULT NULL,
  `u3m2s1q21` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q22` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q23` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q24` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q25` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q26` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q27` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q28` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q29` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q30` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q31` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q32` tinyint(4) DEFAULT NULL,
  `u3m2s1q33` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q34` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q35` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q36` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q37` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q38` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q39` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q40` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q41` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q42` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u3m2s1s` WRITE;
/*!40000 ALTER TABLE `int120u3m2s1s` DISABLE KEYS */;

INSERT INTO `int120u3m2s1s` (`id`, `user_id`, `created_at`, `updated_at`, `u3m2s1q1`, `u3m2s1q2`, `u3m2s1q3`, `u3m2s1q4`, `u3m2s1q5`, `u3m2s1q6`, `u3m2s1q7`, `u3m2s1q8`, `u3m2s1q9`, `u3m2s1q10`, `u3m2s1q11`, `u3m2s1q12`, `u3m2s1q13`, `u3m2s1q14`, `u3m2s1q15`, `u3m2s1q16`, `u3m2s1q17`, `u3m2s1q18`, `u3m2s1q19`, `u3m2s1q20`, `u3m2s1q21`, `u3m2s1q22`, `u3m2s1q23`, `u3m2s1q24`, `u3m2s1q25`, `u3m2s1q26`, `u3m2s1q27`, `u3m2s1q28`, `u3m2s1q29`, `u3m2s1q30`, `u3m2s1q31`, `u3m2s1q32`, `u3m2s1q33`, `u3m2s1q34`, `u3m2s1q35`, `u3m2s1q36`, `u3m2s1q37`, `u3m2s1q38`, `u3m2s1q39`, `u3m2s1q40`, `u3m2s1q41`, `u3m2s1q42`, `status`)
VALUES
	(1,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'gfarclbyaergyae;gryl','gfarclbyaergyae;gryl',1,0,1,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'gfarclbyaergyae;gryl','gfarclbyaergyae;gryl',1,0,1,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'gfdagsfdgs','sfdhgsgh',1,1,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(4,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'fdsgrsvgts','gfsdvt',0,1,0,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(5,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor',NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor',0,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor',NULL,NULL);

/*!40000 ALTER TABLE `int120u3m2s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u3m2s1t1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u3m2s1t1s`;

CREATE TABLE `int120u3m2s1t1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u3m2s1q1` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q2` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q3` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q4` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q5` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q6` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q7` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q8` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q9` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q10` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q11` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q12` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q13` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u3m2s1t1s` WRITE;
/*!40000 ALTER TABLE `int120u3m2s1t1s` DISABLE KEYS */;

INSERT INTO `int120u3m2s1t1s` (`id`, `user_id`, `created_at`, `updated_at`, `u3m2s1q1`, `u3m2s1q2`, `u3m2s1q3`, `u3m2s1q4`, `u3m2s1q5`, `u3m2s1q6`, `u3m2s1q7`, `u3m2s1q8`, `u3m2s1q9`, `u3m2s1q10`, `u3m2s1q11`, `u3m2s1q12`, `u3m2s1q13`, `status`)
VALUES
	(1,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(4,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(5,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `int120u3m2s1t1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u3m2s1t2s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u3m2s1t2s`;

CREATE TABLE `int120u3m2s1t2s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u3m2s1q14` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q15` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q16` tinyint(4) DEFAULT NULL,
  `u3m2s1q17` tinyint(4) DEFAULT NULL,
  `u3m2s1q18` tinyint(4) DEFAULT NULL,
  `u3m2s1q19` tinyint(4) DEFAULT NULL,
  `u3m2s1q20` tinyint(4) DEFAULT NULL,
  `status` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u3m2s1t2s` WRITE;
/*!40000 ALTER TABLE `int120u3m2s1t2s` DISABLE KEYS */;

INSERT INTO `int120u3m2s1t2s` (`id`, `user_id`, `created_at`, `updated_at`, `u3m2s1q14`, `u3m2s1q15`, `u3m2s1q16`, `u3m2s1q17`, `u3m2s1q18`, `u3m2s1q19`, `u3m2s1q20`, `status`)
VALUES
	(1,6,NULL,NULL,'gfarclbyaergyae;gryl','gfarclbyaergyae;gryl',1,0,1,1,0,NULL),
	(2,6,NULL,NULL,'gfarclbyaergyae;gryl','gfarclbyaergyae;gryl',1,0,1,1,0,NULL),
	(3,6,NULL,NULL,'gfdagsfdgs','sfdhgsgh',1,1,1,1,1,NULL),
	(4,6,NULL,NULL,'fdsgrsvgts','gfsdvt',0,1,0,1,0,NULL),
	(5,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `int120u3m2s1t2s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u3m2s1t3s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u3m2s1t3s`;

CREATE TABLE `int120u3m2s1t3s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u3m2s1q21` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q22` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q23` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q24` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q25` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q26` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q27` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q28` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q29` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q30` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q31` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q32` tinyint(4) DEFAULT NULL,
  `u3m2s1q33` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q34` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q35` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q36` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q37` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q38` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q39` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q40` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q41` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q42` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u3m2s1t3s` WRITE;
/*!40000 ALTER TABLE `int120u3m2s1t3s` DISABLE KEYS */;

INSERT INTO `int120u3m2s1t3s` (`id`, `user_id`, `created_at`, `updated_at`, `u3m2s1q21`, `u3m2s1q22`, `u3m2s1q23`, `u3m2s1q24`, `u3m2s1q25`, `u3m2s1q26`, `u3m2s1q27`, `u3m2s1q28`, `u3m2s1q29`, `u3m2s1q30`, `u3m2s1q31`, `u3m2s1q32`, `u3m2s1q33`, `u3m2s1q34`, `u3m2s1q35`, `u3m2s1q36`, `u3m2s1q37`, `u3m2s1q38`, `u3m2s1q39`, `u3m2s1q40`, `u3m2s1q41`, `u3m2s1q42`, `status`)
VALUES
	(1,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(4,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(5,6,NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor',NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor',0,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor',NULL,NULL);

/*!40000 ALTER TABLE `int120u3m2s1t3s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u3m3s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u3m3s1s`;

CREATE TABLE `int120u3m3s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u3m3s1q1` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q2` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q3` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q4` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q5` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q6` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q7` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q8` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q9` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q10` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q11` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q12` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q13` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q14` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q15` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q16` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q17` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q18` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q19` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u3m3s1s` WRITE;
/*!40000 ALTER TABLE `int120u3m3s1s` DISABLE KEYS */;

INSERT INTO `int120u3m3s1s` (`id`, `user_id`, `created_at`, `updated_at`, `u3m3s1q1`, `u3m3s1q2`, `u3m3s1q3`, `u3m3s1q4`, `u3m3s1q5`, `u3m3s1q6`, `u3m3s1q7`, `u3m3s1q8`, `u3m3s1q9`, `u3m3s1q10`, `u3m3s1q11`, `u3m3s1q12`, `u3m3s1q13`, `u3m3s1q14`, `u3m3s1q15`, `u3m3s1q16`, `u3m3s1q17`, `u3m3s1q18`, `u3m3s1q19`, `status`)
VALUES
	(1,6,NULL,NULL,'svsts','cgsrt',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,6,NULL,NULL,'avz','gcrar',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,6,NULL,NULL,NULL,NULL,'htsr htb',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(4,6,NULL,NULL,NULL,NULL,NULL,NULL,'ghkjghkgkgj','gjhkgkvgkbg','gjhkvgkvgk','gvkgkgkgvk','gkvgkvgk','kvgkgkvgk','kvgkvgkvgk','kgkgkgk','gjkfkgkg','gkvgkvgk','gkgvkvk','gkvgkvgkv','kgkvgkvgk','kvgkvgk','gkvgkvgk',NULL),
	(5,4,NULL,NULL,'Module 3 | Speaking and Writing','Module 3 | Speaking and Writing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `int120u3m3s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u3m3s1t1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u3m3s1t1s`;

CREATE TABLE `int120u3m3s1t1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u3m3s1q1` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q2` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u3m3s1t1s` WRITE;
/*!40000 ALTER TABLE `int120u3m3s1t1s` DISABLE KEYS */;

INSERT INTO `int120u3m3s1t1s` (`id`, `user_id`, `created_at`, `updated_at`, `u3m3s1q1`, `u3m3s1q2`, `status`)
VALUES
	(1,6,NULL,NULL,'svsts','cgsrt',NULL),
	(2,6,NULL,NULL,'avz','gcrar',NULL),
	(3,6,NULL,NULL,NULL,NULL,NULL),
	(4,6,NULL,NULL,NULL,NULL,NULL),
	(5,4,NULL,NULL,'Module 3 | Speaking and Writing','Module 3 | Speaking and Writing',NULL);

/*!40000 ALTER TABLE `int120u3m3s1t1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u3m3s1t2s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u3m3s1t2s`;

CREATE TABLE `int120u3m3s1t2s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u3m3s1q3` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u3m3s1t2s` WRITE;
/*!40000 ALTER TABLE `int120u3m3s1t2s` DISABLE KEYS */;

INSERT INTO `int120u3m3s1t2s` (`id`, `user_id`, `created_at`, `updated_at`, `u3m3s1q3`, `status`)
VALUES
	(1,6,NULL,NULL,NULL,NULL),
	(2,6,NULL,NULL,NULL,NULL),
	(3,6,NULL,NULL,'htsr htb',NULL),
	(4,6,NULL,NULL,NULL,NULL),
	(5,4,NULL,NULL,'Module 3 | Speaking and Writing',NULL),
	(6,4,NULL,NULL,'Module 3 | Speaking and Writing',NULL);

/*!40000 ALTER TABLE `int120u3m3s1t2s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u3m3s1t3s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u3m3s1t3s`;

CREATE TABLE `int120u3m3s1t3s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u3m3s1q4` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u3m3s1t3s` WRITE;
/*!40000 ALTER TABLE `int120u3m3s1t3s` DISABLE KEYS */;

INSERT INTO `int120u3m3s1t3s` (`id`, `user_id`, `created_at`, `updated_at`, `u3m3s1q4`, `status`)
VALUES
	(1,6,NULL,NULL,NULL,NULL),
	(2,6,NULL,NULL,NULL,NULL),
	(3,6,NULL,NULL,NULL,NULL),
	(4,6,NULL,NULL,NULL,NULL),
	(5,4,NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"',NULL);

/*!40000 ALTER TABLE `int120u3m3s1t3s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u3m3s1t4s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u3m3s1t4s`;

CREATE TABLE `int120u3m3s1t4s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u3m3s1q5` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q6` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q7` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q8` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q9` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q10` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q11` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q12` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q13` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q14` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q15` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q16` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q17` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q18` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q19` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1tans1` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1tans2` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1tans3` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1tans4` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1tans5` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1dans1` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1dans2` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1dans3` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1dans4` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1dans5` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1idans1` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1idans2` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1idans3` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1idans4` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1idans5` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u3m3s1t4s` WRITE;
/*!40000 ALTER TABLE `int120u3m3s1t4s` DISABLE KEYS */;

INSERT INTO `int120u3m3s1t4s` (`id`, `user_id`, `created_at`, `updated_at`, `u3m3s1q5`, `u3m3s1q6`, `u3m3s1q7`, `u3m3s1q8`, `u3m3s1q9`, `u3m3s1q10`, `u3m3s1q11`, `u3m3s1q12`, `u3m3s1q13`, `u3m3s1q14`, `u3m3s1q15`, `u3m3s1q16`, `u3m3s1q17`, `u3m3s1q18`, `u3m3s1q19`, `u3m3s1tans1`, `u3m3s1tans2`, `u3m3s1tans3`, `u3m3s1tans4`, `u3m3s1tans5`, `u3m3s1dans1`, `u3m3s1dans2`, `u3m3s1dans3`, `u3m3s1dans4`, `u3m3s1dans5`, `u3m3s1idans1`, `u3m3s1idans2`, `u3m3s1idans3`, `u3m3s1idans4`, `u3m3s1idans5`, `status`)
VALUES
	(1,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(4,6,NULL,NULL,'ghkjghkgkgj','gjhkgkvgkbg','gjhkvgkvgk','gvkgkgkgvk','gkvgkvgk','kvgkgkvgk','kvgkvgkvgk','kgkgkgk','gjkfkgkg','gkvgkvgk','gkgvkvk','gkvgkvgkv','kgkvgkvgk','kvgkvgk','gkvgkvgk',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(5,4,NULL,NULL,'Module 3 | Speaking and Writing','Module 3 | Speaking and Writing','Module 3 | Speaking and Writing','Module 3 | Speaking and Writing','Module 3 | Speaking and Writing','Module 3 | Speaking and Writing','Module 3 | Speaking and Writing','Module 3 | Speaking and Writing','Module 3 | Speaking and Writing','Module 3 | Speaking and Writing','Module 3 | Speaking and Writing','Module 3 | Speaking and Writing','Module 3 | Speaking and Writing','Module 3 | Speaking and Writing','Module 3 | Speaking and Writing','/t/','/t/','/t/','/t/','/t/','/d/','/d/','/d/','/d/','/d/',NULL,'/id/','/id/','/id/','/id/',NULL);

/*!40000 ALTER TABLE `int120u3m3s1t4s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u3m4s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u3m4s1s`;

CREATE TABLE `int120u3m4s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table int120u3m5s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u3m5s1s`;

CREATE TABLE `int120u3m5s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u3m5s1q1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q3` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q4` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q5` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q6` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q7` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q8` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q9` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q10` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q11` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q12` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q13` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q14` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q15` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q16` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q17` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q18` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q19` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q20` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u3m5s1s` WRITE;
/*!40000 ALTER TABLE `int120u3m5s1s` DISABLE KEYS */;

INSERT INTO `int120u3m5s1s` (`id`, `user_id`, `created_at`, `updated_at`, `u3m5s1q1`, `u3m5s1q2`, `u3m5s1q3`, `u3m5s1q4`, `u3m5s1q5`, `u3m5s1q6`, `u3m5s1q7`, `u3m5s1q8`, `u3m5s1q9`, `u3m5s1q10`, `u3m5s1q11`, `u3m5s1q12`, `u3m5s1q13`, `u3m5s1q14`, `u3m5s1q15`, `u3m5s1q16`, `u3m5s1q17`, `u3m5s1q18`, `u3m5s1q19`, `u3m5s1q20`, `status`)
VALUES
	(1,6,NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','','','','','','','','','','','','','','','',''),
	(2,4,NULL,NULL,'Unit 3 | The Teaching and Learning of EFL','Unit 3 | The Teaching and Learning of EFL','Unit 3 | The Teaching and Learning of EFL','Unit 3 | The Teaching and Learning of EFL','Unit 3 | The Teaching and Learning of EFL','Unit 3 | The Teaching and Learning of EFL','Unit 3 | The Teaching and Learning of EFL','Unit 3 | The Teaching and Learning of EFL','Unit 3 | The Teaching and Learning of EFL','Unit 3 | The Teaching and Learning of EFL','Unit 3 | The Teaching and Learning of EFL','Unit 3 | The Teaching and Learning of EFL','Unit 3 | The Teaching and Learning of EFL','Unit 3 | The Teaching and Learning of EFL','Unit 3 | The Teaching and Learning of EFL','Unit 3 | The Teaching and Learning of EFL','Unit 3 | The Teaching and Learning of EFL','Unit 3 | The Teaching and Learning of EFL','Unit 3 | The Teaching and Learning of EFL','Unit 3 | The Teaching and Learning of EFL','');

/*!40000 ALTER TABLE `int120u3m5s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u4m1s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u4m1s1s`;

CREATE TABLE `int120u4m1s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `u4m1s1q1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m1s1q2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m1s1q3` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m1s1q4` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m1s1q5` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` text COLLATE utf8mb4_unicode_ci,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u4m1s1s` WRITE;
/*!40000 ALTER TABLE `int120u4m1s1s` DISABLE KEYS */;

INSERT INTO `int120u4m1s1s` (`id`, `user_id`, `u4m1s1q1`, `u4m1s1q2`, `u4m1s1q3`, `u4m1s1q4`, `u4m1s1q5`, `created_at`, `updated_at`, `status`)
VALUES
	(1,9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-04-16 14:26:48',NULL,''),
	(2,9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','','','','','2018-04-16 14:26:48',NULL,''),
	(3,9,'seperate  input 1','','','','','2018-04-16 14:26:48',NULL,''),
	(4,9,'Sint timeam inciderint nam te, mel ut percipit gubergren scriptorem. Eu quando accommodare consequuntur sed, vel quando facilis invidunt te. At nam veri legendos comprehensam, melius detraxit complectitur ne sea. Eum ex volumus dignissim, ut utamur vituperata sit, qui ut periculis consectetuer. Ne lucilius petentium vel, vix ei possim reprehendunt.','Sint timeam inciderint nam te, mel ut percipit gubergren scriptorem. Eu quando accommodare consequuntur sed, vel quando facilis invidunt te. At nam veri legendos comprehensam, melius detraxit complectitur ne sea. Eum ex volumus dignissim, ut utamur vituperata sit, qui ut periculis consectetuer. Ne lucilius petentium vel, vix ei possim reprehendunt.','Sint timeam inciderint nam te, mel ut percipit gubergren scriptorem. Eu quando accommodare consequuntur sed, vel quando facilis invidunt te. At nam veri legendos comprehensam, melius detraxit complectitur ne sea. Eum ex volumus dignissim, ut utamur vituperata sit, qui ut periculis consectetuer. Ne lucilius petentium vel, vix ei possim reprehendunt.','Sint timeam inciderint nam te, mel ut percipit gubergren scriptorem. Eu quando accommodare consequuntur sed, vel quando facilis invidunt te. At nam veri legendos comprehensam, melius detraxit complectitur ne sea. Eum ex volumus dignissim, ut utamur vituperata sit, qui ut periculis consectetuer. Ne lucilius petentium vel, vix ei possim reprehendunt.','Sint timeam inciderint nam te, mel ut percipit gubergren scriptorem. Eu quando accommodare consequuntur sed, vel quando facilis invidunt te. At nam veri legendos comprehensam, melius detraxit complectitur ne sea. Eum ex volumus dignissim, ut utamur vituperata sit, qui ut periculis consectetuer. Ne lucilius petentium vel, vix ei possim reprehendunt.','2018-04-16 14:26:48',NULL,''),
	(5,9,'hgeio qgrqeug','gr eaget','aerhtuejyethre','tsrhjd rhae','aesht','2018-04-16 14:26:48',NULL,''),
	(6,9,'hruewigyberso','griow;rghslybgt','f;w toi;shtosg','bgkf;d\' nkh','hbfwgtb sluvofdjb','2018-04-16 14:26:48',NULL,''),
	(7,6,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"',NULL,NULL,''),
	(8,4,'Module 1 | Classroom Management','Module 1 | Classroom Management','Module 1 | Classroom Management','Module 1 | Classroom Management','Module 1 | Classroom Management',NULL,NULL,''),
	(9,4,'Module 1 | Classroom Management','Module 1 | Classroom Management','Module 1 | Classroom Management','Module 1 | Classroom Management','Module 1 | Classroom Management',NULL,NULL,''),
	(10,4,'lorem ipsum test demo','lorem ipsum test demo','lorem ipsum test demo','lorem ipsum test demo','lorem ipsum test demo',NULL,NULL,NULL),
	(11,4,'lorem ipsum test demo','lorem ipsum test demo','lorem ipsum test demo','lorem ipsum test demo','lorem ipsum test demo',NULL,NULL,NULL);

/*!40000 ALTER TABLE `int120u4m1s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u4m2s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u4m2s1s`;

CREATE TABLE `int120u4m2s1s` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `u4m2s1q1` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q2` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q3` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q4` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q5` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u4m2s1q6` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q7` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q8` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q9` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q10` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `int120u4m2s1s` WRITE;
/*!40000 ALTER TABLE `int120u4m2s1s` DISABLE KEYS */;

INSERT INTO `int120u4m2s1s` (`id`, `user_id`, `u4m2s1q1`, `u4m2s1q2`, `u4m2s1q3`, `u4m2s1q4`, `u4m2s1q5`, `created_at`, `updated_at`, `u4m2s1q6`, `u4m2s1q7`, `u4m2s1q8`, `u4m2s1q9`, `u4m2s1q10`, `status`)
VALUES
	(1,9,'','','Id ad tempus bibendum arcu duis.Id ad tempus bibendum arcu duis.','Id ad tempus bibendum arcu duis.Id ad tempus bibendum arcu duis.','Id ad tempus bibendum arcu duis.Id ad tempus bibendum arcu duis.',NULL,NULL,'Id ad tempus bibendum arcu duis.Id ad tempus bibendum arcu duis.','Id ad tempus bibendum arcu duis.Id ad tempus bibendum arcu duis.','Id ad tempus bibendum arcu duis.','Id ad tempus bibendum arcu duis.','Id ad tempus bibendum arcu duis.',''),
	(2,9,'','','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,NULL,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Excepteur sint occaecat cupidatat non proident,','Excepteur sint occaecat cupidatat non proident,','Excepteur sint occaecat cupidatat non proident,',''),
	(3,6,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','','','',NULL,NULL,'','','','','',''),
	(4,6,'','','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"',NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"',''),
	(5,4,'Module 2 | Lesson Stages and Plans','Module 2 | Lesson Stages and Plans','','','',NULL,NULL,'','','','','','');

/*!40000 ALTER TABLE `int120u4m2s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u4m2s1t1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u4m2s1t1s`;

CREATE TABLE `int120u4m2s1t1s` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `u4m2s1q1` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q2` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `int120u4m2s1t1s` WRITE;
/*!40000 ALTER TABLE `int120u4m2s1t1s` DISABLE KEYS */;

INSERT INTO `int120u4m2s1t1s` (`id`, `user_id`, `u4m2s1q1`, `u4m2s1q2`, `created_at`, `updated_at`, `status`)
VALUES
	(1,9,'','',NULL,NULL,''),
	(2,9,'','',NULL,NULL,''),
	(3,6,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"',NULL,NULL,''),
	(4,6,'','',NULL,NULL,''),
	(5,4,'Module 2 | Lesson Stages and Plans','Module 2 | Lesson Stages and Plans',NULL,NULL,'');

/*!40000 ALTER TABLE `int120u4m2s1t1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u4m2s1t2s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u4m2s1t2s`;

CREATE TABLE `int120u4m2s1t2s` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `u4m2s1q3` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q4` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q5` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u4m2s1q6` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q7` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q8` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q9` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q10` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `int120u4m2s1t2s` WRITE;
/*!40000 ALTER TABLE `int120u4m2s1t2s` DISABLE KEYS */;

INSERT INTO `int120u4m2s1t2s` (`id`, `user_id`, `u4m2s1q3`, `u4m2s1q4`, `u4m2s1q5`, `created_at`, `updated_at`, `u4m2s1q6`, `u4m2s1q7`, `u4m2s1q8`, `u4m2s1q9`, `u4m2s1q10`, `status`)
VALUES
	(1,9,'Id ad tempus bibendum arcu duis.Id ad tempus bibendum arcu duis.','Id ad tempus bibendum arcu duis.Id ad tempus bibendum arcu duis.','Id ad tempus bibendum arcu duis.Id ad tempus bibendum arcu duis.',NULL,NULL,'Id ad tempus bibendum arcu duis.Id ad tempus bibendum arcu duis.','Id ad tempus bibendum arcu duis.Id ad tempus bibendum arcu duis.','Id ad tempus bibendum arcu duis.','Id ad tempus bibendum arcu duis.','Id ad tempus bibendum arcu duis.',''),
	(2,9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,NULL,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Excepteur sint occaecat cupidatat non proident,','Excepteur sint occaecat cupidatat non proident,','Excepteur sint occaecat cupidatat non proident,',''),
	(3,6,'','','',NULL,NULL,'','','','','',''),
	(4,6,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"',NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"',''),
	(5,4,'Module 2 | Lesson Stages and Plans','Module 2 | Lesson Stages and Plans','Module 2 | Lesson Stages and Plans',NULL,NULL,'Module 2 | Lesson Stages and Plans','Module 2 | Lesson Stages and Plans','Module 2 | Lesson Stages and Plans','Module 2 | Lesson Stages and Plans','Module 2 | Lesson Stages and Plans','');

/*!40000 ALTER TABLE `int120u4m2s1t2s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u5m1s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u5m1s1s`;

CREATE TABLE `int120u5m1s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `u5m1s1q1` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u5m1s1q2` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q3` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q4` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q5` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q6` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q7` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q8` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q9` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q10` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q11` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q12` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q13` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q14` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q15` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q16` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `int120u5m1s1s` WRITE;
/*!40000 ALTER TABLE `int120u5m1s1s` DISABLE KEYS */;

INSERT INTO `int120u5m1s1s` (`id`, `user_id`, `u5m1s1q1`, `created_at`, `updated_at`, `u5m1s1q2`, `u5m1s1q3`, `u5m1s1q4`, `u5m1s1q5`, `u5m1s1q6`, `u5m1s1q7`, `u5m1s1q8`, `u5m1s1q9`, `u5m1s1q10`, `u5m1s1q11`, `u5m1s1q12`, `u5m1s1q13`, `u5m1s1q14`, `u5m1s1q15`, `u5m1s1q16`, `status`)
VALUES
	(1,9,'',NULL,NULL,'things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','vthings that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.',''),
	(2,9,'',NULL,NULL,'things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','vthings that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.',''),
	(3,9,'this is a test input for u5m1s1',NULL,NULL,'','','','','','','','','','','','','','','',''),
	(4,9,'',NULL,NULL,'this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheckthis is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck',''),
	(5,4,'Unit 5 | Teaching Young Learners',NULL,NULL,'Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners',''),
	(6,4,'Unit 5 | Teaching Young Learners',NULL,NULL,'Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','Unit 5 | Teaching Young Learners','');

/*!40000 ALTER TABLE `int120u5m1s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table lesson_student
# ------------------------------------------------------------

DROP TABLE IF EXISTS `lesson_student`;

CREATE TABLE `lesson_student` (
  `lesson_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `lesson_student_lesson_id_foreign` (`lesson_id`),
  KEY `lesson_student_user_id_foreign` (`user_id`),
  CONSTRAINT `lesson_student_lesson_id_foreign` FOREIGN KEY (`lesson_id`) REFERENCES `lessons` (`id`) ON DELETE CASCADE,
  CONSTRAINT `lesson_student_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table lessons
# ------------------------------------------------------------

DROP TABLE IF EXISTS `lessons`;

CREATE TABLE `lessons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lesson_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_text` text COLLATE utf8mb4_unicode_ci,
  `full_text` text COLLATE utf8mb4_unicode_ci,
  `position` int(10) unsigned DEFAULT NULL,
  `free_lesson` tinyint(4) DEFAULT '0',
  `published` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `54419_596eedbb6686e` (`course_id`),
  KEY `lessons_deleted_at_index` (`deleted_at`),
  CONSTRAINT `54419_596eedbb6686e` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `lessons` WRITE;
/*!40000 ALTER TABLE `lessons` DISABLE KEYS */;

INSERT INTO `lessons` (`id`, `course_id`, `title`, `slug`, `lesson_image`, `short_text`, `full_text`, `position`, `free_lesson`, `published`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,'Dolor magni repellendus ipsum.','dolor-magni-repellendus-ipsum',NULL,'Quo unde eum et consequatur modi repellat nostrum iusto. Tempore adipisci ut doloribus eaque magni. In molestiae aut nemo eveniet ad quia et delectus. Quos et atque odit qui.','Consequatur eligendi accusantium nihil quia ut nostrum animi. Nihil maxime quidem officia ratione quasi aut. Doloribus quisquam natus distinctio accusamus.\nSed delectus qui delectus laboriosam earum eos. Ab magnam harum expedita et sit aut. Maxime quam similique quisquam. Nesciunt inventore et dolores harum quibusdam expedita ea.\nNihil iure ipsam vitae quos commodi provident repudiandae. Debitis est qui mollitia odit omnis minus. Aut corporis voluptatem blanditiis est quaerat debitis. Sint qui eum assumenda nihil voluptate. Error neque voluptatem rerum repudiandae.\nIste soluta quis nobis voluptatem perspiciatis fugiat. Qui porro est reprehenderit ea beatae. Aut vel possimus at id blanditiis debitis.\nConsequuntur dolore maxime est architecto. Illo maiores alias laboriosam quaerat eius. Quia sunt dolorum quaerat voluptatum officia. Fugiat animi labore perspiciatis.',7,1,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(2,1,'Officiis debitis iste eos accusantium est ut.','officiis-debitis-iste-eos-accusantium-est-ut',NULL,'Ex laborum aspernatur et alias corrupti necessitatibus. Reiciendis earum dolorem expedita. Itaque natus ducimus consequatur veritatis. Quibusdam et esse ut veniam.','Ea voluptatibus placeat inventore ut sed. Voluptatem molestias suscipit iste. Eum et et voluptas ut debitis quia temporibus. Et reiciendis molestias sapiente voluptas sunt deleniti.\nEaque iste recusandae cumque. Omnis ut ducimus qui atque. Et suscipit animi sed recusandae. Dolore laborum eius enim aut in.\nEt ex consequatur sed. Eos sed voluptas sunt harum similique ut. Sunt reiciendis porro tempore animi. Quia blanditiis quo ut ab est distinctio aliquam in.\nMaiores qui qui error odio sapiente enim. Nemo aut nisi ipsa consequatur sunt. Facilis soluta natus enim ut saepe libero. Corrupti qui quo explicabo dolor.\nQuidem et est ea alias dolor non assumenda. Deserunt in vel similique dolores quo dolorum voluptatem. Eaque voluptatum ipsum rerum dolore est labore. Esse et quia nemo dolor at voluptatem. Illo qui blanditiis quasi voluptatum maiores dolores cumque.',1,0,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(3,1,'Officia aperiam et aliquid.','officia-aperiam-et-aliquid',NULL,'Vero beatae adipisci possimus est sapiente. Id repellendus quaerat enim quod quos voluptas cupiditate libero. Laboriosam odit asperiores iste accusantium.','Alias corporis quae fugit sint. Doloremque qui rerum fugiat a expedita voluptates.\nQuia occaecati officiis perspiciatis est atque. Minima repellat est pariatur quo accusamus amet sint. Commodi sed ipsa asperiores soluta odio dolores ut.\nDucimus totam voluptatem non quos ut omnis non est. Similique reprehenderit molestiae velit sit dolor totam molestias.\nVeniam eligendi in eaque voluptates deleniti eos et. Quibusdam et unde optio. Perspiciatis consequatur quaerat asperiores inventore maxime numquam.\nQuam eius deleniti mollitia tempora explicabo dolores et nam. Impedit eos vel ut est molestiae. Ut illum natus non aut quia.\nVero possimus esse ad sint officia. Voluptates iusto explicabo recusandae accusamus accusantium qui quo. Perspiciatis occaecati sit magnam harum aut et.\nIpsa sequi maiores veniam alias quia et tempore. Commodi aut maiores eligendi soluta illo sit molestiae at.',6,1,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(4,1,'Harum quam quia nulla quis.','harum-quam-quia-nulla-quis',NULL,'Eligendi provident aut optio fuga blanditiis. Corporis aut et dolorem mollitia at incidunt. Quisquam est omnis quo molestias rerum. A vero id sunt quod nesciunt libero quaerat est. Laudantium et nobis sint qui laboriosam.','A totam officiis possimus non voluptates aliquid. Corrupti expedita dolorem id consequatur odio dolor. Nam est maiores nihil mollitia ipsum. Commodi et aut necessitatibus rerum consectetur quia libero iste.\nNulla id molestias eum modi cumque ut voluptatem. Autem ut repellendus sed quam est excepturi. Nihil quis maiores porro aperiam ut quisquam expedita.\nAnimi accusamus ex numquam vero. Ab non dolores quisquam consequatur doloremque minus. Nemo excepturi ex repellendus labore quod aut maxime.\nTempore doloribus perferendis inventore iure ut. Velit sint consectetur eos et tempora repellendus ut. Ut et ullam qui distinctio non. Fuga vero ipsum soluta enim.\nUt minima voluptates a ex. Sapiente mollitia iste non illum. Sit harum occaecati iste ut neque.\nRecusandae facilis dolore esse quo quos. Tenetur voluptatem tenetur est. Consequatur et fugiat quas omnis ea maiores qui. Dicta laborum expedita occaecati laborum similique adipisci.',10,0,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(5,1,'Odit sunt quis perspiciatis fuga repellendus.','odit-sunt-quis-perspiciatis-fuga-repellendus',NULL,'Rerum qui quia earum. Vitae consequuntur iure harum asperiores cum beatae voluptatem. Nisi placeat autem quia maxime illum non enim. Sint iste earum dolores illum ut.','Magnam placeat sequi praesentium id maiores corrupti. Sint mollitia in consequatur et fugiat asperiores. Sit reprehenderit animi tempora voluptates dolorum. Vel id qui ab hic et totam.\nNesciunt sapiente ab aut vel. In accusamus sequi ipsam qui est consequuntur. Veritatis aut eos ullam qui aut. Odit sed et minima facilis. Ut optio cupiditate ut illo exercitationem adipisci itaque.\nAb quia est eum quasi ea id. Facilis voluptatem placeat tempora consectetur dolorum unde quis. Aut non doloribus aspernatur dolore amet harum.\nUllam nobis molestiae expedita fugit illum. Enim quasi velit non optio consequatur et voluptas. Quos neque aut amet. Iusto id labore eaque aut hic quam error.\nDolore beatae ea sunt consequatur voluptatem. Voluptas velit modi enim expedita reprehenderit qui. Delectus eum debitis sint molestiae dolorem.\nEa natus incidunt non molestiae. Odio aliquid praesentium dolor. Quam et assumenda minus incidunt eligendi quod. Itaque neque consequatur sint enim optio animi.',2,0,0,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(6,1,'Quis perferendis et officiis necessitatibus.','quis-perferendis-et-officiis-necessitatibus',NULL,'Deleniti vel sunt neque veniam corrupti. Ipsa iusto enim itaque qui officiis. Molestiae consequuntur odit sunt impedit.','Sunt consequuntur vitae occaecati consequatur ut laboriosam voluptate. Eum quia non deserunt. Eius eum suscipit sed cupiditate. Laudantium et odit enim non dolores. Ut quia natus odio officiis voluptates omnis quia.\nDucimus reiciendis autem aut maiores expedita voluptatem. Amet temporibus molestiae eius maxime. A tenetur qui laborum repellat asperiores velit fugiat. Odio quae ipsa repellat voluptas iure ipsum tenetur.\nVoluptatem qui et itaque ut velit doloremque optio. Culpa impedit explicabo enim architecto. Quis officiis unde laborum.\nAdipisci corrupti esse velit occaecati repellendus. Et soluta repudiandae repellendus ab illo qui. Impedit accusamus voluptatem omnis tempore nemo. Totam reprehenderit cumque est est alias.\nLibero ut et consequatur qui possimus. Eum aspernatur consequatur inventore debitis ut autem. Sunt excepturi dolor qui ea placeat iste. Quis vel officiis eius dolorum. Itaque et quaerat consequatur quas velit quia sunt.',5,1,0,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(7,1,'Ea et enim nobis error minus alias nisi.','ea-et-enim-nobis-error-minus-alias-nisi',NULL,'Sit commodi mollitia sit voluptas consequatur. Doloremque enim rerum eum et.','Quia illo totam doloremque. Non deleniti necessitatibus voluptates ea perspiciatis odit. Laboriosam nihil accusamus accusamus aut inventore et et. Tempore voluptas repellendus expedita dolorem excepturi delectus velit minima.\nCumque odit et aperiam soluta minima. Veniam sunt ex sint aliquam qui quidem vel. Quibusdam ex amet ea maiores vitae nemo ipsum rem.\nRecusandae voluptas corporis atque odio illo eos. Nam doloribus sed qui eos voluptatibus assumenda. Assumenda ullam tempore dolorum enim sed suscipit quis.\nAut quibusdam quisquam neque. Iure quas deleniti totam quos modi. Labore sint ullam nihil assumenda explicabo. Quia ut saepe libero provident.\nMolestias qui repellat atque at suscipit vel. Est dignissimos cupiditate amet sunt quae vel. Aut corporis suscipit voluptate sed pariatur.\nEst dicta cumque nisi quaerat perferendis mollitia odit. Doloribus hic adipisci natus quas. Minus ut ullam illo eligendi aperiam laborum. Et odit odit sit unde veritatis velit aspernatur rerum.',9,1,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(8,1,'Doloremque corrupti quia velit.','doloremque-corrupti-quia-velit',NULL,'Et nam aut quibusdam et voluptas alias. Dolores exercitationem temporibus hic doloribus excepturi eum. Praesentium voluptatem eveniet dolores. Aut architecto deserunt libero quis amet esse qui.','Corporis est velit illo amet. Saepe natus consequatur sequi quasi ipsum eius corrupti modi. Labore earum iusto porro.\nSint qui quis at illum quo laboriosam in. Sapiente nihil quos nostrum numquam sed. Repellendus eos nam id et minus. Iusto explicabo voluptatem a aut eum.\nEx eligendi asperiores exercitationem veniam optio magnam sunt. Ratione veniam optio eos aut quo. Et quo minus error ex fugit.\nRepellat ratione est alias eos eaque eius. Doloremque accusamus perferendis aut consectetur facere perspiciatis. Cupiditate sunt animi sit quo voluptatem. Magni voluptatem ut possimus.\nEt molestiae qui expedita totam necessitatibus perferendis aut. Dolorem sequi deleniti facere rerum cum quae et. Pariatur sit voluptatem ut rerum.\nQui distinctio ipsam quibusdam vel excepturi. Velit vero odit impedit. Quis eum quos quam autem excepturi modi.',5,0,0,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(9,1,'Aut ut non facilis harum placeat.','aut-ut-non-facilis-harum-placeat',NULL,'Ea maxime deserunt sapiente ab tempore aut. Eveniet non adipisci omnis tempore maiores quo. Voluptatem sed eaque dolorem ea itaque animi. Sit porro dolores est delectus eum.','Deleniti quasi inventore nihil nihil aut. Ut rerum quia sed blanditiis minima. Atque praesentium repellat ad qui.\nSunt et quae cum et aliquam. Voluptas quam aspernatur qui accusamus. Eaque perferendis nam ipsa quis. Et praesentium ut molestiae explicabo ex qui numquam.\nCum nisi suscipit iste suscipit voluptatem. Nostrum laborum possimus sint animi inventore id porro corporis. Dignissimos culpa explicabo eos doloribus voluptates voluptas.\nIn sunt fugiat voluptatibus. Magni quia rerum minus soluta mollitia optio dignissimos. Cupiditate aut culpa nihil.\nMagnam odit et perspiciatis distinctio voluptatum recusandae possimus. Eaque ipsum aut velit nobis magni excepturi nisi. Aspernatur eos quibusdam aut ut fugit ipsam consectetur.\nFuga sit ipsum enim vel quia. Nisi quis aut nemo velit officiis. Maxime exercitationem fugiat sequi corrupti nihil aut odio iste. Est ex similique praesentium aut aspernatur pariatur sunt.',5,1,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(10,1,'Modi voluptas totam reprehenderit.','modi-voluptas-totam-reprehenderit',NULL,'Ea minus et laborum eveniet voluptatum earum et. Omnis voluptates est unde laudantium deserunt. Harum aut eos quae quia quis ut illo. Ipsum mollitia laboriosam et placeat.','Ratione doloremque et quidem. Et enim sit nam voluptas. Quia nisi voluptatum quam labore numquam vero est. Aut ut asperiores deleniti necessitatibus similique voluptas.\nVoluptate cum non sequi recusandae quis sed. Aut occaecati labore reiciendis tempore est sed. Recusandae facilis assumenda et maiores a cumque.\nUnde rem ab et iusto. Consequatur laudantium consequatur eveniet neque et et minima. Molestiae error deleniti delectus rerum fuga tempora animi. Maiores et nemo iste sit tenetur.\nNihil ad deserunt odit vel exercitationem eos voluptas. Iste itaque quod et. Laboriosam voluptates suscipit totam occaecati at.\nMinus rerum ipsam ipsa non harum. Voluptas neque laborum excepturi tempora voluptate tenetur.\nNemo est voluptates nihil perspiciatis rerum. Nisi et sunt amet illum quisquam. Quod veniam sunt facilis et.\nUnde vitae illo amet explicabo architecto blanditiis corrupti. Nihil praesentium libero et dolorem commodi sit impedit debitis. Ratione similique suscipit commodi sit et et.',6,1,0,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(11,2,'Ut quam nesciunt aperiam voluptas delectus esse.','ut-quam-nesciunt-aperiam-voluptas-delectus-esse',NULL,'Est mollitia expedita voluptate itaque cum laboriosam. Dolores amet qui est asperiores. Consequatur fugit in ut qui corporis aut modi id.','Similique illum aut nulla nulla. Nobis vitae molestiae natus atque quibusdam sed. Voluptatem ullam distinctio enim velit facere.\nNesciunt sapiente mollitia omnis pariatur et autem. Dignissimos eum tempora in at expedita praesentium veritatis. Autem laboriosam molestiae est eius sed voluptatem ad.\nQuis tempora laboriosam nihil non voluptate. Officia quia accusantium enim ratione. Suscipit quis blanditiis veniam est.\nSuscipit vel architecto aut et sit sed rerum. Et nam iste placeat nihil ipsum error. Fugiat eveniet eius sed quisquam fugit. Sit aut tenetur quo similique qui ut rerum.\nEx maiores et quaerat adipisci. Rerum fugit sint officiis et aspernatur. Sequi voluptatem hic saepe similique voluptatibus facilis. Est eveniet amet deleniti vitae vel fugiat recusandae. Quia est architecto occaecati nihil veritatis provident.',10,1,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(12,2,'Ut dolor quidem voluptatem sit esse laborum.','ut-dolor-quidem-voluptatem-sit-esse-laborum',NULL,'Est ut et et ad dolorem. Nemo necessitatibus sapiente quia illo qui et illo dolorum. Quisquam voluptatum repellat repellat sit molestias velit qui.','Sint quaerat quo tempore et eveniet quas. Asperiores ab ea aliquam nam.\nEa ad vel architecto maxime quae. Aliquam non et officiis enim voluptate. Distinctio qui et tenetur dicta error.\nEt saepe et velit quod voluptatem quia. Consectetur voluptatem eos provident aut. Non recusandae optio voluptas perferendis et debitis. Quia nihil voluptatibus et libero enim quos id. Veniam ut architecto consequatur quas vel.\nModi similique et sed autem quaerat fugiat eligendi. Veritatis molestias qui ea dolore. Aut fugit quo quisquam et. Dolorem illum minima ipsam laudantium sit ut. Sint fugiat libero veritatis.\nVeniam ipsum vel odio exercitationem iste. In voluptatem perspiciatis similique sed. Cum quia repudiandae nam. Numquam facere quis modi omnis officia.\nEt impedit sit unde id placeat facilis. Cupiditate ullam voluptatem officiis sint et in sint. Enim et corrupti officia voluptatem dolorem. Dolore doloribus et dolor commodi aliquid quasi aperiam.',4,0,0,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(13,2,'In omnis repellat veritatis possimus.','in-omnis-repellat-veritatis-possimus',NULL,'Laboriosam magni magni incidunt ut. Perspiciatis hic iste mollitia. Tempora sit iure molestiae sed quis.','Qui maxime repellat cum itaque. Rerum et non qui. Repellendus quo itaque quaerat porro et sunt. Qui dolor dolores neque cum voluptas enim quis.\nVitae quod tempora dolorem dolorem. Rerum excepturi sint est explicabo.\nNecessitatibus dicta sequi aut illo. Unde tempora nesciunt et quaerat debitis. Adipisci voluptate dolore et cupiditate aut.\nQui eum ex aut natus ipsam alias officia molestiae. Est voluptates doloremque ut tenetur aliquid. Dolore dolorem hic voluptatem illum.\nAb beatae ex provident nesciunt soluta non. Enim necessitatibus quod in consequatur debitis consequatur. Et maiores modi aut ad voluptate. Quam eum est sunt corrupti quibusdam sit facilis.\nEos nihil laborum accusamus magnam explicabo voluptatem. Est enim qui dolores ipsam ut consequatur odio sed. Exercitationem corrupti provident sunt aliquam non possimus quod.',7,0,0,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(14,2,'Cumque eum aliquid perferendis sit voluptas qui.','cumque-eum-aliquid-perferendis-sit-voluptas-qui',NULL,'Saepe quidem inventore ut ut quidem. Ipsam qui itaque repudiandae et dolor. Quis ut soluta sed et ex.','Et aut perferendis iste laudantium. Aut fuga in sed delectus nam quaerat. Non est mollitia consequatur similique praesentium qui magnam.\nNon totam tempore hic commodi. Consequatur qui aut aut aut est in nisi. Voluptatum tempora quibusdam accusamus et quaerat qui. Omnis facilis dolorem voluptas.\nReiciendis maiores harum commodi unde maxime. Dolor occaecati quos id et incidunt repellendus. Incidunt aut eum voluptatem adipisci optio neque. Nemo sunt enim vel recusandae mollitia.\nBlanditiis ut quas rerum numquam doloremque sint. Est voluptate ipsam corporis et modi quas voluptatem debitis. Odit dignissimos consequatur eum ratione ipsa aliquid.\nAdipisci eveniet est et unde. Illo minus doloribus fuga rerum non. Laborum debitis ut asperiores nihil blanditiis minima tempore. Dolorem et magnam saepe mollitia illo culpa dicta vitae.\nAliquid aut in voluptatibus impedit. Enim nisi aut voluptatem beatae atque.',3,0,0,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(15,2,'Voluptas pariatur voluptates sit non ab.','voluptas-pariatur-voluptates-sit-non-ab',NULL,'Voluptas aut ad fuga consequatur. Mollitia minima rem minima. Provident cumque quis magni alias expedita incidunt autem. Omnis quam corrupti inventore sint tempore sit eos id. Asperiores aliquid tempore numquam porro cumque rem hic eum.','Mollitia accusantium sed ad illum cupiditate sed. Sit aperiam consequatur sequi vitae quisquam cum aut. Ut doloribus ipsum perspiciatis ab. Dolorum dignissimos quam nostrum porro qui enim est.\nRepellat et sed voluptatem illo. Ex officiis debitis et voluptatem ad quasi et. Blanditiis recusandae ex nemo ad ut est. Repellendus sapiente tempora et ullam placeat.\nAut expedita eos maiores nisi et. Et aut perferendis ut adipisci. Commodi inventore autem voluptates delectus voluptatem qui. Quo et fugit et.\nOdio iure illo maxime velit eum. Vel quo sit et quod itaque error nam molestiae. Voluptatem distinctio consequatur dolores quidem est.\nSunt et nobis quae numquam soluta temporibus quasi. Velit quasi dolor reiciendis.\nRecusandae omnis architecto in. Reprehenderit unde aut iure unde officiis.\nEos autem quibusdam deserunt consectetur debitis. Et sint omnis officia expedita. In incidunt et rem necessitatibus beatae dolorum ad et. Repellat ut consectetur delectus voluptatem deserunt.',8,1,0,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(16,2,'Vel non doloremque dolor rem.','vel-non-doloremque-dolor-rem',NULL,'Eos iusto voluptatibus id ea atque atque. Non sequi nihil porro vel. Quod qui laboriosam sed illum. Molestias doloribus quis dolor vel ducimus.','Minima incidunt provident nostrum quibusdam in odit. Facilis nihil et voluptas inventore laudantium. Quia eius qui libero necessitatibus.\nConsequatur hic laboriosam non sit ut. Numquam eaque qui sint culpa consequatur assumenda. Non aliquid molestias ducimus iure beatae odio consequatur.\nDolorem ullam pariatur officia illo. Aliquid aut praesentium doloribus cumque. Qui nostrum aliquam nemo. Labore voluptatibus error ratione perspiciatis aut consequatur temporibus.\nEum eligendi fuga in sequi veniam repellendus voluptas. Illum non vero deserunt perspiciatis saepe. Omnis voluptas eos eaque.\nAut blanditiis consequatur eum. Et enim sit ut nisi non blanditiis. Aliquam consectetur et unde et hic vero.\nQuaerat accusamus perferendis iste nulla nulla repudiandae. Id animi sequi aut dignissimos velit voluptatibus. Quidem beatae ea qui autem aliquid ut sit.\nAut voluptatem ipsum dolor mollitia atque ex. Enim ex adipisci corrupti ut repellendus. Voluptas aspernatur hic aut consequuntur ipsam.',10,1,0,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(17,2,'Laudantium et nam in non animi eligendi.','laudantium-et-nam-in-non-animi-eligendi',NULL,'Natus qui ut qui nisi porro sit consequatur sit. Corrupti assumenda ipsa aut iure maiores. Atque eius repellendus architecto corporis dolor ut similique.','Fuga voluptas temporibus omnis sequi occaecati nostrum et. Ut et sed recusandae minus et earum ipsum.\nVoluptatum non sequi ut tempore quaerat quisquam rerum. Nulla odio vero nemo sint aut eius. Quidem facilis vitae quia sequi non repudiandae. Provident alias asperiores suscipit ipsam tempore molestias.\nPorro tenetur dolorem quo officiis voluptas. Debitis ab qui excepturi. Voluptates veniam quidem eum non odio laudantium. Sit accusantium quisquam incidunt cum.\nVeritatis odit accusamus ut maxime quod sint voluptatem. Sed quis nesciunt totam repellendus molestiae sint aliquid. Explicabo vel soluta non explicabo.\nQuo quaerat veniam rem consequatur ipsa quae quasi. Eveniet non perferendis at impedit deserunt harum facere. Atque nisi repellendus magni qui voluptatum porro. Commodi et aliquid in ab accusantium consequatur pariatur.',9,1,0,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(18,2,'Itaque aspernatur minus quidem commodi tenetur.','itaque-aspernatur-minus-quidem-commodi-tenetur',NULL,'Enim accusamus distinctio magnam. Velit quas enim vitae corporis blanditiis magnam doloremque. Sunt similique id nam soluta recusandae natus. Blanditiis deserunt eaque quo placeat itaque sunt doloribus fuga. Libero laudantium consequatur ad aut accusamus.','Itaque repellendus eius et ex illo temporibus. Ipsa repellendus laborum pariatur fugiat deserunt.\nEt rerum velit voluptatum dignissimos. Veniam exercitationem dolore quidem sunt similique. Facere blanditiis quo non inventore. Voluptate illo accusamus in illo dolorem dolorum.\nModi error id autem minima aperiam. Voluptate delectus sapiente ut debitis architecto quam. Eos totam excepturi ut et dolorem deserunt.\nFugit omnis maxime assumenda incidunt. Qui eaque nemo et deleniti. Aut est praesentium sint molestiae. Odio totam quo dicta necessitatibus porro nulla.\nVoluptatem quia ullam aliquid ipsa. Impedit tempora soluta libero cumque blanditiis commodi. Iusto dicta omnis architecto natus nostrum.\nQuia qui deserunt totam omnis labore autem. Laboriosam molestias fugit reprehenderit totam error quas eius aut. Ipsa unde in vel necessitatibus autem atque aut.',9,0,0,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(19,2,'Recusandae praesentium aut et cumque sed sed id.','recusandae-praesentium-aut-et-cumque-sed-sed-id',NULL,'Velit quae quia consequatur aut et rerum. Esse exercitationem maiores qui iure. Error quis commodi mollitia necessitatibus. Praesentium eum atque voluptatibus eaque. Asperiores voluptatem corrupti sit debitis.','Blanditiis rem et commodi et debitis at. Nihil odit eos esse omnis impedit fugiat molestiae. Vel velit est impedit tempore quia nesciunt et.\nPerspiciatis aliquid in porro architecto et vel quam. Incidunt quo odio perspiciatis et in. Totam ducimus similique aut molestiae.\nIpsam natus repellendus facilis minus tempore maiores maxime. Et voluptatem numquam ad optio rem. Impedit recusandae quae qui. Asperiores recusandae qui fuga quisquam tempore ratione a.\nSed veniam omnis voluptate dolorem corrupti vel nihil. Et qui deserunt voluptatibus maiores. Ipsam qui et maxime minus aut. Sed reiciendis et magni.\nRem cupiditate ut laboriosam atque et eos. Et quasi rem sequi consequatur facilis dolorum unde. Quo aut quae et. Id placeat quasi est quidem doloribus dolor id vel.\nCulpa soluta aut ea qui necessitatibus. Voluptatem veniam amet fugit quia et architecto. Maxime doloribus explicabo modi error. Id iure adipisci culpa sed eos et. Laudantium error ea vel a.',8,1,0,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(20,2,'Quasi necessitatibus est qui sequi.','quasi-necessitatibus-est-qui-sequi',NULL,'Quibusdam porro velit aut blanditiis in. Tenetur aperiam consequatur eum distinctio. Itaque et laboriosam eligendi placeat qui dolores.','Dolorum debitis nihil harum sequi officia rerum sint. Omnis incidunt accusamus consectetur velit qui quae. Aut nihil repellendus consequuntur iusto. Ut aliquid perferendis voluptates quia dolore voluptas est.\nEt qui eveniet fuga rerum id corrupti. Ut voluptatem fugit est consequatur est corrupti. Autem hic mollitia suscipit consequatur dicta optio.\nRem incidunt et blanditiis officia qui provident quam fuga. In voluptatibus esse quis reiciendis consectetur sunt ab et. Odit fugit in aut amet.\nUt exercitationem neque non quam dolor. Nobis accusantium eum commodi vel repellendus incidunt pariatur. Et eum adipisci sint.\nIllo molestias quisquam non et est qui. Quas veniam laudantium eius non assumenda sint. Aspernatur vel ut a non similique.\nReprehenderit aut culpa odit harum beatae omnis libero. Culpa minus at magni architecto pariatur porro. Dolorem illum in eligendi et. Occaecati quidem rerum quia sapiente eos possimus.',10,1,0,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(21,3,'Iste et illo fugiat debitis quis quae.','iste-et-illo-fugiat-debitis-quis-quae',NULL,'Qui itaque quaerat neque accusantium est quasi excepturi. Est quod inventore laborum quibusdam quis iste. Consequuntur id a ut eos. Ipsam qui excepturi voluptatum molestias qui explicabo.','Distinctio optio qui quia aspernatur. Eum consequatur facere quam. Ut voluptatem amet accusamus veniam. Saepe sunt similique velit. Voluptatem quia perferendis sequi.\nQuos aut accusantium a corporis repudiandae assumenda sapiente. Totam fuga et perferendis explicabo. Quas ratione vitae ad doloremque sequi modi distinctio maiores. Labore non necessitatibus consequatur at.\nQuo quisquam amet ab voluptate ut in. Ut necessitatibus et eius omnis dolorem possimus voluptatem. Id adipisci dolores ex non veniam omnis. Sapiente vero et placeat quis debitis inventore molestias aut. Nostrum esse dicta sint dolor dolores ratione hic.\nOptio sint odio at aut aliquam officia. Qui quibusdam recusandae similique quis consequatur a. Ut cum animi tenetur debitis quia nam. Praesentium iure iste quis provident hic quibusdam repellat. Repudiandae voluptatum voluptas eos facere.\nEx ea minima ut quia. Soluta repellat animi alias eaque nobis ut incidunt non. Rerum magni maiores doloribus.',7,0,0,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(22,3,'Tempora vitae temporibus ex dolorem sed.','tempora-vitae-temporibus-ex-dolorem-sed',NULL,'Ullam fugiat consequatur ea omnis. Molestiae sapiente dolor iusto sunt. Dolor sapiente molestiae molestiae nostrum culpa dignissimos sapiente consequuntur. Temporibus sed quia officiis qui eaque.','Unde et expedita voluptatum quos repellat. Molestiae ut vel provident distinctio provident dolor. Vitae veritatis magni animi non minima.\nVoluptatem ut qui nostrum omnis laudantium rerum. Culpa doloribus eos pariatur voluptatem. Totam incidunt omnis quas dolores. Sit exercitationem et facere quis consequatur quam est.\nAd tenetur est sapiente repellendus praesentium. Doloribus in fuga et.\nMinima modi voluptate eos excepturi qui. Temporibus eaque qui dolor asperiores est officia in. Non magni consequatur qui maxime saepe ex fugit.\nSapiente asperiores sit amet beatae vel at. Molestiae et blanditiis vel minima. Quo quam quas aut molestiae dicta vel. Dignissimos amet qui explicabo accusamus unde ad.\nNesciunt praesentium quaerat numquam cumque ut. Delectus eum dolores iste consequatur est totam nostrum. Eaque vel aut vel delectus id hic sapiente.\nQui qui assumenda esse pariatur blanditiis. Autem laboriosam magni quod praesentium consectetur. Iste a qui mollitia quia non voluptatem quasi.',10,1,0,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(23,3,'Et recusandae blanditiis quisquam id quia quo.','et-recusandae-blanditiis-quisquam-id-quia-quo',NULL,'Non odit deleniti beatae aspernatur sint. Quod aut voluptas dolore inventore non tempora sit. Ex asperiores omnis nostrum voluptas nesciunt. Aut modi impedit et et. Veniam possimus corrupti aliquam architecto vel aut debitis.','Mollitia cumque consectetur enim quos praesentium et earum ut. Dolorum ut non adipisci non dolores.\nEx corrupti mollitia ut voluptatem. Rerum id expedita sed sequi. Ab ex et magnam quia ab eveniet. Voluptatum omnis fuga temporibus est qui dignissimos.\nDicta voluptas voluptatem consequatur a non ut. Dolorem soluta cum aut quaerat. Quisquam eligendi dolor pariatur aut sint provident vitae corrupti.\nDolores est tempora sed voluptatibus et incidunt. Necessitatibus sed deleniti ut reprehenderit tempora soluta. Sint laboriosam et corporis accusantium sint.\nAdipisci est voluptatibus quaerat aut corporis aut. Magnam voluptatem quas blanditiis excepturi quo est. Magnam nobis veniam eos nostrum.\nRerum quisquam sed pariatur at eum quis. Et et eos ut facilis veritatis nihil. Suscipit esse qui dolorem. Rerum nostrum deleniti eos.',9,0,0,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(24,3,'Dolorum et asperiores et tempora voluptas.','dolorum-et-asperiores-et-tempora-voluptas',NULL,'Eaque incidunt id quia vitae fugiat. Voluptatem fugit fugit qui. Sed sed voluptas aut praesentium optio.','Omnis sunt sint fugiat doloremque rerum quas voluptates. Omnis assumenda illo unde deleniti. Qui enim dolorum aut et quis quo sed quas. Qui possimus dolorem non rerum saepe vitae alias.\nAd amet dolorum veritatis voluptatibus. Qui dolores harum voluptatem alias perspiciatis ipsa excepturi. Sapiente qui ducimus aliquam ex.\nOfficia temporibus ratione rerum tenetur enim atque. Aperiam reprehenderit et est perspiciatis laudantium. Occaecati omnis quo voluptatibus optio. Possimus ducimus dolores eius aut et optio.\nTotam aperiam quasi iusto et. Reprehenderit consequatur quo culpa laborum. Quam velit aut ea quidem libero nam. Earum explicabo qui sed quia et aut.\nAb debitis eos error ut nam quia illum. Minima quibusdam et ad pariatur earum aut totam. Ipsa est ipsam est quae non est ut.\nAut omnis placeat iure rerum. Qui voluptatem nostrum consequatur iusto. Officia et nemo cumque molestiae non non.',4,1,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(25,3,'Numquam iusto et incidunt qui.','numquam-iusto-et-incidunt-qui',NULL,'Impedit non odit enim quas. Aliquid et aut pariatur facilis molestiae. Qui et quia quibusdam cupiditate.','Itaque mollitia laudantium nulla molestiae dignissimos deleniti. Et veritatis quidem officiis nam nihil totam harum.\nVoluptatum quo similique quidem. Maiores est et nihil quaerat. Maiores dolor rerum consequuntur blanditiis dolor nihil soluta.\nTempora eius mollitia animi quae vitae minima dolore. Aut blanditiis incidunt earum delectus magnam quae. Et id qui iure doloremque. Aperiam tempore consequuntur eum omnis ipsa ad voluptatum.\nIn consectetur nam itaque soluta. Incidunt vel libero vel debitis ab. Laudantium non beatae velit est architecto corporis dolores.\nUt veniam eos et ut quaerat amet et. Beatae unde dolor maiores at. Illo asperiores veritatis incidunt voluptas in repellat.\nNisi quisquam libero voluptatem ratione sed totam aut. Tempora distinctio corrupti animi sed non nulla.\nDolor illum ut et rerum. Quis exercitationem facilis provident veniam qui corporis amet.',8,0,0,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(26,3,'Provident suscipit modi nulla sit.','provident-suscipit-modi-nulla-sit',NULL,'Fuga magnam tempora nisi. Odio corporis maxime eum autem non. Quia ut officia ratione hic perspiciatis facilis eum. At fugit recusandae magni non quasi deserunt aliquid.','Expedita voluptatum ea quis in est aut. Similique quae quidem odio ipsa est ab. Omnis est numquam eum aut voluptatum facilis ipsum. Qui quam quia et reprehenderit consectetur. Qui similique ut ad numquam odit et ut similique.\nConsequuntur ut qui dolore saepe rerum et voluptas. Quo autem qui doloremque in. Omnis quos eaque dolore voluptatibus consequuntur aliquid.\nQuia iusto cum sint odit culpa nulla. Ex quasi fugiat quia facilis. Voluptatem molestiae dignissimos aliquid voluptates facere et voluptas.\nIllo fugiat et autem quas quia. Sit rerum est voluptatem molestias. Veritatis in optio repellendus mollitia ratione sunt ullam. Consequatur voluptatem qui necessitatibus fugit.\nExcepturi molestiae culpa delectus autem amet. Aspernatur autem blanditiis eum. Debitis beatae esse quibusdam odio.\nSed neque corporis nemo omnis neque aut. Deserunt quos placeat aut quae earum nulla. Vel voluptatem ducimus quia velit dolorem enim. Quo dignissimos impedit porro vitae.',6,1,0,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(27,3,'Dolor amet temporibus odio at ut natus autem.','dolor-amet-temporibus-odio-at-ut-natus-autem',NULL,'Magnam asperiores a et. Neque debitis repellendus dolorem sunt nesciunt odit quisquam. Explicabo nihil rerum expedita nisi. Officiis veritatis est atque voluptas expedita. Consequatur a eos exercitationem dolores eligendi.','Ut voluptas totam fuga doloremque non. Dolorem omnis dolor et odio voluptas voluptatem qui maiores. Eos ipsam odit omnis iusto aut. Et ratione debitis officia fugiat.\nDolores vitae et exercitationem autem quisquam aperiam. Cupiditate corporis nihil est quidem et non sunt nihil. Doloremque occaecati deserunt optio id et rerum quis dolorem. Tenetur fugiat et assumenda ut modi quia.\nEst animi explicabo sint harum quo nobis vero. Sint animi accusantium consequatur reiciendis voluptatum reprehenderit nemo. Ipsum iste ratione et et et sint. Impedit autem magnam aut.\nAut sint quasi et velit. Accusamus enim autem numquam et aut debitis. Voluptas quisquam dignissimos consequatur odit.\nDolore sapiente perferendis est quibusdam aut non. Accusantium repellat hic soluta odio nihil molestias ut. Ut quibusdam maiores provident libero et. Eveniet officiis sed quia aliquid est.',3,1,0,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(28,3,'Rerum ea quod consequatur.','rerum-ea-quod-consequatur',NULL,'Rerum quibusdam mollitia qui tempora dolorum. Sed eum aut facere voluptates neque eius ut veritatis. Quaerat blanditiis distinctio aut illum.','Et iste in totam est sequi. Temporibus ut voluptatibus quaerat. Dolorem rerum eos dolor provident dignissimos illum et.\nQui non saepe sint rerum eum et quas. Facere ratione facilis est consectetur facilis perferendis. Ad et ut omnis temporibus ipsam qui ut. Qui eligendi delectus quibusdam illo.\nDeserunt quibusdam suscipit et quis et quia eum. Quia magni est voluptatem vel similique non rerum nam. Vero soluta at eum expedita nisi et voluptatem. Dolores rerum minima minus quod saepe odit et. Dolorem rerum delectus minima maxime.\nAliquam sint est ex ad id ducimus necessitatibus. Quia saepe odio laudantium eum qui optio. At et non odio error atque suscipit. Vel error numquam quis praesentium labore ea dignissimos. Totam qui ut voluptas officia corporis ea.\nQuam nisi omnis omnis quod voluptas a non. Sint officiis dolor ea iusto ullam non. Qui provident ut ad officia. Sint ad culpa optio assumenda blanditiis pariatur. Unde labore molestiae et sint.',6,1,0,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(29,3,'Et enim non modi consequuntur ab et.','et-enim-non-modi-consequuntur-ab-et',NULL,'Quis accusamus a similique dicta distinctio vel. Voluptatum eum aperiam voluptate ab eum. Ut sunt perferendis dolor in. Sequi commodi aut eum ullam in sit velit laboriosam.','Eos provident et quaerat excepturi. Molestias et amet aut expedita magni. Dicta quos et rerum beatae deleniti. Dolores aspernatur voluptatem sapiente fugit assumenda.\nEt est itaque est et perferendis voluptatibus sapiente dolores. Dolor autem labore et eaque incidunt qui esse accusantium. Quo praesentium cupiditate quis quos.\nExercitationem eos ex omnis ut veritatis enim libero. Optio soluta animi excepturi sequi. Sunt distinctio aut blanditiis non deserunt. Quae rerum voluptas quasi autem.\nNesciunt ratione possimus dolore voluptas temporibus ratione doloribus. Ut excepturi quibusdam tempore dolor ducimus autem. Aut alias laborum voluptates commodi omnis.\nAb soluta repudiandae ut voluptatem voluptatem quo. Pariatur voluptatem ipsam id molestiae. Sed tempora vel dolorem ab blanditiis neque quam. Sunt reprehenderit nesciunt in asperiores reprehenderit.',10,0,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(30,3,'Non est eius nulla magni numquam.','non-est-eius-nulla-magni-numquam',NULL,'Molestiae non ut ullam atque. Modi velit ad aut sequi deserunt rerum vero.','Corporis et qui consequatur cumque. Quas esse repellendus similique quia et quaerat aut. Quia perspiciatis velit sed ratione neque et. Et doloremque iure quasi consequuntur incidunt expedita.\nDicta eius quisquam incidunt amet consequatur at aliquam excepturi. Aliquam omnis eum adipisci quibusdam sed. Quia qui natus asperiores omnis sequi voluptatibus. Ut error accusantium eaque fugit dolorem beatae totam.\nAut tenetur asperiores eaque consequuntur. Autem odio assumenda sed ea nulla ut illum. Mollitia quis sed inventore error pariatur.\nOdit quaerat est voluptas velit. Voluptates consequatur molestias est consequatur. Architecto ducimus suscipit cupiditate nisi nihil officia impedit. Quis est et aperiam aut non amet et minima. Tempora veritatis optio dolore voluptatem nisi.\nOfficiis at voluptas quia necessitatibus expedita deleniti possimus. Sit ullam rem quibusdam non aut nostrum. Ut quibusdam necessitatibus qui laudantium iusto asperiores. Cumque praesentium et veritatis doloribus.',3,1,0,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(31,4,'Cum ex eos odit omnis neque sit consequatur.','cum-ex-eos-odit-omnis-neque-sit-consequatur',NULL,'In iure rerum omnis ea ipsam. Libero magni ut deleniti iste repellat. Magni sit hic tenetur aut asperiores eum quaerat.','Cum a est repellendus adipisci culpa. Incidunt natus error voluptatem quibusdam non eligendi quae. Quos odit nemo iure quos qui.\nLabore unde qui laboriosam expedita doloribus placeat. Consequatur quam suscipit expedita doloribus asperiores quisquam vel enim. Soluta beatae culpa similique sunt accusamus. Perferendis enim modi voluptate maxime iste aut harum.\nLaborum in enim nesciunt velit. Animi voluptates officia et est similique beatae. Consequuntur laborum cumque accusamus omnis dignissimos mollitia odit.\nQuisquam qui blanditiis molestiae laborum optio rerum consequatur. Quia omnis soluta tenetur. Voluptate ratione amet voluptatem totam error. Sit vitae ut commodi ut.\nNulla culpa ut nesciunt voluptatem aut nesciunt tempore. Minus et delectus enim voluptatum distinctio officiis aperiam velit. Labore neque dolorum non. Eos quia dolores autem ut commodi.',9,1,0,'2018-05-11 14:19:23','2018-05-11 14:19:24',NULL),
	(32,4,'Qui et voluptas provident dolorem et.','qui-et-voluptas-provident-dolorem-et',NULL,'Labore ut quo laborum dolor est vel. In sed culpa est eligendi velit hic voluptatem. Omnis eaque quo officiis tempore. Quasi impedit rerum delectus ut ea.','Exercitationem hic praesentium excepturi temporibus eaque. Omnis rerum et quos consequatur. Dolor est impedit laudantium et repellendus reprehenderit.\nTempore autem quis eum. Officia sequi qui consequatur. Est quidem ut odio quaerat tenetur. Consectetur quia est quaerat voluptatem ullam rerum consectetur consequuntur.\nBlanditiis dolor ipsum dolorum laborum provident. Exercitationem dicta ut ea voluptatem id culpa libero. Minus aliquid asperiores et aliquid impedit exercitationem. Quia non exercitationem impedit quia corrupti alias ipsam omnis.\nLibero doloremque sequi voluptas dolore vel voluptate. Velit magnam modi nihil dolorem.\nEum velit aliquid est odio. Autem dolores natus amet numquam. Temporibus ducimus odio in sunt.\nNeque quasi sint nobis. Et libero quis et. Esse aut et perspiciatis odio illum et.\nEaque voluptatem consectetur mollitia nam quasi rerum ipsum est. Natus iure et earum non tempore ipsam temporibus. Error inventore ut excepturi et. Aut quia possimus omnis mollitia.',5,1,0,'2018-05-11 14:19:23','2018-05-11 14:19:24',NULL),
	(33,4,'Voluptatem excepturi sunt ab aut a saepe sunt.','voluptatem-excepturi-sunt-ab-aut-a-saepe-sunt',NULL,'Sed corrupti nam et dignissimos id maxime fugit. Accusantium non reiciendis ipsam aut qui. Doloribus veniam quis quo qui et voluptatem. Dolorem corporis velit voluptas autem est.','Earum et sed non voluptatibus facilis. Adipisci non ipsum placeat natus enim voluptatum. Aspernatur qui officiis non facere iure. Iure et voluptatem sed beatae nulla possimus quis ut.\nOfficia non sit laboriosam quo ab non asperiores. Amet est omnis illum. Doloribus sunt aut aliquid in aut voluptas earum.\nSoluta molestiae eum temporibus sed reiciendis. Veritatis iure odit vel numquam. Voluptates eos necessitatibus optio quia dignissimos omnis. Consectetur et voluptate ratione modi voluptas omnis.\nEt reprehenderit est maiores maxime est illo omnis. Consectetur quis suscipit laboriosam atque maiores corporis quia et. Modi eveniet iusto corporis rerum. Doloremque veritatis illum ut corrupti beatae.\nDolore voluptatum non similique magnam adipisci voluptatem. Est enim sed esse quibusdam qui. Quaerat impedit vitae rerum nesciunt.\nCulpa aut quasi doloremque dolore commodi. Similique quas ullam nulla veritatis repellat ut ullam possimus. Magnam beatae sequi sit et minus nam ut aperiam.',8,0,1,'2018-05-11 14:19:23','2018-05-11 14:19:24',NULL),
	(34,4,'Eligendi molestiae ut est fugit.','eligendi-molestiae-ut-est-fugit',NULL,'Doloribus voluptates vel maxime delectus ut quidem. Animi velit odit repellat voluptatem in vel neque distinctio. Ullam autem id voluptas asperiores qui. Quo id officia cumque optio.','Molestiae officiis ducimus nobis. At nam omnis rerum laborum minus omnis. Cumque omnis eveniet cum minus. Et voluptas cum nulla beatae pariatur.\nUt magni ut sint omnis iusto suscipit. Culpa cumque quos quia placeat quod voluptates. Impedit inventore doloremque cum expedita.\nEarum rerum saepe veritatis sit exercitationem magni inventore unde. Repudiandae harum laborum aut impedit labore officia. Repellat alias voluptatum a dolor molestiae ut. Quam quibusdam sed impedit eum. In incidunt tempore vel sit et eaque.\nEius possimus ab magnam quia facilis. Ratione voluptatem officia doloribus eligendi consequuntur eius. In consequatur hic quos beatae voluptate minus. Ut dolorum dolorem sunt reiciendis reiciendis.\nVoluptatem veniam molestiae reiciendis tempore voluptas explicabo et. Eaque autem non dolor aut accusantium nemo est. Adipisci ullam et autem voluptatem rerum numquam ipsa.',3,0,1,'2018-05-11 14:19:23','2018-05-11 14:19:24',NULL),
	(35,4,'Ut odio veniam et deleniti dignissimos quos enim.','ut-odio-veniam-et-deleniti-dignissimos-quos-enim',NULL,'Quibusdam non et nihil voluptates eligendi. Cupiditate eos consequatur a omnis laboriosam dolores. Et dicta dignissimos qui ut provident rem ducimus.','Qui assumenda nostrum voluptatem omnis quis ut ipsam odit. Non animi earum natus quia quae necessitatibus. Consequatur quos nobis doloremque aut. Sunt consectetur nam hic facere qui. Fugit velit qui necessitatibus ut quia.\nTotam fugiat eos velit ut dolor et consequatur. Eaque nobis dolores consequuntur.\nEa sed suscipit illo et saepe asperiores. Dolores consequuntur ea id at iste. Consequatur voluptate dolores id esse molestiae eum a.\nUt repellat quasi reprehenderit eligendi quia enim accusantium. Et consectetur officiis commodi atque voluptatem nam perferendis. Quia voluptatem in earum placeat nihil. Deserunt eos unde ullam laboriosam saepe eveniet.\nAut corporis dolorem quam est asperiores officiis. Ipsa tenetur ut quidem qui id quia. Cum quia aut cupiditate repellendus perferendis voluptas qui.\nNobis quibusdam dolores repellendus occaecati hic. Doloribus architecto omnis et et quod architecto neque. Cumque autem voluptates at placeat et et et.',9,1,0,'2018-05-11 14:19:23','2018-05-11 14:19:24',NULL),
	(36,4,'Et veniam quam dolorum consectetur.','et-veniam-quam-dolorum-consectetur',NULL,'Aliquam eius hic incidunt cupiditate. Aliquam dolores quos autem rerum vero repellat sunt. Expedita ullam fugiat a rerum autem.','Sint fuga eligendi maiores. Culpa est ipsam laudantium dolore nemo similique. Consequatur voluptatem et quas.\nPorro autem expedita aliquam et aspernatur. Non voluptas sit sint. Fugit non ratione error repellat eaque unde cupiditate.\nAut rerum eaque qui similique qui consequatur. Dolores blanditiis illum ut ut eius. Laboriosam placeat doloribus non neque. Id molestias consequatur aliquam non dolorum cupiditate.\nSit minima rem deleniti consequatur pariatur repudiandae. Praesentium enim deleniti cupiditate neque itaque accusamus voluptatem nemo. Dolorem sequi pariatur eius et occaecati harum quia corporis. Itaque aliquam eum qui.\nExplicabo architecto deleniti voluptate perferendis sed. Totam id id non consectetur sit doloribus. Veritatis porro accusamus dolor dolores.\nAdipisci quis enim nobis natus doloribus officia. Sunt odio tempore perferendis eum maxime sit. Quae voluptatem sed omnis nemo ipsam esse. Nulla et vel rem et voluptatem et tenetur.',7,1,0,'2018-05-11 14:19:23','2018-05-11 14:19:24',NULL),
	(37,4,'Itaque rerum non facilis natus.','itaque-rerum-non-facilis-natus',NULL,'Repellendus rerum et voluptates neque delectus veniam qui. Ab provident quaerat et veritatis et officiis asperiores voluptatem. Dolorem ipsam aut quisquam laudantium consequatur et. Aut autem quod modi inventore.','Molestiae rerum dolorum earum alias sunt nesciunt. Numquam a corrupti inventore dolor et quia. Minus nostrum cum omnis et cumque voluptatem. Odit tenetur est sint dolorem autem et aliquam.\nSapiente molestiae officiis hic iusto consequatur. Facere magnam quos excepturi incidunt. Soluta eum quo molestias ut voluptas. Praesentium voluptatibus labore eum ullam eum qui velit consequatur. Magnam voluptatem sit quia a accusamus numquam velit quaerat.\nAut quis non nisi amet harum dolor laborum ullam. Sit illum aliquam veniam reiciendis officiis. Amet magni sit ullam qui ipsum minus. Sint quis sit excepturi voluptas dignissimos corporis et.\nEt nam molestias esse nostrum veniam non enim. Assumenda quas quod quod dolor porro est. Reprehenderit iure et quia et. Non qui quos molestiae saepe veniam.\nVero autem perferendis explicabo ad eum laboriosam deleniti. Illum excepturi asperiores incidunt dolores omnis quia. Odio velit ut quia at qui quae.',7,0,0,'2018-05-11 14:19:23','2018-05-11 14:19:24',NULL),
	(38,4,'Sint quibusdam quibusdam laborum.','sint-quibusdam-quibusdam-laborum',NULL,'Et tempora quibusdam enim molestiae expedita. Similique rem dolores odit.','Iste quasi qui adipisci dicta iure voluptatem occaecati. Est voluptas quo assumenda autem quibusdam. Quos soluta hic ut consequatur et rerum qui. Sed et fuga itaque et porro. Dolores dignissimos eum quo.\nExcepturi autem maxime velit sit qui repudiandae. Officia eligendi quia inventore sequi. Dolorem recusandae voluptatem delectus qui. Sed sit laboriosam expedita neque aut delectus dicta.\nEt autem perspiciatis culpa. A ab consectetur beatae quis. Esse molestiae dolores molestiae quo dolorum quos. Omnis voluptates laudantium ea porro laborum et labore.\nQuibusdam soluta amet expedita labore dolorum tenetur delectus. Rem consequuntur velit veniam consectetur. Nihil vitae aut sunt quae nostrum. Sint rerum esse corporis explicabo deleniti totam esse et.\nEt hic natus quam quis quia consequatur corrupti. Libero voluptatibus neque officia provident. Natus voluptates non tempora occaecati magni laudantium omnis.',3,0,0,'2018-05-11 14:19:23','2018-05-11 14:19:24',NULL),
	(39,4,'At est aut qui rerum sapiente.','at-est-aut-qui-rerum-sapiente',NULL,'Qui exercitationem fugiat veniam laboriosam et accusamus dolores. Maxime natus porro quaerat vel. Quia eos ipsa voluptas dolores est. Hic ullam recusandae tempora adipisci molestiae.','Nihil et quasi perferendis fugit vel fuga. Facere et minus sint ipsum. Beatae nesciunt est quo aut doloremque. Ipsa iure est magnam ratione est. Voluptatem eum unde est doloribus architecto ex iusto.\nDolorem voluptas qui est qui nulla sit. Repellat itaque velit reprehenderit. Sit ipsa libero quis. Architecto adipisci quis dolor doloribus omnis sit ducimus.\nDolore quo est quia impedit. Placeat occaecati eum aut est sequi ab repudiandae. Sequi ducimus commodi debitis similique esse quidem illum aut. Ad eius et omnis asperiores nobis. Fugiat necessitatibus asperiores nam ut provident corporis.\nFugit harum doloribus dolorem mollitia voluptates enim non. Hic quidem necessitatibus molestias consectetur omnis repellendus expedita minus.\nNecessitatibus asperiores impedit voluptatum doloremque aut sed dignissimos. Quos sint soluta sunt voluptatibus quibusdam quae placeat. Ipsa perferendis id sed sint perspiciatis. Ipsam ea dignissimos quisquam a quia.',6,0,1,'2018-05-11 14:19:23','2018-05-11 14:19:24',NULL),
	(40,4,'Placeat placeat et vero est corrupti impedit.','placeat-placeat-et-vero-est-corrupti-impedit',NULL,'Officia placeat perferendis quae aperiam. Pariatur officia ullam voluptatem cumque molestias. Minus repellendus omnis culpa et quae et. Enim perspiciatis voluptatem ipsum. Eum nobis odio provident ab aliquam.','Velit et nisi dolore quia dolorem quis eos. Occaecati earum et dolor autem quibusdam aliquam libero.\nIn officiis qui saepe labore. Unde officia nemo repellat. Facilis et consequuntur qui dolor optio. Quos est fugiat eveniet deleniti quis.\nRepudiandae nihil quo et vitae omnis qui. Necessitatibus modi non quos ut cumque laboriosam. Voluptatibus occaecati omnis sunt beatae.\nUt aut qui enim similique recusandae. Totam vitae dolor sed vitae. Voluptas in eos expedita porro sit dolores assumenda. Accusamus velit excepturi nihil dignissimos voluptatem excepturi.\nEst consequatur quod voluptatibus et illum ex. Omnis aliquid consequatur autem eum.\nNumquam est excepturi consequatur aut et inventore delectus provident. Tempora non molestias sed dicta. Est laudantium eum ipsa qui ut natus voluptas. Tempore in ea aut deleniti in. Tempore repellat aut quod voluptatem.\nA natus quo est quas et qui eos. Placeat eum eum temporibus autem. Expedita est voluptatum atque voluptates quia repudiandae cum.',2,1,0,'2018-05-11 14:19:23','2018-05-11 14:19:24',NULL),
	(41,5,'Amet temporibus facere atque est dolore.','amet-temporibus-facere-atque-est-dolore',NULL,'Maxime velit laboriosam et. Ut itaque quo sit ut enim. Voluptatibus eos consequuntur voluptas voluptatem.','Similique aut cumque nemo reprehenderit officiis. Sit rerum laboriosam consectetur laborum eos rem consequuntur. Sint natus nisi quia facere.\nMinus facere qui rerum facilis id. Nemo quidem veritatis dicta voluptatem modi. Omnis ea aspernatur corrupti officia quia.\nMagnam voluptatem dolores autem voluptatibus non. Ipsam mollitia magni sed in dolores ex. Quia nihil qui voluptas necessitatibus occaecati ut. Adipisci vero ex qui occaecati facilis est consequuntur ex.\nSunt nisi ea impedit rerum ea amet sunt iste. Ipsum et tempora quaerat consequatur. Officia ratione animi cupiditate nobis eum amet. In aliquid autem placeat aut aut. Porro et autem aut est incidunt est.\nAd cumque sed non laborum odit minima. Quos architecto praesentium sapiente ex iusto. Tempora quasi eveniet voluptatibus consectetur sit nobis est tempora. Voluptas numquam ipsam repellendus impedit velit qui commodi.',8,1,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(42,5,'Est dolores dignissimos ut ea ut.','est-dolores-dignissimos-ut-ea-ut',NULL,'Recusandae cumque quasi excepturi. Dolorem in aliquid non. Nobis deserunt sequi et illo voluptatem voluptate.','Rerum sit velit ut fugit sint earum molestiae. Ab optio sit consequuntur ipsa ut perferendis voluptate. Sint incidunt quisquam tempore dolores. Ea tenetur aut similique debitis assumenda incidunt.\nLabore est quasi autem eos cum. Rerum nihil id debitis quibusdam eum. Vel qui iure cumque dolorum cupiditate excepturi veritatis. Ut consequatur optio et.\nUt distinctio quae aperiam sapiente fuga. Expedita quas voluptas est error est occaecati voluptates vitae. Eius possimus incidunt adipisci nobis. Voluptate mollitia quis suscipit ut.\nAspernatur asperiores iure in architecto. Eos necessitatibus atque quia fugiat et id minus. Consequatur voluptatem fugiat rem culpa maxime consequatur. In libero molestiae quia quo. Ut non sed corrupti veritatis qui iste.\nRepellat odio est eos officiis qui iure. Blanditiis adipisci ut reiciendis aspernatur. Nesciunt dolor inventore voluptatem laboriosam non molestias. Adipisci non eos voluptas harum ad sed doloremque.',3,0,0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(43,5,'Iste officia est iste molestias corrupti.','iste-officia-est-iste-molestias-corrupti',NULL,'Maiores est aut sed rem. Nihil rerum delectus sequi eos occaecati ipsam quae quo. Quam unde accusamus odit nostrum.','Aliquam dolores soluta porro. Quia dolores magni ratione dicta. Consequatur suscipit quia autem asperiores enim.\nVoluptatem autem pariatur reiciendis in facilis assumenda itaque. Doloremque et excepturi sunt maiores perferendis rerum rerum qui.\nHarum assumenda voluptatem debitis qui. Minima fugit quod quaerat repellat quam dicta. Fugit consequatur et vero architecto placeat maiores aut.\nEt ducimus est aut exercitationem. Ea cumque totam consequuntur sit reiciendis.\nUt alias qui sint amet doloribus. Perferendis sed neque sit. Non porro optio ut quia. Quia et rem illum provident autem eos rem.\nNulla qui rerum corporis maiores. Aperiam quisquam eos ipsa dolor dolorem molestias et porro. Cumque ipsam laudantium impedit omnis. Voluptas sit cupiditate totam sapiente eos.\nCulpa labore et voluptatem voluptatibus quod eius laboriosam. Voluptas quas velit delectus. Qui dolorum est aliquid perspiciatis. Et a rerum ut eos sit nisi quo.',10,0,0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(44,5,'Aut sed provident sint sed earum.','aut-sed-provident-sint-sed-earum',NULL,'Provident ipsum aut esse cum. Neque magni voluptatibus quidem quos iure veritatis. Quo dolores velit dolorem repudiandae et delectus doloremque.','Eum explicabo necessitatibus quo at eligendi. Quas vel voluptas iste qui et. Cumque sit voluptates earum consequatur perspiciatis aliquid.\nNostrum harum ut nobis reprehenderit atque nisi numquam. Voluptas sed sunt eos distinctio sint modi vero. Iure delectus eos magnam qui. Voluptas aut quo saepe adipisci perferendis.\nBlanditiis sit dignissimos unde quis voluptates possimus. Ut id voluptatibus architecto porro ut. Eum voluptatem praesentium consequatur voluptate omnis qui.\nVelit sint necessitatibus animi qui dolorem labore. Adipisci atque eius quam porro debitis rem. Et non pariatur sed adipisci voluptatem delectus esse. Qui et numquam reiciendis nesciunt eveniet placeat.\nVoluptatem atque id eligendi esse placeat. Deserunt consequatur expedita reprehenderit voluptas quia omnis.\nEt sunt quasi expedita reprehenderit. Enim reprehenderit ab itaque. Culpa eum reprehenderit maxime aliquam dolores quia. Perferendis eveniet enim explicabo repudiandae sapiente recusandae ut.',10,0,0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(45,5,'Nemo officia inventore veritatis modi.','nemo-officia-inventore-veritatis-modi',NULL,'Sed omnis eos repellendus voluptatem distinctio quis veritatis. Accusantium sint esse enim velit nisi. Placeat quisquam totam facere facilis odit. Beatae vitae quidem officiis perspiciatis.','Voluptas ea ut aut omnis officiis possimus consequatur. Numquam maxime explicabo dicta harum vitae atque. Est impedit incidunt quam aut et totam. Rerum a voluptatem fugit consectetur cum provident voluptas.\nDeserunt consequatur iusto exercitationem dolore impedit eius commodi aut. Perferendis nobis corrupti soluta provident neque qui. Deserunt reprehenderit illum tempore veritatis. Id debitis aut qui minima.\nVeniam sapiente impedit repellendus voluptatem repellendus. Doloremque fugiat consectetur consequatur laborum ipsa. Autem tempora sed a repellendus. Ipsum sequi sit nihil nostrum maiores.\nEt explicabo non nihil non sapiente. Quis optio deleniti corrupti beatae. Iste necessitatibus et qui ea sed maiores fuga. Laboriosam sint nisi ut quia.\nOdit eos odio placeat id. Iure nam est recusandae porro. Hic iste voluptas voluptas quia. Ut aut voluptatem non et qui et maiores.',6,0,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(46,5,'Quo cum dicta velit repudiandae voluptas.','quo-cum-dicta-velit-repudiandae-voluptas',NULL,'Repellendus cupiditate commodi voluptatem. Tempore fuga et nihil quas. Et voluptates enim necessitatibus consectetur repellendus aut reprehenderit. Vel vel sit quo optio adipisci sint est.','Voluptatem dolorem rem vero delectus tempora est. Quos similique itaque sit quis magnam. Laboriosam velit optio voluptas quibusdam culpa. Et ut quia ullam est animi neque.\nQui est odio fugiat reiciendis minus voluptas maxime. Dicta quisquam quaerat quae doloribus aut. Animi sunt eos omnis deserunt ut velit aliquid dolore.\nOmnis aut minus perspiciatis dolor eum et delectus. Perspiciatis ut et praesentium voluptates.\nSint sunt dolorum non et. Pariatur dolores nobis quia voluptates ut eum eum. Modi ut aperiam asperiores ut.\nVel natus aut dolore quaerat laudantium doloribus. Minima itaque eos illo accusamus qui ducimus odit. Expedita delectus praesentium omnis aut in.\nNatus a quas veritatis. Non sit vel tempore error. Nihil rerum et modi porro cumque necessitatibus voluptatibus. Et facilis vel dolor ipsa.\nRecusandae occaecati inventore enim omnis. Non neque qui quis quam repellat ullam maxime. Itaque perferendis eveniet quod consequatur iusto.',6,1,0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(47,5,'Perferendis tenetur facere dolores quos expedita.','perferendis-tenetur-facere-dolores-quos-expedita',NULL,'Consequatur laboriosam quia voluptatem omnis quia. Sed voluptas laudantium possimus dolorum facere. Blanditiis sapiente suscipit recusandae a fuga eius doloribus quia. Sit eaque sint enim minima.','Aspernatur illo eaque ut vel. Sed asperiores aspernatur saepe inventore voluptatem adipisci maiores. Et atque molestiae nihil veritatis voluptatum. Explicabo sit quae sit ea in atque.\nAut eaque culpa et non quam quasi. Magni quod voluptatibus veniam totam excepturi ducimus. Voluptatibus suscipit sed id aut neque sit eligendi. Sed dignissimos aut quibusdam aut sit delectus.\nA doloremque non ea accusantium. Porro quidem nobis nihil fugit ipsa. In nam id et incidunt illo quia mollitia aut. Occaecati consequatur nostrum dolores libero.\nSoluta commodi corporis molestiae dolor aliquid voluptatem optio. Quas quidem consequuntur sint velit sed magni dolor. Quas voluptas optio voluptatem nulla rerum. Molestias quod voluptatem totam.\nTenetur culpa quisquam voluptatem adipisci dolores culpa voluptas natus. Porro consectetur et qui. Non qui consequatur laboriosam.',3,1,0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(48,5,'Modi ipsam vel minus sequi quia mollitia dolorum.','modi-ipsam-vel-minus-sequi-quia-mollitia-dolorum',NULL,'Non tempore odit ipsum rerum. Suscipit accusamus vitae mollitia molestiae doloribus. Dolores odio incidunt tenetur ea quas dolorum.','Quibusdam totam omnis vel assumenda. Ratione dolores necessitatibus incidunt. Et nam mollitia corporis sapiente est.\nOdit mollitia illo et aperiam iusto. Laborum suscipit asperiores sit libero voluptatem modi.\nIusto fugiat vero nulla ea id est. Voluptatem optio delectus ipsum quisquam. Sit dignissimos optio et est aut rerum eos earum.\nFugit vero illo aperiam et. Temporibus perferendis illo aut totam quisquam dicta aut.\nExpedita possimus dolorum voluptas quis et velit officiis. Tempore rerum dolore magni optio qui asperiores aut. Sapiente rerum dignissimos recusandae tempore. Adipisci veritatis tempore officiis hic deserunt facere.\nHic molestias et ab minus. Enim velit vel vel minus. Iure ipsum possimus est dolor. Eius neque quis libero ut blanditiis possimus.\nCulpa distinctio ipsa dolorem voluptatum rerum officiis nihil voluptas. Ut autem delectus illo libero. Eveniet vel labore qui eos sed modi voluptatem. Quis tempora eveniet sed porro magnam eum.',2,0,0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(49,5,'Iure saepe saepe aliquid optio et laboriosam.','iure-saepe-saepe-aliquid-optio-et-laboriosam',NULL,'Odio praesentium ut iste ut dolorem minus vel. In sint quas vel corrupti nihil aut. Sint nostrum itaque possimus maxime. Ut repellendus doloribus totam.','Eaque quis voluptatem voluptas dolor. Distinctio facere id aut exercitationem veritatis. Incidunt sapiente neque ut expedita. Veritatis quo omnis voluptas impedit aut ex deleniti. Eveniet et modi magni.\nAlias commodi debitis omnis et et qui ab. Aut nihil unde ullam provident aut ipsam rem. Reprehenderit beatae et nisi praesentium voluptas.\nPossimus cum quia est optio. Voluptatum veniam ratione tempora ut tempora dolor. Eos deleniti neque quia et. Ratione enim totam aliquid.\nRatione atque ut consequatur aliquam tenetur officiis. Libero natus laudantium ducimus aut. Iure dolores assumenda ea assumenda beatae id quia. Et in molestias ut aut optio.\nHarum repellat veritatis sit dolorum voluptatibus nesciunt veritatis excepturi. Esse laborum quos aut ratione ut molestiae officia laudantium. Repellat quibusdam odio corrupti impedit. Non voluptatem rem exercitationem saepe provident quisquam error qui. Atque sed facere modi deleniti maiores recusandae.',7,1,0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(50,5,'Dignissimos esse porro totam totam.','dignissimos-esse-porro-totam-totam',NULL,'Id illum enim cupiditate beatae aut. Illum quis quasi maxime libero veritatis voluptatem. Aspernatur repellat dignissimos atque sit odit. Et id itaque ex nisi est deleniti est in.','In non provident et et autem cumque fugiat. Sunt temporibus doloribus quia cum amet. Et suscipit nihil perferendis velit dolorem non voluptas consequatur. Tempore qui eos voluptates illum corrupti autem voluptate.\nSed suscipit totam voluptates quia. Voluptas at sapiente necessitatibus. Qui velit exercitationem laborum aspernatur quaerat. Et sed nisi consequatur aut qui.\nAd ipsum ut sapiente adipisci voluptatem delectus. Alias maxime sequi similique quas illo blanditiis. Ut praesentium delectus velit placeat sunt est. Voluptates impedit velit cum ducimus expedita ut non.\nMagni animi alias iusto et. Harum numquam dolorum officia nobis et voluptatem inventore. Eum dolorem placeat ipsa iste.\nAlias ea doloribus necessitatibus. Corrupti nihil corporis earum reprehenderit rem ipsum. Et iste vel culpa. Quis provident sit perferendis ea. Aut occaecati vero dolores et.',8,0,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL);

/*!40000 ALTER TABLE `lessons` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table logins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `logins`;

CREATE TABLE `logins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lid` int(11) NOT NULL,
  `trid` int(11) NOT NULL,
  `user` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pwd` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login` datetime NOT NULL,
  `level` int(11) NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table media
# ------------------------------------------------------------

DROP TABLE IF EXISTS `media`;

CREATE TABLE `media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model_id` int(10) unsigned DEFAULT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collection_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `disk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) unsigned NOT NULL,
  `manipulations` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_properties` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_column` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table messagings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `messagings`;

CREATE TABLE `messagings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `tid` int(11) NOT NULL,
  `trid` int(11) NOT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mdate` date NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(53,'2014_10_12_100000_create_password_resets_table',1),
	(54,'2017_07_19_082005_create_1500441605_permissions_table',1),
	(55,'2017_07_19_082006_create_1500441606_roles_table',1),
	(56,'2017_07_19_082009_create_1500441609_users_table',1),
	(57,'2017_07_19_082347_create_1500441827_courses_table',1),
	(58,'2017_07_19_082723_create_1500442043_lessons_table',1),
	(59,'2017_07_19_082724_create_media_table',1),
	(60,'2017_07_19_082929_create_1500442169_questions_table',1),
	(61,'2017_07_19_083047_create_1500442247_questions_options_table',1),
	(62,'2017_07_19_083236_create_1500442356_tests_table',1),
	(63,'2017_07_19_120427_create_596eec08307cd_permission_role_table',1),
	(64,'2017_07_19_120430_create_596eec0af366b_role_user_table',1),
	(65,'2017_07_19_120808_create_596eece522a6e_course_user_table',1),
	(66,'2017_07_19_121657_create_596eeef709839_question_test_table',1),
	(67,'2017_08_14_085956_create_course_students_table',1),
	(68,'2017_08_17_051131_create_tests_results_table',1),
	(69,'2017_08_17_051254_create_tests_results_answers_table',1),
	(70,'2017_08_18_054622_create_lesson_student_table',1),
	(71,'2017_08_18_060324_add_rating_to_course_student_table',1),
	(72,'2018_05_11_094511_create_canneds_table',1),
	(73,'2018_05_11_094538_create_logins_table',1),
	(74,'2018_05_11_094552_create_messagings_table',1),
	(75,'2018_05_11_094617_create_students_table',1),
	(76,'2018_05_11_094627_create_trainees_table',1),
	(77,'2018_05_11_094632_create_trainers_table',1),
	(78,'2018_05_11_094649_create_t_messagings_table',1),
	(79,'2018_05_11_094700_create_notifications_table',2),
	(80,'2018_05_12_223217_create_classrooms_table',3);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table new_notifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `new_notifications`;

CREATE TABLE `new_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nid` int(11) NOT NULL,
  `trid` int(11) NOT NULL,
  `task` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ndate` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table notifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NID` int(11) NOT NULL,
  `user_id` int(10) NOT NULL,
  `SID` int(11) NOT NULL,
  `TRID` int(11) NOT NULL,
  `TASK` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `newnotificationtitle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `newnotification` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NSTATUS` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `NDATE` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;

INSERT INTO `notifications` (`id`, `NID`, `user_id`, `SID`, `TRID`, `TASK`, `newnotificationtitle`, `newnotification`, `NSTATUS`, `NDATE`, `created_at`, `updated_at`)
VALUES
	(1,0,0,0,0,'Notification test','','','Active','0000-00-00 00:00:00',NULL,NULL),
	(2,0,0,0,0,'Another test','','','Inactive','0000-00-00 00:00:00',NULL,NULL),
	(3,0,2,0,0,'','0405 Notification','Hello, this is a new notification message.\r\n\r\nThe standard Lorem Ipsum passage, used since the 1500s\r\n\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididun','','0000-00-00 00:00:00',NULL,NULL),
	(4,0,2,0,0,'','Another Notification','Another Notification  Test\r\n\r\nThe standard Lorem Ipsum passage, used since the 1500s\r\n\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et d','','0000-00-00 00:00:00',NULL,NULL),
	(5,0,2,0,0,'','0605 test message','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididun','','0000-00-00 00:00:00',NULL,NULL),
	(6,0,2,0,0,'','45233647687','eqwrthyjukighklnhmjk.h,gmiufndyvbvfngjnhd .  eqwrthyjukighklnhmjk.h,gmiufndyvbvfngjnhd  eqwrthyjukighklnhmjk.h,gmiufndyvbvfngjnhd .  eqwrthyjukighklnhmjk.h,gmiufndyvbvfngjnhd  eqwrthyjukighkl','','0000-00-00 00:00:00',NULL,NULL);

/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table permission_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permission_role`;

CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned DEFAULT NULL,
  `role_id` int(10) unsigned DEFAULT NULL,
  KEY `fk_p_54415_54416_role_per_596eec08308d0` (`permission_id`),
  KEY `fk_p_54416_54415_permissi_596eec0830986` (`role_id`),
  CONSTRAINT `fk_p_54415_54416_role_per_596eec08308d0` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_p_54416_54415_permissi_596eec0830986` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;

INSERT INTO `permission_role` (`permission_id`, `role_id`)
VALUES
	(1,1),
	(2,1),
	(3,1),
	(4,1),
	(5,1),
	(6,1),
	(7,1),
	(8,1),
	(9,1),
	(10,1),
	(11,1),
	(12,1),
	(13,1),
	(14,1),
	(15,1),
	(16,1),
	(17,1),
	(18,1),
	(19,1),
	(20,1),
	(21,1),
	(22,1),
	(23,1),
	(24,1),
	(25,1),
	(26,1),
	(27,1),
	(28,1),
	(29,1),
	(30,1),
	(31,1),
	(32,1),
	(33,1),
	(34,1),
	(35,1),
	(36,1),
	(37,1),
	(38,1),
	(39,1),
	(40,1),
	(41,1),
	(42,1),
	(43,1),
	(44,1),
	(45,1),
	(1,2),
	(21,2),
	(22,2),
	(23,2),
	(24,2),
	(26,2),
	(27,2),
	(28,2),
	(29,2),
	(31,2),
	(32,2),
	(33,2),
	(34,2),
	(36,2),
	(37,2),
	(38,2),
	(39,2),
	(40,2),
	(41,2),
	(42,2),
	(43,2),
	(44,2),
	(45,2),
	(1,3),
	(21,3),
	(24,3),
	(26,3),
	(29,3),
	(31,3),
	(34,3),
	(36,3),
	(37,3),
	(38,3),
	(39,3),
	(40,3),
	(41,3),
	(44,3),
	(4,3),
	(1,4),
	(4,4),
	(16,4),
	(19,4);

/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;

INSERT INTO `permissions` (`id`, `title`, `created_at`, `updated_at`)
VALUES
	(1,'user_management_access','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(2,'user_management_create','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(3,'user_management_edit','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(4,'user_management_view','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(5,'user_management_delete','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(6,'permission_access','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(7,'permission_create','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(8,'permission_edit','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(9,'permission_view','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(10,'permission_delete','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(11,'role_access','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(12,'role_create','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(13,'role_edit','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(14,'role_view','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(15,'role_delete','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(16,'user_access','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(17,'user_create','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(18,'user_edit','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(19,'user_view','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(20,'user_delete','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(21,'course_access','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(22,'course_create','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(23,'course_edit','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(24,'course_view','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(25,'course_delete','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(26,'lesson_access','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(27,'lesson_create','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(28,'lesson_edit','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(29,'lesson_view','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(30,'lesson_delete','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(31,'question_access','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(32,'question_create','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(33,'question_edit','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(34,'question_view','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(35,'question_delete','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(36,'questions_option_access','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(37,'questions_option_create','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(38,'questions_option_edit','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(39,'questions_option_view','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(40,'questions_option_delete','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(41,'test_access','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(42,'test_create','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(43,'test_edit','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(44,'test_view','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(45,'test_delete','2018-05-11 14:19:23','2018-05-11 14:19:23');

/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table question_test
# ------------------------------------------------------------

DROP TABLE IF EXISTS `question_test`;

CREATE TABLE `question_test` (
  `question_id` int(10) unsigned DEFAULT NULL,
  `test_id` int(10) unsigned DEFAULT NULL,
  KEY `fk_p_54420_54422_test_que_596eeef70992f` (`question_id`),
  KEY `fk_p_54422_54420_question_596eeef7099af` (`test_id`),
  CONSTRAINT `fk_p_54420_54422_test_que_596eeef70992f` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_p_54422_54420_question_596eeef7099af` FOREIGN KEY (`test_id`) REFERENCES `tests` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `question_test` WRITE;
/*!40000 ALTER TABLE `question_test` DISABLE KEYS */;

INSERT INTO `question_test` (`question_id`, `test_id`)
VALUES
	(1,43),
	(2,3),
	(3,32),
	(4,30),
	(5,44),
	(6,35),
	(7,37),
	(8,31),
	(9,30),
	(10,34),
	(11,41),
	(12,30),
	(13,27),
	(14,48),
	(15,16),
	(16,33),
	(17,41),
	(18,38),
	(19,3),
	(20,49),
	(21,9),
	(22,38),
	(23,7),
	(24,12),
	(25,28),
	(26,11),
	(27,41),
	(28,30),
	(29,31),
	(30,44),
	(31,46),
	(32,37),
	(33,19),
	(34,45),
	(35,23),
	(36,49),
	(37,13),
	(38,16),
	(39,15),
	(40,31),
	(41,34),
	(42,33),
	(43,17),
	(44,9),
	(45,34),
	(46,13),
	(47,30),
	(48,49),
	(49,8),
	(50,4);

/*!40000 ALTER TABLE `question_test` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table questions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `questions`;

CREATE TABLE `questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `question_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `questions_deleted_at_index` (`deleted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;

INSERT INTO `questions` (`id`, `question`, `question_image`, `score`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'Aut ut provident quidem.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(2,'Expedita iure quaerat itaque.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(3,'Consectetur expedita soluta ut quae.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(4,'Consequatur repudiandae ut consequatur facere.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(5,'Iste asperiores at quidem voluptas.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(6,'Odio molestiae id aut.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(7,'Tempora optio amet ipsam in incidunt voluptas.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(8,'Nostrum illum esse alias et.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(9,'Reiciendis ab et placeat.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(10,'Aut aperiam ab et magnam sapiente sint.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(11,'Sequi odio quas ipsa non aut delectus excepturi.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(12,'Iure dolores magnam fugit excepturi.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(13,'Fugit impedit placeat aspernatur iusto nihil non.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(14,'Sit et ea sunt aspernatur.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(15,'Magni et ab expedita exercitationem molestias.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(16,'Totam modi temporibus ut.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(17,'Rerum aliquam maiores iure.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(18,'Dolorem ex tempore deleniti dicta adipisci sed.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(19,'Qui recusandae et harum in.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(20,'Iure fugit tempora nulla qui.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(21,'Soluta occaecati quia aut explicabo.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(22,'Iusto praesentium qui animi omnis.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(23,'Optio libero nihil labore dolor.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(24,'Nihil quo modi alias doloribus aut doloribus.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(25,'Ea ut nemo inventore officiis rem labore est.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(26,'Et aut amet omnis soluta impedit.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(27,'Maxime odit fuga fugiat sunt optio aut.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(28,'Non quibusdam nemo aut dolorum.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(29,'Saepe aliquid natus consequuntur qui.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(30,'Labore in distinctio incidunt corrupti corporis.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(31,'Odio sed quas placeat nesciunt.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(32,'Est fugit explicabo magnam sint nesciunt sed.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(33,'Repellat saepe optio omnis et voluptatem.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(34,'Ad delectus optio facere qui quos exercitationem.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(35,'Nam et est inventore quisquam est quasi.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(36,'Et et nulla ex qui consequatur.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(37,'Qui qui in vero occaecati.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(38,'Porro reiciendis quos vel.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(39,'Laboriosam veritatis fuga eum aut.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(40,'Similique et et soluta itaque.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(41,'Cumque eius occaecati et architecto.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(42,'Dolores non voluptas qui numquam amet.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(43,'Fugit non sint deserunt vero.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(44,'Sunt ducimus sunt voluptatibus laborum.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(45,'Ratione non assumenda quasi maxime esse.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(46,'Quo assumenda illum quis dolorem eos.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(47,'Atque corporis vero voluptas at dolorum vitae.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(48,'Ut magnam dolorem excepturi laboriosam tempore.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(49,'Aspernatur enim nulla iste aut.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(50,'Velit fugiat beatae ut rerum.?',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL);

/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table questions_options
# ------------------------------------------------------------

DROP TABLE IF EXISTS `questions_options`;

CREATE TABLE `questions_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(10) unsigned DEFAULT NULL,
  `option_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `correct` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `54421_596eee8745a1e` (`question_id`),
  KEY `questions_options_deleted_at_index` (`deleted_at`),
  CONSTRAINT `54421_596eee8745a1e` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `questions_options` WRITE;
/*!40000 ALTER TABLE `questions_options` DISABLE KEYS */;

INSERT INTO `questions_options` (`id`, `question_id`, `option_text`, `correct`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,'Modi quod voluptate rerum sapiente tenetur.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(2,1,'Quae illo dolor dicta quae.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(3,1,'At odit omnis quae occaecati ab molestiae ea.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(4,1,'Odit omnis odio non natus.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(5,2,'Porro fugit et est suscipit sunt in.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(6,2,'Aut non exercitationem tempore.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(7,2,'Dolores dolor amet omnis facilis enim facere.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(8,2,'Exercitationem sint nisi magni voluptatum.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(9,3,'Provident ab nostrum quam non beatae odit ab.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(10,3,'Impedit ipsum veritatis rerum.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(11,3,'Et nisi qui dolorem et non quibusdam.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(12,3,'Laborum consequatur perspiciatis quae dolor.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(13,4,'Minus facere qui vel perspiciatis.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(14,4,'Nobis alias inventore sunt provident sit a id.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(15,4,'Nesciunt tempora aut omnis aliquid unde ducimus.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(16,4,'Ut quis dolorem error sit quae perferendis.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(17,5,'Praesentium vero ut facilis et harum harum.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(18,5,'Quis itaque voluptatum quia nulla.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(19,5,'Cum vitae illo doloremque rerum veniam.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(20,5,'Illo enim sunt in corporis repellendus sed.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(21,6,'Nostrum earum nostrum placeat ratione possimus.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(22,6,'Sed quo labore velit.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(23,6,'Exercitationem suscipit ipsum ab explicabo.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(24,6,'Voluptas placeat sapiente et in.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(25,7,'Sunt aut deserunt commodi iste.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(26,7,'Et perspiciatis sed odio eos sed aperiam dolorum.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(27,7,'Velit aliquam repellat error culpa dolor.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(28,7,'Illo ex id corrupti at id rerum sint.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(29,8,'Illum eligendi ea sed.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(30,8,'Qui fugit atque enim necessitatibus voluptatem.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(31,8,'Quaerat saepe voluptas totam ut dolorem.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(32,8,'Et dolorem unde facilis nostrum.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(33,9,'Aut totam sunt nemo.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(34,9,'Aut fugit in ab. Vel quod sunt vero placeat.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(35,9,'Rerum modi porro qui officia.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(36,9,'Eum tempora expedita culpa eius.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(37,10,'Dolorum accusantium voluptates sequi magnam.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(38,10,'Non quam ea quae libero.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(39,10,'Iure quaerat quis quas aut.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(40,10,'Velit autem hic ducimus unde nobis.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(41,11,'Explicabo unde veniam qui.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(42,11,'Nihil nulla possimus autem.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(43,11,'Quis cum aliquam ipsa in alias quaerat.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(44,11,'Ut quas corrupti doloremque aut.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(45,12,'Est reiciendis magnam esse aliquid.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(46,12,'Nesciunt provident molestiae magnam aut quis.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(47,12,'Odit nam ut explicabo.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(48,12,'Aut error quis sequi sapiente cum.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(49,13,'At quasi error reiciendis quis omnis.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(50,13,'Tempora quia id sunt necessitatibus neque.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(51,13,'Numquam aperiam dignissimos suscipit.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(52,13,'Officiis nam et quia quam.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(53,14,'Maiores qui quia incidunt earum voluptatem.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(54,14,'Id expedita temporibus ratione dolor id odit.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(55,14,'Rem nisi et sed placeat tenetur dignissimos.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(56,14,'Et ducimus perferendis aut dolor ad illum.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(57,15,'Dolore eos repellendus iste voluptatem.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(58,15,'Ut aperiam placeat consequatur quidem.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(59,15,'Ut quia molestiae nisi voluptatum earum.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(60,15,'Aspernatur odit omnis qui.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(61,16,'Tempora magnam iusto et id ut.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(62,16,'Aut porro ad id quas porro.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(63,16,'Corrupti aliquam debitis ea et ab ipsa ut.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(64,16,'Impedit earum facilis nesciunt enim.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(65,17,'Ea et aliquid sed quia.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(66,17,'Eligendi explicabo molestias aperiam ab.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(67,17,'Dolor voluptate asperiores quibusdam.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(68,17,'Eaque non eaque sed veritatis eaque vero.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(69,18,'Assumenda odio qui ut qui vel et animi.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(70,18,'Voluptatem nisi illo aut sit voluptatibus quod.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(71,18,'Omnis voluptatum a amet itaque qui officia saepe.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(72,18,'Sapiente explicabo voluptas ut.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(73,19,'Sit dignissimos aut non.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(74,19,'Sunt fuga sint est fuga deleniti aliquid commodi.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(75,19,'Deleniti dolores aut dolores qui consequatur.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(76,19,'Aliquid est et rerum sunt quidem laudantium.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(77,20,'Occaecati odio amet dolorem repellendus.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(78,20,'Inventore eos labore illo ut voluptas nesciunt.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(79,20,'Velit libero rem enim velit accusamus.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(80,20,'Sunt autem totam exercitationem est.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(81,21,'Vel ad debitis ut ipsum consequatur eos quia.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(82,21,'Nihil deleniti distinctio aliquid est eveniet.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(83,21,'Omnis facere ea fuga harum alias aliquid.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(84,21,'Dolorum nihil consequatur aut sequi provident.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(85,22,'Sunt quo exercitationem nemo sed eos.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(86,22,'Qui voluptatum omnis atque nulla.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(87,22,'Sed maiores aliquid mollitia inventore.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(88,22,'Velit modi velit non dolores.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(89,23,'Dolores et iste aliquam et.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(90,23,'Dolor earum voluptatem unde delectus et natus.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(91,23,'Eos occaecati ut a et eaque.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(92,23,'Non quo molestiae magnam quia optio.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(93,24,'Dolores provident ipsam illum suscipit.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(94,24,'Vel odio optio necessitatibus magni.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(95,24,'Natus omnis expedita est dolore.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(96,24,'Ut est totam consequatur eos distinctio error.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(97,25,'Et et officiis ea porro.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(98,25,'Ut alias voluptatem accusamus repudiandae autem.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(99,25,'Ut voluptate rerum temporibus ducimus.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(100,25,'Voluptatem dolorum fugit sunt quis.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(101,26,'Ipsam adipisci mollitia nulla sit.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(102,26,'Voluptatibus non quo eum in.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(103,26,'Ut est earum sed quo unde.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(104,26,'Et ea suscipit sed cumque.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(105,27,'Enim ullam soluta officia.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(106,27,'Voluptas ipsam quia explicabo.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(107,27,'Debitis sint qui expedita beatae neque.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(108,27,'Et quasi magnam nulla delectus et sint explicabo.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(109,28,'Nam nam atque deleniti quidem explicabo.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(110,28,'Dolorem esse id aut quia officia eius nisi eum.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(111,28,'Sint tempora porro debitis et.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(112,28,'Quia et non sed quis et illo sunt.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(113,29,'Autem omnis rerum nostrum saepe qui modi.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(114,29,'Atque aut commodi cupiditate fugit.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(115,29,'Et id qui distinctio incidunt perspiciatis.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(116,29,'Error earum similique voluptatibus adipisci vero.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(117,30,'Earum architecto cumque est odio.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(118,30,'Amet quis rem quas error illum illum qui.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(119,30,'Odio et aliquid non omnis ea.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(120,30,'At aspernatur perferendis ut sed hic commodi.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(121,31,'Rerum sed vero id reprehenderit saepe.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(122,31,'Qui velit dolorem ipsam sit.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(123,31,'Ex laborum rem nemo est qui et optio molestiae.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(124,31,'Veritatis quam doloremque corporis minima.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(125,32,'Officia error reprehenderit nemo quis in soluta.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(126,32,'Corporis id ab magni qui minima ut suscipit.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(127,32,'Saepe vel ipsam voluptas facere.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(128,32,'Culpa reiciendis consequatur sed et.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(129,33,'Voluptas quidem possimus voluptate.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(130,33,'Sed aut nisi distinctio et.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(131,33,'Sint quo distinctio amet nihil.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(132,33,'Rerum et recusandae est ipsam veniam.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(133,34,'Esse aut perspiciatis quam temporibus.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(134,34,'Rerum aut minus velit ratione.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(135,34,'Quia reiciendis nulla voluptas est.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(136,34,'Suscipit at qui omnis.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(137,35,'Omnis ullam voluptatem odit.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(138,35,'Et nam sequi et quis eveniet et ratione.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(139,35,'Et ea qui quod perspiciatis ipsum autem nulla.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(140,35,'Porro et recusandae qui aut.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(141,36,'Magni deleniti eum sequi consequatur quam ut et.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(142,36,'Aliquam ut vel quia distinctio consequatur.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(143,36,'Doloribus sit eos a in ut facilis.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(144,36,'Culpa tempore accusantium qui minus excepturi.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(145,37,'Reiciendis commodi omnis dolore quaerat.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(146,37,'Atque est quo expedita.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(147,37,'Incidunt et autem qui asperiores quia.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(148,37,'Id omnis nulla fugiat ut.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(149,38,'Autem asperiores sunt sunt quia omnis voluptas.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(150,38,'Ipsa voluptatem sit voluptatem doloremque.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(151,38,'Itaque commodi ad consectetur dolor sint.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(152,38,'Architecto quia animi amet rerum adipisci est id.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(153,39,'Id est deleniti quaerat iste qui reprehenderit.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(154,39,'Beatae molestiae magnam repellendus ea.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(155,39,'A et debitis tenetur sit ipsam unde consequatur.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(156,39,'Facere quidem in dicta nam tempora asperiores.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(157,40,'Eaque earum eos iste necessitatibus.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(158,40,'Sint quia quasi ratione. Vel sed eius dolorem.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(159,40,'Nisi quo est in aperiam.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(160,40,'At totam non vitae quisquam.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(161,41,'Saepe dolor odio aut recusandae ipsam quis vitae.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(162,41,'Aut voluptatem quasi hic earum qui et.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(163,41,'Non dolores est sed quas vitae dolor sunt.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(164,41,'Delectus soluta eum ratione exercitationem.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(165,42,'Consequatur ut adipisci hic magni est.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(166,42,'Fuga maxime pariatur quo ipsa enim.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(167,42,'Excepturi rerum officia ullam.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(168,42,'Autem cum sed corporis reprehenderit ea.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(169,43,'Voluptas quo ipsam rem et et necessitatibus.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(170,43,'Velit doloremque dicta error similique nam.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(171,43,'Fuga est ex mollitia velit.?',1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(172,43,'Inventore nisi neque illo exercitationem.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(173,44,'Est blanditiis necessitatibus qui ex quasi autem.?',0,'2018-05-11 14:19:24','2018-05-11 14:19:25',NULL),
	(174,44,'Reprehenderit vel aut provident rerum quasi.?',0,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(175,44,'Exercitationem labore cum et blanditiis.?',0,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(176,44,'Nihil nostrum ut inventore fuga natus.?',0,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(177,45,'Iure ducimus a voluptates accusantium velit.?',1,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(178,45,'Fugiat a quae facilis.?',0,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(179,45,'Delectus repudiandae veniam sapiente.?',1,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(180,45,'Earum et mollitia sunt deserunt.?',0,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(181,46,'Officia beatae est laborum corporis.?',0,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(182,46,'Porro sapiente minima blanditiis.?',0,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(183,46,'Quaerat amet nulla perspiciatis sunt aut.?',1,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(184,46,'In molestiae in soluta aperiam quo corrupti.?',0,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(185,47,'Debitis maiores unde numquam. Earum sit cum quod.?',0,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(186,47,'At eos necessitatibus quidem perferendis.?',0,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(187,47,'Expedita nihil id dolore.?',1,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(188,47,'Ipsam voluptatem distinctio saepe dignissimos.?',1,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(189,48,'Quod qui repudiandae corporis nam delectus saepe.?',1,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(190,48,'Officiis maxime ipsum officiis repudiandae.?',0,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(191,48,'Labore et sit nobis blanditiis.?',0,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(192,48,'Sit vitae quibusdam velit impedit autem ut cum.?',0,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(193,49,'Illo omnis distinctio nobis voluptatibus.?',1,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(194,49,'Quidem voluptatibus libero commodi sequi.?',1,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(195,49,'Qui ratione omnis perferendis aliquid alias.?',1,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(196,49,'Illum cumque nihil sint similique.?',1,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(197,50,'Aliquam error rerum nesciunt enim vel eum.?',1,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(198,50,'Quae beatae quam et vitae quo.?',1,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(199,50,'Distinctio et hic exercitationem cum.?',0,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL),
	(200,50,'Eum rem cupiditate in labore adipisci ad cum.?',0,'2018-05-11 14:19:25','2018-05-11 14:19:25',NULL);

/*!40000 ALTER TABLE `questions_options` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table role_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `role_user`;

CREATE TABLE `role_user` (
  `role_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  KEY `fk_p_54416_54417_user_rol_596eec0af3746` (`role_id`),
  KEY `fk_p_54417_54416_role_use_596eec0af37c1` (`user_id`),
  CONSTRAINT `fk_p_54416_54417_user_rol_596eec0af3746` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_p_54417_54416_role_use_596eec0af37c1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;

INSERT INTO `role_user` (`role_id`, `user_id`)
VALUES
	(1,1),
	(2,2),
	(3,3),
	(4,4),
	(2,5),
	(2,6),
	(2,7),
	(2,8),
	(4,9),
	(4,10),
	(4,11),
	(4,12),
	(4,13);

/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`)
VALUES
	(1,'Administrator (can create other users)','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(2,'Teacher','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(3,'Student','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(4,'Trainee','2018-05-11 14:23:14','2018-05-11 14:23:14');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table students
# ------------------------------------------------------------

DROP TABLE IF EXISTS `students`;

CREATE TABLE `students` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `full_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel_no` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `d_o_b` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `reg_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table t_messagings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_messagings`;

CREATE TABLE `t_messagings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tmid` int(10) NOT NULL,
  `trid` int(10) NOT NULL,
  `tid` int(10) NOT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mdate` date NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table tests
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tests`;

CREATE TABLE `tests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(10) unsigned DEFAULT NULL,
  `lesson_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `published` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `54422_596eeef514d00` (`course_id`),
  KEY `54422_596eeef53411a` (`lesson_id`),
  KEY `tests_deleted_at_index` (`deleted_at`),
  CONSTRAINT `54422_596eeef514d00` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE,
  CONSTRAINT `54422_596eeef53411a` FOREIGN KEY (`lesson_id`) REFERENCES `lessons` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `tests` WRITE;
/*!40000 ALTER TABLE `tests` DISABLE KEYS */;

INSERT INTO `tests` (`id`, `course_id`, `lesson_id`, `title`, `description`, `published`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,NULL,1,'Vel animi adipisci accusamus qui explicabo sit.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(2,NULL,2,'Atque dignissimos eos quam asperiores in.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(3,NULL,3,'Molestiae ut distinctio ut.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(4,NULL,4,'Labore ipsa quisquam voluptatum itaque.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(5,NULL,5,'Et eaque quod at itaque inventore consectetur et.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(6,NULL,6,'Qui alias dignissimos totam.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(7,NULL,7,'Occaecati delectus repellat sed ipsam.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(8,NULL,8,'Sed dignissimos ab maiores iste ab modi harum.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(9,NULL,9,'Voluptates omnis fugiat odio est.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(10,NULL,10,'Molestiae repellat aut iure.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(11,NULL,11,'Sint facere sint et itaque dolores.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(12,NULL,12,'Dolorem minima non quas dolores magni.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(13,NULL,13,'Minima beatae incidunt repellendus corrupti.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(14,NULL,14,'Blanditiis et qui autem eum ad.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(15,NULL,15,'Soluta eum ut eaque.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(16,NULL,16,'Velit animi voluptatum in.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(17,NULL,17,'Vero eaque sunt sed assumenda.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(18,NULL,18,'Sit ratione et ut fuga molestiae culpa atque.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(19,NULL,19,'Sequi pariatur neque sint aperiam.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(20,NULL,20,'Architecto omnis consequuntur quod distinctio ea.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(21,NULL,21,'Ex facere eveniet eius perferendis eaque totam.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(22,NULL,22,'Ab veritatis illo aut provident voluptas.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(23,NULL,23,'Blanditiis ratione eum in ea aut qui.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(24,NULL,24,'Sed enim maiores molestias et hic ipsum.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(25,NULL,25,'Molestiae rerum assumenda reprehenderit rerum.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(26,NULL,26,'Porro ut cum enim quia. Est quos id vel quis.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(27,NULL,27,'Harum assumenda ea porro est voluptate sint.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(28,NULL,28,'Quia ullam dolor quaerat rerum nisi inventore.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(29,NULL,29,'Quos enim doloremque quod provident fugiat.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(30,NULL,30,'Tempora eaque soluta provident iure est dolor.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(31,NULL,31,'Debitis dolores reiciendis consequatur est.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(32,NULL,32,'Illo et sunt accusantium tenetur iusto ea.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(33,NULL,33,'Hic dignissimos placeat eum commodi ratione quae.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(34,NULL,34,'Omnis aliquid impedit id consequuntur.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(35,NULL,35,'Consequatur et non et sed.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(36,NULL,36,'Aut aut delectus officiis voluptate.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(37,NULL,37,'Amet voluptatem facilis molestias corporis.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(38,NULL,38,'Enim aliquid possimus consequatur.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:23',NULL),
	(39,NULL,39,'Est ut consectetur aliquam et quia fugit.',NULL,1,'2018-05-11 14:19:23','2018-05-11 14:19:24',NULL),
	(40,NULL,40,'Voluptatem molestiae earum blanditiis sit.',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(41,NULL,41,'Magnam non fuga voluptas iure optio laborum.',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(42,NULL,42,'Occaecati cupiditate blanditiis sed porro.',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(43,NULL,43,'Nesciunt maiores aut modi.',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(44,NULL,44,'Maxime et maxime vitae cum.',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(45,NULL,45,'Cumque rerum eveniet nisi modi.',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(46,NULL,46,'Iusto fuga et expedita nisi beatae velit ratione.',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(47,NULL,47,'Quia suscipit voluptas et impedit.',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(48,NULL,48,'Id sapiente quia eos earum quis rerum.',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(49,NULL,49,'Molestiae voluptatem quas qui.',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL),
	(50,NULL,50,'Repellat fuga ut sed eaque.',NULL,1,'2018-05-11 14:19:24','2018-05-11 14:19:24',NULL);

/*!40000 ALTER TABLE `tests` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tests_results
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tests_results`;

CREATE TABLE `tests_results` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `test_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `test_result` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tests_results_test_id_foreign` (`test_id`),
  KEY `tests_results_user_id_foreign` (`user_id`),
  CONSTRAINT `tests_results_test_id_foreign` FOREIGN KEY (`test_id`) REFERENCES `tests` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tests_results_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table tests_results_answers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tests_results_answers`;

CREATE TABLE `tests_results_answers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tests_result_id` int(10) unsigned DEFAULT NULL,
  `question_id` int(10) unsigned DEFAULT NULL,
  `option_id` int(10) unsigned DEFAULT NULL,
  `correct` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tests_results_answers_tests_result_id_foreign` (`tests_result_id`),
  KEY `tests_results_answers_question_id_foreign` (`question_id`),
  KEY `tests_results_answers_option_id_foreign` (`option_id`),
  CONSTRAINT `tests_results_answers_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `questions_options` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tests_results_answers_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tests_results_answers_tests_result_id_foreign` FOREIGN KEY (`tests_result_id`) REFERENCES `tests_results` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table trainees
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trainees`;

CREATE TABLE `trainees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) DEFAULT NULL,
  `course_id` int(10) DEFAULT NULL,
  `full_name` text COLLATE utf8mb4_unicode_ci,
  `email` text COLLATE utf8mb4_unicode_ci,
  `password` text COLLATE utf8mb4_unicode_ci,
  `tel_no` text COLLATE utf8mb4_unicode_ci,
  `address` text COLLATE utf8mb4_unicode_ci,
  `d_o_b` text COLLATE utf8mb4_unicode_ci,
  `level` int(10) NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `reg_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `trainees` WRITE;
/*!40000 ALTER TABLE `trainees` DISABLE KEYS */;

INSERT INTO `trainees` (`id`, `tid`, `course_id`, `full_name`, `email`, `password`, `tel_no`, `address`, `d_o_b`, `level`, `status`, `reg_date`, `created_at`, `updated_at`)
VALUES
	(1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'',NULL,'2018-05-13 09:41:13','2018-05-13 09:41:13'),
	(2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'',NULL,'2018-05-13 09:44:47','2018-05-13 09:44:47'),
	(3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'',NULL,'2018-05-13 09:53:11','2018-05-13 09:53:11'),
	(4,NULL,NULL,'Jay Trainee','jay@demon.com','password','01234567789','flat 1, 1 street. bn1','01/11/1111',0,'',NULL,NULL,NULL),
	(5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'',NULL,'2018-05-13 10:41:50','2018-05-13 10:41:50'),
	(6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'',NULL,'2018-05-13 10:41:58','2018-05-13 10:41:58'),
	(7,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'',NULL,'2018-05-13 10:43:58','2018-05-13 10:43:58');

/*!40000 ALTER TABLE `trainees` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trainers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trainers`;

CREATE TABLE `trainers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `trid` int(11) NOT NULL,
  `full_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel_no` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `d_o_b` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `reg_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationality` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `d_o_b` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `tel_no` int(30) DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) DEFAULT NULL,
  `status` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `nationality`, `d_o_b`, `address`, `tel_no`, `email`, `password`, `level`, `status`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'Admin',NULL,NULL,NULL,NULL,'admin@admin.com','$2y$10$l4MghrLnKXTRUDlR07XQeesKHRIaAe7WzDf90g751BEf70AwnJ5m.',NULL,NULL,'','2018-05-11 14:19:23','2018-05-11 14:19:23'),
	(2,'Trainer Demo',NULL,NULL,NULL,NULL,'trainer@demo.com','$2y$10$tWLLzIwsxdd1376J/nNe/OGsnFPcZ3QlEZfYa.ap38wsyyeSKNHcm',NULL,NULL,NULL,'2018-05-11 14:20:12','2018-05-11 14:20:12'),
	(3,'Trainee Demo',NULL,NULL,NULL,NULL,'trainee@demo.com','$2y$10$RkAzQ7HcqASVIhAypmemf.DFsQgeS.RznpKIyBIcGMnsFdHwyTbhG',NULL,NULL,'V6YuYuaUfFLABTS86TnEuM9EJ4K0YUDrcjyHy8613Dtfh8KJE3eDbXbbHQAA','2018-05-11 14:20:33','2018-05-11 14:20:33'),
	(4,'Trainee 2',NULL,NULL,NULL,NULL,'trainee@two.com','$2y$10$LcEQ67gD2nYCXo78JsIKfu7ircxurmdGYwwXMlBkRAJHNz.O4Q7Ge',NULL,NULL,'9Q5hrt0z75I1qcoR8AFBSZN7kT950K1xqR6AqZCTDXb4zPcrT6n434pPxrCg','2018-05-11 14:24:14','2018-05-11 14:24:14'),
	(5,'120 Trainer',NULL,NULL,NULL,NULL,'trainer@120.com','$2y$10$r9l9.LwiCiH0.8.Ro3FIIuSP51JqfjccbwhKXFYk5W9vjnL.KLRFq',NULL,NULL,NULL,'2018-05-13 10:18:30','2018-05-13 10:18:30'),
	(6,'140 Trainer',NULL,NULL,NULL,NULL,'trainer@140.com','$2y$10$XekFRdWFTpVrGXJJf49lyuf/Sv6/1RaIJ1LbjC94OlLoZYn8IxyLe',NULL,NULL,NULL,'2018-05-13 10:18:49','2018-05-13 10:18:49'),
	(7,'190 Trainer',NULL,NULL,NULL,NULL,'trainer@190.com','$2y$10$6WMdsuSAVLLSBMAIRGWAGeuyE6V5mA5sCtTWZ7auqCaa3YBuzm8ZO',NULL,NULL,NULL,'2018-05-13 10:19:08','2018-05-13 10:19:08'),
	(8,'IELTS Trainer',NULL,NULL,NULL,NULL,'trainer@ielts.com','$2y$10$7t6gujEPKrEj14KyPs/rdurjM/Xjt/zUNSH1e/VxAszWjbqjdmAfG',NULL,NULL,NULL,'2018-05-13 10:19:39','2018-05-13 10:19:39'),
	(9,'John Smith',NULL,NULL,NULL,NULL,'John@smith.com','$2y$10$rRhN7g1y7daVctcWbC2NP.DNNisJUi3vYilUHwIwZb9wv5tGUekXq',11,'Active',NULL,'2018-05-13 10:54:35','2018-05-13 10:54:54'),
	(10,'Jane Doe',NULL,NULL,NULL,NULL,'jane@dow.com','$2y$10$ijy1dEgCUVCsU/NVDQ0PRuXtIR7ah1Jm5rfu/ya9SzP8sn0jZ8uui',2,'Active','Agv2YBrVK8t9ISAo0DbONBPkcXOzhKa9TZSmuaCCcJoQ9vMIiZNMfHl9fxMc','2018-05-13 10:55:11','2018-05-13 10:55:11'),
	(11,'joe bloggs',NULL,NULL,NULL,NULL,'joe@blogg.com','$2y$10$sfNK6IK9GcAttCyoow9qk.WsOh6Q8ItaBhIWAp9Hm8pWzPGc.um3q',5,'Active',NULL,'2018-05-13 10:55:40','2018-05-13 10:55:40'),
	(12,'John Doe',NULL,NULL,NULL,NULL,'john@doe.com','$2y$10$GW8aH3qzxgxZuPmjxVrwIer636qHVTmfqQHkHRbB9.Xpcp5SOvcWS',7,'Inactive',NULL,'2018-05-13 10:56:31','2018-05-13 10:56:31'),
	(13,'Jay King',NULL,NULL,NULL,NULL,'jay@demon1.com','$2y$10$VvDNTlYdwQnQpUBuXr/UfOs67zKGlJ.HMQlcwC8y7i4ACAU0qP/DC',2,'Active',NULL,'2018-05-13 11:19:16','2018-05-13 11:19:16');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
