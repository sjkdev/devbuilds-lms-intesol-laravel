@inject('request', 'Illuminate\Http\Request')
<!-- Left side column. contains the sidebar -->



<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">

          

            <li class="{{ $request->segment(1) == 'home' ? 'active' : '' }}">
                <a href="{{ url('/') }}">
                    <i class="fa fa-wrench"></i>
                    <span class="title">@lang('global.app_dashboard')</span>
                </a>
            </li>



            @if(Auth::user()->isAdmin())

            <li>
                <a href="#">
                    <i class="fa fa-gears">
                        <span class="title">Admin Menu</span>
                    </i>    
                </a>
            </li>
            @can('user_management_access')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span class="title">@lang('global.user-management.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                
                @can('permission_access')
                <li class="{{ $request->segment(2) == 'permissions' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.permissions.index') }}">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                                @lang('global.permissions.title')
                            </span>
                        </a>
                    </li>
                @endcan
                @can('role_access')
                <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.roles.index') }}">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                                @lang('global.roles.title')
                            </span>
                        </a>
                    </li>
                @endcan
                @can('user_access')
                <li class="{{ $request->segment(2) == 'users' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.users.index') }}">
                            <i class="fa fa-user"></i>
                            <span class="title">
                                @lang('global.users.title')
                            </span>
                        </a>
                    </li>
                @endcan
                </ul>
            </li>
            
            @endcan
            

            @can('course_access_nil')
            <li class="{{ $request->segment(2) == 'courses' ? 'active' : '' }}">
                <a href="{{ route('admin.courses.index') }}">
                    <i class="fa fa-gears"></i>
                    <span class="title">@lang('global.courses.title')</span>
                </a>
            </li>
            @endcan
            
            @can('lesson_access_nil')
            <li class="{{ $request->segment(2) == 'lessons' ? 'active' : '' }}">
                <a href="{{ route('admin.lessons.index') }}">
                    <i class="fa fa-gears"></i>
                    <span class="title">@lang('global.lessons.title')</span>
                </a>
            </li>
            @endcan
            
            @can('question_access_nil')
            <li class="{{ $request->segment(2) == 'questions' ? 'active' : '' }}">
                <a href="{{ route('admin.questions.index') }}">
                    <i class="fa fa-question"></i>
                    <span class="title">@lang('global.questions.title')</span>
                </a>
            </li>
            @endcan
        {{--      <li class="{{ $request->segment(2) == 'trainee' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.trainee.index') }}">
                    <i class="fa fa-gears">
                        <span class="title">@lang('global.createtrainee.title')</span>
                    </i>    
                </a>
            </li>
            <li class="{{ $request->segment(2) == 'trainer' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.trainer.index') }}">
                    <i class="fa fa-gears">
                        <span class="title">@lang('global.createtrainer.title')</span>
                    </i>    
                </a>
            </li>
            <li class="{{ $request->segment(2) == 'classroom' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.classroom.index') }}">
                    <i class="fa fa-gears">
                        <span class="title">@lang('global.createclassroom.title')</span>
                    </i>    
                </a>
            </li> --}}
            <li>
                <a href="{{ route('admin.courses.index')}}">
                    <i class="fa fa-gears">
                        <span class="title">@lang('global.createcourse.title')</span>
                    </i>    
                </a>
            </li>
            
            @can('questions_option_access_nil')
            <li class="{{ $request->segment(2) == 'questions_options' ? 'active' : '' }}">
                <a href="{{ route('admin.questions_options.index') }}">
                    <i class="fa fa-gears"></i>
                    <span class="title">@lang('global.questions-options.title')</span>
                </a>
            </li>
            @endcan
            
            @can('test_access_nil')
            <li class="{{ $request->segment(2) == 'tests' ? 'active' : '' }}">
                <a href="{{ route('admin.tests.index') }}">
                    <i class="fa fa-gears"></i>
                    <span class="title">@lang('global.tests.title')</span>
                </a>
            </li>
            @endcan
            @endif

            @if(Auth::user()->isTrainer())
            <li>
                <a href="#">
                    <i class="fa fa-briefcase">
                        <span class="title">Trainer Menu</span>
                    </i>
                </a>
            </li>

            
             <li class="{{ $request->segment(2) == 'trainerhome' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.trainerhome.index')}}">
                    <i class="fa fa-briefcase">
                        <span class="title">Control Panel</span>
                    </i>
                </a>
            </li>

             <li class="{{ $request->segment(2) == 'canned' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.canned.index')}}">
                    <i class="fa fa-briefcase">
                        <span class="title">Canned</span>
                    </i>
                </a>
            </li>

             <li class="{{ $request->segment(2) == 'messages' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.messages.index')}}">
                    <i class="fa fa-briefcase">
                        <span class="title">Messages</span>
                    </i>
                </a>
            </li>

              <li>
                <a href="{{ route('admin.courses.index')}}">
                    <i class="fa fa-gears">
                        <span class="title">Trainee List</span>
                    </i>    
                </a>
            </li>

            <li class="{{ $request->segment(2) == 'trainer_review' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.trainer_review.index')}}">
                    <i class="fa fa-gears">
                        <span class="title">Trainee Review</span>
                    </i>    
                </a>
            </li>

            @endif

 




            
              @if(Auth::user()->isTrainee())
                <li class="#">
                    <a href="#">
                        <i class="fa fa-user">
                            <span class="title">Trainee Menu</span>
                        </i>
                    </a>
                </li>
             
             
            @endif

            @if(Auth::user()->isStudent())
                <li class="#">
                    <a href="#">
                        <i class="fa fa-user">
                            <span class="title">Student Menu</span>
                        </i>
                    </a>
                </li>

                 <li class="{{ $request->segment(2) == 'onetwentyhourshome' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.home.index') }}">
                    <i class="fa fa-gears"></i>
                    <span class="title">
                        Control Panel
                    </span>
                </a>
            </li>

            <li class="treeview">
               <li class="{{ $request->segment(2) == 'unit-one' ? 'active active-sub' : '' }}">
                <a href="{{ url('admin.onetwentyhours.unit-one.index') }}">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.unit-one.title')
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">

                    <li class="{{ $request->segment(2) == 'U1M1S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-one.module-one.section-one.index') }}">module</a></li>
                 
                </ul>
            </li>
           

            <li class="treeview">
               <li class="{{ $request->segment(2) == 'unit-two' ? 'active active-sub' : '' }}">
                <a href="{{ url('admin.onetwentyhours.unit-two.index') }}">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.unit-two.title')
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">

                    <li class="{{ $request->segment(2) == 'U2M1S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-two.module-one.section-one.index') }}">Module 1</a></li>
                {{-- <li class="{{ $request->segment(2) == 'U2M1S2' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-two.module-one.section-two.index') }}">test section</a></li> --}}
                    <li class="{{ $request->segment(2) == 'U2M2S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-two.module-two.section-one.index') }}">Module 2</a></li>
                    <li class="{{ $request->segment(2) == 'U2M3S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-two.module-three.section-one.index') }}">Module 3</a></li>
                    
                </ul>
            </li>
        
            <li class="treeview">
               <li class="{{ $request->segment(2) == 'unit-three' ? 'active active-sub' : '' }}">
                <a href="{{ url('admin.onetwentyhours.unit-three.index') }}">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.unit-three.title')
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">

                    <li class="{{ $request->segment(2) == 'U3M1S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-three.module-one.section-one.index') }}">Module 1</a></li>
                    <li class="{{ $request->segment(2) == 'U3M2S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-three.module-two.section-one.index') }}">Module 2</a></li>
                    <li class="{{ $request->segment(2) == 'U3M3S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-three.module-three.section-one.index') }}">Module 3</a></li>
                    <li class="{{ $request->segment(2) == 'U3M4S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-three.module-four.section-one.index') }}">Module 4</a></li>
                    <li class="{{ $request->segment(2) == 'U3M5S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-three.module-five.section-one.index') }}">Module 5</a></li>
                    
                </ul>
            </li>
          
            <li class="treeview">
               <li class="{{ $request->segment(2) == 'unit-four' ? 'active active-sub' : '' }}">
                <a href="{{ url('admin.onetwentyhours.unit-four.index') }}">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.unit-four.title')
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">

                    <li class="{{ $request->segment(2) == 'U4M1S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-four.module-one.section-one.index') }}">Module 1</a></li>
                    <li class="{{ $request->segment(2) == 'U4M2S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-four.module-two.section-one.index') }}">Module 2</a></li>
                    
                </ul>
            </li>
          
            <li class="treeview">
               <li class="{{ $request->segment(2) == 'unit-five' ? 'active active-sub' : '' }}">
                <a href="{{ url('admin.onetwentyhours.unit-five.index') }}">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.unit-five.title')
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">

                   <li class="{{ $request->segment(2) == 'U5M1S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-five.module-one.section-one.index') }}">Module 1 - TEYL</a></li>
                    
                    
                </ul>
            </li>
       
      {{--       @php ($unread = App\MessengerTopic::countUnread())
            <li class="{{ $request->segment(2) == 'messenger' ? 'active' : '' }} {{ ($unread > 0 ? 'unread' : '') }}">
                <a href="{{ route('admin.messenger.index') }}">
                    <i class="fa fa-envelope"></i>

                    <span>Messages</span>
                    @if($unread > 0)
                        {{ ($unread > 0 ? '('.$unread.')' : '') }}
                    @endif
                </a>
            </li>
            <style>
                .page-sidebar-menu .unread * {
                    font-weight:bold !important;
                }
            </style>
           --}}

             
             
            @endif
            

            <li class="{{ $request->segment(1) == 'change_password' ? 'active' : '' }}">
                <a href="{{ route('auth.change_password') }}">
                    <i class="fa fa-key"></i>
                    <span class="title">Change password</span>
                </a>
            </li>

            <li>
                <a href="#logout" onclick="$('#logout').submit();">
                    <i class="fa fa-arrow-left"></i>
                    <span class="title">@lang('global.app_logout')</span>
                </a>
            </li>
        </ul>
    </section>
</aside>
{!! Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
<button type="submit">@lang('global.logout')</button>
{!! Form::close() !!}
