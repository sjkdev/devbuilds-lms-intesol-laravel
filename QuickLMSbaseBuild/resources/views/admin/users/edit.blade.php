@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('global.users.title')</h3>
    
    {!! Form::model($user, ['method' => 'PUT', 'route' => ['admin.users.update', $user->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_edit')
        </div>

      <div class="panel-body">

            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                    {!! Form::text('full_name', old('full_name'), ['class' => 'form-control', 'placeholder' => '', ]) !!}
                    <p class="help-block"></p>
                    @if($errors->has('full_name'))
                        <p class="help-block">
                            {{ $errors->first('full_name') }}
                        </p>
                    @endif
                </div>
            </div>

             <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('address', 'Address*', ['class' => 'control-label']) !!}
                    {!! Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => '',]) !!}
                    <p class="help-block"></p>
                    @if($errors->has('address'))
                        <p class="help-block">
                            {{ $errors->first('address') }}
                        </p>
                    @endif
                </div>
            </div>
              <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('tel_no', 'Tel*', ['class' => 'control-label']) !!}
                    {!! Form::text('tel_no', old('tel_no'), ['class' => 'form-control', 'placeholder' => '',]) !!}
                    <p class="help-block"></p>
                    @if($errors->has('tel_no'))
                        <p class="help-block">
                            {{ $errors->first('tel_no') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('email', 'Email*', ['class' => 'control-label']) !!}
                    {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => '',]) !!}
                    <p class="help-block"></p>
                    @if($errors->has('email'))
                        <p class="help-block">
                            {{ $errors->first('email') }}
                        </p>
                    @endif
                </div>
            </div>
            
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('password', 'Password*', ['class' => 'control-label']) !!}
                    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => '',]) !!}
                    <p class="help-block"></p>
                    @if($errors->has('password'))
                        <p class="help-block">
                            {{ $errors->first('password') }}
                        </p>
                    @endif
                </div>
            </div>
     <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('d_o_b', 'Date of Birth*', ['class' => 'control-label']) !!}
                    {!! Form::text('d_o_b', old('d_o_b'), ['class' => 'form-control', 'placeholder' => 'dd/mm/yyyy',]) !!}
                    <p class="help-block"></p>
                    @if($errors->has('d_o_b'))
                        <p class="help-block">
                            {{ $errors->first('d_o_b') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('level', 'Level (Trainees only)*', ['class' => 'control-label']) !!}
                    {!! Form::text('level', old('level'), ['class' => 'form-control', 'placeholder' => '',]) !!}
                    <p class="help-block"></p>
                    @if($errors->has('level'))
                        <p class="help-block">
                            {{ $errors->first('level') }}
                        </p>
                    @endif
                </div>
            </div>
             <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('status', 'Status (Trainees only)*', ['class' => 'control-label']) !!}
                    {!! Form::text('status', old('status'), ['class' => 'form-control', 'placeholder' => '',]) !!}
                    <p class="help-block"></p>
                    @if($errors->has('status'))
                        <p class="help-block">
                            {{ $errors->first('status') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('role', 'Role*', ['class' => 'control-label']) !!}
                    {!! Form::select('role[]', $roles, old('role'), ['class' => 'form-control select2', 'multiple' => 'multiple',]) !!}
                    <p class="help-block"></p>
                    @if($errors->has('role'))
                        <p class="help-block">
                            {{ $errors->first('role') }}
                        </p>
                    @endif
                </div>
            </div>
          {{--      <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('course', 'Course*', ['class' => 'control-label']) !!}
                    {!! Form::select('courses[]', $courses, old('courses'), ['class' => 'form-control select2', 'multiple' => 'multiple', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('courses'))
                        <p class="help-block">
                            {{ $errors->first('courses') }}
                        </p>
                    @endif
                </div>
            </div> --}}
            
        </div>
    </div>

    {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

