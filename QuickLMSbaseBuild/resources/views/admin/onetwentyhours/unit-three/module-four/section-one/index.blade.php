@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('content')

<div class="panel panel-default">
	<div class="panel-body">Unit Three Module Four</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading"><h3>Unit 3 | The Teaching and Learning of EFL</h3></div>
	<div class="panel-body"><h4>Module 4 | Teaching Aids</h4></div>


	<div class="panel-body" id="exTab1">
		<ul  class="nav nav-pills">

			<li><a  href="#1a" data-toggle="tab">Study Skills</a></li>

			<li><a href="#2a" data-toggle="tab">Audio-visual aids</a></li>
			<li><a href="#3a" data-toggle="tab">Simple aids</a></li>
			<li><a href="#4a" data-toggle="tab">Technical aids</a></li>
			<li><a href="#5a" data-toggle="tab" class="blueText">Task</a></li>			
		</ul>

	</div>
</div>

<div class="tab-content clearfix">
	
	<div class="tab-pane" id="1a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">Info</div>
						<div class="panel-body">
							<h4>At the end of this unit you will:-</h4>
							<ul>
								<li>a) recognise a variety of aids</li>
								<li>b) understand the importance of 'need first' teaching</li>
								<li>c) be able to prepare your own aids</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="tab-pane" id="2a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Audio-visual aids</h4>
						</div>
						<div class="panel-body">
							<p>Audio-visual aids should be seen as just that - aids. They are props, pieces of equipment which help us to put across our teaching point. We do not advocate building a lesson 'around the aid', we promote the use of audio-visual aids as our 'helpers'. The aid and the topic content are vehicles for language, not the focus of the lesson.</p>
							<p>If you enter every class with a 'bag of tricks', including a flip chart, a DVD player, a laptop, an LCD computer projector, etc, you may eventually become a slave to these aids and deprive your students of the ultimate aim of communication. They will have no time to communicate if they are watching a video, listening to a song and looking at a presentation on the Interactive Whiteboard all in one lesson. That would be a very teacher-centred lesson, and students would use mainly their receptive skills.</p>
							<p>First write your lesson plan, then see where you can generate more enthusiasm and facilitate more understanding by the use of aids. The need comes first, and the need decides the type of aid and the way of using it. The aids must allow us to explain our target structures and concepts simply; otherwise they are not necessary in that particular lesson.</p>
							<p>Nor should audio-visual aids be used as a 'treat' for the students (do this boring exercise, then I'll show you a music video and you'll go home). They should be integrated into the lesson to promote practice in spoken and written English.</p>
							<p>Avoid over-use and the regrettable temptation to allow the aids to become poor substitutes for preparing a lesson! Remember that your students learn through the quality of your teaching and the use of your materials, not by the use of gimmicks or your technological know-how.</p>
							<p>Now that you are aware of the pitfalls, do make use of realia and audio-visual aids to practise language points and to provide extra stimulus. </p>
							<p>Let us look at the teaching/learning aids that are available to us beginning with simple non-technical ones and continuing with modern technical ones.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="3a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Simple aids</h4>
						</div>
						<div class="panel-body">
							<h4>Realia</h4>
							<p>The word 'realia' means ‘real things’. These can include</p>
							<ul class="cleanList">
								<li>the things normally found in the classroom such as pens, pencils and notebooks, windows and tables;</li>
								<li>the things that you bring in for simulation of some sort of real world activity. </li>
							</ul>
							<br>
							<p>The advantage of using objects normally found around the classroom is that they are already familiar to the students, and the practice you can get out of them is extensive. Place things to practise prepositions - in, on, under and behind. Teach comparatives asking which of several similar objects is bigger, smaller, more expensive and so on. Use them to build dialogues asking for things or buying and selling things. But don't over-use items of stationery! Remember they are not usually found anywhere else! </p>
							<p>Real props brought into the classroom provide an extra, interesting stimulus. Set scenes by using any props you can get hold of. Hats to change characters, flags to show nationalities, toy phones, toy vehicles, dolls, tea-sets, etc, can all be very useful.  Bring in a few objects that have special meaning to you and get your students to guess something about you. They can then bring in something that is special to them and talk about it. </p>
							<br>
							<h4>People</h4>
							<p>If you are introducing or practising the use of has, has got, wears, has got on, etc, what better props than your students? Personal questions (usually the first thing students learn) are best practised with the students themselves: ‘I'm Greek, I'm from Crete’ and so on. Students love to learn more about each other. </p>
							<p>Use your own body as a visual aid. As the teacher in an EFL situation you should be able to mime and not be afraid to do so. Throw yourself into it and you'll enjoy it as much as your students will!</p>
							<br>
							<h4>The board: black or white</h4>
							<p>One of the most commonly available and inexpensive visual aids is the blackboard, or more often now, the whiteboard. All you have to do here is to make sure you have chalk or marker pens and a cleaning cloth or sponge.
								Always make sure your board work is clear. Remember that learners tend to copy down everything that the teacher writes and their notebooks are then used for revision! If there is confusion on the board, there will probably be confusion in the students' heads. Where possible, prepare beforehand and don't stand writing/drawing on the board for a long time during the lesson.
							</p>
							<br>
							<h4>Flashcards</h4>
							<br>
							<p>These are pieces of card on which words and/or pictures are written or drawn. They should be large enough to be seen from the back of the class. Have them laminated if you want to preserve them.</p>
							<p>Flashcards can be used for practice and consolidation of vocabulary, building up sentences, prompts for dialogues or drills, etc. They are invaluable, and several sets can usefully be prepared in advance. What is on the board does not move round the class and is always at the front, but flashcards can be held by students or stuck on the wall. </p>
							<p>It is easy to use flashcards to elicit and drill vocabulary. With a picture on the front and a word on the back of each card, you can use them as cues, holding  up a card with the front side facing the students and getting them to tell you the English word for the thing shown on the card. Get them to repeat the word, chorally and individually, to consolidate its meaning and pronunciation.</p>
							<p>Here are a few more ideas for using flashcards:</p>
							<ul>
								<li>With a small class, stick the pictures or the words on the wall and get students to walk to the right picture/word when you say it. Then they need to pronounce the word they are standing at. This works very well with pronunciation practice. Make a storyboard around the class and get students to follow it.
								</li>
								<li>Shuffle the cards and hand them out face down. Students ask each other questions to work out which card their partner has. </li>
								<li>Give students cards showing actions and objects and get them to make a story from the pictures they have: eg. skating, banana, dog... </li>
							</ul>
							<br>
							<h4>Wallcharts</h4>
							<p>These could be helpful but beware! Many wall charts which look beautiful in the shop are virtually useless and end up spending most of their life rolled up in a cupboard. Flexibility is what is needed in a visual aid. If you can prepare charts which will be helpful for various levels, to demonstrate a variety of points and in different teaching situations, then they will be worth the hours of preparation time which they require.  </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="4a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Technical aids</h4>
						</div>
						<div class="panel-body">
							<h4>LCD Projectors</h4>
							<p>The modern equivalent of the once popular OHP (overhead projector, which used transparencies) is an LCD projector connected to a computer.  </p>
							<p>Because of the accessibility of the internet in more and more places around the world, the LCD projector provides you with almost limitless possibilities to project images onto the big screen for the whole class to see directly from your computer screen.  Unlike with an OHP, these are not only static images but videos as well, for example the content of such popular video sites as You Tube and Vimeo.</p>
							<p>The progress made in the development of external data storage devices in recent years allows teachers to store huge amounts of data on tiny USB flash drives. As you prepare materials for your lessons or look for them online, save them on your personal flash drive. Systematise the materials into  folders and subfolders (for example, you can group them according to the class, year, textbook used or level), and it will be easy to find the necessary file when you need it. Then all you have to bring to a classroom already equipped with a computer and projector is your pendrive. If there is no stationary computer, you'll need to bring your laptop.</p>
							<p>But remember to save a back-up copy of all your materials! Just because the USB flash drives are so small, it's equally easy to keep a large amount of information and to lose it. </p>
						</div>
						<div class="panel-body">
							<h4>The Interactive Whiteboard</h4>
							<p>The Interactive Whiteboard (IWB) is a wonderful development. It remains a luxury in many countries and teaching locations but if it is not in your school yet, then it may come soon. So read about it now and be the first to have training when it arrives!</p>
							<p>An IWB is a large display connected to a computer. The computer's desktop is projected onto the board's surface, where users control the computer using a pen, finger, stylus or another device. The board is typically mounted to a wall or floor stand. </p>
							<p>Since an IWB can recognise handwriting, it becomes a combination of a computer and a regular whiteboard. An IWB can import language, pictures or even movie clips from online sites and project them. Your lessons can look very tidy and can be prepared beforehand. The lesson notes can be printed out at the end to reflect what actually went on.</p>
							<p>Here are some ideas for using the IWB with your students:</p>
							<ul>
								<li>Bring up a web page and do a scan-reading exercise for information. (Cache or bookmark the page first.)</li>
								<li>Project a text and underline features.</li>
								<li>Type in students’ suggestions and make them into a worksheet for the end of the lesson.</li>
								<li>Let students do PowerPoint presentations to the rest of the class. </li>
								<li>Do online quizzes such as ones about the week’s news or a controversial issue as a whole class</li>
							</ul>
						</div>
						<div class="panel-body">
							<h4>Audio recording and playback equipment</h4>
							<p>This is essential because students need to be exposed to a variety of voices and accents; otherwise they will eventually only understand you! There is a great deal of pre-recorded material available in ESOL, and your textbook will probably come with a set of CDs or a link to audio tracks on the web. You can use your own recorded materials as well. Discussions among you and your friends on a variety of contemporary subjects, a conversation in a shop or pub, directions in the street, etc, can prove invaluable aids in overseas teaching situations.</p>
							<p>It can also be useful to record the voices of your students. You can provide them with helpful feedback on their reproduction of English if you play back a dialogue done during pair work, for example. An option still available in some places is a cassette recorder with a record function. This can be moved around the classroom and does not have any compatibility problems. More modern ways of recording include</p>
							<ul>
								<li>a digital Dictaphone</li>
								<li>computer software, such as the free application called Audacity, and a microphone</li>
								<li>a hand-held digital camera</li>
								<li>your smartphone</li>
							</ul>
						</div>
						<div class="panel-body">
							<h4>Video recorder/DVD</h4>
							<p>Bringing the visual elements of communication into our classroom means we can talk about and train students in understanding body language, facial expressions and visual clues from the setting as these are all things they should be using when they are communicating ‘in the real world’ in a foreign language. 
							</p>
							<p>The extracts that you choose from a DVD or TV programme should be visually interesting - not just a ‘talking head’. Choose something with clear speech and a clear topic, even if some of the vocabulary is difficult. Keep it short. We all stop listening properly after 20 minutes or so. </p>
							<p>If you have access to video recording equipment in the school, even on mobile phones, take advantage of this to film the students as they act out dialogues. They will be greatly encouraged and stimulated by watching themselves speaking English on film. As you can stop the recording, fast forward and reverse it; this is invaluable for correction.</p>
						</div>
						<div class="panel-body">
							<h4>Language labs</h4>
							<p>Many schools have some sort of a room that you can book with your students for use on a regular or irregular basis. In some schools this is called a self-access centre or SAC. The advantage of such a room, whatever is in it, is the chance for the students to direct their own learning and have a break from the teacher-led classroom. They may be allowed to enter on their own or they may need to be taken in by their teacher.</p>
							<p>One type of a self-access centre is called the language laboratory. Originally, these were rooms with a number of consoles connected to a master console operated by the teacher. Each console had headphones where students could listen to recordings and speak into a microphone at their own pace. The consoles have recently been replaced by computers in digital language labs.  </p>
							<p>There are some teachers who see language labs as the biggest white elephant ever introduced to the language-teaching world. There are others who believe that they are truly invaluable as a time-saving teaching device. If your school has one, you may already have a view, if not and your school is considering the purchase and needs your opinion, here are some points to bear in mind.</p>
							<ol>
								<li>It must be properly serviced and maintained and therefore requires someone, preferably on the spot, who can do that.</li>
								<li>It needs a considerable supply of materials which wear out.</li>
								<li>It is expensive to install and to run.</li>
								<li>It takes the space of a classroom.</li>
								<li>It must justify the expense in your size of school. Perhaps the money would be better spent on buying a few extra laptops, digital cameras or projectors.</li>
							</ol>
							<p>A software-only language lab changes the concept of where and what a language laboratory is. Software can be installed and accessed on any networked PC placed in any location. Software-only systems can be located in the same room, different rooms or even on different campuses.</p>
						</div>
						<div class="panel-body">
							<h4>The most important aid: your syllabus or textbook</h4>
							<p>Many teachers find themselves in a situation where they have to stick to a rigid course. That can be terribly frustrating. However the opposite can be equally difficult. Teachers asked to teach their classes without a detailed syllabus will find themselves faced with the impossible task of collecting and creating all their own material. </p>
							<p>The ideal is to be allowed (or encouraged) to roughly follow the syllabus, using your own materials where appropriate. Teachers should be free, within limits, to vary the diet of any ESOL class. </p>
							<p>The textbook is our friend because:</p>
							<br>
							<ul class="cleanList">
								<li>It sets out ‘bits’ of language to teach.</li>
								<li>The Teacher's Book helps the teacher plan lessons.</li>
								<li>It has reading and listening materials.</li>
								<li>It helps with managing time. </li>
								<li>It saves on photocopying, provided that each student has a copy of the book.</li>
							</ul>
							<p>The way you use the textbook should take into account the specific needs of your students. Textbooks are written by teams of writers for an abstract class, but you are always teaching a specific class whose interests and needs you know better than anyone else. That is why faithfully following the order of units and activities in a single book without adding, removing or adapting anything can be counterproductive.</p>
							<p><i><span class="underline">Adapting </span></i>means changing your materials so they are more suitable for your class. Sometimes reading and listening activities are too long and complicated, so cut the texts into chunks and write different exercises. In some books every chapter has the same order to the activities, eg. look at the pictures, talk about them, then a listening, then a reading. Bring variety into the lesson by varying that order. </p>
							<p>Unless the school administration demands that each and every chapter be used and you are not able to convince them otherwise, feel free to remove a unit or part of a unit of the course book from your personal syllabus. Sometimes there are too many boring exercises. Sometimes the topic is not one that your students are interested in. Sometimes the materials are very out of date such as an interview with Margaret Thatcher in a unit talking about contemporary politics. Your textbook may be 10 or more years old. </p>
							<p><i><span class="underline">Adding</span></i>in a more up-to-date newspaper article or a different song of your own can be much more interesting. Also look for pictures from websites or magazines to replace or add. You may want to add some extra information on a grammar point too.</p>
							<p>However be careful:</p>
							<p>If you add too much, then it may be easy to lose continuity of the course and your lessons may end up too eclectic and hard to follow. If you remove too much, then you will have a lot of work to do to fill up the time. Excessive changes may cause your students to lose trust and interest in the course book. So maintain a balance. And don’t add or remove at the last minute. Look ahead in the course book and get your materials ready.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="5a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Task</h4>
						</div>
						<div class="panel-body">
							<p>Prepare a set of 8 flashcards for use in class. Each card is to show a different nationality, for example French, Spanish, Chinese, Mexican, etc. </p>

							<p>Include more than one image on a card but not too many, so that the cards are not too eclectic and the images are easy to see from a distance. The images may include a country's flag, a person wearing a national costume, a famous landmark, etc. Good presentation will gain better marks.  Avoid using pictures which could cause offence.</p>

							<p>Illustrations taken from the internet are acceptable, however you need to demonstrate that you have done some work on the design and not copied ready flashcards from the web. </p>

							<p>Remember that the cards are used as prompts to elicit the target vocabulary, therefore the countries or nationalities should not be written on the cards (the words would usually be written on the back). You only need to show the front of the cards.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



</div>

@endsection