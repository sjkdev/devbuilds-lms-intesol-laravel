 @inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('content')




<div class="panel panel-default">
	<div class="panel-body">Unit Three Module Two</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading"><h3>Unit 3 | The Teaching and Learning of EFL</h3></div>
	<div class="panel-body"><h4>Module 2 | Listening and Reading</h4></div>


	<div class="panel-body" id="exTab1">
		<ul  class="nav nav-pills">

			<li class="active"><a  href="#1a" data-toggle="tab" style="background-color:#fff;">Study Skills</a></li>

			<li><a href="#2a" data-toggle="tab">Part 1. The Four Skills - Introduction</a></li>
			<li><a href="#3a" data-toggle="tab">Part 2. Listening skills</a></li>

			<li><a href="#4a" data-toggle="tab" class="orangeText">Self Check 1</a></li>
			<li><a href="#5a" data-toggle="tab" class="orangeText">Self Check 2</a></li>
			<li><a href="#6a" data-toggle="tab">Part 3. Reading skills</a></li>
			<li><a href="#7a" data-toggle="tab" class="orangeText">Self Check 3</a></li>
			<li><a href="#8a" data-toggle="tab">Part 4. Planning a receptive skills lesson</a></li>
			<li><a href="#9a" data-toggle="tab" class="blueText">Task</a></li>
		</ul>

	</div>
</div>

<div class="tab-content clearfix">

		<div class="tab-pane active" id="1a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">

					<div class="panel panel-default">
						<div class="panel-body">
							<h4>At the end of this unit you will:-</h4>
							<ul>
								<li>a) be familiar with the four primary language skills</li>
								<li>b) understand why learners have difficulty with listening</li>
								<li>c) be able to identify a range of skills needed for successful listening and reading</li>
								<li>d) now ways of training learners to develop those skills</li>
								<li>e) be able to plan a receptive skills lesson</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="tab-pane" id="2a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Part 1. The Four Skills - Introduction</h4></div>
						<div class="panel-body">
							<p>There are 4 primary language skills, referred to as speaking, listening, reading and writing. It is important to distinguish between them (though they are very much interlinked in many situations) as they demand different abilities. For example, giving a speech requires a different skill from understanding what you hear when someone else gives a speech, or from presenting the information in the speech in written form, or from reading what someone else has written on the subject. All these four skills must be included in a general English teaching syllabus.</p>
							<p>There are courses in English for Specific Purposes, which need to be heavily weighted in one or two of the skills, eg courses for telephone operators teach more listening and speaking. However, even courses as specific as that need a certain amount of the other skills – a telephonist may need to read a memo, or leave a written message for someone, thus needing the skills of reading and writing.</p>
							<p>There are further sound reasons for including all the four skills in the syllabus and often in a single lesson:</p>
							<ol>
								<li>People get tired after a certain period of activity and they need
								    a change of activity. The saying 'a change is as good as a rest' 
								    certainly applies to the language classroom.
								</li>
								<li> In any group there will be several different styles of learner. Some students need to write what they learn in oral practice because they get comfort from the written word, others will not need to see the written word. If you give the class the chance to hear and say and see and write a piece of language you will be catering for all styles of learner. </li>
							</ol>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Skills and the textbook</h4></div>
						<div class="panel-body"><p>
							Some textbooks put emphasis on one or two skills and either omit or downgrade the others. Books which lay great importance on grammar often focus mainly on reading and writing, whereas audio-lingual course books concentrate on listening and speaking (they are, however, course books which have been written and need to be read). Before taking on a new class, look carefully at the textbook to see if it provides sufficient practice in all the four skills. Plan ahead and be ready with supplementary material should the book be lacking in practice in any particular skill. Writing is the most commonly neglected! Authentic texts, readers, recordings of dialogues, extracts from DVD and contemporary news downloads or articles from the internet will be very useful for this purpose. Start making your collection now!
						</p>
						<p>
							Although real life communication rarely consists of only one of the four skills, it is important to look at the skill areas separately to begin with in order to identify what learners need to be able to do, and how we, as teachers, can best help them acquire that ability.
						</p></div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Two types of skills</h4></div>
						<div class="panel-body"><p>
							The 4 skills are traditionally divided into receptive and productive skills. As the names suggest, the receptive skills are those which enable the learner to understand language and to receive information via language. They are listening and reading. The productive skills are those which enable the learner to produce language. They are speaking and writing.
						</p>
						<p>One misconception is that the receptive skills are passive and the productive skills are active. Because any act of listening or reading is supposed to have an aim – whether it be understanding the main idea of a text, identifying the characters in a play or deciding on your attitude to the speaker's opinion – the listener or reader is actively involved in the process.</p>
						<p>In this module our focus will be on the two receptive skills: listening and reading. </p></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="3a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Part 2. Listening skills</h4></div>
						<div class="panel-body"><p>
							When teaching listening skills, we have to make sure a range of training techniques are employed and not rely on students to 'pick up' by themselves what the language sounds like. This rarely happens, and a failure to employ training techniques may lead to the situation where learners may be highly competent in written skills, or have an excellent knowledge of grammar, but be unable to comprehend the simplest of listening passages. It is essential that we recognise areas of potential difficulty and plan our listening activities and materials accordingly.
						</p>
						<p>First, however, we need to consider problem areas in listening and then possible solutions to those difficulties.</p></div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="tab-pane" id="4a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><h3>Self Check 1</h3></div>
						<div class="panel-body"><p>
							Listen to the sound file (Listening Task.mp3) and complete the table below:-
						</p>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">Audio</div>
					<div class="panel-body">
						<audio controls>
						 
						  <source src="https://res.cloudinary.com/dhdlgjrtu/video/upload/v1525271856/Listening_Task.mp3" type="audio/mpeg">
						  Your browser does not support the audio element.
						</audio>

					</div>
				</div>


				<div class="panel panel-default">
					{!! Form::open(['action' => 'Admin\Int120u3m2s1t1Controller@store', 'method' => 'POST']) !!}
					<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">
					<div class="panel-body">
						<div class="col-md-6"><h3>Problems learners have</h3>

							<div class="panel-body">
								<div class="form-group">
									{!! Form::label('u3m2s1q1', 'Item 1:') !!}
									{!! Form::text('u3m2s1q1', null, ['class' => 'form-control'])!!}
								</div>
								<br>
								<div class="form-group">
									{!! Form::label('u3m2s1q2', 'Item 2:') !!}
									{!! Form::text('u3m2s1q2', null, ['class' => 'form-control'])!!}
								</div>
								<br>
								<div class="form-group">
									{!! Form::label('u3m2s1q3', 'Item 3:') !!}
									{!! Form::text('u3m2s1q3', null, ['class' => 'form-control'])!!}
								</div>
								<br>
								<div class="form-group">
									{!! Form::label('u3m2s1q4', 'Item 4:') !!}
									{!! Form::text('u3m2s1q4', null, ['class' => 'form-control'])!!}
								</div>
								<br>
								<div class="form-group">
									{!! Form::label('u3m2s1q5', 'Item 5:') !!}
									{!! Form::text('u3m2s1q5', null, ['class' => 'form-control'])!!}
								</div>
								<br>
								<div class="form-group">
									{!! Form::label('u3m2s1q6', 'Item 6:') !!}
									{!! Form::text('u3m2s1q6', null, ['class' => 'form-control'])!!}
								</div>
								<br>
							</div>

						</div>
						<div class="col-md-6"><h3>Ways we can help</h3>

							<div class="form-group">
								{!! Form::label('u3m2s1q7', 'Item 1:') !!}
								{!! Form::text('u3m2s1q7', null, ['class' => 'form-control'])!!}
							</div>
							<br>
							<div class="form-group">
								{!! Form::label('u3m2s1q8', 'Item 2:') !!}
								{!! Form::text('u3m2s1q8', null, ['class' => 'form-control'])!!}
							</div>
							<br>
							<div class="form-group">
								{!! Form::label('u3m2s1q9', 'Item 3:') !!}
								{!! Form::text('u3m2s1q9', null, ['class' => 'form-control'])!!}
							</div>
							<br>
							<div class="form-group">
								{!! Form::label('u3m2s1q10', 'Item 4:') !!}
								{!! Form::text('u3m2s1q10', null, ['class' => 'form-control'])!!}
							</div>
							<br>
							<div class="form-group">
								{!! Form::label('u3m2s1q11', 'Item 5:') !!}
								{!! Form::text('u3m2s1q11', null, ['class' => 'form-control'])!!}
							</div>
							<br>
							<div class="form-group">
								{!! Form::label('u3m2s1q12', 'Item 6:') !!}
								{!! Form::text('u3m2s1q12', null, ['class' => 'form-control'])!!}
							</div>
							<br>
							<div class="form-group">
								{!! Form::label('u3m2s1q13', 'Item 7:') !!}
								{!! Form::text('u3m2s1q13', null, ['class' => 'form-control'])!!}
							</div>
							<br>

						</div>
						{!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}
						{!! Form::close() !!}
					</div>
				</div>

			</div>


	<div class="row col-md-12">
		<div class="col-md-12">
			
	

			<div class="panel panel-default">
				<div class="panel-heading"><h4>Different kinds of listening (listening sub-skills)</h4></div>
				<div class="panel-body"><p>
					Students should be encouraged to practise extensive listening, ie listen to the English language from various sources outside the classroom, listen for pleasure. 
				</p>
				<p>
					In this section we will focus on the listening activities that take place inside the classroom and are referred to as intensive listening, ie listening to relatively short dialogues or texts with a specific purpose. Such listening involves two main sub-skills: listening for gist and listening for detail.

				</p></div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading"><h4>Listening for gist</h4></div>
				<div class="panel-body">
					<p>There are times when we listen to something in order to get a general idea of the content, or 'the gist', rather than specific details. Sometimes we need to recognise the function of the dialogue – for example, is the speaker making arrangements, expressing an opinion, making an enquiry; are the speakers discussing their opinions of a book they have both read or are they having a row? At higher levels – intermediate and above – students need practice in recognising attitude (by work on intonation patterns) and recognising changes in direction or topic when listening to speeches, long texts, or taking notes in university lectures.</p>
					<p>A pre-listening gist question can prepare the students and encourage them not to worry about details but to concentrate on understanding the general idea. They will listen with the question in mind and then give their answer.</p>
					<p>Post-listening questions such as 'How would you describe A's feelings?' allow them to interpret what they have understood without worrying about specifics.</p>

				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading"><h4>Listening for detail</h4></div>
				<div class="panel-body"><p>
					When we listen for detail, our attention is focused and we are searching for specific information in the listening passage. For example, we could be listening for details of the weather in our region, a train departure time or the football results of our favourite team. As we listen carefully, we select the information we require and ignore the rest. Because we know beforehand what we want to hear, it becomes easier to concentrate and focus our attention to listen selectively. There are several ways of training our students to develop the sub-skill of listening for detail.
				</p>
				<p>a) Prediction</p>
				<p>By asking students to predict what they are going to hear, based on a topic word or sentence, you are preparing your learners for what to expect. Guided questions help them decide what to listen for, and keep them focused on the main points. This technique can be repeated towards the end of a listening passage by asking students to predict the ending. This can be done in pairs or groups and it keeps students actively involved in the listening process.</p>
				<p>b) Comprehension exercises</p>
				<p>Different types of exercises will ensure that listening skills are being developed. Exercises can be set midway as well as at the end of a listening passage, and can be in the form of true/false questions, 'wh' questions (who, what, where), sentence completion, gap-filling, error correction, table filling, form-filling, etc.</p>
				<p>c) Listening for language items</p>
				<p>An exercise may require that you listen and identify specific lexical or grammatical items in a text, eg note all the past participle forms of verbs or all the superlative adjectives</p>
				<p>Listening for gist and listening for detail should be carried out separately from each other. It is difficult for students to do both at the same time. Check tasks to make sure that you do not have them trying to do too many things at once. When a new listening passage, a monologue or a dialogue, is introduced, students will naturally want to know what it is generally about first and discover details later. So it's logical to begin with gist exercises for the first listening and give detailed exercises for the second listening.</p>
				<p>Students often find listening exercises to be one of the most stressful parts of any lesson. The most stress is aroused when students are asked to listen ‘cold’ (ie they are not prepared) and then perform an exercise. Therefore, it is important to activate schemata before they listen. That means discuss the general topic of the text and make students aware of what they already know about it, so that the new information they hear will be laid on some sort of a foundation. That, in turn, will improve understanding.
				</p>
			</div>
		</div>
			</div>
	</div>
	</div>
</div>
</div>


		<div class="tab-pane" id="5a">
			<div class="#">
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading"><h4>Self Check 2</h4></div>
							<div class="panel-body">
								<p>Identify the type of listening in the following activities:</p>
								<ul class="cleanList">
									<li>a) listening to a group discussing the Royal Family and deciding whether            
									    the general feeling is pro- or anti- Royalists
									</li>
									<li>
										b) listening to the travel news for motorway hold-up information
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="tab-pane" id="6a">
			<div class="#">
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>Part 3. Reading skills</h4>
							</div>
							<div class="panel-body">
								<p>It does not necessarily follow that because a student can read, he/she is an efficient reader. Training in the skills involved in reading must be given as reading is not an inbuilt skill. Reading is not a passive process, either. It is an active process in which practice in all the sub-skills is vital as no improvement can be effected without guided practice.</p>
								<p>Different kinds of reading (reading sub-skills)</p>
								<p>A student needs to master different ways of reading a text. The purpose for which the student requires the language determines the particular sub-skills of reading which will be needed. </p>
								<p>Think back to the listening section. As with listening, students need to be encouraged to read extensively, ie read a variety of texts on their own, such as fiction, magazine articles, or Wikipedia. When they encounter the same structures and vocabulary multiple times, their ability to understand written English improves and they are able to deal with more and more difficult texts.</p>
								<p>The two common types, or sub-skills, of intensive reading in the classroom, are skimming (or 'reading for gist') and scanning (or 'reading for detail').</p>
								<p><b>Skimming</b> involves running your eyes over a piece of text in order to understand its overall idea. For example, you may want to ascertain if it is relevant to your needs and whether it's worth being read more carefully. You may want to establish if any exciting events are described in the text or it is just an opinion piece. You may need to find out whether the text is negative or positive in tone. Or, if the author comments on a conflict, you may want to find out which side he/she is on or whether he/she tries to remain neutral.  </p>
								<p><b>Scanning</b> involves looking for specific information in the text. For example, you want to find out the score of a game between Real Madrid and Barcelona and you want to know whether Christiano Ronaldo has scored. You will then read through the match report looking for numbers and identifying which of them refer to the final score and you will also look for any mention of Christiano's name in the text and, when you locate it, you'll read around that to find out whether he scored a goal. </p>
								<p>Scanning may also be in the form of looking for specific language items or structures, eg “find all instances of the present perfect” or “find all descriptive adjectives in the text”.</p>
								<p>In another classification there are four sub-skills of reading.</p>
								<p>The first sub-skill involves 'superficial understanding' and is used in reading a newspaper or detective story, for example, in order to pick out the main points of the story, look for clues etc. The main concerns here could be 'what is going on?' ‘why are they doing what they are doing?’ or 'how will it all end?' This is quite similar to what happens during extensive reading, where you read large amounts of text for pleasure. </p>
								<p>The following techniques are more intensive.</p>
								<p>The second sub-skill is described as 'imaginative understanding' and is used in the study of literature. A task requiring imaginative understanding could be, for example: </p>
								<p>Where Seamus Heaney says: <i>‘I rhyme to see myself, to set the darkness echoing’</i> - what is he trying to tell us about his attitude to poetry? </p>
								<p>The third sub-skill is referred to as 'precise understanding' and it involves thorough comprehension of a text or parts of a text with focus on the exact meaning of every word and sentence. (Unfortunately, sometimes this turns out to be the only sub-skill practised by students in some classes).</p>
								<p>The final sub-skill involves 'practical understanding', and this is when we read in order to act upon what we read. This is something we do with packets and instructions - which button to press to make the TV work or how many pills to take and how often.</p>
								<p>Before setting reading tasks for your students, you need to decide what your aim is. For instance:</p>
								<ul class="cleanList">
									<li><i>Do you want to train your students to answer questions precisely?</i></li>
									<li><i>Do you want to increase vocabulary? </i></li>
									<li><i>Do you want your students to decide if the text is relevant to their needs? </i></li>
									<li><i>Are you looking at the grammar of certain types of texts?</i></li>
									<li><i>Do you want the students to act on the information?</i></li>
								</ul>
								<p>Depending on your answers to these questions, you will select the reading text and the reading tasks. Your choice will depend on the nature of the class - are they general English students, University students or Business English students? Where are they now? In their own country or in an English-speaking environment? </p>
								<p>Similarly to listening, begin a lesson by activating schemata, ie discussing with students what they already know about the general subject of the text, thus building the foundation for the new information they are going to read.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="7a">
			<div class="#">
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>Self Check 3</h4>
							</div>
							<div class="panel-body"><p>Here are two texts and some examples of exercises which have been written to improve students' reading skills. Study them carefully – which sub-skills are being taught? Who are they suitable for?</p></div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>Exercise 1</h4>
							</div>
							<div class="panel-body">
								<p>Read through the text quickly and say whether it </p>
								<ul>
									<li>describes what a typical English house looks like</li>
									<li>describes how a typical English house has changed in the recent decades</li>
									<li>describes the writer's attitude to a typical English house</li>
								</ul>
								<h5>THE TYPICAL ENGLISH HOUSE</h5>
								<p>A detached house stands on its own. A semi-detached house is joined to the house next door along the central wall. The ‘semi’ is the most typical kind of English house. It has front and back gardens and often a garage at the side.</p>
								<p>The outline on the next page shows the ground floor of a typical ‘left-hand’ semi. It has a lot of rooms, but if you look at the rooms you will see that they are all quite small. People often ‘live’ in the dining room, keeping the lounge for visitors. This means that they spend most of their time in a room only about 11' 6" x 10' 6". The dining room in this house is connected to the lounge by a room divider, and the kitchen is connected to the dining room by a hatch in the wall. Both the lounge and the dining room have open fireplaces. The kitchen has a sink unit on the back wall of the house and the back door is on the left. The front door opens into the hall. There is one other way in and out of the house and this is through French windows which open onto the garden to the rear of the dining room. The house from front to back measures 24' 6", the lounge being 13' in length.</p>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>Exercise 2</h4>
							</div>	
							<div class="panel-body"><p>
								Now read the text more carefully and answer the following questions about the house.
							</p>
							<ul>
								<li>[diagram] Put in the dimensions which you know.</li>
								<li>[diagram] Mark with a cross the location of the attached "semi".</li>
								<li> [picture] - which room is this?</li>
							</ul>
						</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>Exercise 3</h4>
								<ol>
									<li>Figure 1 marks the position of a ..........</li>
									<li>Figure 2 marks the position of a ..........</li>
									<li>Figure 3 marks the position of a ..........</li>
									<li>The two figures 4 mark the position of the ..........</li>
									<li>Figure 5 marks the position of the .........</li>
									<li>Figure 6 marks the position of the ..........</li>
									<li>Figure 7 marks the position of the ..........</li>
								</ol>
							</div>
							<div class="panel-body"></div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>EXAMPLE TEXT 2</h4>
							</div>
							<div class="panel-body">
								<h5>Pre-reading question</h5>
								<p>The following words will be used in the text. What do you think the text will be about? What events will happen in the text? Make notes of your ideas.</p>
								<ul class="cleanList">
									<li>careless</li>
									<li>criticized</li>
									<li>violation</li>
									<li>freeway</li>
									<li>damage</li>
								</ul>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>Exercise 1</h4>
							</div>
							<div class="panel-body">
								<p>
								Read the following text and answer the question after each paragraph. Then read on and find out if your guesses are correct. [Note: the text is projected onto a screen. Students are not shown the next part of the text until the teacher has elicited their answer to the question on the previous part].
								</p>
								<ul>
								<li>Colleen was in a hurry, which made her driving even more careless than usual. Her boyfriend Simon had already criticized her many times for failing to stop completely at stop signs. That's what they call a “California, or rolling, stop,” he told her.</li>
								<li> “If the cops catch you sliding through a stop like that,” he said, wagging a finger at her, “they'll give you a ticket for running a stop sign. That's a moving violation. That means at least a $100 ticket, plus eight hours of driving school for another $30.” <br><i>What do you think Colleen said in reply?</i></li>
								<li> “I know, I know,” she replied. “But I never do it when they're around, so how can they catch me?” Simon was about to tell her that cops have a habit of suddenly appearing out of nowhere, but Colleen told him to stop thinking so negatively. “You are bad luck,” she said. “When you talk like that, you make bad things happen.” He told her that life doesn't work that way. <br><i>Why do you think Colleen was in a hurry on that particular day?</i></li>
								<li>Colleen was in a hurry because she needed to drop off a package at the post office. It had to get to New York by Wednesday. She exited the freeway and pulled up at the stop sign. No cars were coming. It was safe to pull out. She hit the gas pedal. <br> <i>What do you think happened next?</i></li>
								<li>Bang! The car in front of her was still sitting there. The driver was a young woman, who got out of her car, walked back to look at the damage to her new car, and started yelling at Colleen. <br> <i>“What were you waiting for?” Colleen demanded.</i></li>
							</ul>
							<p>Now read the notes you made before you read the text. Were any of your initial guesses about the text correct?</p>

						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Exercise 2</h4>
						</div>
						<div class="panel-body">
							{!! Form::open(['action' => 'Admin\Int120u3m2s1t2Controller@store', 'method' => 'POST']) !!}
														<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">
							<p>Make notes of the expressions and structures used in the text to</p>
							<ul>
								<li>a) warn somebody</li>
								{!! Form::text('u3m2s1q14', null, ['class' => ' form-control'])!!}
								<li>b) reply tp a warning</li>
								{!! Form::text('u3m2s1q15', null, ['class' => ' form-control'])!!}
							</ul>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Exercise 3</h4>
						</div>
						<div class="panel-body">
							<p>Are the following statements true or false?</p>
							<ol>
								<li>It was the first time that Simon criticised Colleen for her driving style.</li>
							
								{{ Form::select('u3m2s1q16', ['true', 'false']) }}
								   
								<li>Colleen believed that if she couldn't see any cops when going through a stop sign, she wouldn't be caught.</li>
								{{ Form::select('u3m2s1q17', ['true', 'false']) }}
								   
								<li>Colleen was in a hurry to get to New York.</li>
								{{ Form::select('u3m2s1q18', ['true', 'false']) }}
								   
								<li>The young woman reacted calmly to the incident.</li>
								{{ Form::select('u3m2s1q19', ['true', 'false']) }}
								   
								<li> The young woman's car was damaged.</li>
								{{ Form::select('u3m2s1q20', ['true', 'false']) }}
								   
								
							</ol>
							<br>
							{!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}
						   {!! Form::close() !!}
							
						</div>
						
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Exercise 4</h4>
						</div>
						<div class="panel-body">
							<p>Explain what is meant by the following words ie what the author is referring to.</p>
							<ol>
								<li>1. that – line 3</li>
								<li>2. it – line 9</li>
								<li>3.  they – line 9</li>
								<li>4.  it – line 16</li>
								<li>5.  there – line 19</li>
							</ol>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>

		<div class="tab-pane" id="8a">
			<div class="#">
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>Part 4. Planning a receptive skills lesson</h4>
							</div>	
							<div class="panel-body"><p>
								The main stages of a receptive skills lesson, ie a lesson in which reading or listening skills are practised, are as follows:
							</p>
							
								<h4>Before Reading / Listening</h4>
								<p>In real life, we bring a level of knowledge, our expectations and purpose, ie why we are reading / listening, what we already know, the knowledge and expectations of the topic and the text etc. This needs to be replicated in the classroom. Activate schemata, don't make them listen or read 'cold'.</p>
							<h4>While Reading / Listening</h4>
							<p>This also needs to resemble real life reading. We ‘process’ the texts we read in different ways depending on the type of text and the aims of reading. We can read for the main idea, specific facts, for practical use of the information, etc, ie use different strategies or sub-skills of reading.</p>
							<h4>After Reading / Listening</h4>
							<p>What do we do after we have processed the text? Depending on purpose we respond in different ways, eg tell someone about it, fill in a form, summarise the information etc.</p>
							<p>Breaking these main stages down into smaller stages, your lesson plan could be like this:</p>
							<h4>Before Reading / Listening</h4>
							<ol>
								<li><span class="underline">Arouse interest in the general topic.</span> Find out what students already know and what their experience of the topic/text type is. <br>Rationale: To activate the knowledge that will assist learners in understanding the text. To provide motivation to read / listen.</li>
								<li><span>Set the scene.</span> Give any essential background information. This context will vary according to the nature of the text, but may include such information as who is writing/speaking, to whom, about what topic, why? where? <br>Rationale: To provide students with a context to help them decode meaning. To activate students’ knowledge and enable them to make predictions about the type and nature of the text they are about to read / hear.</li>
								<li><span class="underline">Teach or elicit any essential vocabulary.</span> Only focus on vocabulary which is essential to the understanding of the text. <br>Rationale: To avoid students ‘blocking’ when they don’t understand a key vocabulary item.</li>
								<li><span class="underline">Set an achievable task for the first reading / listening.</span> This will usually be a gist comprehension task.
								<br>Rationale: To provide learners with a reason to read / listen.
									To motivate students and develop confidence that the text is manageable.
								</li>
							</ol>
							<br>
							<h4>While Reading / Listening</h4>
							<ol>
								<li>Give students time to complete the task, listening / reading again if necessary, then check answers with the whole class. If reading, they need to be encourage to skip-read, ie read quickly to get the gist. <br>Rationale: To ensure that everyone has completed the first task and has a general understanding of the text. This could be identifying the main idea, the writer's attitude, or follow the general organisation of the text.</li>
								<li>Set further tasks to guide more detailed comprehension of the text, and give students time to do this. The nature of the task will depend on the text and should be determined by how the text would be processed in real life. <br>Rationale: To help students to identify specific information, infer opinion etc. depending on the nature of the text.</li>
								<li>Get learners to check their answers in pairs. <br>Rationale: To focus students on problem areas and to provide support for individuals.</li>
								<li>Check the answers with the class and refer back to the text as necessary. <br>Rationale: To ensure that everyone understands the text and understands how the answers have been arrived at.</li>
							</ol>
							<br>
							<h4>After Reading / Listening</h4>
							<p> There is a range of options here and what you decide to do depends on the text type and the aims of the lesson. Here are some possibilities:</p>
							<ol>
								<li>Select some useful vocabulary of grammar items from the text and develop understanding of these through formal exercises or oral use. <br>Rationale: To expand students’ vocabulary and grammar.</li>
								<li>Initiate a discussion or stage a role-play based on the subject of the text. <br>Rationale: To develop speaking skills and enable students to use the language of the text in other contexts.</li>
								<li>Set a writing task (eg write a letter to someone in the text, fill in a form or write a for-and-against essay about the topic in the text). <br>Rationale: To develop writing skills and enable students to use the language of the text in other contexts.</li>
							</ol>
							<br>
							<p>Example Reading and Listening Activities</p>
							<p>Pre-reading/Listening:</p>
							<ol>
								<li>Predicting: Ask students to look at the title and / or pictures and try to identify the topic.
</li>
								<li> Lead-in Questions: Ask students to ask each other questions about the topic.</li>
								<li>Brainstorm: Ask students to brainstorm and list everything they can think of related to the topic within a set time-limit.</li>
								<li>Pre-Reading / Listening Questions: Ask students to write three or four questions about the topic. As they read / hear the text, they should see if their questions were answered.</li>
								<li>Speed Read: Ask students to read the first paragraph, the first sentence of subsequent paragraphs, and the last paragraph. The students then make predictions about the passage, or answer comprehension questions.</li>
								<li>Word Clouds: <br>
   								Version 1 (Brainstorming): Write the topic on the board and draw a cloud shape beneath it. Elicit any words that students associate with the topic and write them in the cloud. Assign pairs or small groups and ask students to connect the words in sentences related to the topic. Follow up with a class check.
							   <br>Version 2 (Predicting): Select several words from the reading or listening, directly related to the topic. Try to make the connections a little obscure. Draw a cloud shape and write in the words one at a time. Students should try to guess the topic as you write up the words. Then in pairs or groups students should try to make predictions about the text based on the words, and try to guess in what ways the words are related to the topic. Elicit the predictions from the students without giving away the correct answers. With reading texts you could follow up with a scan reading where the students check their predictions.
</li>
								<li>Mind Maps (Nuclear notes - see the Study Skills unit for an example): This allows students, in pairs or small groups, to brainstorm the topic by starting with one word and linking it to new words to create word fields.</li>
								<li>Mix and Match: Make up and write a headline for every paragraph in the article. Make enough copies for each pair/small group. Cut and separate each headline and mix them up. Students then have to place the headlines in the correct order and try to speculate about the text in detail.</li>
							</ol>
							<p>While Reading/Listening:</p>
							<ol>
								<li>Comprehension Race: This can be carried out individually or in teams. With the reading texts closed, the teacher asks the first comprehension question and the students open their books and try to find the answers as quickly as possible. The first person or team gets a point for each correct answer.</li>
								<li>Information Sharing: Assign pairs. ‘A’ students receive a number of general comprehension questions, and ‘B’ students receive an equal number of different questions. Check vocabulary. Set a time limit of two or three minutes and have students read for the answers to their questions. Then place students into A and B pairs and have them exchange information.</li>
								<li>Do-It-Yourself Quiz: Students individually scan the reading text. Each student then writes three of four questions about the text. Then, in pairs, students ask each other their questions. This can be done with books open or closed.</li>
								<li>Co-operative Reading: Elicit / teach vocabulary and assign pairs. Allocate one or two paragraphs of the text to each pair. The pairs should then read and summarise their paragraphs. Regroup the students so that each student in the group represents a different paragraph. Each student should then summarise their paragraph to the group in the correct sequence.</li>
								<li>Read and Listen: Elicit / teach vocabulary. In pairs, student A reads the first paragraph while student B listens and takes notes. After student A has finished reading they should ask student B a few general comprehension questions. Change roles after each paragraph. Model with the first paragraph. </li>
								<li>Scanning to find information: Prepare several factual questions or tasks that can be answered from the text and write them on the board. Assign pairs. Students discuss these and guess an answer to each question. Students then scan the passage to check their answers. Class check.</li>
								<li>Cloze Reading: Make two copies of a reading text with different words blanked out for A and B students. Assign pairs. Each student then asks their partner questions to elicit the missing words, eg if a student  has the sentence ‘I had a ………….day’, student A must ask student B, ’What kind of a day did you have?’ Student B would then answer according to their text, ’You had a terrible day.’</li>
								<li>Find the mistakes: Choose a student to read the first paragraph of a text. Then model summarising the text. However, you must change some of the details, and the students should try to identify your ‘mistakes’. Avoid changing words by using synonyms; you want students to think about meaning, not just words. Students can then practise this format in pairs with the remaining paragraphs.</li>
								<li>Pair Conference: Assign pairs. Student A reads the first paragraph aloud. Together students A and B check vocabulary and their understanding of the paragraph. Student B reads the next paragraph and they confer again. Pairs alternate reading. Class check. Elicit remaining problems, allowing the class to answer if possible. Clarify final vocabulary and comprehension questions.</li>
							</ol>
							<p>Dealing with Vocabulary:</p>
							<p>When preparing a reading or listening activity, decide which items of vocabulary are essential to the understanding of the activity and thus need to be pre-taught. Do not try pre-teaching all the new vocabulary.</p>
							<p>First try to activate and elicit prior knowledge:</p>
							<ul>
								<li>Brainstorm in pairs / small groups vocabulary related to the topic. A competitive element and time limit can be used.</li>
								<li>Use mind maps to introduce key words from the text; elicit their meaning from students</li>
								<li>Give students short lists of vocabulary. Ask them to work together in small groups to look up or describe to each other the meaning of the words.</li>
								<li>Give students lists of mixed up words and definitions. They can work individually or in pairs to try to match the words to the correct definitions.</li>
								<li>Give students a list of words with example sentences of the words used in context. In pairs or individually ask the students to guess the meaning of the words and formulate their own example sentences using those words.</li>
							</ul>
							<p>Avoid translating the words for students; instead define, give example sentences, use gestures and modelling or pictures, or encourage students to look up the words in their dictionaries. </p>
							<p>When carrying out reading and listening tasks, always remind students that they do not need to know every word; encourage them to initially ignore the unknown words and focus on understanding the overall topic. Still ignoring the unknown words, move on to identifying specific details. Continue analysing the texts, showing students how far they are able to understand a text even when they do not know all the words. Once the text has been analysed as far as possible, then you can follow up with vocabulary work. You can prepare quick controlled practice exercises, such as matching, gap-fill or multiple choice. After that, students should be encouraged to use the words from the text in context, such as talking about themselves, acting out a role-play or discussing their opinions.</p>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		<div class="tab-pane" id="9a">
			<div class="#">
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">Task</div>
							<div class="panel-body">
									<div class="panel-body">
						{!! Form::open(['action' => 'Admin\Int120u3m2s1t3Controller@store', 'method' => 'POST']) !!}
													<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">
						<div class="form-group">
							{!! Form::label('u3m2s1q21', 'Name the primary skills in TEFL/ESL.') !!}
							{!! Form::text('u3m2s1q21', null, ['class' => 'form-control'])!!}
							 
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u3m2s1q22', 'Which should be included in a general English teaching syllabus?:') !!}
							{!! Form::text('u3m2s1q22', null, ['class' => 'form-control'])!!}
							  
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u3m2s1q23', 'Which 2 skills appear more often in a course specifically designed for telephone operators / receptionists?') !!}
							{!! Form::text('u3m2s1q23', null, ['class' => 'form-control'])!!}
							  
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u3m2s1q24', 'How can you ensure that you are considering the needs of every learner?') !!}
							{!! Form::text('u3m2s1q24', null, ['class' => 'form-control'])!!}
							  
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u3m2s1q25', 'Why could it be necessary for you, as the teacher, to provide extra materials?') !!}
							{!! Form::text('u3m2s1q25', null, ['class' => 'form-control'])!!}
							  
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u3m2s1q26', 'What extra materials could be useful? (Name 3)') !!}
							{!! Form::text('u3m2s1q26', null, ['class' => 'form-control'])!!}
							  
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u3m2s1q27', 'Which receptive skill is used when conversing?') !!}
							{!! Form::text('u3m2s1q27', null, ['class' => 'form-control'])!!}
							  
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u3m2s1q28', 'Which productive skill is used when making notes at lectures?') !!}
							{!! Form::text('u3m2s1q28', null, ['class' => 'form-control'])!!}
							  
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u3m2s1q29', 'What do we call a discussion of the text in which the teacher raises the awareness of what the learners already know about the text, in order to lay a foundation for understanding the new information they are going to hear?') !!}
							{!! Form::text('u3m2s1q29', null, ['class' => 'form-control'])!!}
							  
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u3m2s1q30', 'What do we call a discussion of the text in which the teacher raises the awareness of what the learners already know about the text, in order to lay a foundation for understanding the new information they are going to hear?') !!}
							{!! Form::text('u3m2s1q31', null, ['class' => 'form-control'])!!}
							  
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u3m2s1q32', '“Reading is an inbuilt skill.” Is this statement true or false?') !!}
							{{ Form::select('u3m2s1q32', ['true', 'false']) }}
							  
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u3m2s1q33', 'What decides which sub-skills of reading will be necessary in a particular reading exercise?') !!}
							{!! Form::text('u3m2s1q33', null, ['class' => 'form-control'])!!}
							  
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u3m2s1q34', ' Is reading a novel for pleasure an example of intensive or extensive reading?') !!}
							{!! Form::text('u3m2s1q34', null, ['class' => 'form-control'])!!}
							  
						</div>
						<br>

						<div class="form-group">
							{!! Form::label('u3m2s1q35', 'What name is given to the sub-skill involved in running over a text to identify its general topic?') !!}
							{!! Form::text('u3m2s1q35', null, ['class' => 'form-control'])!!}
							  
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u3m2s1q36', 'Which reading sub-skill involves in-depth study of the text?') !!}
							{!! Form::text('u3m2s1q36', null, ['class' => 'form-control'])!!}
							  
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u3m2s1q37', 'Why do you need to know whether your learners are eg university students, business English students etc?') !!}
							{!! Form::text('u3m2s1q37', null, ['class' => 'form-control'])!!}
							  
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u3m2s1q38', 'In Example Text 1 are your students required to understand  every word before completing the exercises?') !!}
							{!! Form::text('u3m2s1q38', null, ['class' => 'form-control'])!!}
							  
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u3m2s1q39', 'In Exercise 1 of Example Text 1 what kind of reading is required – skimming or scanning?') !!}
							{!! Form::text('u3m2s1q39', null, ['class' => 'form-control'])!!}
							  
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u3m2s1q40', 'In which of the 2 texts is the student required to predict?') !!}
							{!! Form::text('u3m2s1q40', null, ['class' => 'form-control'])!!}
							  
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u3m2s1q41', 'In Exercise 3 of Sample Text 2, would the students skim or scan?') !!}
							{!! Form::text('u3m2s1q41', null, ['class' => 'form-control'])!!}
							  
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u3m2s1q41', 'For which stage of a receptive skills lesson would you plan exercises on grammar structures used in the text, a role-play based on the text or an essay on the topic of the text?') !!}
							{!! Form::text('u3m2s1q41', null, ['class' => 'form-control'])!!}
							  
						</div>
						<br>
						{!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}
													{!! Form::close() !!}
						
					</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	





{{-- end tab content clearfix section --}}
</div>

@endsection