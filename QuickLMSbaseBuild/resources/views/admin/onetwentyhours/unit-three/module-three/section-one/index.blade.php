@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('content')


<div class="panel panel-default">
	<div class="panel-body">Unit Three Module Three</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading"><h3>Unit 3 | The Teaching and Learning of EFL</h3></div>
	<div class="panel-body"><h4>Module 3 | Speaking and Writing</h4></div>


	<div class="panel-body" id="exTab1">
		<ul  class="nav nav-pills">

			<li><a  href="#1a" data-toggle="tab" style="background-color:#fff;">Study Skills</a></li>

			<li><a href="#2a" data-toggle="tab">Part 1. Speaking</a></li>
			<li><a href="#selfcheck1" data-toggle="tab" class="orangeText">Self Check 1</a></li>
			<li><a href="#selfcheck2" data-toggle="tab" class="orangeText">Self Check 2</a></li>
			<li><a href="#4a" data-toggle="tab">Part 2. Speaking</a></li>
			<li><a href="#5a" data-toggle="tab">Phonology</a></li>
			<li><a href="#6a" data-toggle="tab">Getting students to Talk</a></li>
			<li><a href="#selfcheck3" data-toggle="tab" class="orangeText">Self Check 3</a></li>
			<li><a href="#7a" data-toggle="tab">Part 1. Writing</a></li>
			<li><a href="#selfcheck4" data-toggle="tab" class="orangeText">Self Check 4</a></li>
			<li><a href="#8a" data-toggle="tab">Teaching writing skills</a></li>
			<li><a href="#9a" data-toggle="tab">Examples of writing lessons</a></li>
			<li><a href="#10a" data-toggle="tab" class="blueText">Tasks</a></li>		
		</ul>

	</div>
</div>

<div class="tab-content clearfix">
	{{--  start content section --}}

	<div class="tab-pane" id="1a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<h4>At the end of this unit you will:-</h4>
							<ul>
								<li>a) understand some of the problems students experience when speaking</li>
								<li>b) be able to teach speaking skills</li>
								<li>c) be able to identify problems associated with successful writing</li>
								<li>d) be able to train students in the skills needed for effective writing</li>
							</ul>
							<br>
							<p>In this module, we will look at the two productive skills: speaking and writing.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="2a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Part 1. Speaking</h4>
						</div>
						<div class="panel-body">
							<h4>Introduction</h4>
							<p>Oral communication is a two-way process involving a speaker and a listener, yet it is useful to look at ways of teaching and learning speaking as separate from those of listening. As language teachers, one of our tasks is to equip our learners with the skills needed for effective oral communication by providing opportunities for the situational practice of speaking skills.</p>
						</div>
						<div class="panel-body">
							<h4>Speaking problems</h4>
							<p>Students have various problems with speaking skills. These may be due to one's previous learning experience emphasising the structural basis of the language with little or no communicative methodology employed and therefore little realistic oral language practice of the language having been done. </p>
							<p>Poor spoken phonological ability (ie pronunciation, stress, rhythm, intonation) can cause one to mispronounce sounds, leading to poor comprehension on the part of the listener. If someone fails to understand what you're saying, you’ll soon give up and stay quiet.</p>
							<p>Another problem is demotivation due to overcorrection. This has to do with the old argument of fluency versus accuracy. For a communicative approach  the priority is successful communication, and if a message spoken has been correctly understood, errors of syntax or tense should be regarded as being of minor concern. To repeatedly correct a student attempting oral communication would simply silence him/her. Students often prefer not to speak rather than to make mistakes. Bear in mind also that they need to learn to talk in phrases and ‘paragraphs’ so if they keep stopping after one or two words because you have corrected them, they will never get a feel for the overall ‘shape’ of a sentence or phrase.</p>
							<p>Think of the phrase: ‘I’d like to enquire about the trip to the British Museum’. It has stress and intonation, and if the student has had practice getting the whole phrase out without stopping, then they are more likely to get to the end when they actually have to use it! (Perhaps with a different ending: the trip to France/outing to the theatre/trains to Manchester etc). If you interrupt them in the middle with a minor correction, they may not be able to finish after that.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



	<div class="tab-pane" id="selfcheck1">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Self Check 1</h4>
						</div>
						<div class="panel-body">
							<p>Do any of the points above remind you of your own experiences of language learning? How did you deal with them?</p>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							<h4>How to teach speaking skills. Activities in a Presentation-Practice-Production (PPP) lesson</h4>
							<p>If your lesson is to target speaking skills, you can use the PPP procedure in the following way.</p>
							<br>
							<p>1) Setting the scene/warm up</p>
							<p>First of all, you need to set the scene. This is absolutely essential. Expecting students to speak ‘cold’ on any topic is just not fair. They will not be mentally prepared or enthused enough to do their best. You must make sure that the students have the background to the task.</p>
							<p>If the class is to practise the language of invitation, for example, tell them about a party you are planning and ask them what you need to do to make sure people come. Students will recognise the need for the functional language you are set to introduce.</p>
							<br>
							<p>After the scene has been set, you need to present or elicit the language appropriate to the particular task you wish to carry out. Forms such as </p>
							<p><i>'I'd like to invite you to ....' </i></p>
							<p>and</p>
							<p>'would you like to come to .....'</p>
							<p>can be given as they are now in context. This is the time in the lesson when you could use a model dialogue, written or even better recorded, to drill students on the key language of invitations. 
							</p>
							<br>
							<h4>3) Controlled Practice</h4>
							<p>During the next, controlled practice stage, give students the opportunity to practise the structures involved, ie by choral and individual drilling, doing exercises, then by guided pairwork, ie with short dialogues or role plays using prompts you have prepared. You could build from the model dialogue in a number of ways including taking short phrases for choral repetition and doing short substitution practice:</p>
							<ul class="cleanList">
								<li>- I’d like to invite you to dinner. </li>
								<li>My party: I’d like to invite you to my party.</li>
								<li>The theatre: I’d like to invite you to the theatre.</li>
								<li>and so on</li>
							</ul>
							<br>
							<h4>4) Production (Free practice)</h4>
							<p>Finally it’s the production stage. At the controlled practice stage, accuracy of stress, rhythm and pronunciation were important, but now that the students are involved in producing language in a freer environment, fluency is important. Teacher input here should be minimal, with errors monitored but not necessarily corrected. The teacher has guided the class to this stage and must now encourage and support the learners as they complete their task. The aim for them is to communicate effectively using language practised during the lesson. There are several activities generally employed by teachers to achieve results at this stage of the lesson:</p>
							<ol>
								<li>Games - eg Bingo (number, phonology, spelling), Hangman, Monopoly.</li><br>
								<li>Role plays - with role cards and prompts prepared in advance, giving the students a clear role.</li><br>
								<li>Information gap activities <br>
								These demand that students exchange information to complete a task or solve a problem. For example, pairs have two street plans with 5 different buildings on their plans. Face to face, they ask for the location of those buildings they do not have, and give details to their partner of those they do.</li><br>
								<li>Opinion gap activities <br>
									These are controversial texts, pictures or statements to promote debates, discussions and arguments between partners or groups. Eg  Marriage is an outdated institution. <br>  A class can be split into an even number of groups, eg 2 for and 2 against, to prepare their side of the issue. New groups would then be formed with one student from each of the four original groups, to debate the issue using ideas prepared beforehand.
								</li><br>
								<li>Collaborative problem-solving activities <br>Sometimes students can be given the same information and must discuss a solution to a problem. The well known ‘desert island debate’ in which students in groups choose ‘5 things from a list that they would take to a desert island and why’ falls into this category.</li>
							</ol>
						</div>
						
					</div>
					
				</div>
			</div>
		</div>
	</div>

<div class="tab-pane" id="selfcheck2">
	<div class="#">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
						<div class="panel-heading"><h4>Self Check 2</h4></div>
						<div class="panel-body"><p>
							Which of the activities from Self Check 1 would you use at</p>

                     			{!! Form::open(['action' => 'Admin\Int120u3m3s1t1Controller@store', 'method' => 'POST']) !!}
                      							<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">
								{{ Form::label('u3m3s1q1', 'a) lower levels?')}}<br>
								{{ Form::text('u3m3s1q1', '', ['class' => 'form-control'])}} 
								<br>
								{{ Form::label('u3m3s1q2', 'b) lower levels?')}}<br>
								{{ Form::text('u3m3s1q2', '', ['class' => 'form-control'])}} 
								<br>
								{!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}
								{!! Form::close() !!}
					

						</div>
					</div>
			</div>
		</div>
	</div>
</div>

	<div class="tab-pane" id="4a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Tips for effective speaking lessons</h4></div>
						<div class="panel-body"><p>
							Remember that language is about communication. The language teacher is aiming to teach the student to communicate effectively in the four main skills. To enable students to effectively communicate orally, they need to be provided with as much opportunity as possible to use the language. With regards to speaking, this means not limiting it to specific speaking slots, but providing opportunities to speak throughout the lessons whatever the focus of the lesson (eg reading or writing etc). This means allowing students to express themselves at all times.</p>
							<p>
							Here are a few things to think about when setting up speaking activities. </p>
							<p>
							1) Pick a topic that everyone can access and enjoy discussing or practising. Perhaps ask the students for something they are interested in or feel strongly about.</p>
							<p>
							2) Avoid students always interacting with the same students. It is easy to fall into the trap of always pairing students with the person they are sitting next to (ie always the same person). Students should be given the opportunity to interact with as many people as possible.</p>
							<p>
							3) Remember to L.T.T.T. (limit teacher talk time), and avoid completely teacher-centred lessons where the teacher does most of the talking, and the majority of the interactions are teacher - student. Make full use of pair work and groupwork to increase student - student interactions. The teacher should be the facilitator, not the director.</p>
							<p>
							4) Make sure that everyone knows exactly what they are doing before you begin or you will have to go round to ten different pairs re-explaining the task! This is best done by modeling and also by clear instructions. </p>
							<p>
							 5) Remember the importance of fluency practice; give students the opportunity to use the language flexibly without being hindered by constant correction. Errors can be observed and fed back to the class anonymously at the end of the exercise. If students are constantly corrected, their flow of speech will be interrupted, and their confidence will suffer.</p>
							<p>
							6) Remember to try to simulate real life situations as far as possible to make the interactions realistic and purposeful. With role-play exercises, use props to increase authenticity, eg menus for restaurant role plays, holiday brochures for planning a holiday role play etc.</p>
							<p>
							7) Allow for student thinking time. If students are asked to carry out role plays, give them time to think and plan, but don’t let them write down complete dialogues - this is speaking, and not writing practice.</p>
							<p>
							8) Don’t forget what it is like to be in the students’ shoes as a language learner – it can be both difficult and frustrating, and it is easy to lose confidence and motivation. Therefore, both praise and encouragement cannot be overused in the EFL classroom!</p>
							<p>
							9) To develop the skill of being able to continue with what you are saying, even when you have forgotten or do not know a word, word games can be used. An invaluable tool is to teach students how to describe objects or concepts. There will always be times when students will not know a word. If at those times the student is able to elicit the word from someone else without too many circumlocutions, they will be able to communicate much more effectively. However, if students do not know how to describe words, they will be both frustrated and handicapped. Therefore, we should teach such phrases as, ‘It’s a bit like . . .’, ‘It’s a kind of . . .’, ‘You use it to . . .’, ‘You use it when . . .’, ‘It’s the stuff you use to . . .’ etc. Word games like Taboo, What’s My Line etc are both fun and useful in the EFL classroom.
						</p>
					</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>The use of dialogue in the EFL classroom</h4></div>
						<div class="panel-body">
							<p>Dialogues offer an excellent opportunity to show language in action, in natural, meaningful contexts. They also offer an excellent opportunity to focus on stress and intonation. When using dialogues in the classroom, ensure that you use the Look up & Speak technique, ie students read a line silently, then look at their partners and say the line from memory. This encourages more natural dialogues.</p>

							<p>Dialogue practice focusing on stress, rhythm and intonation can be followed by students using the basic format of the dialogue, but altering some of the details. This could then lead on to students being asked to produce similar dialogues without referring to their texts. This also corresponds to the transition from controlled to freer practice (production)
							</p>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>The use of discussion in the EFL classroom</h4>
						</div>
						<div class="panel-body">
							<p>
							Discussions are a effective and natural way to practise talking freely in English. When you introduce discussions find topics that will appeal to your students. Be clear on the aims of the discussion and the specific skills you wish to focus on. Always introduce the specific language of discussion you wish to develop (eg agreeing, disagreeing, conceding a point, keeping a discussion going by seeking others’ opinions and giving minimal encouragers, expressing disbelief, asking for clarification etc) as the target language prior to the discussion, and elicit or teach important vocabulary.</p>
							<p>
							Topic is seen by most teachers as the central focus of a classroom discussion. However, another very important point to consider is not only what to talk about, but why we need to talk about it. Of course a discussion which has no aim but to discuss the topic may succeed, but often, the discussion gradually subsides until you hear the familiar cry of ‘I can’t think of anything to say.’</p>
							<p>
							What the students who say this often mean is that they have no reason to say anything. This kind of discussion only imitates real conversation for it lacks the purpose of genuine discourse. In short, students need a reason to speak more than they need something to speak about. When a group is given a task to perform through verbal interaction, all speech becomes purposeful, and therefore more interesting. Language also involves thought; and a task involving talking must also involve thinking out.
							</p>
					</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4></h4>
						</div>
						<div class="panel-body">
							<p>It is not enough to provide students with the opportunity to speak in English. As teachers, we must encourage students to speak in a number of different situations and help them to develop their confidence in using the language flexibly. One of the most difficult and frustrating things about learning a language is making the transition from the classroom to the real world. Role-play is one of the ways of easing students into the real world by helping us to recreate and enact a wide variety of real life situations in the classroom.</p><p>
							The effective use of role-play adds variety to the kinds of activities we can use in the classroom; it encourages thinking, creativity and spontaneity; it allows students to practise new structures in a relatively safe environment, and it introduces a fun element into the classroom which is conducive to real learning.
							</p>
							<p>Good preparation is essential to success. Effective role-plays shouldn’t be scripted out in detail. They should give a basic description of the scenario and characters while leaving room for creativity. Role-play cards are an effective tool. e.g.</p>
						</div>
						<div class="panel-body">
							<table>
								<tr>
									<th>Student A</th>
									<th>Student B</th>
								</tr>
								<tr>
									<td>Hotel guest booking a room</td>
									<td>Hotel receptionist</td>
								</tr>
								<tr>
									<td>Elements:</td>
									<td>Elements:</td>
								</tr>
								<tr>
									<td>Book in</td>
									<td> Welcome the guest</td>
								</tr>
								<tr>
									<td>You have a reservation</td>
									<td>Find them a room</td>
								</tr>
								<br>
								<tr>
									<td>Complications:</td>
									<td>Complications:</td>
								</tr>
								<tr>
									<td>You are on your own	</td>
									<td>You can’t find the reservation</td>
								</tr>
								<tr>
									<td>You want a shower and breakfast</td>
									<td>You only have a double room with bath available</td>
								</tr>
								<tr>
									<td>You have an early morning meeting and mustn’t be late</td>
									<td></td>
								</tr>
							</table>
							<br>
							<p>Always prepare well. Ask questions and elicit ideas first, identify target 
							structures and vocabulary. If there are a few students who will be acting out 
							the same role, but in different groups, allow them to work together to plan 
							ideas and work out key phrases (otherwise allow students time to plan 
							individually). Role-plays should generally be carried out at least twice, 
							allowing students to change roles. Avoid correction during role-plays; observe 
							and debrief at the end. Role-play is an excellent tool but can be difficult to 
							manage due to its unpredictability. It is useful therefore to consider 
							beforehand the various ways that role-plays may develop so that you are 
							more prepared. Encourage students to really get into role by exaggerating 
							actions, gestures, tone and stress.
							</p>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Teaching complete beginners</h4></div>
						<div class="panel-body">
							<p>
								Teaching complete beginners using the target language only can be a very 
								daunting prospect for less experienced teachers. Some questions are likely to 
								come to mind, eg ‘Where do I start?’, ‘What can I do when they clearly don’t 
								understand me?’ etc.
							</p>
							<p>
								Here are a few points to consider:
							</p>
							<ul>
								<li>Students obviously will not have sufficient knowledge of the language to enable them to understand anything more than basic instructions, therefore it is essential that we use all the tools at our command, eg voice and intonation, pictures, realia, diagrams, gestures and modeling, written language matched with pictures etc. Be confident. You will be surprised how quickly a group of complete beginners will be able to master some of the basics of the language eg giving basic personal details, distinguishing between different personal pronouns or expressing possession.</li><br>
								<li>Introduce basic classroom language immediately and encourage students to use these. You can demonstrate the meaning of these through the use of modelling, facial expression and pictures. Students will quickly learn the meaning of simple commands / instructions if you reinforce what you say with the use of gestures. For example: cupping your hand to your ear when you say ‘listen’, pointing from your eye to an object when you say ‘look’, putting your palms together then opening your hands when you say ‘open your books’, etc.</li><br>
								<li>Remember that repetition and practice are essential at this stage to enable real language acquisition.</li><br>
								<li>Careful preparation is essential and it is important to consider potential problems and plan how to deal with these if the need arises.</li><br>
								<li>Remember the importance of praise and encouragement; at this level these are even more important than usual, otherwise students will quickly become demotivated.</li><br>
								<li>Be creative and energetic in the classroom.</li><br>
								<li>Encourage self-correction, a valuable skill which will continue to aid students as they progress to higher levels.</li><br>
								<li>Include lots of pairwork and groupwork to provide students with ample opportunity to use the language.</li><br>
								<li>Don’t be afraid to use games in the classroom with adult learners. You may worry that students will find these silly and be reluctant to participate. However, if you are confident in your approach and students can see the communicative value of the games, you shouldn’t experience any problems.</li><br>
								<li>Remember to grade the language that you use to the students’ level.</li>
							</ul>
							<p>It is important to introduce the following basic phrases immediately: ‘Sorry / pardon!’, ‘Can you repeat that please?’, ‘Can you speak more slowly please?’, ‘I don’t understand’, ‘I don’t know’ etc.</p>
							<p>To model the meaning of ‘sorry / pardon’ you can ask a student their name, then after their reply cup your hand to your ear, shake your head and say ‘pardon?’</p>
							<p>To demonstrate the meaning of ‘can you repeat that please’ you could use a simple hand gesture, rolling your hand in a forwards motion. If students do not understand that, you can give an example.</p>
							<p>To demonstrate the meaning of ‘can you speak more slowly please’ you can simply say the phrase slowly yourself.</p>
							<p>To demonstrate the meaning of ‘I don’t understand’ you can point to your head, shake your head and use a palms-up gesture, and a confused facial expression.</p>
							<p>To demonstrate the meaning of ‘I don’t know’ you can point to a picture of a person in the students’ coursebook and ask ‘What is his / her name?’ and reply ‘I don’t know’ while pointing to your head</p>
							<p>These are all basic phrases which are essential in the classroom as they encourage students to use the target language right from the start rather than always reverting to their mother tongue. You can write these phrases on the board and point to them during the lesson to help students to use them. Another way to encourage students to learn and use these phrases is to alternate asking different students basic questions in a very quiet voice or at a very fast pace; you can also ask a few advanced questions that they will not be able to understand, to encourage them to say that they don’t understand. Time spent on these exercises is always time well spent and successfully encourages students to use these basic communication strategies.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane" id="5a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Phonology</h4></div>
						<div class="panel-body">
							<p>
								To be able to speak English well enough for successful communication to occur, our learners need to hear sounds, words and connected speech before they can reproduce them. When given a model and a reason to communicate orally, learners can master the complexities of our sound system, and stress and intonation patterns.
							</p>
							<br>
							<h4 calss="underline">Pronunciation</h4>
							<p>Certain sounds in English are difficult for foreign learners, especially when those sounds are non-existent in the learner’s first language. It is unrealistic for many students to expect to speak English with R.P. (Received pronunciation) but when pronunciation patterns impede communicative success, remedial work is essential. The Spanish student whose aim is 'to find a yob' (j / y confusion) and the Japanese student who eats 'flied lice' (r / l confusion) obviously need help. Minimal pair exercises and the phonemic script can be helpful here, as can exercises in consonant clusters (str/ cl/) etc.</p>
							<br>
							<h4 class="underline">Stress/Rhythm</h4>
							<p>Word stress needs to be practised, with rules being given as well as exceptions to the rules (eg photograph, photography). Incorrect syllable stress can confuse the listener, and when coupled with sound confusion, can give completely incorrect information. A student who is asked what deserts he can name and answers 'strawberry ice-cream' clearly got the stress wrong, and this stress error impedes communication.</p>
							<br>
							<h4 class="underline">Intonation</h4>
							<p>Intonation and sentence stress need to be taught together. Just as the wrong stress alters the meaning of a sentence, the wrong intonation pattern can convey the wrong mood or attitude.</p>
						</div>
					</div>
						<div class="panel panel-default">
						<div class="panel-heading"><h4>Accuracy vs Fluency</h4></div>
						<div class="panel-body"><p>
							The question of whether students should be encouraged to speak freely and fluently, often making grammatical errors which are not immediately dealt with, or whether we should encourage accuracy, is one which continues to evade a solution.  It really depends on what the aim of your oral lesson is, and therefore the emphasis in different lessons or at different stages of a lesson can be placed either on accuracy or on fluency.
						</p>
						<p>
							If the aim is to build up students' confidence speaking English, encourage them to deal with new communicative situations or to practise informal exchange of information or opinions, then fluency takes precedence. Errors can be monitored and addressed in a follow-up activity or lesson, however during the activity the speakers are not interrupted unless a particular error impedes communication. 
						</p>
						<p>
							If the aim is to practise, for instance, ways of connecting ideas or of paraphrasing (that is to say..) or exemplifying (take, for example....'), or correct tense usage, then accuracy has to be concentrated upon, entrenched errors dealt with systematically and new errors prevented. Correction may still be delayed, however when the target language point is used incorrectly, the teacher may interrupt and correct (or better still, prompt self-correction).  Pair work is helpful as learners prefer to make mistakes with classmates as long as they feel their errors are being monitored. Making notes around the class as you listen can then lead to group / class work and guided correction by the teacher. Choral repetition backchaining and language lab work can all be useful, and practising weak forms (I want to buy a pen) and running words together can aid fluency.
						</p>
						<p>
							In any case, it is important that whatever language point you decide to focus on for accuracy work, the reasons for choosing that point are communicative. It should not be accuracy for accuracy's sake, but students should be helped to achieve accuracy in communicating meaningful messages to other people.
						</p>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="6a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Getting students to Talk</h4></div>
						<div class="panel-body"><p>
							Here are some possible activities which may help to get students talking. Remember, they need a reason to talk to someone, and a reason to listen.
						</p></div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="underline">Beginners</h4>
						</div>
						<div class="panel-body">
							<p>Teacher uses flashcard of Peter on board. Elicits information from students with prompts, eg 'He's 22 years old, he comes from London'. Only use present simple of is, has, comes.  Give 8 sentences. Practise via choral repetition. Teacher then elicits question forms.  These are written on the board and practised all together (eg he has 2 sisters, he has a dog called 'Rover' and he comes to Manchester every weekend). The teacher asks individual questions and students give answers.</p>
							<br>
							<h5 class="underline">Pairwork practice - information gap (guided practice)</h5>
							<p>Students are given information about a famous British person. Each student has to find out details from their partner to fill in an information table. Roles can then be reversed.</p>
							<br>
							<h5 class="underline">Free practice</h5>
							<p>Students choose a famous person from their own country and their partners ask questions.</p>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4 class="underline">Intermediate</h4></div>
						<div class="panel-body">
							<p>
								Aim: To use persuasive language, organise information and use personal knowledge about one's homeland for free oral practice.</p>

							<p>Students are asked what they know about Hawaii, or an exotic location elsewhere.  Categories are put on to the board, eg climate / religion / local customs / typical food / places of interest / cultural events etc. These are discussed to check understanding. Students are then told to make notes on these topics with regard to their own country, and to add any categories they feel are important. This can be done in pairs (if monolingual) or groups, or individually if multi-lingual. It's useful to explain that they are going to take on the role of travel agents trying to persuade potential tourists that their own country would be fantastic to visit. Some expressions, such as "you'll love the...", or "you'll really enjoy the...", are useful. When this has been prepared, give them one more task. Explain that tourists can offend the locals if they are not warned of certain 'do's or don'ts' in their host country, for example forming queues in Britain, removing your shoes when entering a Japanese or Korean home, and visiting Catholic churches anywhere wearing shorts.
							</p>
							<p>Students are then paired with a student from another country or region, and the 'travel agent' has to answer questions and give information about his/her homeland.
							</p>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4 class="underline">Advanced</h4></div>
						<div class="panel-body">
							<p>
								At this level, ellipsis is often something learners have never studied and do not use.  For example:
							</p>

							<table>
								<tr>
									<td>1.   A.  I like apples</td>
									<td class="tab-space">B.  (Agreeing)  So do I</td>
								</tr>
							
							<br>
						
								<tr>
									<td>A. I don't like cheese</td>
									<td class="tab-space">B.  (Agreeing)   Neither do I</td>
								</tr>
							
							
								<tr>
									<td>3.   A.  I like milk</td>
									<td class="tab-space">B.  (Disagreeing)  Do you?  I don't</td>
								</tr>
							
							<br>
						
								<tr>
									<td>4.   A.  I don't like rice</td>
									<td class="tab-space">B.  (Disagreeing)  Don't you? I do</td>
								</tr>
							</table>
							<br>
						    <p>Each of these forms would be practised with several examples before moving on to the next one (describing hobbies or types of films etc). All four can then be put together in guided practice. For example, the following cue card can be given to students:</p>
						    <br>

						    <table>
						    	<tr>
						    		<td>A.  I like..............</td>
						    		<td class="tab-space">B.  (Agree)</td>
						    	</tr>
						    
						   
						    	<tr>
						    		<td>A.  I don't like</td>
						    		<td class="tab-space">B.  (Agree)</td>
						    	</tr>
						    
						    <br>
						   
						    	<tr>
						    		<td>A.  Express preference </td>
						    		<td class="tab-space">B.  (Respond)</td>
						    	</tr>
						    </table>
						    <br>
						    <p>This can become more complex</p>
						    <table>
						    	<tr>
						    		<td>A.   Do you think you'll get married?</td>
						    		<td class="tab-space">I hope to <br>
										I'm planning to <br>
										I'd like to <br>
										I think so <br>
										I hope so 
										</td>
						    		
						    	</tr>
						    	<tr>
						    		<td>Do you think you'll fail your exams? I hope not
						    		<td>I hope not <br>
										I don't think so
						    		</td>
						    	</tr>
						    	<tr>
						    		<td>A.   Have you ever been to France?</td>
						    		<td class="tab-space">No, I haven't, but I'd <span class="underline">like</span> to</td>
						    	</tr>
						    </table>
						    <br>
								<p>
									Students can then be asked to make a list of about ten questions, paying special attention to pronunciation, stress and rhythm both in asking questions and answering, which is then done with a partner.
								</p>
								<p>This is a simple exercise to practise, yet can make a big difference to making the language learners use sound much more natural and helping them understand everyday speech.</p>
								<br>
								<h4><b>PRACTICAL PROBLEMS </b></h4>
								<br>
								<p>1. Many learners of English fail to pronounce the endings of words, especially past tense regular verbs, which can cause the listener some confusion. Look at these verbs in the past simple tense.</p>
								<table>
									<tr>
										<td>stopped</td>
										<td class="tab-space">asked</td>
										<td class="tab-space">listened</td>
									</tr>
									<tr>
										<td>played</td>
										<td class="tab-space">called</td>
										<td class="tab-space">picked</td>
									</tr>
									<tr>
										<td>visited</td>
										<td class="tab-space">missed</td>
										<td class="tab-space"></td>
									</tr>
									<tr>
										<td>lived</td>
										<td class="tab-space">missed</td>
										<td></td>
									</tr>
								</table>
								<br>
								<p>Although they all end in 'ed' the pronunciation of the 'ed' differs. Sometimes it is pronounced /t/, sometimes /d/ and can also be /id/.</p>
								<br>
								<p>2. Most nationalities will have difficulties with pronunciation due to English spelling. The same spelling can be pronounced in a variety of ways. Look at the following spellings:</p>
								<table>
									<tr>
										<td>cou<span class="underline">gh</span></td>
										<td class="tab-space">bou<span class="underline">gh</span></td>
										<td class="tab-space"><span class="underline">gh</span>etto</td>
									</tr>
								</table>
								<br>
								<p>3. Different spellings can be pronounced the same way. Look at the following words:</p>
								<ul class="cleanList">
									<li>di<span class="underline">sh</span></li>
									<li>sta<span class="underline">ti</span>on</li>
									<li>ra<span class="underline">ci</span>al</li>
									<li>para<span class="underline">ch</span>ute</li>
								</ul>
								<br>
								<p>4. Students may find it difficult to know which way to pronounce 'th'. Sometimes it is voiced as in:</p>
								<table>
									<tr>
										<td>the</td>
										<td>then</td>
										<td>this</td>
										<td>although</td>
									</tr>
								</table>
								<br>
								<p>Sometimes it is unvoiced as in:</p>
								<br>
								<table>
									<tr>
										<td>think</td>
										<td>through</td>
										<td>threw</td>
										<td>tenth</td>
									</tr>
								</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="selfcheck3">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Self Check 3</h4></div>
						<div class="panel-body">
							<ol>
								<li>Look at Practical Problem No 1. Can you give a rule to help your students distinguish between the different pronunciations of the ending?</li><br>
								<li>Write a short passage incorporating at least 10 of the verbs from Practical Problem No 1 which you could record and play to your class.</li><br>
								<li>Write an exercise for students to do in pairs, based on your passage, to give practice in the above pronunciation.</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="7a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Part 1. Writing</h4></div>
						<div class="panel-body">
							<p>We have said that to read successfully, training must be given - the same applies to writing, a skill closely linked with reading and often the one learners find most difficult to acquire. This may be because</p>
							<br>
							<ul class="cleanlist">
								<li>a) writing requires organisational skills and careful thought: planning first, then checking and rewriting. This makes spontaneity somewhat difficult. </li>
								<li>b) it is usually regarded as a solitary activity.</li>
								<li>c) teachers often assume that students know how to organise and develop written work, if only in their first language, but this is often untrue. Just try writing one of the tasks you ask your students to do!</li>
							</ul>
							<br>
							<p>Given these problems, it makes sense to try to offer students help in a variety of ways, for example:</p>
							<br>
							<ul class="cleanList">
								<li>a) By giving plenty of exposure to model texts, both good models and faulty models (clearly indicating which is which) and give checklists for student reference (ie do's and don'ts).</li>
								<li>b) By creating a collaborative environment: using pairwork and groupwork to prepare the ground, giving support, ideas and the chance to participate before the actual writing begins.</li>
								<li>c) By ensuring that the writing takes place in a relaxed learning environment.</li>
								<li>d) By actually teaching organisational skills, the way to develop ideas and  
								how to reach a logical conclusion, through regular practice.
								</li>
								<li>e) By making sure that students have clear criteria to work towards - that   
						          is, they know what you are looking for and how you are going to mark   
						          their work</li>
							</ul>
							<br>
							<h4><b>Preparing the way</b></h4>
							<p>Because of the range of writing tasks students may wish to complete, some basic questions need answering before the student knows how to approach a particular piece of writing. The first 4 things to consider are:</p>
							<br>
							<ul class="cleanList">
								<li>a) What is the purpose of this piece of writing?  (eg to inform, request, apologise, instruct, etc)</li>
								<li>b) Who will be the reader? (eg a potential employer, a child, the general public, a loved one, etc)</li>
								<li>c) What is the appropriate style and register? (eg very formal and polite, semi-formal and respectful, friendly and informal)</li>
								<li>d) What is the correct layout or format? (eg positioning of addresses, complementary close, paragraphing, etc)</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="selfcheck4">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>SELF-CHECK 4</h4></div>
						<div class="panel-body">
							{!! Form::open(['action' => 'Admin\Int120u3m3s1t2Controller@store', 'method' => 'POST']) !!}
														<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">
							<p>
								Why is it that students are so often loath to put pen to paper to produce a piece of written work?
							</p>
							<br>
							
							{{ Form::text('u3m3s1q3', null, [ 'class' => 'form-control' ]) }}
							<br>
							{!! Form::submit('Save', ['class' => 'btn btn-info form-control'])!!}
							{{ Form::close() }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="8a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Teaching writing skills</h4></div>
						<div class="panel-body"><p>
							In order to write effectively students must be assisted in developing the necessary skills. Just because a student is able to write isolated, grammatically correct sentences, does not necessarily mean that they will be able to organise a written composition.  Through exposure to a range of selected texts and by studying techniques and devices used to achieve particular results, language learners begin to understand how to produce a piece of writing. To teach writing skills effectively, it's important to:
						</p>
						<ul class="cleanlist">
							<li>a) have a regular, weekly slot dedicated to writing (not the last ten minutes of a lesson as written homework is set).</li>
							<li>b) use a short listening to set the scene, with note-taking, group discussion (ie for / against points) with the teacher giving guidance on contrastive devices (however, although) or words of addition (furthermore, in addition), etc. Ideas can then be joined and paragraphs developed on the board.</li>
							<li>c) incorporate a short reading presented as a model. This gives students something to pull apart and learn how to reconstruct. By focusing on tenses, linking devices, reference words, vocabulary items etc, it gradually becomes clear how words, sentences and paragraphs fit together to fulfil a particular task for a particular reader in the appropriate style and register.</li>
						</ul>

					<
					</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Product writing and basic stages of a writing lesson</h4></div>
						<div class="panel-body">
							<p>Effective writing involves much more than grammatical correctness; students need to know how to organise a composition relevant to the genre and target audience. The genres include narrative essays, discursive essays, report writing, formal and informal letter writing, among others. These need to be tackled at the appropriate levels. Whatever the genre, we can outline basic lesson stages:</p>
					<br>
					<ol>
						<li>Lead-in / Discussion <br>
							Rationale: To elicit learners’ knowledge (activate schemata) <br>
								        To develop predictive skills <br>
								        To stimulate interest in the topic
						</li><br>
						<li>Stimulus (Model Text) <br>
						Rationale: To exhibit the relationship between stimulus and target text type <br>
						        To provide model text for analysis - to derive explicit organisational framework <br></li><br>
												<li> Enabling activities for Main Writing Task <br>
						Rationale: To work on information structure and language features <br>
							        To work on text generation techniques
						</li> <br>
						<li>Main Writing Task <br>
							Rationale: To go through a process to generate a product similar to the model text</li> <br>
						<li>Follow-Up: Comparing texts with a partner / Improving texts <br>
							Rationale: To exchange texts with partners <br>
	        				To evaluate own and partner’s text against a Key text <br>
	        				To improve own text</li>
					</ol>
					<br>
					<p>This planning relies on students following a model text and producing a similar text of their own and is often referred to as product writing.  Product writing focuses on the organisational conventions of particular text types, or genres. At the end of this module, you will find examples of product writing lessons for different levels.</p>
					<br>
					<h4>Process writing and writing sub-skills</h4>
					<br>
					<p>Another approach to teaching writing is called process writing. Process writing focuses on the skills a writer needs to complete a particular writing task. These are writing sub-skills.</p>
					<p>As follows are the 6 writing sub-skills, which are also the 6 consecutive stages of a process writing lesson (after a lead-in/discussion and before a follow-up).</p>
					<br>
					<ul class="cleanList">
						<li>a) Brainstorming: making notes of all possible things to be included in the text.</li>
						<li>b) Planning: writing a general outline of the text.</li>
						<li>c) Drafting: writing the first version of the text.</li>
						<li>d) Peer evaluation: giving feedback to classmates on their drafts.</li>
						<li>e) Re-drafting: making changes to the text based on the peer feedback and the writer's own analysis – improving the organisation and coherence, correcting language errors.</li>
						<li>f) Proofreading: final reading and checking for minor mistakes, such as punctuation and spelling.</li>
					</ul>
					<br>
					<p>The process is not necessary a linear one though and will vary depending on the writer and the type of correspondence involved. For example, a lot less time on planning will be spent on a simple postcard and there may only be one draft, without the revisiting / editing phase. However, a lot more time will be spent on the planning and editing stages when writing a formal speech. </p>
					<p>As effective teachers we should be incorporating both the product and process approaches to writing into our lessons.</p>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading"><h4>Writing in class and for homework</h4></div>
						<div class="panel-body">
							<p>Writing is a skill that is often overlooked in the classroom because of the potential difficulties involved, but which deserves more attention in this environment.</p>

							<p>Writing in class is a helpful exercise as it shows the teacher what the student can produce without the support of textbooks etc, and can be monitored with the most common mistakes dealt with on the board during the lesson.</p>
							 
							<p>Collaborative writing also offers a lot of benefits to students. Although the ultimate aim is to develop the writing skills of students individually, students can support each other and can benefit from learning from each other’s strengths. It also helps to generate interest and enthusiasm and meaningful discussion which can then benefit the actual writing process.</p>

							<p>Writing is also useful for homework. When giving writing tasks as homework, however, it is essential to prepare students thoroughly by providing them with a purpose for writing, a target audience and a model, if appropriate. For example, the homework task ‘Describe your home’ does not give the student any guidelines on how they should approach the task. Should they write a formal, detailed description for an advertisement, or an informal description to a new pen-friend aiming to share personal information, or part of a description to a twin school giving detailed information on life in their own country?</p>

							<p>There will be a different focus to writing skills depending on the students’ level. At the lower levels for example, the teacher will be focussing on the linking of simple sentences together and the correct use of basic grammar and vocabulary. At higher levels, however, the teacher will be focussing on much more, eg conventions of formal and informal writing, the use of synonyms, appropriate use of conjunctions etc.</p>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Marking written work</h4></div>
						<div class="panel-body">
							<p>Judging a piece of writing can be quite difficult for a teacher. The best way to mark a long piece of work is using criteria. Look carefully at the types of errors your students are making. Give credit for what has been got right and highlight points to work on. These can be in any area of writing, for example:</p>
						<br>
						<ul class="cleanList">
							<li><b>GRAMMAR</b><span class="tab-space"><i>Don’t forget your -ed endings on verbs in your story</i></span></li><br>
							<li><b>WORD CHOICE</b><span class="tab-space"><i>Try not to use the word ‘nice’ so much.</i></span></li><br>
							<li><b>ORGANISATION</b><span class="tab-space"><i>Look again at where the paragraphs should go.</i></span></li><br>
							<li><b>CONTENT</b><span class="tab-space"><i>How did the boy get to the castle? </i></span></li><br>
							<li><b>MECHANICS</b><span class="tab-space"><i>I can’t read this sentence, please work on your handwriting. </i></span></li>
						</ul>
						</div>
						<br>
						<div class="panel-body">
							<p>Make a clear difference between content (what they wrote about), form (the layout and style) and accuracy (language correctness). </p>
							<p>Then when you comment on the students' work, you can make clear the difference between an accurate but rather rude letter and a very polite and sensible letter that has a lot of mistakes.</p>
							<p>Always make the writing something to share and show to others. Make a list of good phrases that students use and go over them in class together. Read little bits from good work. Stick them up on posters and put them on the wall for all to see. Whatever you do, make the students proud of their writing so that they want to make it look beautiful next time.</p>
							<p>Another useful way of dealing with errors is for a sheet to be handed out with the students’ mistakes, for example 20 sentences taken from their writing. Let them correct the mistakes – working in pairs or in a whole-class session. It's amazing how many mistakes students themselves are able to correct. They also seem to enjoy this technique.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<div class="tab-pane" id="9a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Examples of writing lessons</h4></div>
						<div class="panel-body">
							
							<h4>1.  Beginner/elementary</h4>

								<p>An informal letter giving directions</p>
								<p>
								Presentation:  Use flash cards to revise basic building vocabulary (ie The Bank, Cinema, Shop etc). Use flannelboard to present basic directions/location/language. 'On the left, on the right'  'Turn left ......
								Go down Low Street ......’  choral repetition. <br>
								Highlight imperative forms </p>

								<p>Practice 1:  Oral practice. Question and answer practice. Written reinforcement for sentences on board.</p>

								<p>Practice 2:  Pairwork (Information Gap Activity) <br>
								Pairs (A+B) have street plans with 4 buildings located on each. They must ask their partner for directions to, and locations of, the 4 buildings they do not have, and give details of those which they do have.</p>

								<p>Reading  (Model text):  A short passage giving directions to new students in the town is given out. Students answer 5 questions (general comprehension) and have to locate the school on the plan given in practice 2 above. </p>
								<p>Students check in pairs. <br>
								Focus on any new verbs/vocabulary that did not come in the earlier practice, such as ‘roundabout’ or ‘cross over’.</p>

								<p>Free Writing  Teacher tells students: "It's your birthday on Friday and you are having a party. You have invited your class to your house. They know where the school is, but not where you live. Write a paragraph giving directions for them to follow from the language school/college to your house".
								(NB: If students live some distance away, you may need to pre-teach
								take the number four bus from ..... get off at ......, etc) A simple handout with the layout for an informal letter makes this easier. The students simply fill in the actual body of the letter giving directions. </p>

								<p>Encourage the students to draw a small map to go with the directions as this is not only realistic but gives you a chance to check if they are accurate or not.</p>

								<br>
								<h4>2. Intermediate</h4>

								<p>Writing a film review</p>

								<p>Stage 1:   Lead-in</p>
								<ul class="cleanList">
									<li>a) Ask students to describe different types of films (ie comedy,  
								    science fiction etc). Teacher puts on board.</li>
									<li>b) Pairwork - students discuss 'What makes a good film'.  
								    Feedback to teacher who puts ideas onto the board (ie plot,   
								    soundtrack, actors, photography etc).</li>
								</ul>
								<br>
								<p>Stage 2:  Reading (Model text)</p>
								<ul class="cleanList">
									<li>Hand out the text of a film review with general comprehension questions (short exercise). The questions should simply check understanding of characters, plot, etc. Students can also identify which of the characteristics of good films raised in 1b) above were mentioned.</li>
								</ul>
								 <br>	
								<p>Stage 3:  Grammar <br>
								Elicit grammatical features of the model text.</p>
								
								<ul class="cleanList">
									<li>a) Structures to be used in a review = present tenses</li>
									<li> b)  Sequencing = after that, then, next, etc.</li>
									<li>c)  Passive voice, adjectives and adverbs. For example:</li>
									<li>The direction is brilliant. - It is brilliantly directed.</li>
									<li>The photography is impressive. - It is impressively photographed.</li>
									<li></li>
								</ul>
								<p>Exercises in these three give practice in the grammatical structures   
								 commonly found in reviews.</p>

								<br>
								<p>Stage 4:  Pairwork/oral practice</p>
								<p>Students describe a film they have recently seen or particularly enjoyed.  Their partner takes notes. All the above should be included and the following basic outline followed.</p>
								<ul class="cleanList">
									<li>a) the background to the film</li>
									<li>b) the plot</li>
									<li>c) a scene the student particularly enjoyed</li>
									<li>d) the students' feelings about it, possible recommendation</li>
								</ul>

								<p>Discussing these points together familiarises the group with the language, grammar and organisation. If the teacher takes the opportunity to make a clear collection of notes on the board, this will then provide a plan for paragraphing a piece of writing.</p>
								
								<br>
								<p>Stage 5:  Free Writing</p>
								<p>"Write a review of a film you have recently seen". <br>
								A word limit of, say, 300-400 words is good as this is realistically how much space they would be allowed in a magazine or newspaper.</p>
								<p>NOTES</p>
								<p>
								Before you begin this lesson you should think carefully: <br><br>

								What do my students need to know about how a review is written? <br><br>

								You cannot teach review writing if you have not first understood yourself how one is structured. Be careful about the type of review you choose. <br><br>

								Reviews are often full of cultural references that the students will find difficult. Here is an example:
								</p>
								<br>

								<h4><b>Pygmalion</b></h4>
								<p><b>Anyone who sees this marvellous production will have to agree that the play is far superior to Lerner and Lowe’s sugar candied musical. It’s a pleasure to see the play that Shaw wrote and to be reminded of his serious purpose back in 1914 when the play caused a scandal. The notion that turning a flowergirl into a lady might actually be a disservice may pack less punch today, but still resonates in these days of the WAG and airhead celeb.</b></p>	
								<p>Avoid giving such reviews as models; opt for ones with fewer cultural references, which your students will be able to understand. </p>
								<p>It's useful to prepare this lesson then either take your group to the cinema or watch a movie together and ask them to write a review of the film you have seen together.</p>
								<br>

								<h4>3.  Advanced</h4>
								<p>
								Advanced students need to recognise how signals are sent to the reader in order to guide them through how a text hangs together. Using listening passages or reading texts, or having other students giving an oral presentation can be used to present model texts for studying linking devices at higher levels.
								Composition writing - expressing an opinion</p>
								<br>	
								<p>Model text</p> 
								<br>
								<p>Nowadays it is difficult for students in Further and Higher Education to study as much as their course tutors would like them to. Indeed, it is unusual for students to spend anything more than a quarter of the recommended number of hours doing their coursework. Take the case of two students from Bristol University. Because of grant cuts and rising costs, they are forced to take low paid work, for example, bar work, supermarket shelf-stacking and kitchen work. For this, they can be paid less than £5.00 per hour. In other words, an hour's work would earn them just enough to buy a sandwich.</p>
								<br>
								
								<p><b>Stage 1  Students read the text and identify</b></p>
								<ul class="cleanList">
									<li>a) phrases which exemplify (for example, take the case of .....)</li>
									<li>b) phrases which amplify (indeed…..)</li>
									<li>c) language used for rephrasing (in other words…..)</li>
								</ul>
								<br>
								<p><b>Stage 2  Guided Writing</b></p>

								<p>Once the language for certain functions has been identified, it can be practised. A simple sentence can be given and students can be asked to amplify or exemplify accordingly. Gradually, language can be added with different functions, for example, presenting a balanced argument (While it may be time that . . .  Despite statistics which reveal that . . .  Although one can understand concern about . . .) The language for counterarguments, rejecting these and leading to a final conclusion gives the basics for a composition giving one's opinion.</p>
								<br>
								<p><b>Stage 3  Preparation for Writing</b></p>
								<p>Give students a plan</p>
								<ol>
									<li>Topic sentence - amplify</li>
									<li>Main viewpoint  - amplify</li>
									<li>Main viewpoint  - rephrase</li>
									<li>Counterargument - reject it and say why</li>
									<li>Conclusion - your own opinion</li>
								</ol>

								<p><b>Stage 4  Free Writing</b></p>

								{!! Form::open(['action' => 'Admin\Int120u3m3s1t3Controller@store', 'method' => 'POST']) !!}
															<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">
						
									<br>
									<div class="form-group">
										{!! Form::label('u3m3s1q4', '"All post-16 education should be free" - Give your opinion in about 100 words:') !!}
										{!! Form::text('u3m3s1q4', null, ['class' => 'form-control'])!!}
										
									</div>
									<br>
									{!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}
									{!! Form::close() !!}


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane" id="10a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						{{-- {!! Form::open(['action' => 'Admin\Int120u1m1s1Controller@store', 'method' => 'POST']) !!} --}}
						<div class="panel-heading">Task 1</div>
						<div class="panel-body">
						<p>Complete the table below using the 10 verbs in the list.</p>
						
						<table class="wp-table">
							{!! Form::open(['action' => 'Admin\Int120u3m3s1t4Controller@store', 'method' => 'POST']) !!}
						<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">
					    <tr>
					      <th>/t/</th>
					      <th>/d/</th>
					      <th>/id/</th>
					    </tr>
					    <tr>
					      <td>{{ Form::text('u3m3s1tans1', null, ['class' => 'form-control']) }}</td>
					      <td>{{ Form::text('u3m3s1dans1', null, ['class' => 'form-control']) }}</td>
					      <td>{{ Form::text('u3m3s1idans2', null, ['class' => 'form-control']) }}</td>
					      <
					    </tr>
					    <tr>
					      <td>{{ Form::text('u3m3s1tans2', null, ['class' => 'form-control']) }}</td>
					      <td>{{ Form::text('u3m3s1dans2', null, ['class' => 'form-control']) }}</td>
					      <td>{{ Form::text('u3m3s1idans2', null, ['class' => 'form-control']) }}</td>
					     
					    </tr>
					    <tr>
					      <td>{{ Form::text('u3m3s1tans3', null, ['class' => 'form-control']) }}</td>
					      <td>{{ Form::text('u3m3s1dans3', null, ['class' => 'form-control']) }}</td>
					      <td>{{ Form::text('u3m3s1idans3', null, ['class' => 'form-control']) }}</td>
					   
					    </tr>
					     <tr>
					      <td>{{ Form::text('u3m3s1tans4', null, ['class' => 'form-control']) }}</td>
					      <td>{{ Form::text('u3m3s1dans4', null, ['class' => 'form-control']) }}</td>
					      <td>{{ Form::text('u3m3s1idans4', null, ['class' => 'form-control']) }}</td>
					     
					    </tr>
					    <tr>
					      <td>{{ Form::text('u3m3s1tans5', null, ['class' => 'form-control']) }}</td>
					      <td>{{ Form::text('u3m3s1dans5', null, ['class' => 'form-control']) }}</td>
					      <td>{{ Form::text('u3m3s1idans5', null, ['class' => 'form-control']) }}</td>
					   
					    </tr>
					  
					  </table>
						</div>
						{{-- {!! Form::close() !!} --}}
					</div>
					<br>
					
						<div class="panel-heading">Task 2</div>
						<div class="panel-body">
						<br>
						<div class="form-group">
							<p>Write 3 words of your own (do not use examples from the module) containing 'gh' pronounced in 3 different ways.</p>
							<ol>	
								<li>{{ Form::text('u3m3s1q5', null, ['class' => 'form-control']) }}</li> <br>
								<li>{{ Form::text('u3m3s1q6', null, ['class' => 'form-control']) }}</li> <br>
								<li>{{ Form::text('u3m3s1q7', null, ['class' => 'form-control']) }}</li> <br>
							</ol>
						
						</div>
						
						</div>
				
					</div>
					<br>
					<div class="panel panel-default">
					
						<div class="panel-heading">Task 3</div>
						<div class="panel-body">
						<br>
						<div class="form-group">
							<p>Write 2 examples of your own for a voiced 'th' sound and 2 examples for an unvoiced 'th' sound. Clearly mark which is which!</p>
							<ol>
								<li>{{ Form::text('u3m3s1q8', null, ['class' => 'form-control']) }}</li>
								<li>{{ Form::text('u3m3s1q9', null, ['class' => 'form-control']) }}</li>
								<li>{{ Form::text('u3m3s1q10', null, ['class' => 'form-control']) }}</li>
								<li>{{ Form::text('u3m3s1q11', null, ['class' => 'form-control']) }}</li>
							</ol>
						
						</div>
						
						</div>
					
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">Task 5</div>
						<div class="panel-body">
							<p> Which other skill is closely linked with writing?<br>
								{{ Form::text('u3m3s1q12', null, ['class' => 'form-control']) }}</p>
								<p><li>What sort of classroom environment is best to encourage your students to write? <br>
								{{ Form::text('u3m3s1q13', null, ['class' => 'form-control']) }}</p>
								<p> What are the 4 basic questions which need answering before the student can tackle a particular piece of writing?<br>
								{{ Form::text('u3m3s1q14', null, ['class' => 'form-control']) }} <br>
								{{ Form::text('u3m3s1q15', null, ['class' => 'form-control']) }} <br>
								{{ Form::text('u3m3s1q16', null, ['class' => 'form-control']) }}<br>
								{{ Form::text('u3m3s1q17', null, ['class' => 'form-control']) }} </p>
								<br>
								<p>Here are the basic stages of a product writing lesson discussed in the module:</p>
								<p>A) Lead-in / Discussion</p>
								<ul class="cleanlist">

									<li>B) Stimulus (Model Text)</li>
									<li>C) Enabling activities for Main Writing Task</li>
									<li>D) Main Writing Task</li>
									<li>E) Follow-Up</li>
									<li>Match the following activities with the stages above:</li>
									<li>1. Reading and checking comprehension.</li>
									<li>2. Brainstorming what students know about the topic.</li>
									<li>3. Collaboration between and among students in the process of feedback.</li>
									<li>4. Solitary work; drafting and redrafting.</li>
								</ul>

								<p>5. Identifying grammatical structures and vocabulary appropriate for the writing task; exercises and games based on such structures and vocabulary. <br>

									Simply match the letter and number, eg <br>
									C-3 <br>
									(NB: this is just an example, NOT a correct answer)</p>

									<br>
									<p>Why is writing in class a useful exercise? Give at least two reasons.</p>
									{{ Form::text('u3m3s1q18', null, ['class' => 'form-control']) }} <br>
									{{ Form::text('u3m3s1q19', null, ['class' => 'form-control']) }} <br>
					
						</div>
						<br>
						{!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}
						{!! Form::close() !!}
						
					</div>
				</div>
			</div>
		</div>
	</div>

	
	{{-- / content section --}}
</div>



@endsection






