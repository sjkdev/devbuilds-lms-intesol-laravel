@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('content')

<div class="panel panel-default">
  <div class="panel-body">Unit Two Module Two</div>
</div>
<div class="panel panel-group">

  <div class="panel-heading"><h3>Unit 2 | The Study of English</h3></div>
  <div class="panel-heading"><h4>Module 2  Pronunciation, Stress and Intonation</h4></div>

  <div class="panel-body" id="exTab1">
    <ul  class="nav nav-pills">

      <li><a href="#0a" data-toggle="tab">Info</a></li>

      <li class="active"><a  href="#1a" data-toggle="tab" class="orangeText">Self Check 1</a></li>

      <li><a href="#2a" data-toggle="tab" class="orangeText">Self Check 2</a></li>

      <li><a href="#stress" data-toggle="tab">Stress</a></li>

      <li><a href="#3a" data-toggle="tab" class="orangeText">Self Check 3</a></li>

      <li><a href="#4a" data-toggle="tab" class="orangeText">Self Check 4</a></li>

      <li><a href="#5a" data-toggle="tab" class="orangeText">Self Check 5</a></li>

      <li><a href="#6a" data-toggle="tab" class="orangeText">Self Check 6</a></li>

    </ul>
    
  </div>
</div>

<div class="tab-content clearfix">
  <div class="tab-pane" id="0a">
    <div class="#">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3>Info</h3></div>
              <div class="panel-body">
                <ul>
                  <li>At the end of this module you will:-</li>
                  <li>a) understand the importance of teaching pronunciation</li>
                  <li>b) have some ideas on how to do this</li>

                </ul>

              </div>
              <div class="panel-body">
                <p>
                  Many teachers do not often involve their classes in formal speaking practice but are at pains to correct their students' pronunciation errors as and when they occur and to give advice on stress and intonation.
                </p>

                <p>
                  The matter of pronunciation rarely takes up a whole lesson because of its potentially repetitive nature; in fact it is not unusual to see it restricted to a five or ten minute portion in some lessons. It is good policy to include it in response to the making of a common error; some teachers do this at the end of a lesson, others consider it as a useful tool for varying the pace of a lesson and schedule it as a secondary activity in the middle. Wherever they occur pronunciation activities enable the students to concentrate on the same thing at the same time thus bringing the whole group under the teacher's control.
                </p>
                <p>
                  Although it may seem less important than the teaching of structures and vocabulary in terms of the limited time spent on it, pronunciation is an element essential to effective communication and is a subject with which all EFL student teachers are expected to be familiar.
                </p>
                <p>
                  Pronunciation is understood to include:-
                  <ul>
                    <li>a) intonation</li>
                    <li>b) stress</li>
                    <li>c) phonology (the sounds of the language)</li>
                  </ul>
                  
                  
                </p>
                <p>
                  Not all textbooks agree that the concept of pronunciation should be taught in this order although the sounds produced in individual words should come without too much difficulty if intonation and stress are focused upon within the structure of a sentence or phrase.
                </p>
                <p>

                  a) Intonation
                </p>
                <p>
                  Intonation is to do with how you say a word or phrase rather than what you say. Speakers can change the pitch of their voice making it higher or lower as and when required. Thus intonation is the 'music' of speech which can convey various feelings or attitudes such as surprise, curiosity, boredom, politeness, abruptness etc. It is therefore important for the speaker to convey his appropriate feelings at the time otherwise an incorrect impression might be gained by the listener and confusion or offence might be caused.
                </p>
                <p>

                  It is difficult to learn the rules of intonation as English has a wide intonation range compared with other languages; nevertheless students should be encouraged to acquire them naturally rather than to consciously learn them.
                </p>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="tab-pane" id="1a">
    <div class="#">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3>Self Check 1</h3></div>
              <div class="panel-body">


                <div class="panel-heading">
                  <p>
                    Experiment by saying the following as described in the brackets.
                  </div>
                  <div class="panel-body">
                    <ul>
                      <li>i) Oh! It's you!   (pleasant surprise)
                      (lack of enthusiasm)</li>
                      <li>ii) Turn the radio on please.  (polite request)
                      (repeated request, signs of impatience)</li>
                      <li>iii) He jogs three times every day.
                        Does he?  (with surprise)
                      (without surprise) </li>
                    </ul>
                  </p>

                  <br>  

                  <p>                                             
                    When teaching English to students with little experience of the language, teachers should note that there are two basic intonation patterns:
                  </p>
                  - the rising tone which is used in questions expecting a yes/no response or    to express surprise, disbelief etc. The voice rises sharply on the stressed      syllable.
                  <p>
                    eg The single word - Really? expects a yes/no response
                  </p>
                  <p>
                    As does:-
                    <ul>
                      <li>Did you see the Queen?</li>
                      <li>Would you like a scone?</li>
                      <li>The falling tone is used for statements, commands and for wh... questions. The voice rises sharply earlier in the sentence and then falls on the key word being stressed.</li>
                      <li>eg How's your brother?</li>
                      <li>Stand back!</li>
                      <li>Two returns to Bristol, please.</li>
                    </ul>

                  </p>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="tab-pane" id="2a">
      <div class="#">
        <div class="row">
          <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3>Self Check 2</h3></div>
                <div class="panel-body">

                  <div class="panel-heading">
                    Read the examples below and decide for each one if the tone is rising or falling:-
                   
                         {!! Form::open(['action' => 'Admin\Int120u2m2s1t1Controller@store', 'method' => 'POST']) !!}
        
                         <input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">

           
{{--  --}}
                    <div class="panel-heading">
                    A) Is Pierre French?</div>
                    <div class="panel-body">{{ Form::text('u2m2s1q1', null, ['class' => 'form-control'])  }}</div>

                    <div class="panel-heading">
                    B) No, he's from Belgium.</div>
                    <div class="panel-body">{{ Form::text('u2m2s1q2', null, ['class' => 'form-control'])  }}</div>
                    <hr>
                    <div class="panel-heading">
                    A) Which area? </div>
                    <div class="panel-body">{{ Form::text('u2m2s1q3', null, ['class' => 'form-control'])  }}</div>

                    <div class="panel-heading">
                    B) Bruges, I believe.   </div>
                    <div class="panel-body">{{ Form::text('u2m2s1q4', null, ['class' => 'form-control'])  }}</div>
                    <hr>
                    <div class="panel-heading">
                    A) Will you lend me £10?</div>
                    <div class="panel-body">{{ Form::text('u2m2s1q5', null, ['class' => 'form-control'])  }}</div>

                    <div class="panel-heading">
                    B) Certainly not.</div>
                    <div class="panel-body">{{ Form::text('u2m2s1q6', null, ['class' => 'form-control'])  }}</div>


                     {!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}

                    {!! Form::close() !!} 

                     </div>

                    <div class="panel panel-group">
                      <div class="panel-heading">
                        <p>
                          In repetition activities there are two main techniques of demonstrating falling and rising tones: the first is by gesture using arm and hand movements, the teacher taking care that the student will observe each movement starting on the left and finishing on the right; the second is simply by drawing arrows on the blackboard after the sentence or phrase, thus\ or /.
                      </p>
                    </div>
                  </div>

                  {{--  --}}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>



   
      <div class="tab-pane" id="stress">
        <div class="#">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4>Stress</h4>
                </div>
                <div class="panel-body">
                  <p>
                  Stress refers to the emphasis we place on the syllable of a word or on (a) word(s) within a sentence. It presents great difficulty for the foreign learner of English.</p>
                  <p>Unlike in a language such as Spanish, there are no easy rules in governing where the stress falls on a word. We have all made mistakes ourselves when pronouncing a word we have not seen or heard before. A native speaker can only work from experience with similar words but is not always guided towards correct pronunciation.</p>
                  <p>If we take the average sentence or utterance in English we will find stressed and unstressed words. The speaker will demonstrate the words that are of most importance to the listener by stressing them more, ie by making them more audible. For example, consider the sentence
                  </p>
                  <p>"I've lost my wallet!"
                    The words which are of most importance are 'lost' and 'wallet' and they will therefore be stressed.
                  There are various ways of marking where the stress falls in a sentence or a word. Some EFL teachers draw small squares above the place where the stress falls. For written exercises you could mark the stressed syllable in bold or underline where the stress falls.</p>
                </div>
                <div class="panel-body">
                  English makes ample use of stress in order to point to a context. You can ask the same question but can place the stress on different words depending on which fact you would like to have confirmed or denied.
                  <br>
                  <br>
                  <br>
                  <ul>
                    <li>eg Is Bernard going to France in July? (Stress on Bernard)</li>
                    <br>
                    <li>No, Zoe is.</li>
                    <hr>
                    <li>Is Bernard going to France in July? (Stress on France)</li>
                    <br>
                    <li>No, he's going to Belgium.</li>
                    <hr>
                    <li>Is Bernard going to France in July? (Stress on July)</li>
                    <br>
                    <li>No, he's going in August.</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  






        <div class="tab-pane" id="3a">
          <div class="#">
            <div class="row">
              <div class="col-md-12">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3>Self Check 3</h3></div>
                    <div class="panel-body">

                      <div class="panel-heading">
                        {!! Form::open(['action' => 'Admin\Int120u2m2s1t2Controller@store', 'method' => 'POST']) !!}
        
            <input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">

            

                        Indicate what part of the sentence in each question should be stressed to elicit each of the answers. Choose the word you would stress in the question.
                      </div>
                      <div class="panel-body">
                        <ul>
                          <li>Are you going to the disco tonight?{{ Form::text('u2m2s1q7', null, ['class' => 'form-control'])  }}</li>
                          <br>
                          <li>a) No, I'm going tomorrow.{{ Form::text('u2m2s1q8', null, ['class' => 'form-control'])  }}</li>
                          <br>
                          <li>b) No, I'm going to the cinema.{{ Form::text('u2m2s1q9', null, ['class' => 'form-control'])  }}</li>
                          <br>
                          <li>c) No, my friend is though.{{ Form::text('u2m2s1q10', null, ['class' => 'form-control'])  }}</li>
                          <br>
                          <li>d) I most certainly am.{{ Form::text('u2m2s1q11', null, ['class' => 'form-control'])  }} </li>
                        </ul>
                        {!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}

            {!! Form::close() !!} 

                      </div>

                      <div class="panel-heading">
                        As already stated, in English we place stress on the most important parts of the sentence or message we wish to be conveyed. The unstressed part of the sentence is more difficult to catch so the foreign learner has to train his ear to pick up the less important part of the message so he/she can fully understand what is being said.
                      </div>
                      <div class="panel-heading">
                        English is often referred to as a 'stress-timed' language. This means that the length of time between the stressed syllables is always about the same. The greater the number of unstressed syllables between those that are stressed, the quicker the unstressed syllables are uttered.
                      </div>
                      <div class="panel-body">

                        <ul>
                          <li>eg   He <span class="underline">gave</span> a <span class="underline">speech</span>.</li>
                          <li>He <span class="underline">gave</span> a short <span class="underline">speech</span>.</li>
                          <li>He <span class="underline">gave</span> a very short <span class="underline">speech</span>.</li>
                        </ul>
                        <div class="panel-body">
                          In each sentence, the unstressed syllables ( 'a', 'a short', 'a very short' ) took about the same amount of time to say, so 'a very short' had to be said more quickly.
                        </div>
                        <div class="panel-body">
                          However, there are times when normally unstressed words are stressed for obvious reasons:-
                          <ul>
                            <li>eg Bill and George are coming to the party. (stressing that George is to be included in the party guest list)</li>
                            <li>'And' and lots of other small words or weak forms (listed below) are habitually unstressed within the structure of a sentence unless used in isolation.</li>
                          </ul>                     
                        </div>
                        <div class="panel-heading">WEAK FORMS</div>  
                        
                        <div class="panel-body">
                          Prepositions:- at, to, of, for, from
                        </div>
                        <div class="panel-body">
                          Auxiliary and modal verbs:- be, been, am, is, are, was, were, have, has, had, do, does, shall, should, will, would, can, could, must
                        </div>
                        <div class="panel-body">
                          Pronouns:- me, he, him, his, she, we, us, you, your, them
                        </div>
                        <div class="panel-body">
                          Others:- who, that (as a relative pronoun), a, an, the, some, and, but, as, than, there, not
                        </div>
                        <div class="panel-body">
                          The sound produced in the weak form is called 'shwa' and is represented by the phonemic symbol as seen on the phonemic script chart at the end of this unit.
                        </div>
                        <div class="panel-body">
                          Practise these examples. The weak forms are underlined.
                          <ul>
                            <li>Take a look <span class="underline">at</span> it.</li>
                            <li>It's <span class="underline">for</span> you.</li>
                            <li>I <span class="underline">was</span> here yesterday.</li>
                            <li>We <span class="underline">must</span> go.</li>
                          </ul>


                        </div>
                        <div class="panl-heading">
                        Stress-timing is a noticeable characteristic of the spoken language. By getting used to hearing English spoken with a natural rhythm in class, students will find it easier to understand real English beyond the confines of the classroom. It will take some time however, for the students themselves to produce this sort of language so they need a lot of time and every encouragement in this endeavour.
                        </div>

                        <div class="panel-body">
                          <h4>
                         SOUNDS OF THE LANGUAGE/PHONOLOGY
                        </h4>
                        </div>
                        <div class="panel-body">
                        It is important when teaching students new vocabulary to indicate where the stress falls. With the majority there is no argument
                        </div>
                        <div class="panel-body">
                        eg <span class="underline">prog</span>ramme     <span class="tab-space">      cigar<span class="underline">ette</span>       <span class="tab-space">       <span class="underline">less</span>on
                        </div>
                        <div class="panel-body">
                        Remember, the stressed syllable is longer and more audible.
                        </div>
                        <div class="panel-body">
                        With other words there is disagreement on pronunciation, sometimes depending on your origins.
                        </div>
                        <div class="panel-body">
                          <ul>
                            <li>eg <span class="underline">har</span>ass and har<span class="underline">ass</span> (American origin)</li>
                            <li><span class="underline">cin</span>ema and cinem<span class="underline">a</span> (Southern English pronunciation)</li>
                            <li>adv<span class="underline">ert</span>isement and advert<span class="underline">ise</span>ment (localised northern pronunciation)</li>
                            <li>Carl<span class="underline">isle</span> and C<span class="underline">ar</span>lisle (local pronunciation)</li>
                          </ul>

                        </div>
                        <div class="panel-heading">
                                                At other times it is essential to differentiate:
                        <ul>
                          <li>eg <span class="underline">in</span>valid  (noun = incapacitated)</li>
                          <li>inv<span class="underline">al</span>id  (adjective = cannot be accepted)</li>
                        </ul>
                                                

                                                

                        </div>
                        <div class="panel-body">
                        Unstressed syllables are often pronounced as shwa (see chart) regardless of spelling. It is the vowel sound many British people make when hesitating in speech, spelt as 'uh' or 'er'. Here are some examples:-
                        <br>
                        <br>
                        <ul class="cleanList">
                          <li>Occupations: teacher, driver, doctor, sailor</li>
                          <br>
                          <li>Comparatives: longer, bigger, better</li>
                          <br>
                          <li>Beginning with'a': ago, about, along</li>
                          <br>
                          <li>Ending in : '-ory', '-ary : factory, library</li>
                          <br>
                          <li>Ending : '-ion', '-ian' : nation, Egyptian</li>
                          <br>
                          <li>Ending : '-man' : woman</li>
                          <br>
                          <li>Days: Sunday, etc.</li>
                          <br>
                          <li>Plurals: horses, matches etc.</li>
                          <br>
                          <li>Third person endings: washes etc.</li>
                          <br>
                          <li>Superlatives: shortest, fastest etc.</li>
                          <br>
                          <li>Ending '-age', '-ege' : luggage, language, college</li>
                          <br>
                          <li>Beginning 'be-', 're-' : begin, reply</li>
                        </ul>                     

                        </div>
                        <div class="panel-heading">
                          <h3>VOWELS AND CONSONANTS</h3>
                          <p>
                            All sounds, whether they are made by a vowel or a consonant are represented by a phonemic symbol (see chart) to enable the native as well as the foreign speaker to pronounce words correctly. The same letters or combination of letters can make a different sound eg a word such as 'the' is pronounced differently according to whether a vowel or a consonant follows. Compare the pronunciation of 'the' before 'book' with 'the' before 'apple'.
                          </p>
                        </div>
                        <div class="panel-body">
                          <h4>In English there are variations in the pronunciation of the same vowels</h4>
                          <ul>
                            
                            <li> eg contrast   hat    -     hard</li>
                            <li>bed   -     beer</li>
                            <li>bid    -     bird</li>
                            <li>dog   -     do       -      ford</li>
                            <li>cut    -     cute    -      could</li>
                          </ul>
                        </div>
                                                
                        <div class="panel-body">
                          <div class="panel-body">
                                                As you can see from this, different letters can be pronounced in an identical way: note the case of 'do' and 'cute'.
                        </div>
                        <div class="panel-heading">
                        <h4>
                        A FEW POINTS TO CONSIDER</h4></div>

                        <div class="panel-body">

                        Do not assume that students will automatically pick up (acquire) the ability to use stress and intonation if they are given enough exposure to the language; the natural acquisition of stress and intonation can be both difficult and slow. Teachers should not overlook the importance of pronunciation work in the classroom; simple exercises introduced into the EFL classroom can be of great value. Pronunciation work can be both fun and rewarding.
                        </div>
                        <div class="panel-body">
                                                DIALOGUES offer the perfect opportunity to focus on stress and intonation. When working with dialogues, clearly model the stress and intonation patterns of words, sentences or phrases to the class, and get the students to repeat after you (known as chorusing - there will be more information on chorusing in Unit 3 Module 1). Taking the time to focus on stress and intonation during dialogue work can help to promote more natural communication.
                        </div>
                        <div class="panel-body">
                          <h3>
                                                Look at the dialogue below:-</h3>
                        <ul>
                          <li>‘Hi, ……..(name), how’re you?’</li>
                          <li>‘Hi, ……..(name). Not so bad thanks, and you?’</li>
                          <hr>
                          <li>A) ‘Fine thanks. I haven’t seen you for ages. What’ve you been up to?’</li>
                          <li>B) ‘Nothing much. Just the same old things. And you? What have you been doing with yourself?’</li>
                          <hr>
                          <li>A) ’Well, I’ve been really busy with my new job, and I’m moving house next week.’</li>
                          <li> B) ’Really? Good luck with the move’ ……………..‘Anyway, it’s been nice seeing you.’</li>
                          <hr>
                          <li>A) ’You too. Take care. Bye.</li>
                          <li>B) ‘Bye.’</li>
                        </ul>
                                           
                        </div>
                        <div class="panel-body">
                        A simple dialogue such as this can be introduced into the EFL classroom to focus on stress and intonation. The teacher should model the dialogue first (taking the part of B) with a student. The teacher should use a flat, monotone voice. After the exchange, the teacher should ask for feedback on how the student felt during the exchange (eg that the teacher was not interested, etc). The teacher should then model the dialogue again with the same student, but this time focusing on stress and intonation patterns. The teacher should then ask for feedback again on how the student felt, thus increasing student awareness of the importance of stress and intonation. The teacher could then chorus certain pronunciation patterns form the dialogue before asking the students to practice the dialogue in pairs. (Facial expression and body language could also be encouraged.)
                        </div>
                        <div class="panel-body">
                        If you are teaching a homogenous group (of the same nationality), it is important to pay attention to the common pronunciation problems experienced by that nationality. For example, with Turkish students the most common problems are the pronunciation of the ‘soft’ th sound, and the V sound (it tends to be pronounced very softly, and can sound more like a W). Listen out for those problematic phonemes, ensure that you model them clearly and correctly, and give the students plenty of opportunity to practise them.
                        </div>
                        <div class="panel-body">
                        <strong>MINIMAL PAIRS</strong>:- Minimal pair work can be beneficial for contrasting and practising similar sounds which students experience problems with. The term ‘minimal pair’ refers to two words within a language which have different meanings, but which differ in one sound (phoneme) only. Examples of this in English are words like FAT and HAT. There are many of these in the English language. Which minimal pairs cause a student problems, depends on the phonetics of their own language and their language of study (L1 and L2). For example, Japanese learners of English have difficulty differentiating between FAT and HAT. Another example would be eel and heel for French learners of English because the French language lacks an H sound. <br> <br>
                        As a result of the students’ lacking certain sounds in their native language, they will have a very difficult time clearly differentiating between the sounds both when they hear them, and when they attempt to pronounce them. <br> <br>
                        When using minimal pair activities in the classroom, It is important firstly to increase students’ awareness of what minimal pairs are, and why they can cause students problems. <br> <br>
                        A teacher who understands his or her students’ L1 can create lists and card sets of minimal pairs which cause her students difficulties. There are a number of ways of introducing minimal pair activities, eg the pairs can be recorded and played back to students for listening practice, asking students to identify which sounds they hear. The minimal pairs can also be recorded within the context of sentences, again asking students to identify the sounds they hear. Card games can be introduced where students have to ask other students for particular cards, providing practice in both productive and listening skills.
                      </div>
                      </p>

                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
          <div class="tab-pane" id="4a">
            <div class="#">
              <div class="row">
                <div class="col-md-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3>SELF-CHECK 4</h3></div>
                      <div class="panel-heading">
                      

                          Assign each of the following words to the phonemic symbol which represents the vowel sound:
                        <div class="panel-body">
                               {!! Form::open(['action' => 'Admin\Int120u2m2s1t3Controller@store', 'method' => 'POST']) !!}
                                
                                    <input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">

                                   
                          <ul class="cleanList">
                            <li>horn {{ Form::text('u2m2s1q12', null, ['class' => 'form-control']) }}</li><br>
                      
                            <li>foot {{ Form::text('u2m2s1q13', null, ['class' => 'form-control']) }}</li><br>
                            <li>seat {{ Form::text('u2m2s1q14', null, ['class' => 'form-control']) }}</li><br>
                            <li>word {{ Form::text('u2m2s1q15', null, ['class' => 'form-control']) }}</li><br>
                            <li>lard {{ Form::text('u2m2s1q16', null, ['class' => 'form-control']) }}</li><br>
                            <li>mud {{ Form::text('u2m2s1q17', null, ['class' => 'form-control']) }}</li><br>
                            <li>rod {{ Form::text('u2m2s1q18', null, ['class' => 'form-control']) }}</li><br>
                            <li>sit {{ Form::text('u2m2s1q19', null, ['class' => 'form-control']) }}</li><br>
                            <li>rude {{ Form::text('u2m2s1q20', null, ['class' => 'form-control']) }}</li><br>
                            <li>fed {{ Form::text('u2m2s1q21', null, ['class' => 'form-control']) }}</li><br>
                            <li>mad {{ Form::text('u2m2s1q22', null, ['class' => 'form-control']) }}</li>
                          </ul>
                          {!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}

                                    {!! Form::close() !!} 
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="tab-pane" id="5a">
              <div class="#">
                <div class="row">
                  <div class="col-md-12">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3>Self Check 5</h3></div>
                        <div class="panel-body">
                          <p>
                            Find a word of your own to assign to each symbol on the chart provided. <br><br>Write the symbol after each of your 44 chosen words.
                            There are two types of vowel in English: those represented by the phonemic symbols above are monothongs meaning that the vowel sound consists of one phoneme. (A phoneme is the smallest unit of speech). <br><br> There are also diphthongs which are often recognised by two vowels together, but not always.
                            <br><br>
                            'Beer' contains a diphthong, where you experience a gradual change in lip and tongue position during the making of the sound. 'Bay' is another example, it is monosyllabic but has two phonemes.
                            <br><br>
                            In all there are 20 vowel sounds and 24 consonant sounds compared with, for example, Japanese and Spanish which have only 5 vowel sounds.
                          </p>
                        </div>
                      </div>
                      <div class="panel panel-group">
                        <div class="panel-body">
                          {!! Form::image('https://photos-6.dropbox.com/t/2/AABERfNtHIohberDY6cNfQK_O7V8zJIywwrn3h9hp4esMg/12/870100512/jpeg/32x32/1/_/1/2/phonemic-chart-900x480.jpg/EICxirMJGIEBIAIoAg/uIk9wQcxBHhW4mWbjYjlTZ7wvkvLcZWRUvuHgWMEmXU?size=2048x1536&size_mode=3') !!}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="tab-pane" id="6a">
                <div class="#">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h3>Self Check 6</h3></div>
                          <div class="panel-body">
                            <p>Now look at the consonant section of the phonemic script chart. Which phonemic symbol represents the letters underlined in each of the following words?</p>

                            <table>
                                <tbody>
                                <tr>
                                <td>lorry</td>
                                <td>mash</td>
                                </tr>
                                <tr>
                                <td>vast</td>
                                <td>ring</td>
                                </tr>
                                <tr>
                                <td>then</td>
                                <td>yellow</td>
                                </tr>
                                <tr>
                                <td>nylon</td>
                                <td>measure</td>
                                </tr>
                                <tr>
                                <td>hope</td>
                                <td>go</td>
                                </tr>
                                <tr>
                                <td>road</td>
                                <td>weep</td>
                                </tr>
                                <tr>
                                  <td>joke</td>
                                  <td>fast</td>
                                </tr>
                                <tr>
                                  <td>choke</td>
                                  <td>crisp</td>
                                </tr>
                                <tr>
                                  <td>think</td>
                                  <td>plus</td>
                                </tr>
                                <tr>
                                  <td>puzzle</td>
                                  <td>press</td>
                                </tr>
                                </tbody>
                                </table>

                              </div>


                              <div class="panel panel-group">
                                <div class="panel-heading">
                                  <h4>TASK 1</h4>
                                </div>
                                <div class="panel-body">
                                  <h5>Read the examples below and decide for each one if the tone is rising or falling:-</h5>
                                  {!! Form::open(['action' => 'Admin\Int120u2m2s1t4Controller@store', 'method' => 'POST']) !!}
        
                                  <input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">
                                  <br>

                                  <div class="panel-body"><h5>A) Who goes there?</h5>
                                    {{ Form::text('u2m2s1q23', null, ['class' => ' form-control']) }}
                                    
                                  </div>

                                  <div class="panel-body"><h5>B) It's only me.</h5>
                                    {{ Form::text('u2m2s1q24', null, ['class' => ' form-control']) }}
                                    
                                  </div>

                                  <div class="panel-body"><h5>A) Bread?</h5>
                                    {{ Form::text('u2m2s1q25', null, ['class' => ' form-control']) }}
                                    
                                  </div>

                                  <div class="panel-body"><h5>B) No thanks.</h5>
                                    {{ Form::text('u2m2s1q26', null, ['class' => ' form-control']) }}
                                    
                                  </div>

                                  <div class="panel-body"><h5> A) I'm not going to Paris.</h5>
                                    {{ Form::text('u2m2s1q27', null, ['class' => ' form-control']) }}
                                    
                                  </div>

                                  <div class="panel-body"><h5> B) No?</h5>
                                    {{ Form::text('u2m2s1q28', null, ['class' => ' form-control']) }}
                                    
                                  </div>
                                 

                                </div>
                                
                              </div>

                              <div class="panel panel-group">{{-- start panel group --}}
                                <div class="panel-heading">
                                  Indicate two tone changes in each of the following sentences; decide on which words the tone rises and falls.
                                </div>
                                <div class="panel-body">
                                

                                  <div class="panel-body"><h5> A) Do you travel to France by boat or by plane? </h5>
                                    Rising{{ Form::text('u2m2s1q29',null, ['class' => 'form-control']) }}
                                    <br><br>
                                    Falling{{ Form::text('u2m2s1q30',null, ['class' => 'form-control']) }}
                                    
                                  </div>
                                  <div class="panel-body"><h5> B) Oh! We always fly.</h5>
                                    Rising{{ Form::text('u2m2s1q31',null, ['class' => 'form-control']) }}
                                    <br><br>
                                    Falling{{ Form::text('u2m2s1q32',null, ['class' => 'form-control']) }}
                                    
                                  </div>
                               
                                </div>
                                
                              </div> {{-- end panel group --}}

                              

                              <div class="panel panel-group">{{-- start panel group --}}
                                <div class="panel-heading">
                                  <h4>TASK 2</h4>
                                </div>
                                <div class="panel-body">
                                  <p>Identify the words you think could be stressed in each of the sentences below. Explain why those words are being stressed (only one word or one set of words per sentence). This task is NOT about normal sentence stress; it is about how we can stress particular words to point to a context.
                              eg ‘I don’t want to go there’: ‘I’ might be stressed to emphasise that although everyone else might want to go, I definitely do not want to go. Or, ’there’ might be stressed to emphasize that it is the place that is the problem, but that I might be willing to go somewhere else.
                              Hint: Read out each sentence several times so you can work out which word or words would be stressed and the reason for that stress.</p>
                                </div>
                               
                                  <div class="form">
                              

            
                                  <div class="panel-body">
                                    <h4>1. He telephoned the police yesterday</h4>
                                    {{ Form::text('u2m2s1q32', null, ['class' => '  form-control'])  }}
                                    
                                  </div>
                                  <div class="panel-body">
                                    <h4>2. Can you show me the diamonds?</h4>
                                    {{ Form::text('u2m2s1q33', null, ['class' => '  form-control'])  }}
                                    
                                  </div>
                                  <div class="panel-body">
                                    <h4>3. Paul has gone to the theatre.</h4>
                                    {{ Form::text('u2m2s1q34', null, ['class' => '  form-control'])  }}
                                    
                                  </div>
                                  <div class="panel-body">
                                    <h4>4. Have you heard that Marcella's won £50,000 on the lottery?</h4>
                                    {{ Form::text('u2m2s1q34', null, ['class' => '  form-control'])  }}
                                    
                                  </div>
                                  <div class="panel-body">
                                    <h4>5. I'm not doing anything this evening. </h4>
                                    {{ Form::text('u2m2s1q35', null, ['class' => '  form-control'])  }}
                                    
                                  </div>
                          
                              </div> {{-- end panel group --}}

                              <div class="panel panel-group">{{-- start panel group --}}
                                <div class="panel-heading">
                                  <h4>TASK 3</h4>
                                </div>
                                <div class="panel-body">
                                  
                                </div>
                              </div> {{-- end panel group --}}

                              <div class="panel panel-group">{{-- start panel group --}}
                                <div class="panel-heading">
                                  Indicate by writing the appropriate word, what part of the question would be stressed in order to elicit each of the answers. 
                                </div>
                                
                              </div> {{-- end panel group --}} 

                              <div class="panel panel-group">{{-- start panel group --}}
                                <div class="panel-heading">
                                  <h5>1. Did you say you were leaving tonight, for Wales?</h5>
                                </div>
                                <div class="panel-body">
                               

            
                                  <ul class="cleanList">
                                    <li>a) No, I said Wells, in Somerset. <span class="tab-space">{{ Form::text('u2m2s1q36') }}</span></li>
                                    <li>b) No, I'm off tomorrow. <span class="tab-space">{{ Form::text('u2m2s1q37') }}</span></li>
                                    <li>c) I most certainly did not. <span class="tab-space">{{ Form::text('u2m2s1q38') }}</span></li>
                                    <li> d) No, my twin brother said that. <span class="tab-space">{{ Form::text('u2m2s1q38') }}</span></li>
                                  </ul>
                              
                                </div>
                              </div>{{-- end panel group --}} 

                              <div class="panel panel-group">{{-- start panel group --}}
                            
                                <div class="panel-heading">
                                  <h5>2. Is he going to take that job as a salesman abroad?</h5>
                                </div>
                                <div class="panel-body">
                                  <ul class="cleanList">
                                    <li>a) No, he's staying in this country. <span class="tab-space">{{ Form::text('u2m2s1q39') }}</span></li>
                                    <li> b) No, he's changed his mind. <span class="tab-space">{{ Form::text('u2m2s1q40') }}</span></li>
                                    <li>c) No, he's been offered a job as a divisional manager overseas. <span class="tab-space">{{ Form::text('u2m2s1q41') }}</span></li>
                                  </ul>
                                </div>
                              </div>{{-- end panel group --}} 

                              <div class="panel panel-group">{{-- start panel group --}}
                                <div class="panel-heading">
                                  <h5>3. Are you seeing Colin this afternoon on the promenade?</h5>
                                </div>
                                <div class="panel-body">
                                  <ul class="cleanList">
                                    <li>a) No, I'm seeing Roger.<span class="tab-space">{{ Form::text('u2m2s1q42') }}</span></li>
                                    <li>b) No, it'll have to be this evening as I'm busy.<span class="tab-space">{{ Form::text('u2m2s1q43') }}</span></li>
                                    <li>c) Yes, most definitely.<span class="tab-space">{{ Form::text('u2m2s1q44') }}</span></li>
                                    <li>d) No, I'm seeing him in the arcade.<span class="tab-space">{{ Form::text('u2m2s1q45') }}</span></li>
                                  </ul>
                                </div>
                              </div>{{-- end panel group --}} 

                              <div class="panel panel-group">{{-- start panel group --}}
                                <div class="panel-heading">
                                  <h5>4. Is your son flying to Paris?</h5>
                                </div>
                                <div class="panel-body">
                                  <ul class="cleanList">
                                    <li>a) No, he's off to Brussels.<span class="tab-space">{{ Form::text('u2m2s1q46') }}</span></li>
                                    <li>b) No, it's my daughter who's going.<span class="tab-space">{{ Form::text('u2m2s1q47') }}</span></li>
                                    <li>c) No, you know he's scared of flying.<span class="tab-space">{{ Form::text('u2m2s1q48') }}</span></li>
                                    <li>d) Yes, he's going tomorrow night.<span class="tab-space">{{ Form::text('u2m2s1q49') }}</span></li>
                                  </ul>
                                </div>
                              </div>{{-- end panel group --}} 


                              <ul>
                               {{--  <li>
                                1. v o {{ Form::radio('single') }}c a{{ Form::radio('single') }} b u {{ Form::radio('single') }} l a {{ Form::radio('single') }} r y {{ Form::radio('single') }}
                                </li> --}}
                              </ul>
                            <div class="panel panel-group">{{-- start panel group --}}
                                <div class="panel-heading">
                                  <h5>TASK 4</h5>
                                </div>
                                <div class="panel-body">
                                  <p>Identify the weak forms in these sentences. Hint: there is more than one weak form present in most of the sentences; some of the multi-syllabic words may also contain the weak form. Try saying the sentences aloud a few times in order to identify them.</p>
                                  
                                  <ul class="cleanList">
                                    <li>1. A quarter to eight.{{ Form::text('u2m2s1q50', null, ['class' => ' form-control']) }}
                                     </li>
                                    <li>2. She can speak Spanish.{{ Form::text('u2m2s1q51', null, ['class' => ' form-control']) }}
                                     </li>
                                    <li>3. The book that I want.{{ Form::text('u2m2s1q52', null, ['class' => ' form-control']) }}
                                     </li>
                                    <li>4. Where does the bus go?{{ Form::text('u2m2s1q53', null, ['class' => ' form-control']) }}
                                     </li>
                                    <li>5. How do you do?{{ Form::text('u2m2s1q54', null, ['class' => ' form-control']) }}
                                     </li>
                                    <li>6. I bought a present for Leo.{{ Form::text('u2m2s1q54', null, ['class' => ' form-control']) }}
                                     </li>
                                  </ul>
                                </div>
                              </div>{{-- end panel group --}} 

                              <div class="panel panel-group">{{-- start panel group --}}
                                <div class="panel-heading">
                                  <h5>TASK 5</h5>

                              <p>In brackets, identify the stressed syllable in the following words (i.e. h[e]llo):-</p>
                                </div>
                                <div class="panel-body">
                                  <ul class="cleanList">
                                    <li>1. vocabulary<span class="tab-space">{{ Form::text('u2m2s1q55') }}</span></li>
                                    <li> 2. permanent.<span class="tab-space">{{ Form::text('u2m2s1q56') }}</span></li>
                                    <li>3. spoken<span class="tab-space">{{ Form::text('u2m2s1q57') }}</span></li>
                                    <li>4. however<span class="tab-space">{{ Form::text('u2m2s1q58') }}</span></li>
                                    <li>5. procedure<span class="tab-space">{{ Form::text('u2m2s1q59') }}</span></li>
                                    <li>6. produce (verb)<span class="tab-space">{{ Form::text('u2m2s1q60') }}</span></li>
                                    <li>7. produce (noun)<span class="tab-space">{{ Form::text('u2m2s1q61') }}</span></li>
                                    <li>8. beyond<span class="tab-space">{{ Form::text('u2m2s1q62') }}</span></li>
                                    <li>9. encourage<span class="tab-space">{{ Form::text('u2m2s1q63') }}</span></li>
                                    <li>10. linoleum<span class="tab-space">{{ Form::text('u2m2s1q64') }}</span></li>
                                    
                                  </ul>
                                </div>
                              </div>{{-- end panel group --}} 

                              <div class="panel panel-group">{{-- start panel group --}}
                                <div class="panel-heading">
                                  <h5>TASK 6</h5>

                              <p>Look at the words below, identify where there would be a shwa sound by marking that part of the words in brackets. Hint: a number of the words contain more than one shwa sound. Again try saying the words aloud a few times to identify them.</p>
                                </div>
                                <div class="panel-body">
                                  <ul class="cleanList">
                                    <li>1. newspaper<span class="tab-space">{{ Form::text('u2m2s1q65') }}</span></li>
                                    <li> 2. director<span class="tab-space">{{ Form::text('u2m2s1q66') }}</span></li>
                                    <li>3. industry<span class="tab-space">{{ Form::text('u2m2s1q67') }}</span></li>
                                    <li>4. omelette<span class="tab-space">{{ Form::text('u2m2s1q68') }}</span></li>
                                    <li>5. recognise<span class="tab-space">{{ Form::text('u2m2s1q69') }}</span></li>
                                    <li>6. photograph<span class="tab-space">{{ Form::text('u2m2s1q70') }}</span></li>
                                    <li>7. photographer<span class="tab-space">{{ Form::text('u2m2s1q71') }}</span></li>
                                    <li>8. furniture<span class="tab-space">{{ Form::text('u2m2s1q72') }}</span></li>
                                    <li>9. hesitation<span class="tab-space">{{ Form::text('u2m2s1q73') }}</span></li>
                                    <li>10. approximate<span class="tab-space">{{ Form::text('u2m2s1q74') }}</span></li>
                                    
                                  </ul>

                                  <br>
                                  {!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}

                                  {!! Form::close() !!} 
                                </div>
                                <div class="panel panel-group">
                                <div class="panel-body">
                                  {!! Form::image('https://photos-6.dropbox.com/t/2/AABERfNtHIohberDY6cNfQK_O7V8zJIywwrn3h9hp4esMg/12/870100512/jpeg/32x32/1/_/1/2/phonemic-chart-900x480.jpg/EICxirMJGIEBIAIoAg/uIk9wQcxBHhW4mWbjYjlTZ7wvkvLcZWRUvuHgWMEmXU?size=2048x1536&size_mode=3') !!}
                                  {!! Form::image('http://cdn.eslbase.com/wp-content/uploads/phonemic-chart-900x480.jpg')!!}
                                </div>
                              </div>
                              </div>{{-- end panel group --}} 
                              


                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              @endsection