@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('content')


<div class="panel panel-default">
    <div class="panel-body">Unit Two Module One</div>
  </div>
<div class="panel panel-group">
<div class="panel-heading"><h3>Unit 2 | The Study of English</h3></div>
<div class="panel-heading"><h4>Module 1 | Grammar</h4></div>

	<div class="panel-body" id="exTab1">
		<ul  class="nav nav-pills">

		<li class="active"><a href="#0a" data-toggle="tab" style="background-color:#fff;">Info</a></li>

        <li><a  href="#1a" data-toggle="tab">Grammar</a></li>

        <li><a href="#2a" data-toggle="tab" class="orangeText">Self Check</a></li>

        <li><a href="#3a" data-toggle="tab" class="blueText">Task 1</a></li>

        <li><a href="#4a" data-toggle="tab" class="blueText"> Task 2</a></li>

        <li><a href="#5a" data-toggle="tab" class="blueText"> Task 3</a></li>

        <li><a href="#6a" data-toggle="tab" class="blueText"> Task 4</a></li>

        <li><a href="#7a" data-toggle="tab" class="blueText"> Task 5</a></li>
    </ul>
		
	</div>
</div>


    <div class="tab-content clearfix">
        <div class="tab-pane active" id="1a">
            <div class="#">
                <div class="row">
                    <div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">GRAMMAR & GRAMMATICAL TERMINOLOGY</div>

							<div class="panel-body">
							USE THE
							GRAMMAR REFERENCE
							IN THIS COURSE
							</div>
							<div class="panel-body">

						NB! You will not achieve a good grade without using it! It is not cheating, it is part of the learning process! <br></div>

						<hr>
						
						<div class="panel-body">
							<p>
							This module is designed to make sure that you are aware of your own grammar, and aware that it is essential to get to grips with it. When you are teaching EFL you do not need to tell your students the grammatical terminology unless they specifically ask for the information (language-conscious nationalities will, even at lower levels) or unless you are teaching advanced students who need the terminology. However you do need to know both what point you are teaching and why you are teaching it.
							<br> <br>
							In order to complete this unit you will need to refer to the Back to Basics Grammar Reference included with the course. For your future career in TESOL you would be advised to obtain a grammar book. If you have not examined different grammars, we do not suggest that you buy one at this stage, as there are many on the market and you may not be comfortable with the first one you see. A good library will have a selection to choose from. When you have examined a few we suggest that you get hold of a copy of Practical English Usage by Swan or A Practical English Grammar by Thomson and Martinet, whichever you feel you can most comfortably use. If you are already working in the field of TEFL and have a different grammar you should be able to use that so long as it was not written for use by Shakespeare!
							<br> <br>
							It is worth remembering that people enter the world of TEFL from a variety of backgrounds. Therefore some of you will find these tasks quite straightforward due to your academic background, while others will need to spend time searching through a grammar in order to make sense of them. Take your time, that is the advantage of a Distance Learning Course - there is no pressure to be the first to finish.
							<br> <br>
							Do not depress yourself with the notion that there is somewhere one ultimate set of grammatical rules which we 'know' and you must learn. This is not true. There are many descriptions of the grammar, fashions change, and some are better in some areas than others. When you are teaching and you do not know the rule (let us assume this is a momentary lapse as you should know it!) use deductive analysis (explained in this module).
							<br> <br>
							By definition you, as a native speaker (if you are a native speaker), 'know' the grammar of English perfectly - your problem is to see it as a foreign language, to know about it and to understand the problems it poses to a foreign learner. (Non-native speakers have a distinct advantage here!) Think of this all the time that you are doing the tasks, think of simple examples where you are asked for them, make life easy for yourself and your future students!
							DEDUCTIVE ANALYSIS
							<br> <br>
							Always check out any grammatical structure which you are unsure of and are expected to teach. Have your own 'bible' which you can rely on as a reference before a lesson. You will need your grammar throughout your TEFL career. In this unit you are expected to use your grammar, to familiarise yourself with the layout and become skilled at using it as a quick reference.
							<br> <br>
							BUT WHAT HAPPENS IF I HAVEN'T GOT MY GRAMMAR RESOURCE WITH ME?
							<br> <br>
							If you do not know a particular grammatical structure well, but you wish to explain it to your students try using DEDUCTIVE ANALYSIS. This means working it out for yourself.  Write yourself some examples. One example is very dangerous in English, there is a good chance it will be an idiom or an exception. Two examples have a better chance of illustrating an emerging pattern, but there is still a chance that one will be an exception. Three examples are much safer, hopefully two of the three will illustrate the structure and you should then be able to explain the structure successfully.
							<br> <br>
							In theory it should never happen that you do not know a grammatical structure well enough to be able to explain it, exemplify it and name it. HOWEVER, early on in your TEFL career it may happen. It may also happen if you are given no warning nor preparation time before a lesson. This can happen if you are substituting for an absent colleague or on badly organised summer schools. Otherwise you should MAKE SURE you know what you are teaching by thoroughly preparing your lessons.
							<br> <br>
							NB  One hour of preparation is the minimum for a one-hour lesson, especially in your first year of teaching.
							</p>

						</div>
						</div>

                    	

                    </div>
                </div>
            </div>

        </div>

        <div class="tab-pane" id="0a">
        	<div class="#">
        		<div class="row">
        			<div class="col-md-12">
        				<div class="panel panel-default">
        				<div class="panel-heading">
        				<h3>Info</h3></div>
        				<div class="panel-body">
							<ul>
								<li>At the end of this module you will:-</li>
								<li>a) be able to find your way around a grammar</li>
								<li>b) have more understanding of grammatical terminology</li>
								<li>c) understand the importance of knowing grammar</li>
							</ul>

						</div>
						</div>
        			</div>
        		</div>
        	</div>
        </div>

       

        <div class="tab-pane" id="2a">
            <div class="#">
                <div class="row">
                    <div class="col-md-12">
                    	<div class="panel panel-default">
                    	<div class="panel-heading">

                        <h3>Self Check</h3>

                        </div>
                        <div class="panel-body"> 
						Try to use Deductive Analysis here:
						</div>
						{!! Form::open(['action' => 'Admin\Int120u2m1s1t1Controller@store', 'method' => 'POST']) !!}
        
            			<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">

            

						{{-- unit content --}}
						<div class="panel-heading">Explain when to use the preposition 'at'</div>
						<div class="panel-body">{{ Form::text('u2m1s1q1', null, ['class' => 'bladeInputWidth100'])  }}</div>

						 
						<div class="panel-heading">Explain the use of the three prepositions 'on', 'in' and 'at' as used with expressions of time.</div>
						<div class="panel-body">{{ Form::text('u2m1s1q2', null, ['class' => 'bladeInputWidth100'])  }}</div>

						<div class="panel-heading">Explain the difference between 'count' and 'mass' nouns</div>
						<div class="panel-body">{{ Form::text('u2m1s1q3', null, ['class' => 'bladeInputWidth100'])  }}</div>



						<div class="panel-heading">Work out some rules for the use of count and mass nouns. (Include the verb)</div>
						<div class="panel-heading">MY RULES</div>
						<div class="panel-body">{{ Form::text('u2m1s1q4', null, ['class' => 'bladeInputWidth100'])  }}</div>


						<div class="panel-heading">
						Write examples to show the use of 'some' and 'any' in these cases:</div>
						<br> <br>
						<div class="panel-heading">
						Beginners - 'some' in positive sentences (in contrast to 'a')</div>
						<div class="panel-body">{{ Form::text('u2m1s1q5', null, ['class' => 'bladeInputWidth100'])  }}</div>


						<div class="panel-body">
						Elementary - 'any' in negative sentences</div>
						<div class="panel-body">{{ Form::text('u2m1s1q6', null, ['class' => 'bladeInputWidth100'])  }}</div>



						<div class="panel-body">
						Elementary - 'any' in questions</div>
						<div class="panel-body">{{ Form::text('u2m1s1q7', null, ['class' => 'bladeInputWidth100']) }}</div>

						<div class="panel-body">
						Elementary - 'some' with questions (when you expect a positive answer)</div>
						<div class="panel-body">{{ Form::text('u2m1s1q8', null, ['class' => 'bladeInputWidth100'])  }}</div>

						<div class="panel-body">
						Intermediate - 'any' in positive sentences</div>
						<div class="panel-body">{{ Form::text('u2m1s1q9', null, ['class' => 'bladeInputWidth100'])  }}</div>
						<br>
						{!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}

        				{!! Form::close() !!} 

						
						<div class="panel-body">
						Each time a new aspect of the structure is introduced, the previous forms should be practised again and consolidated. Each form can be taught as a definite case which must be followed and only later do you point out and teach alternatives and contradictions.</div>
					</div>


						<div class="panel panel-default">
							
							<div class="panel-body">
								<p>These examples are for you to check your own understanding.</p>
								<p>NOT TO BE SUBMITTED</p>
								<p>GRAMMAR</p>
							</div>
								<div class="panel-body">
						<p>

						Grammar, of course, is the backbone of any language. You cannot successfully communicate in a language if you do not first have an understanding of the grammar rules.
						As an English teacher it is essential to have a good understanding of the grammar rules of English in order to be able to teach and assist students in their language acquisition.
						Later in the course you will look at specific techniques for integrating grammar into the classroom. However, at this stage, we encourage you to first fully acquaint yourself with the grammar materials presented, analyse your own grammar knowledge and identify any areas where you may need to study further.
						<br> <br>
						Inductive and Deductive approaches to Grammar Teaching
						<br> <br>
						In TEFL there are two main approaches to teaching grammar. The more traditional approach is the Deductive approach, while the more modern approach is the Inductive approach.
						<br> <br>
						The Deductive approach is the more traditional approach, which most students are more familiar with, where the grammatical rules and structures are presented first. Only after being presented with the rules are the students asked to apply those rules through a variety of exercises.<br> <br>
						For example, if the lesson is focussing on the present perfect tense, the teacher would outline the uses and rules of this tense. The students would then be asked to put into practice the rules of this tense by completing a number of exercises. In this approach, the class is very teacher directed; the teacher is the centre of attention, responsible for presenting and explaining all new materials.
						<br> <br>
						The Inductive approach represents a more modern, student centred approach to language education where the grammatical rules and structures are presented to the students within a meaningful language context. The students learn the use of the target structures through practice of the language in context and later are asked to identify the rules from the practical examples.
						For example, if the structure to be presented is the comparative form, the teacher might begin the lesson by drawing a figure on the board and say ‘This is Bill. He is tall.’ The teacher would then draw another figure and say ‘This is Ted. Ted is taller than Bill.’ The teacher would then provide many more examples using the students, pictures or items in the classroom, to develop students’ understanding of the structure.<br> <br>
						The students are asked to repeat after the teacher, then the students are led into meaningful practice of the structures in pairs or groups. <br> <br>
						After meaningful practice the students are given worksheets which lead them to identify the rules for themselves. The role of the teacher is to provide meaningful contexts to demonstrate the grammatical rules, while the students develop their understanding of the rules through examples and practice.
						<br> <br>
						In both approaches the students practise and apply the use of grammatical structures, yet there are advantages and disadvantages to both. For higher level students, who already have a good understanding of the basic structures of the language, the Deductive approach can be effective. However, the Deductive approach is less suitable for lower level students and younger learners for presenting grammatical structures that are complex in both form and meaning.
						The advantage of the Inductive approach is that students can focus on the use of the language without being held back by grammatical terminology and rules that can inhibit fluency. The Inductive approach also promotes increased student participation and practice of the target language in meaningful contexts, which should be the aim of all EFL classes.  
						<br> <br>
						The use of the Inductive approach has been noted for its success in the EFL classroom. However, some students used to more traditional teaching styles in the classroom, may initially find this approach difficult. Therefore, it is important to gradually build students’ confidence through carefully graded practice to ensure student success.
						</p>

						



                       {{-- place content here --}}
                    </div>
				</div>
		
                </div>
</div>
            </div>
        </div>
        <div class="tab-pane" id="3a">
        	<div class="#">
        		<div class="row">
        			<div class="col-md-12">
        				<div class="panel panel-default">
        					<div class="panel-heading"><h4>Use an example of each of the following in a sentence and underline it.</h4></div>
        					<div class="panel-body">
        						{!! Form::open(['action' => 'Admin\Int120u2m1s1t2Controller@store', 'method' => 'POST']) !!}
        
				            	<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">

				            
								1. an adjective</div>
								<div class="panel-body">{{ Form::text('u2m1s1q10', null, ['class' => 'bladeInputWidth100'])  }}</div>

								<div class="panel-body">
								2. an adverb</div>
								<div class="panel-body">{{ Form::text('u2m1s1q11', null, ['class' => 'bladeInputWidth100'])  }}</div>
								
								<div class="panel-body">
								3. a countable noun</div>
								<div class="panel-body">{{ Form::text('u2m1s1q12', null, ['class' => 'bladeInputWidth100'])  }}</div>



								<div class="panel-body">
								4. an uncountable or mass noun</div>
								<div class="panel-body">{{ Form::text('u2m1s1q13', null, ['class' => 'bladeInputWidth100'])  }}</div>


								<div class="panel-body">
								5. a conjunction</div>
								<div class="panel-body">{{ Form::text('u2m1s1q14', null, ['class' => 'bladeInputWidth100'])  }}</div>



								<div class="panel-body">
								6. a preposition of time</div>
								<div class="panel-body">{{ Form::text('u2m1s1q15', null, ['class' => 'bladeInputWidth100'])  }}</div>

								<div class="panel-body">
								7. a preposition of movement</div>
								<div class="panel-body">{{ Form::text('u2m1s1q16', null, ['class' => 'bladeInputWidth100'])  }}</div>


								<div class="panel-body">
								8. a question tag</div>
								<div class="panel-body">{{ Form::text('u2m1s1q17', null, ['class' => 'bladeInputWidth100'])  }}</div>

								<div class="panel-body">
								9. an auxiliary verb</div>
								<div class="panel-body">{{ Form::text('u2m1s1q18', null, ['class' => 'bladeInputWidth100'])  }}</div>

								<div class="panel-body">
								10. a gerund (as subject of a sentence)</div>
								<div class="panel-body">{{ Form::text('u2m1s1q19', null, ['class' => 'bladeInputWidth100'])  }}</div>
								<br>
								{!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}

				           		 {!! Form::close() !!} 

        				</div>
        			</div>
        		</div>
        	</div>
        </div>


        <div class="tab-pane" id="4a">
        	<div class="#">
        		<div class="row">
        			<div class="col-md-12">
        				<div class="panel panel-default">

			{{-- place content here --}}

			<div class="panel-body">
			Write an example for each of the following :-
			<br><br>

			{!! Form::open(['action' => 'Admin\Int120u2m1s1t3Controller@store', 'method' => 'POST']) !!}
        
            <input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">

            



			<div class="panel-body">

			1. a positive statement</div>
			<div class="panel-body">{{ Form::text('u2m1s1q20', null, ['class' => 'bladeInputWidth100'])  }}</div>


			<div class="panel-body">
			2. a negative statement</div>
			<div class="panel-body">{{ Form::text('u2m1s1q21', null, ['class' => 'bladeInputWidth100'])  }}</div>

			<div class="panel-body">
			3. an interrogative</div>
			<div class="panel-body">{{ Form::text('u2m1s1q22', null, ['class' => 'bladeInputWidth100'])  }}</div>


			<div class="panel-body">
			3. an imperative</div>
			<div class="panel-body">{{ Form::text('u2m1s1q23', null, ['class' => 'bladeInputWidth100'])  }}</div>
			{!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}

            {!! Form::close() !!} 
			</div>
		</div>
		</div>
		</div>
		</div>

        </div>

        <div class="tab-pane" id="5a">
        	<div class="#">
        		<div class="row">
        			<div class="col-md-12">
        				<div class="panel panel-default">
        					{!! Form::open(['action' => 'Admin\Int120u2m1s1t4Controller@store', 'method' => 'POST']) !!}

        					<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">



        					{{-- place content here --}}

        					<div class="panel-heading">
        						Write 2 sentences in each of the following tenses and where appropriate use complex sentences in order to demonstrate the tense in context  eg ‘When I was......., I .........’



        					<div class="panel-body">1. present simple</div>
        					<div class="panel-body"><h5>a</h5>{{ Form::text('u2m1s1q24', null, ['class' => 'form-control']) }}
        					</div>
        					<div class="panel-body"><h5>b</h5>{{ Form::text('u2m1s1q25', null, ['class' => 'form-control']) }}
        					</div>

        					<div class="panel-body">2. present continuous or present progressive</div>
        					<div class="panel-body"><h5>a</h5>{{ Form::text('u2m1s1q26', null, ['class' => 'form-control']) }}
        					</div>
        					<div class="panel-body"><h5>b</h5>{{ Form::text('u2m1s1q27', null, ['class' => 'form-control']) }}
        					</div>


        					<div class="panel-body">3. past simple</div>
        					<div class="panel-body"><h5>a</h5>{{ Form::text('u2m1s1q28', null, ['class' => 'form-control']) }}
        					</div>
        					<div class="panel-body"><h5>b</h5>{{ Form::text('u2m1s1q29', null, ['class' => 'form-control']) }}
        					</div>


        					<div class="panel-body">4. past continuous (progressive)</div>
        					<div class="panel-body"><h5>a</h5>{{ Form::text('u2m1s1q30', null, ['class' => 'form-control']) }}
        					</div>
        					<div class="panel-body"><h5>b</h5>{{ Form::text('u2m1s1q31', null, ['class' => 'form-control']) }}
        					</div>


        					<div class="panel-body">5. present perfect</div>
        					<div class="panel-body"><h5>a</h5>{{ Form::text('u2m1s1q32', null, ['class' => 'form-control']) }}
        					</div>
        					<div class="panel-body"><h5>b</h5>{{ Form::text('u2m1s1q33', null, ['class' => 'form-control']) }}
        					</div>


        					<div class="panel-body">6. present perfect continuous (progressive)</div>
        					<div class="panel-body"><h5>a</h5>{{ Form::text('u2m1s1q34', null, ['class' => 'form-control']) }}
        					</div>
        					<div class="panel-body"><h5>b</h5>{{ Form::text('u2m1s1q35', null, ['class' => 'form-control']) }}
        					</div>


        					<div class="panel-body">7. past perfect</div>
        					<div class="panel-body"><h5>a</h5>{{ Form::text('u2m1s1q36', null, ['class' => 'form-control']) }}
        					</div>
        					<div class="panel-body"><h5>b</h5>{{ Form::text('u2m1s1q37', null, ['class' => 'form-control']) }}
        					</div>


        					<div class="panel-body">8. past perfect continuous (progressive)</div>
        					<div class="panel-body"><h5>a</h5>{{ Form::text('u2m1s1q38', null, ['class' => 'form-control']) }}
        					</div>
        					<div class="panel-body"><h5>b</h5>{{ Form::text('u2m1s1q39', null, ['class' => 'form-control']) }}
        					</div>


        					<div class="panel-body">9. future simple with ‘will’</div>
        					<div class="panel-body"><h5>a</h5>{{ Form::text('u2m1s1q40', null, ['class' => 'form-control']) }}
        					</div>
        					<div class="panel-body"><h5>b</h5>{{ Form::text('u2m1s1q41', null, ['class' => 'form-control']) }}
        					</div>


        					<div class="panel-body">10. 'going to' form (as a future)</div>
        					<div class="panel-body"><h5>a</h5>{{ Form::text('u2m1s1q42', null, ['class' => 'form-control']) }}
        					</div>
        					<div class="panel-body"><h5>b</h5>{{ Form::text('u2m1s1q43', null, ['class' => 'form-control']) }}
        					</div>


        					<div class="panel-body">11. present simple used as a future form</div>
        					<div class="panel-body"><h5>a</h5>{{ Form::text('u2m1s1q44', null, ['class' => 'form-control']) }}
        					</div>
        					<div class="panel-body"><h5>b</h5>{{ Form::text('u2m1s1q45', null, ['class' => 'form-control']) }}
        					</div>
        					<div class="panel-body">12. present continuous/progressive used as a future form</div>
        					<div class="panel-body"><h5>a</h5>{{ Form::text('u2m1s1q46', null, ['class' => 'form-control']) }}
        					</div>
        					<div class="panel-body"><h5>b</h5>{{ Form::text('u2m1s1q47', null, ['class' => 'form-control']) }}
        					</div>


        					<div class="panel-body">13. future perfect</div>
        					<div class="panel-body"><h5>a</h5>{{ Form::text('u2m1s1q48', null, ['class' => 'form-control']) }}
        					</div>
        					<div class="panel-body"><h5>b</h5>{{ Form::text('u2m1s1q49', null, ['class' => 'form-control']) }}
        					</div>


        					<div class="panel-body">14. first conditional (probable condition)</div>
        					<div class="panel-body"><h5>a</h5>{{ Form::text('u2m1s1q50', null, ['class' => 'form-control']) }}
        					</div>
        					<div class="panel-body"><h5>b</h5>{{ Form::text('u2m1s1q51', null, ['class' => 'form-control']) }}
        					</div>


        					<div class="panel-body">15. second conditional (improbable condition)</div>
        					<div class="panel-body"><h5>a</h5>{{ Form::text('u2m1s1q52', null, ['class' => 'form-control']) }}
        					</div>
        					<div class="panel-body"><h5>b</h5>{{ Form::text('u2m1s1q53', null, ['class' => 'form-control']) }}
        					</div>


        					<div class="panel-body">16. third conditional (impossible condition)</div>
        					<div class="panel-body"><h5>a</h5>{{ Form::text('u2m1s1q54', null, ['class' => 'form-control']) }}
        					</div>
        					<div class="panel-body"><h5>b</h5>{{ Form::text('u2m1s1q55', null, ['class' => 'form-control']) }}
        					</div>
        					<br>
        					{!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}

        					{!! Form::close() !!} 
        				</div>
        				</div>
        				
        			</div>
        		</div>
        	</div>
        </div>

        <div class="tab-pane" id="6a">
        	<div class="#">
        		<div class="row">
        			<div class="col-md-12">
        				<div class="panel panel-default">
        					 {!! Form::open(['action' => 'Admin\Int120u2m1s1t5Controller@store', 'method' => 'POST']) !!}
        
            					<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">

						{{-- place content here --}}
						<div class="panel-heading">
						Change the following from active to passive voice:-
						<br><br>
						Please note it is not always necessary to include who carried out the action.</div>

						<div class="panel-body">
						1.We keep the milk in the fridge.</div>
						<div class="panel-body">{{ Form::text('u2m1s1q56', null, ['class' => 'form-control'])  }} </div>

						<div class="panel-body">
						2. The boys kicked the ball through the window.</div>
						<div class="panel-body">{{ Form::text('u2m1s1q57', null, ['class' => 'form-control'])  }}</div>

						<div class="panel-body">
						3. The workmen are repairing the road.</div>
						<div class="panel-body">{{ Form::text('u2m1s1q58', null, ['class' => 'form-control'])  }}</div>

			           </div>
			           {!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}

			            {!! Form::close() !!} 
			       </div>
			   </div>
			</div>
		</div>

		<div class="tab-pane" id="7a">
			<div class="#">
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							{!! Form::open(['action' => 'Admin\Int120u2m1s1t6Controller@store', 'method' => 'POST']) !!}
							
							<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">

							{{-- place content here --}}

							<div class="panel-body">
								Change the following from indirect to direct speech, including the reporting verb and appropriate punctuation.
							</div>


							<div class="panel-heading">
							1. Jean said that she had lost her shoes.</div>
							<div class="panel-body">{{ Form::text('u2m1s1q59', null, ['class' => 'bladeInputWidth100'])  }}</div>

							<div class="panel-heading">
								2. Fred insisted that he would be driving his own car in the rally.
							</div>
							<div class="panel-body">{{ Form::text('u2m1s1q60', null, ['class' => 'bladeInputWidth100'])  }}</div>


							<div class="panel-heading">
								3. Sally shouted that she would be on the train.
							</div>
							<div class="panel-body">{{ Form::text('u2m1s1q61', null, ['class' => 'bladeInputWidth100'])  }}</div>


							<div class="panel-heading">
							Change the following from direct to indirect speech</div>

							<div class="panel-heading">
								1. She said, "I'm cold."
							</div>
							<div class="panel-body">{{ Form::text('u2m1s1q62', null, ['class' => 'bladeInputWidth100'])  }}</div>

							<div class="panel-heading">
								2. The officer said, " They look like fugitives from Alcatraz."
							</div>
							<div class="panel-body">{{ Form::text('u2m1s1q63', null, ['class' => 'bladeInputWidth100'])  }}</div>


							<div class="panel-heading">
								3. "What are you doing?" asked Sally.
							</div>
							<div class="panel-body">{{ Form::text('u2m1s1q64', null, ['class' => 'bladeInputWidth100'])  }}</div>
							{!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}

							{!! Form::close() !!} 

						</div>
					</div>
				</div>
			</div>
		</div>

        </div>
    </div>
</div>
</div>


@endsection