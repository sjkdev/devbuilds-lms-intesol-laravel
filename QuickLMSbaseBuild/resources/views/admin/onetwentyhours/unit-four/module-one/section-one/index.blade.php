@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('content')

<div class="panel panel-default">
	<div class="panel-body">Unit Four Module One</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading"><h3>Unit 4 | Lesson Planning</h3></div>
	<div class="panel-body"><h4>Module 1 | Classroom Management</h4></div>


	<div class="panel-body" id="exTab1">
		<ul  class="nav nav-pills">

			<li><a  href="#1a" data-toggle="tab">Info</a></li>

			<li><a href="#2a" data-toggle="tab">Aims</a></li>
			<li><a href="#3a" data-toggle="tab">Techniques</a></li>
			<li><a href="#4a" data-toggle="tab" class="blueText">Task</a></li>	
		</ul>

	</div>
</div>

<div class="tab-content clearfix">

	<div class="tab-pane" id="1a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">Info</div>
						<div class="panel-body">
							<h4>At the end of this unit you will:-</h4>
							<ul>
								<li>a) understand the overall aim of TEFL</li>
								<li>b) know how to facilitate communication</li>
								<li>c) know techniques which encourage students to listen and learn</li>
								<li>d) know techniques which encourage students to speak</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="2a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Aims</h4></div>
						<div class="panel-body">
							<p>The overall aim of teaching English as a Foreign Language is to involve all the students all the time.
							</p>
							<p>
							Remember that we are teaching communication, this means we want our students to talk (in English of course). It can be difficult for teachers of other disciplines to switch from preventing students from talking!</p>
							<p>
							You have very little teaching time and the students need to learn a great deal if they are to achieve any degree of fluency or if their goal is to pass an exam.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane" id="3a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
					<div class="panel-heading">Techniques</div>
						
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>CLASSROOM MANAGEMENT</h4></div>
						<div class="panel-body">
							<p>In order to achieve your aim you must be able to 'manage' your class. There are techniques which good EFL teachers follow making the management of their classes more professional, skilful and above all which facilitate the achievement of the overall aim of involving all the students all the time.</p>
			
							<p>It is not expected that teachers will religiously stick to all the following techniques, every teacher develops his/her own style of management, but incorporating as many as possible into every lesson is essential in order to achieve your aim.
							</p>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>LOOK AT ALL THE STUDENTS IN THE CLASS</h4></div>
						<div class="panel-body"><p>
							Switch your gaze from side to side and back to front of your class. It is so easy to stick with an area of friendly faces, but this is unfair on the others who will then not be so involved in the lesson.
						</p></div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>VARY YOUR QUESTION TECHNIQUE</h4></div>	
						<div class="panel-body">
							<p>
								Questions are a way of compelling attention. If someone is yawning at the back of the class it is tempting to wake them up with a direct question, but there is a more useful method which compels the attention of everyone.</p>

								<p>Don't start with the name of the student you are addressing eg "Halil, what was the name of the driver?" Immediately you say "Halil", the rest of the class switch off. Instead say, "What was the name of the driver?" Pause, look around. Everyone will work out the answer, not knowing who you will ask. When you finally ask Halil, the other students will listen to see if his answer corresponds with theirs. BUT remember not to humiliate students who are regularly in this position!

							</p>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>WHO TO ASK</h4></div>
						<div class="panel-body"><p>
							Don't go round the class when asking questions. Those furthest away will relax, knowing that their turn is some time off, and those already questioned will completely switch off as they will probably not be asked again. </p>

							<p>Particularly make it a rule that you don't ask comprehension type questions in order so that e.g. the fifth student along knows he will get question 5 - he will spend all his time working out the answer to question 5 and will not hear or read the text nor will he hear anyone else's question or answer.</p>

							<p>Instead, dart around with your questions and sometimes go back to keep everyone on their toes.

						</p></div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>INCLUDE EVERYONE</h4></div>
						<div class="panel-body"><p>Make sure everyone is called upon equally. This is particularly difficult when you are darting around with your questions. Students at the back and the 'wings' must not be forgotten.</p></div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>SEATING</h4></div>
						<div class="panel-body"><p>Make sure that all the students can see the visual aids. If you make your own, think of the student with poor sight!</p>

							<p>Group students rather than seat them in rows. No communication can take place when students are looking at the back of someone's head. The best arrangement is a circle - all students can be involved and they become a community. Often you will need to rearrange the furniture, do not be afraid to do this and to explain that you are doing this to aid communication. If you cannot achieve the best, a semi-circle or some sort of group arrangement will work.
							</p>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>L.T.T.T.</h4></div>
						<div class="panel-body"><p>Limit Teacher Talking Time - the teacher is the stimulator and the students should be responding.</p></div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>MAKE IT EASY FOR YOUR STUDENTS</h4></div>
						<div class="panel-body"><p>It is essential to write clearly. You may need practice. It is not as easy as it looks to write on a black/white board and keep it straight!</p>

						<p>Print wherever possible. This is especially important if any of the students you are teaching use a different script in their own language.</p>

						<p>Use an orderly, logical arrangement. It can be very confusing if you dart around the board using arrows etc.
						Don't write everything in capital letters. Only use them where they should be used in English. In some languages capital letters have a different significance/purpose.
						</p>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>ENCOURAGEMENT</h4></div>
						<div class="panel-body"><p>
						Your students need encouragement. Even if the answer you get is completely incorrect don't say 'NO!', try intonation which means 'No, but...' then allow someone you know will have the correct answer to give their version. Don't be afraid to use 'good' frequently, even if it has to be 'Good, but let's hear X's answer' (when you know that his/her answer will be better).This will encourage student Y to try again another time. 'NO!' may discourage.
						</p>
					</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>GRAMMATICAL TERMINOLOGY</h4></div>
						<div class="panel-body"><p>Use as little of this as possible, but know it yourself for use with advanced classes and language-conscious nationalities. They will ask and put you on the spot. Remember to use deductive analysis to get out of awkward situations. BUT remember you should know your stuff if you want to keep your credulity intact!</p></div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>USE OUTSIDE CLASS</h4></div>
						<div class="panel-body"><p>
							Encourage students to use English outside the class as much as possible. Graded readers are available from most publishers at most levels; newspapers are always available in English, some are written for the expatriate community and contain useful articles.</p>
							<p>Ask your students for the latest news each morning - this will get them into the habit of finding out on their way into school or listening to the BBC World Service which is especially for the overseas market.</p>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Levels</h4></div>
						<div class="panel-body"><p>

						Take account of different levels in your class. Mix the bright with the less so in pair and group work. Ask the ones who have a grasp of what you are doing FIRST, they will then be a model for the others to copy.</p>
						<p>Use students to teach - the others will sit up and pay attention to a fellow student!</p></div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>INDIVIDUAL PROBLEMS</h4></div>
						<div class="panel-body">It will be appreciated by all if these are dealt with after class, but don't forget to deal with the problem, it may be very important to that student.<p>
						</p>
					</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>LEARNING STYLES</h4></div>
						<div class="panel-body"><p>As you gain experience as a teacher of EFL you will come to realise that not all students learn in the same way. It is worth noting here that you may have many different types of learner in a class at any one time. <br>
						A learning ‘style’ is an individual predisposition to learn in a particular way. It is likely that the preferred learning style of any one learner will manifest itself in all aspects of learning, not just in relation to learning a foreign language. Though you, as their teacher for a few hours per week, do not need to study and analyse each student’s individual style, these differences may become obvious and cannot be ignored. If you are teaching on a one to one basis you will, and should, get to know and work with your student’s individual approach to learning.</p>
					</div>
					</div>
					<div class="panel panel-default">
						
						<div class="panel-body">
							<h4>Look at the following statements made by EFL students:</h4> <br>
							<ul class="cleanList">
								<li>a) I want my teacher to correct all my mistakes</li>
								<li>b) I want to be in an easier class so that I can understand  everything on the tapes</li>
								<li>c) I don’t want to study grammar in class, I can do that at home</li>
								<li>d) I like my teacher because she lets me ask questions about things I want to know</li>
								<li>e) I like working in a pair with a student who is better than me</li>
								<li>f) I had a frustrating weekend because I ran out of simplified readers</li>
								<li>g) I didn’t buy anything when I went out because I didn’t know how to ask for it</li>
								<li>h) I know the rule but I forget it when I’m speaking</li>
								<li>i) I know I make mistakes when I speak, but they understand me</li>
							</ul>
							<br>
							<p>Of course you cannot deal with every individual positive or negative approach to learning, but be aware that these, and more, exist and are possibly all in your class!
</p>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>CORRECTION</h4></div>
						<div class="panel-body"><p>Correction of written work has already been covered earlier in the course, but oral work can be more difficult to correct. <br>
						Make notes during discussion work rather than interrupting to correct individual mistakes. Afterwards you can take up individual mistakes or make a point of something that seems to be a problem for more than one student.</p>
					</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>STUDENT NAMES</h4></div>
						<div class="panel-body"><p>
							Pronounce the names of your students correctly. It is no excuse that their names are very difficult to remember let alone pronounce. </p>

							<p>Make yourself a seating plan/chart and write their names phonetically, you would not like it if you were always called the wrong name or almost the right name but not quite. You may inadvertently change the sex of the person by changing one letter in their name! Getting their names right as soon as possible shows your respect for them as people.
						</p></div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane" id="4a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">Task</div>
						<div class="panel-body"><p>
							Write a list of 5 of the most important ways to ensure that you achieve the overall aim of involving all the students all the time when you are teaching EFL. Explain each point on your list in 2-3 sentences.
						</p></div>
						<div class="panel-body">
							{!! Form::open(['action' => 'Admin\Int120u4m1s1Controller@store', 'method' => 'POST']) !!}
						
						<br>
						<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">
				

				
						<div class="form-group">
							{!! Form::label('u4m1s1q1', 'Item 1:') !!}
							{!! Form::text('u4m1s1q1', null, ['class' => 'form-control'])!!}
						
							
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u4m1s1q2', 'Item 2:') !!}
							{!! Form::text('u4m1s1q2', null, ['class' => 'form-control'])!!}
						
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u4m1s1q3', 'Item 3:') !!}
							{!! Form::text('u4m1s1q3', null, ['class' => 'form-control'])!!}
					
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u4m1s1q4', 'Item 4:') !!}
							{!! Form::text('u4m1s1q4', null, ['class' => 'form-control'])!!}
						
						</div>
						<br>
						<div class="form-group">
							{!! Form::label('u4m1s1q5', 'Item 5:') !!}
							{!! Form::text('u4m1s1q5', null, ['class' => 'form-control'])!!}
						
						</div>
						
						<br>
						
							{!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}

						{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

@endsection