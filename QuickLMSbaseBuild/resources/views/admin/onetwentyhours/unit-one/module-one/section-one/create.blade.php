@extends('layouts.app')

@section('content')
<div class="panel panel-default">
                    <div class="panel-heading">Form</div>
                    <div class="panel-body">
                        {!! Form::open(['route' => 'admin.onetwentyhours.unit-one.module-one.section-one.store']) !!}
                        <div class="form-group">
                            {!! Form::label('u1m1sm1q1', 'Item 1:') !!}
                            {!! Form::text('u1m1sm1q1', null, ['class' => 'form-control'])!!}
                            {!! Form::submit('Save', ['class' => 'btn btn-info'])!!}
                        </div>
                        <br>
                        <div class="form-group">
                            {!! Form::label('u1m1sm1q1', 'Item 1:') !!}
                            {!! Form::text('u1m1sm1q1', null, ['class' => 'form-control'])!!}
                            {!! Form::submit('Save', ['class' => 'btn btn-info '])!!}
                        </div>
                        <br>
                        <div class="form-group">
                            {!! Form::label('u1m1sm1q2', 'Item 2:') !!}
                            {!! Form::text('u1m1sm1q2', null, ['class' => 'form-control'])!!}
                            {!! Form::submit('Save', ['class' => 'btn btn-info '])!!}
                        </div>
                        <br>
                        <div class="form-group">
                            {!! Form::label('u1m1sm1q3', 'Item 3:') !!}
                            {!! Form::text('u1m1sm1q3', null, ['class' => 'form-control'])!!}
                            {!! Form::submit('Save', ['class' => 'btn btn-info '])!!}
                        </div>
                        <br>
                        <div class="form-group">
                            {!! Form::label('u1m1sm1q4', 'Item 4:') !!}
                            {!! Form::text('u1m1sm1q4', null, ['class' => 'form-control'])!!}
                            {!! Form::submit('Save', ['class' => 'btn btn-info '])!!}
                        </div>
                        <br>
                        <div class="form-group">
                            {!! Form::label('u1m1sm1q5', 'Item 5:') !!}
                            {!! Form::text('u1m1sm1q5', null, ['class' => 'form-control'])!!}
                            {!! Form::submit('Save', ['class' => 'btn btn-info '])!!}
                        </div>
                        <br>
                        <div class="form-group">
                            {!! Form::label('u1m1sm1q6', 'Item 6:') !!}
                            {!! Form::text('u1m1sm1q6', null, ['class' => 'form-control'])!!}
                            {!! Form::submit('Save', ['class' => 'btn btn-info '])!!}
                        </div>
                        <br>
                        <div class="form-group">
                            {!! Form::label('u1m1sm1q7', 'Item 7:') !!}
                            {!! Form::text('u1m1sm1q7', null, ['class' => 'form-control'])!!}
                            {!! Form::submit('Save', ['class' => 'btn btn-info'])!!}
                        </div>
                        <br>

                        {!! Form::close() !!}
                    </div>
                </div>
    
@endsection