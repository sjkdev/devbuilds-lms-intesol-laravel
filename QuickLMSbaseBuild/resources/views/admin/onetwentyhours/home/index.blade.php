@extends('layouts.app')
@section('content')

<div class="row">
	<div class="col-md-8">
	

				<ul class="list-group">
				  <li class="list-group-item active">Units and Modules</li>
				  <li class="list-group-item list-group-item-info">Unit 1 | Study Skills</li>
				  <li class="list-group-item list-group-item-warning">[L 1]<a href="{{ route('admin.onetwentyhours.unit-one.module-one.section-one.index') }}">Module 1</a> </li>
				  <li class="list-group-item list-group-item-info">Unit 2 | The Study of English</li>
				  <li class="list-group-item list-group-item-warning">[L 2]<a href="{{ route('admin.onetwentyhours.unit-two.module-one.section-one.index') }}">Module 1</a>| Grammar</li>
				  <li class="list-group-item list-group-item-warning">[L 3]<a href="{{ route('admin.onetwentyhours.unit-two.module-two.section-one.index') }}">Module 2</a> | Pronunciation, Stress and Intonation <i class="fa fa-video-camera" aria-hidden="true"></i></li>
				  <li class="list-group-item list-group-item-warning">[L 4]<a href="{{ route('admin.onetwentyhours.unit-two.module-three.section-one.index') }}">Module 3</a> Lexis <i class="fa fa-video-camera" aria-hidden="true"></i></li>

				  <li class="list-group-item list-group-item-info">Unit 3 | The Teaching and Lerning of ESOL</li>
				  <li class="list-group-item list-group-item-warning">[L 5]<a href="{{ route('admin.onetwentyhours.unit-three.module-one.section-one.index') }}">Module 1</a>| The Basic Principles of Tesol     <i class="fa fa-video-camera" aria-hidden="true"></i></li>
				  <li class="list-group-item list-group-item-warning">[L 6]<a href="{{ route('admin.onetwentyhours.unit-three.module-two.section-one.index') }}">Module 2</a> | Listening and Reading     <i class="fa fa-video-camera" aria-hidden="true"></i></li>
				  <li class="list-group-item list-group-item-warning">[L 7]<a href="{{ route('admin.onetwentyhours.unit-three.module-three.section-one.index') }}">Module 3</a> Speaking and Writing</li>
				  <li class="list-group-item list-group-item-warning">[L 8]<a href="{{ route('admin.onetwentyhours.unit-three.module-four.section-one.index') }}">Module 4</a>| Visual Aids</li>
				  <li class="list-group-item list-group-item-warning">[L 9]<a href="{{ route('admin.onetwentyhours.unit-three.module-five.section-one.index') }}">Module 5</a> | Error</li>
				  <li class="list-group-item list-group-item-info">Unit 4 | Lesson Planning</li>
				  <li class="list-group-item list-group-item-warning">[L 10]<a href="{{ route('admin.onetwentyhours.unit-four.module-one.section-one.index') }}">Module 1</a> | Classroom Managament</li>
				  <li class="list-group-item list-group-item-warning">[L 11]<a href="{{ route('admin.onetwentyhours.unit-four.module-two.section-one.index') }}">Module 2</a> | Lesson Stages and Plans</li>
				  <li class="list-group-item list-group-item-info">Unit 5 | Lesson Planning</li>
				  <li class="list-group-item list-group-item-warning">[L 12]<a href="{{ route('admin.onetwentyhours.unit-five.module-one.section-one.index') }}">Module 1</a> | TEYL</li>
				</ul>
			
		

	</div>
	<div class="col-md-4">
		
		<ul class="list-group">
			<li class="list-group-item side-list-item active">Login Details</li>
			<li class="list-group-item side-list-item">Last Login</li>
			<li class="list-group-item side-list-item">Access Level [$Trainee->student_access_level]</li>
		</ul>
		<br>
		<ul class="list-group">
			<li class="list-group-item side-list-item active">Your Notifications </li>
			<li>[$notifications->NID]</li>
	
			<li class="list-group-item side-list-item">Message</li>
			<li class="list-group-item side-list-item">Tasks</li>
		</ul>
		<br>
		<ul class="list-group">
			<li class="list-group-item side-list-item active">Your Tutor</li>
			<li class="list-group-item side-list-item">[ $Trainer->name ]</li>
		</ul>

	</div>



	</div>











@endsection