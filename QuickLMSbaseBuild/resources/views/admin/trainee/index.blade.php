@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')

@if(Auth::user()->isAdmin())
	<h3 class="page-title">Create Trainee</h3>
    
    <p>
        <a href="{{ route('admin.trainee.create') }}" class="btn btn-success">@lang('global.app_add_new')</a>
        
    </p>

    <div class="panel panel-default">
    	<div class="panel-heading">List of Trainees</div>
    	<div class="panel-body">
    		
    		<table>
    			<tr>
    				<th>Name</th>
    				<th>Course</th>
    				<th>Status</th>
    			</tr>
    			{{-- @foreach($trainees as $trainee)
    			<tr>
    				<th>{{ $trainee->full_name }}</th>
    				<th>{{ $trainee->course_id}}</th>
    				<th>Status</th>
    			</tr>
    			@endforeach --}}
    		</table>
    	</div>
    </div>


@endif
   
@endsection