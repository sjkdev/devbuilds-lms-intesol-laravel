<?php
Route::get('/', 'HomeController@index');
Route::get('course/{slug}', ['uses' => 'CoursesController@show', 'as' => 'courses.show']);
Route::post('course/payment', ['uses' => 'CoursesController@payment', 'as' => 'courses.payment']);
Route::post('course/{course_id}/rating', ['uses' => 'CoursesController@rating', 'as' => 'courses.rating']);




Route::get('lesson/{course_id}/{slug}', ['uses' => 'LessonsController@show', 'as' => 'lessons.show']);
Route::post('lesson/{slug}/test', ['uses' => 'LessonsController@test', 'as' => 'lessons.test']);

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->post('logout', 'Auth\LoginController@logout')->name('auth.logout');

// Registration Routes...
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('auth.register');
$this->post('register', 'Auth\RegisterController@register')->name('auth.register');

// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
$this->patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

Route::group(['middleware' => ['admin'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/home', 'Admin\DashboardController@index');
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::post('permissions_mass_destroy', ['uses' => 'Admin\PermissionsController@massDestroy', 'as' => 'permissions.mass_destroy']);
    Route::resource('roles', 'Admin\RolesController');
    Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
    Route::resource('users', 'Admin\UsersController');
    Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);
    Route::resource('courses', 'Admin\CoursesController');
    Route::post('courses_mass_destroy', ['uses' => 'Admin\CoursesController@massDestroy', 'as' => 'courses.mass_destroy']);
    Route::post('courses_restore/{id}', ['uses' => 'Admin\CoursesController@restore', 'as' => 'courses.restore']);
    Route::delete('courses_perma_del/{id}', ['uses' => 'Admin\CoursesController@perma_del', 'as' => 'courses.perma_del']);
    Route::resource('lessons', 'Admin\LessonsController');
    Route::post('lessons_mass_destroy', ['uses' => 'Admin\LessonsController@massDestroy', 'as' => 'lessons.mass_destroy']);
    Route::post('lessons_restore/{id}', ['uses' => 'Admin\LessonsController@restore', 'as' => 'lessons.restore']);
    Route::delete('lessons_perma_del/{id}', ['uses' => 'Admin\LessonsController@perma_del', 'as' => 'lessons.perma_del']);
    Route::resource('questions', 'Admin\QuestionsController');
    Route::post('questions_mass_destroy', ['uses' => 'Admin\QuestionsController@massDestroy', 'as' => 'questions.mass_destroy']);
    Route::post('questions_restore/{id}', ['uses' => 'Admin\QuestionsController@restore', 'as' => 'questions.restore']);
    Route::delete('questions_perma_del/{id}', ['uses' => 'Admin\QuestionsController@perma_del', 'as' => 'questions.perma_del']);
    Route::resource('questions_options', 'Admin\QuestionsOptionsController');
    Route::post('questions_options_mass_destroy', ['uses' => 'Admin\QuestionsOptionsController@massDestroy', 'as' => 'questions_options.mass_destroy']);
    Route::post('questions_options_restore/{id}', ['uses' => 'Admin\QuestionsOptionsController@restore', 'as' => 'questions_options.restore']);
    Route::delete('questions_options_perma_del/{id}', ['uses' => 'Admin\QuestionsOptionsController@perma_del', 'as' => 'questions_options.perma_del']);
    Route::resource('tests', 'Admin\TestsController');
    Route::post('tests_mass_destroy', ['uses' => 'Admin\TestsController@massDestroy', 'as' => 'tests.mass_destroy']);
    Route::post('tests_restore/{id}', ['uses' => 'Admin\TestsController@restore', 'as' => 'tests.restore']);
    Route::delete('tests_perma_del/{id}', ['uses' => 'Admin\TestsController@perma_del', 'as' => 'tests.perma_del']);
    Route::post('/spatie/media/upload', 'Admin\SpatieMediaController@create')->name('media.upload');
    Route::post('/spatie/media/remove', 'Admin\SpatieMediaController@destroy')->name('media.remove');

    Route::get('/canned', ['uses' => 'Admin\CannedController@index', 'as' => 'admin.canned.index']);
    Route::get('/classroom', ['uses' => 'Admin\ClassroomController@index', 'as' => 'admin.classroom.index']);
    Route::get('/messages', ['uses' => 'Admin\MessagingController@index', 'as' => 'admin.messages.index']);
    Route::get('/tmessages', ['uses' => 'Admin\TMessagingController@index', 'as' => 'admin.tmessages.index']);
    Route::get('/notification', ['uses' => 'Admin\NotificationsController@index', 'as' => 'admin.notification.index']);
    Route::get('/trainee', ['uses' => 'Admin\TraineeController@index', 'as' => 'admin.trainee.index']);
    Route::get('/trainer', ['uses' => 'Admin\TrainerController@index', 'as' => 'admin.trainer.index']);
    Route::get('/trainerhome', ['uses' => 'Admin\TrainerhomeController@index', 'as' => 'admin.trainerhome.index']);

    Route::resource('/canned', 'Admin\CannedController');
    Route::resource('/classroom', 'Admin\ClassroomController');
    Route::resource('/messages', 'Admin\MessagingController');
    Route::resource('/tmessages', 'Admin\TMessagingController');
    Route::resource('/notification', 'Admin\NotificationsController');
    Route::resource('/trainee', 'Admin\TraineeController');
    Route::resource('/trainer', 'Admin\TrainerController');
    Route::resource('/trainerhome', 'Admin\TrainerhomeController');

});


 Route::get('/admin/trainer_review', 'Admin\TrainerController@trainer_review')->name('admin.trainer_review.index');

    // Route::get('/admin/canned', ['uses' => 'Admin\CannedController@index', 'as' => 'admin.canned.index']);
    // Route::get('/admin/classroom', ['uses' => 'Admin\ClassroomController@index', 'as' => 'admin.classroom.index']);
    // Route::get('/admin/messages', ['uses' => 'Admin\MessagingController@index', 'as' => 'admin.messages.index']);
    // Route::get('/admin/tmessages', ['uses' => 'Admin\TMessagingController@index', 'as' => 'admin.tmessages.index']);
    // Route::get('/admin/notification', ['uses' => 'Admin\NotificationsController@index', 'as' => 'admin.notification.index']);
    // Route::get('/admin/trainee', ['uses' => 'Admin\TraineeController@index', 'as' => 'admin.trainee.index']);
    // Route::get('/admin/trainer', ['uses' => 'Admin\TrainerController@index', 'as' => 'admin.trainer.index']);
    // Route::get('/admin/trainerhome', ['uses' => 'Admin\TrainerHomeController@index', 'as' => 'admin.trainerhome.index']);


    // Route::get('/admin/canned', 'Admin\CannedController@index')->name('admin.canned.index');
    // Route::get('/admin/classroom', 'Admin\ClassroomController@index')->name('admin.classroom.index');
    // Route::get('/admin/messages', 'Admin\MessagingController@index')->name('admin.messages.index');
    // Route::get('/admin/tmessages', 'Admin\TMessagingController@index')->name('admin.tmessages.index');
    // Route::get('/admin/notification', 'Admin\NotificationsController@index')->name('admin.notification.index');
    // Route::get('/admin/trainee', 'Admin\TraineeController@index')->name('admin.trainee.index');
    // Route::get('/admin/trainer', 'Admin\TrainerController@index')->name('admin.trainer.index');
    // Route::get('/admin/trainerhome', 'Admin\TrainerHomeController@index')->name('admin.trainerhome.index');



    // Route::resource('/admin/canned', 'Admin\CannedController');
    // Route::resource('/admin/classroom', 'Admin\ClassroomController');
    // Route::resource('/admin/messages', 'Admin\MessagingController');
    // Route::resource('/admin/tmessages', 'Admin\TMessagingController');
    // Route::resource('/admin/notification', 'Admin\NotificationsController');
    // Route::resource('/admin/trainee', 'Admin\TraineeController');
    // Route::resource('/admin/trainer', 'Admin\TrainerController');


    // Route::get('/admin/trainerhome', 'Admin\TrainerHomeController');


 ////////////////////
    //
    // 120 hour courses
    // 
    ////////////////////
    
    // 120 Hour Course
// home page
Route::get('/admin/onetwentyhours', 'Admin\OnetwentyhourhomepageController@index')->name('admin.onetwentyhours.index');
Route::get('/admin/onetwentyhours/home', 'Admin\OnetwentyhourhomepageController@indexHome')->name('admin.onetwentyhours.home.index');


Route::get('/admin/onetwentyhours/home', 'Admin\OnetwentyhourhomepageController@indexHome')->name('admin.onetwentyhours.home.index');

// 120 Hour Course
// unit one module one


Route::get('/admin/onetwentyhours/unit-one/module-one/section-one', 'Admin\Int120u1m1s1Controller@index')->name('admin.onetwentyhours.unit-one.module-one.section-one.index');

Route::post('/admin/onetwentyhours/unit-one/module-one/section-one', 'Admin\Int120u1m1s1Controller@store')->name('admin.onetwentyhours.unit-one.module-one.section-one.index');

Route::post('/admin/onetwentyhours/unit-one/module-one/section-one/t1', 'Admin\Int120u1m1s1t1Controller@store');
Route::post('/admin/onetwentyhours/unit-one/module-one/section-one/t2', 'Admin\Int120u1m1s1t2Controller@store');
Route::post('/admin/onetwentyhours/unit-one/module-one/section-one/t3', 'Admin\Int120u1m1s1t3Controller@store');
Route::post('/admin/onetwentyhours/unit-one/module-one/section-one/t4', 'Admin\Int120u1m1s1t4Controller@store');



// 120 Hour Course
// unit two module one
Route::get('/admin/onetwentyhours/unit-two/module-one/section-one', 'Admin\Int120u2m1s1Controller@index')->name('admin.onetwentyhours.unit-two.module-one.section-one.index');

Route::post('/admin/onetwentyhours/unit-two/module-one/section-one', 'Admin\Int120u2m1s1Controller@store@show'); 


Route::post('/admin/onetwentyhours/unit-two/module-one/section-one/t1', 'Admin\Int120u2m1s1t1Controller@store');
Route::post('/admin/onetwentyhours/unit-two/module-one/section-one/t2', 'Admin\Int120u2m1s1t2Controller@store');
Route::post('/admin/onetwentyhours/unit-two/module-one/section-one/t3', 'Admin\Int120u2m1s1t3Controller@store');
Route::post('/admin/onetwentyhours/unit-two/module-one/section-one/t4', 'Admin\Int120u2m1s1t4Controller@store');
Route::post('/admin/onetwentyhours/unit-two/module-one/section-one/t5', 'Admin\Int120u2m1s1t5Controller@store');
Route::post('/admin/onetwentyhours/unit-two/module-one/section-one/t6', 'Admin\Int120u2m1s1t6Controller@store');







//120 unit two module section two test page
Route::get('/admin/onetwentyhours/unit-two/module-one/section-two', 'Admin\Int120u2m2s1Controller@indexTwo')->name('admin.onetwentyhours.unit-two.module-one.section-two.index');

Route::post('/admin/onetwentyhours/unit-two/module-two/section-one', 'Admin\Int120u2m2s1Controller@store'); 


// 120 Hour Course
// unit two module two
Route::get('/admin/onetwentyhours/unit-two/module-two/section-one', 'Admin\Int120u2m2s1Controller@index')->name('admin.onetwentyhours.unit-two.module-two.section-one.index');

Route::post('/admin/onetwentyhours/unit-two/module-two/section-one', 'Admin\Int120u2m2s1Controller@store@show'); 


Route::post('/admin/onetwentyhours/unit-two/module-two/section-one/t1', 'Admin\Int120u2m2s1t1Controller@store');
Route::post('/admin/onetwentyhours/unit-two/module-two/section-one/t2', 'Admin\Int120u2m2s1t2Controller@store');
Route::post('/admin/onetwentyhours/unit-two/module-two/section-one/t3', 'Admin\Int120u2m2s1t3Controller@store');
Route::post('/admin/onetwentyhours/unit-two/module-two/section-one/t4', 'Admin\Int120u2m2s1t4Controller@store');


// 120 Hour Course
// unit two module three
Route::get('/admin/onetwentyhours/unit-two/module-three/section-one', 'Admin\Int120u2m3s1Controller@index')->name('admin.onetwentyhours.unit-two.module-three.section-one.index');

Route::post('/admin/onetwentyhours/unit-two/module-three/section-one', 'Admin\Int120u2m3s1Controller@store'); 

// Route::post('/admin/onetwentyhours/unit-two/module-three/section-one/t1', 'Admin\Int120u2m3s1t1Controller@store');


// 120 Hour Course
// unit three module one
Route::get('/admin/onetwentyhours/unit-three/module-one/section-one', 'Admin\Int120u3m1s1Controller@index')->name('admin.onetwentyhours.unit-three.module-one.section-one.index');


Route::post('/admin/onetwentyhours/unit-three/module-one/section-one', 'Admin\Int120u3m1s1Controller@store'); 

Route::post('/admin/onetwentyhours/unit-three/module-one/section-one/t1', 'Admin\Int120u3m1s1t1Controller@store'); 
Route::post('/admin/onetwentyhours/unit-three/module-one/section-one/t2', 'Admin\Int120u3m1s1t2Controller@store'); 



// 120 Hour Course
// unit three module two
Route::get('/admin/onetwentyhours/unit-three/module-two/section-one', 'Admin\Int120u3m2s1Controller@index')->name('admin.onetwentyhours.unit-three.module-two.section-one.index');

Route::post('/admin/onetwentyhours/unit-three/module-two/section-one', 'Admin\Int120u3m2s1Controller@store'); 
Route::post('/admin/onetwentyhours/unit-three/module-two/section-one/t1', 'Admin\Int120u3m2s1t1Controller@store'); 
Route::post('/admin/onetwentyhours/unit-three/module-two/section-one/t2', 'Admin\Int120u3m2s1t2Controller@store'); 
Route::post('/admin/onetwentyhours/unit-three/module-two/section-one/t3', 'Admin\Int120u3m2s1t3Controller@store'); 

// 120 Hour Course
// unit three module three
Route::get('/admin/onetwentyhours/unit-three/module-three/section-one', 'Admin\Int120u3m3s1Controller@index')->name('admin.onetwentyhours.unit-three.module-three.section-one.index');

Route::post('/admin/onetwentyhours/unit-three/module-three/section-one', 'Admin\Int120u3m3s1Controller@store');
Route::post('/admin/onetwentyhours/unit-three/module-three/section-one/t1', 'Admin\Int120u3m3s1t1Controller@store'); 
Route::post('/admin/onetwentyhours/unit-three/module-three/section-one/t2', 'Admin\Int120u3m3s1t2Controller@store'); 
Route::post('/admin/onetwentyhours/unit-three/module-three/section-one/t3', 'Admin\Int120u3m3s1t3Controller@store');
Route::post('/admin/onetwentyhours/unit-three/module-three/section-one/t4', 'Admin\Int120u3m3s1t4Controller@store'); 

// 120 Hour Course
// unit three module four
Route::get('/admin/onetwentyhours/unit-three/module-four/section-one', 'Admin\Int120u3m4s1Controller@index')->name('admin.onetwentyhours.unit-three.module-four.section-one.index');

// 120 Hour Course
// unit three module five
Route::get('/admin/onetwentyhours/unit-three/module-five/section-one', 'Admin\Int120u3m5s1Controller@index')->name('admin.onetwentyhours.unit-three.module-five.section-one.index');

Route::post('/admin/onetwentyhours/unit-three/module-five/section-one', 'Admin\Int120u3m5s1Controller@store');

// 120 Hour Course
// unit four module one
Route::get('/admin/onetwentyhours/unit-four/module-one/section-one', 'Admin\Int120u4m1s1Controller@index')->name('admin.onetwentyhours.unit-four.module-one.section-one.index');

Route::post('/admin/onetwentyhours/unit-four/module-one/section-one', 'Admin\Int120u4m1s1Controller@store'); 

// 120 Hour Course
// unit four module two
Route::get('/admin/onetwentyhours/unit-four/module-two/section-one', 'Admin\Int120u4m2s1Controller@index')->name('admin.onetwentyhours.unit-four.module-two.section-one.index');
Route::post('/admin/onetwentyhours/unit-four/module-two/section-one', 'Admin\Int120u4m2s1Controller@store'); 

Route::post('/admin/onetwentyhours/unit-four/module-two/section-one/t1', 'Admin\Int120u4m2s1t1Controller@store'); 
Route::post('/admin/onetwentyhours/unit-four/module-two/section-one/t2', 'Admin\Int120u4m2s1t2Controller@store');




// 120 Hour Course
// unit five module one
Route::get('/admin/onetwentyhours/unit-five/module-one/section-one', 'Admin\Int120u5m1s1Controller@index')->name('admin.onetwentyhours.unit-five.module-one.section-one.index');

Route::post('/admin/onetwentyhours/unit-five/module-one/section-one', 'Admin\Int120u5m1s1Controller@store'); 

