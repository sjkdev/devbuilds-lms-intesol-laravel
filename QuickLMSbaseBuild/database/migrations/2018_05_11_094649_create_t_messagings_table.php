<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTMessagingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_messagings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tmid');
            $table->integer('tid');
            $table->integer('trid');
            $table->text('subject');
            $table->text('message');
            $table->date('mdate');
            $table->('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_messagings');
    }
}
