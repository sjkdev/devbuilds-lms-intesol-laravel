<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainer extends Model
{
    protected $table = 'trainers';

    protected $fillable = [
    	'id',
    	'user',
    	'trid',
    	'full_name',
    	'email',
    	'tel_no',
    	'address',
    	'd_o_b',
    	'reg_date',
    ];
}
