<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class int120u3m5s1 extends Model
{
    protected $table = 'in120u3m5s1s';
    protected $fillable = [
    	'user_id',
    	'u3m5s1q1',
    	'u3m5s1q2',
    	'u3m5s1q3',
    	'u3m5s1q4',
    	'u3m5s1q5',
    	'u3m5s1q6',
    	'u3m5s1q7',
    	'u3m5s1q8',
    	'u3m5s1q9',
    	'u3m5s1q10',
    	'u3m5s1q11',
    	'u3m5s1q12',
    	'u3m5s1q13',
    	'u3m5s1q14',
    	'u3m5s1q15',
    	'u3m5s1q16',
        'u3m5s1q17',
        'u3m5s1q18',
        'u3m5s1q19',

    ];
}
