<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class int120u4m1s1 extends Model
{
	protected $table = 'int120u4m1s1s';
    protected $fillable = [
        'user_id',
    	'u4m1s1q1',
    	'u4m1s1q2',
    	'u4m1s1q3',
    	'u4m1s1q4',
    	'u4m1s1q5', 
    ];
}
