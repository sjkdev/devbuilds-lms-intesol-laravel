<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class int120u5m1s1 extends Model
{
    protected $table = 'int120u5m1s1s';
    protected $fillable = [
        'user_id',
    	'u5m1s1q1',
    	'u5m1s1q2',
    	'u5m1s1q3',
    	'u5m1s1q4',
    	'u5m1s1q5',
    	'u5m1s1q6',
    	'u5m1s1q7',
    	'u5m1s1q8',
    	'u5m1s1q9',
    	'u5m1s1q10',
    	'u5m1s1q11',
    	'u5m1s1q12',
    	'u5m1s1q13',
    	'u5m1s1q14',
    	'u5m1s1q15',
    	'u5m1s1q16',
    ];
}
