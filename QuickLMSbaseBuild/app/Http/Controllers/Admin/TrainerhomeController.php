<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\onetwentyhourhomepage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TrainerhomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $trainees= \App\User::whereHas('role', function ($q) { $q->where('role_id', 4); } )->get();
        return view ('admin.trainerhome.index', compact('trainerhome', 'trainees'));
    }

   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\onetwentyhourhomepage  $onetwentyhourhomepage
     * @return \Illuminate\Http\Response
     */
    public function show(onetwentyhourhomepage $onetwentyhourhomepage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\onetwentyhourhomepage  $onetwentyhourhomepage
     * @return \Illuminate\Http\Response
     */
    public function edit(onetwentyhourhomepage $onetwentyhourhomepage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\onetwentyhourhomepage  $onetwentyhourhomepage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, onetwentyhourhomepage $onetwentyhourhomepage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\onetwentyhourhomepage  $onetwentyhourhomepage
     * @return \Illuminate\Http\Response
     */
    public function destroy(onetwentyhourhomepage $onetwentyhourhomepage)
    {
        //
    }
}
