<?php

namespace App\Http\Controllers\Admin;

use App\TMessaging;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TMessagingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.tmessages.index', compact('tmessages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tmessages.index', compact('tmessages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->except(['_token']);
 
        DB::table('messagings')->insert($data);

        return redirect()->route('admin.messages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TMessaging  $tMessaging
     * @return \Illuminate\Http\Response
     */
    public function show(TMessaging $tMessaging)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TMessaging  $tMessaging
     * @return \Illuminate\Http\Response
     */
    public function edit(TMessaging $tMessaging)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TMessaging  $tMessaging
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TMessaging $tMessaging)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TMessaging  $tMessaging
     * @return \Illuminate\Http\Response
     */
    public function destroy(TMessaging $tMessaging)
    {
        //
    }
}
