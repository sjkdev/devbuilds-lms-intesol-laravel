<?php

namespace App\Http\Controllers\Admin;

use App\int120u3m4s1;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Int120u3m4s1Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view ('admin.onetwentyhours.unit-three.module-four.section-one.index', compact('U3M4S1'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\int120u3m4s1  $int120u3m4s1
     * @return \Illuminate\Http\Response
     */
    public function show(int120u3m4s1 $int120u3m4s1)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\int120u3m4s1  $int120u3m4s1
     * @return \Illuminate\Http\Response
     */
    public function edit(int120u3m4s1 $int120u3m4s1)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\int120u3m4s1  $int120u3m4s1
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int120u3m4s1 $int120u3m4s1)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\int120u3m4s1  $int120u3m4s1
     * @return \Illuminate\Http\Response
     */
    public function destroy(int120u3m4s1 $int120u3m4s1)
    {
        //
    }
}
