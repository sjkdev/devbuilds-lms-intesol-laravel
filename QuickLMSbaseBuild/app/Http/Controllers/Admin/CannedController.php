<?php

namespace App\Http\Controllers\admin;

use App\Canned;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CannedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.canned.index', compact('canned'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.canned.create', compact('canned'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->except(['_token']);
 
        DB::table('canneds')->insert($data);

        return redirect()->route('admin.canned.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Canned  $canned
     * @return \Illuminate\Http\Response
     */
    public function show(Canned $canned)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Canned  $canned
     * @return \Illuminate\Http\Response
     */
    public function edit(Canned $canned)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Canned  $canned
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Canned $canned)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Canned  $canned
     * @return \Illuminate\Http\Response
     */
    public function destroy(Canned $canned)
    {
        //
    }
}
