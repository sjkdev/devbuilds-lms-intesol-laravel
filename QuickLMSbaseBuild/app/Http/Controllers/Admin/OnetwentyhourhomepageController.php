<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\onetwentyhourhomepage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OnetwentyhourhomepageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        return view ('admin.onetwentyhours.index', compact('onetwentyhours'));
    }

    public function indexHome()
    {

        $notifications = DB::table('notifications')->get();
        return view ('admin.onetwentyhours.home.index', compact('onetwentyhourshome'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\onetwentyhourhomepage  $onetwentyhourhomepage
     * @return \Illuminate\Http\Response
     */
    public function show(onetwentyhourhomepage $onetwentyhourhomepage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\onetwentyhourhomepage  $onetwentyhourhomepage
     * @return \Illuminate\Http\Response
     */
    public function edit(onetwentyhourhomepage $onetwentyhourhomepage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\onetwentyhourhomepage  $onetwentyhourhomepage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, onetwentyhourhomepage $onetwentyhourhomepage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\onetwentyhourhomepage  $onetwentyhourhomepage
     * @return \Illuminate\Http\Response
     */
    public function destroy(onetwentyhourhomepage $onetwentyhourhomepage)
    {
        //
    }
}
