<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Login extends Model
{
    protected $table = 'logins';

    protected $fillable = [
    	'id',
    	'lid',
    	'trid',
    	'user',
    	'pwd',
    	'last_login',
    	'level',
    	'status',
    ];
}
