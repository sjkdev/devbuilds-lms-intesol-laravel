<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainee extends Model
{
    protected $table = 'trainees';

    protected $fillable = [
    	'id',
    	'tid',
    	'full_name',
    	'email',
    	'tel_no',
    	'address',
    	'd_o_b',
    	'reg_date',
    ];

    public function role()
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }
    public function isTrainee()
    {
        return $this->role()->where('role_id', 3)->first();
    }
    public function isStudent()
    {
        return $this->role()->where('role_id', 4)->first();
    }
}
