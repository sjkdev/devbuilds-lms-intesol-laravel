<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Canned extends Model
{
    protected $table = 'canneds';

    protected $fillable = [
    	'id',
    	'cid',
    	'tid',
    	'response',
    ];
}
