<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messaging extends Model
{
    protected $table = 'messagings';

    protected $fillable = [
     	'id',
     	'mid',
     	'tid',
     	'trid',
     	'subject',
     	'mesaage',
     	'mdate',
     	'status',
     ];
}
