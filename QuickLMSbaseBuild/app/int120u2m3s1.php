<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class int120u2m3s1 extends Model
{
    protected $table = 'int120u2m3s1s';
    protected $fillable = [
        'user_id',
    	'u2m3s1q1', 
    	'u2m3s1q2', 
    	'u2m3s1q3', 
    	'u2m3s1q4',
    	'u2m3s1q5',
    	'u2m3s1q6',
    	'u2m3s1q7'
    	'u2m3s1q8',
    	'u2m3s1q9',
    	'u2m3s1q10',
    	'u2m3s1q11',
    	'u2m3s1q12',
    	'u2m3s1q13',
    	'u2m3s1q14',
    	'u2m3s1q15',
    	'u2m3s1q16',
    	'u2m3s1q17',
    	'u2m3s1q18',
    	'u2m3s1q19',
    	'u2m3s1q20',
    	'u2m3s1q21',
    	'u2m3s1q22',
    	'u2m3s1q23',
        'u2m3s1q24',
        'u2m3s1q25',
        'u2m3s1q26',
        'u2m3s1q27',
        'u2m3s1q28',
        'u2m3s1q29',
        'u2m3s1q30',
        'u2m3s1q31',
        'u2m3s1q32',
        'u2m3s1q33',
        'u2m3s1q34',
        'u2m3s1q35',
        'u2m3s1q36',
        'u2m3s1q37',
    ];
}
