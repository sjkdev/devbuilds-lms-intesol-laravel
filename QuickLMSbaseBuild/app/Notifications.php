<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    protected $table = 'notifications';

    protected $fillable = [
    	'id',
    	'NID',
    	'SID', // course_student id
    	'TRID', // course_user id
    	'TASK',
    	'NSTATUS',
    	'NDATE',
    ];
}
