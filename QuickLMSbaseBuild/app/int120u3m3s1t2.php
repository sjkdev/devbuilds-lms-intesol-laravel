<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class int120u3m3s1t2 extends Model
{
	protected $table = 'in120u3m3s1t2s';
    protected $fillable = [
    	'user_id',
    	'u3m3s1q1',
    	'u3m3s1q2',
    	'u3m3s1q3',
    	'u3m3s1q4',
    	'u3m3s1q5',
    	'u3m3s1q6',
    	'u3m3s1q7',
    	'u3m3s1q8',
    	'u3m3s1q9',
    	'u3m3s1q10',
    	'u3m3s1q11',
    	'u3m3s1q12',
    	'u3m3s1q13',
    	'u3m3s1q14',
    	'u3m3s1q15',
    	'u3m3s1q16',
        'u3m3s1q17',
        'u3m3s1q18',
        'u3m3s1q19',

    ];
}
