<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notification';

    protected $fillable = [
    	'id',
    	'NID',
    	'SID', // course_student id
    	'TRID', // course_user id
    	'TASK',
    	'NSTATUS',
    	'NDATE',
    ];
}
