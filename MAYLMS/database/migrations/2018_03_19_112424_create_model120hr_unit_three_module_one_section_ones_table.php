<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModel120hrUnitThreeModuleOneSectionOnesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('model120hr_unit_three_module_one_section_ones', function (Blueprint $table) {
            $table->increments('id');
            // $table->integer('topic_id')->unsigned()->nullable();
            // $table->foreign('topic_id', 'fk_256_topic_topic_id_question')->references('id')->on('topics');
            $table->text('question_text')->nullable();
            $table->text('u3m1q1')->nullable();
            $table->text('u3m1q1')->nullable();
            $table->text('u3m1q1')->nullable();
            $table->text('u3m1q1')->nullable();
            // $table->text('code_snippet')->nullable();
            // $table->text('answer_explanation')->nullable();
            // $table->string('more_info_link')->nullable();
            
            $table->timestamps();
            $table->softDeletes();
            $table->index(['deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('model120hr_unit_three_module_one_section_ones');
    }
}
