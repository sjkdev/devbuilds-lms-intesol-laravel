<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create120u1m1s1sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('120u1m1s1s', function (Blueprint $table) {
            $table->increments('id');
            $table->text('u1m1s1q1');
            $table->text('u1m1s1q2');
            $table->text('u1m1s1q3');
            $table->text('u1m1s1q4');
            $table->text('u1m1s1q5');
            $table->text('u1m1s1q6');
            $table->text('u1m1s1q7');
            $table->text('u1m1s1q8');
            $table->text('u1m1s1q9');
            $table->text('u1m1s1q10');
            $table->text('u1m1s1q11');
            $table->text('u1m1s1q12');
            $table->text('u1m1s1q13');
            $table->text('u1m1s1q14');
            $table->text('u1m1s1q15');
            $table->text('u1m1s1q16');
            $table->text('u1m1s1q17');
            $table->text('u1m1s1q18');
            $table->text('u1m1s1q19');
            $table->text('u1m1s1q20');
            $table->text('u1m1s1q21');
            $table->text('u1m1s1q22');
            $table->text('u1m1s1q23');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('120u1m1s1s');
    }
}
