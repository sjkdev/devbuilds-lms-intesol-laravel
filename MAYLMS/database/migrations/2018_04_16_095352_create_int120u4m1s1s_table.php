<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInt120u4m1s1sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('int120u4m1s1s', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unique()->nullable();
            $table->text('u4m1s1q1');
            $table->text('u4m1s1q2');
            $table->text('u4m1s1q3');
            $table->text('u4m1s1q4');
            $table->text('u4m1s1q5');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('int120u4m1s1s');
    }
}
