<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuestionIdsToInt120u1m1s1s extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('int120u1m1s1s', function(Blueprint $table){
            $table->text('u1m1s1q8');
            $table->text('u1m1s1q9');
            $table->text('u1m1s1q10');
            $table->text('u1m1s1q11');
            $table->text('u1m1s1q12');
            $table->text('u1m1s1q13');
            $table->text('u1m1s1q14');
            $table->text('u1m1s1q15');
            $table->text('u1m1s1q16');
            $table->text('u1m1s1q17');
            $table->text('u1m1s1q18');
            $table->text('u1m1s1q19');
            $table->text('u1m1s1q20');
            $table->text('u1m1s1q21');
            $table->text('u1m1s1q22');
            $table->text('u1m1s1q23');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('int120u1m1s1s', function(Blueprint $table){
            $table->text('u1m1s1q8');
            $table->text('u1m1s1q9');
            $table->text('u1m1s1q10');
            $table->text('u1m1s1q11');
            $table->text('u1m1s1q12');
            $table->text('u1m1s1q13');
            $table->text('u1m1s1q14');
            $table->text('u1m1s1q15');
            $table->text('u1m1s1q16');
            $table->text('u1m1s1q17');
            $table->text('u1m1s1q18');
            $table->text('u1m1s1q19');
            $table->text('u1m1s1q20');
            $table->text('u1m1s1q21');
            $table->text('u1m1s1q22');
            $table->text('u1m1s1q23');
        });
    }
}


            