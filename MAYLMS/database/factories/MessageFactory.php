<?php

$factory->define(App\Message::class, function (Faker\Generator $faker) {
    return [
        "mid" => $faker->randomNumber(2),
        "sid" => $faker->randomNumber(2),
        "trid" => $faker->randomNumber(2),
        "subject" => $faker->name,
        "message" => $faker->name,
        "mdate" => $faker->date("Y-m-d", $max = 'now'),
        "status" => $faker->name,
    ];
});
