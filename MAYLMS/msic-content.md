




SELF-CHECK 2

Which of the above activities would you use at
 
                       a) lower levels?
                       b) advanced levels?











The use of dialogue in the EFL classroom

Dialogues offer an excellent opportunity to show language in action, in natural, meaningful contexts. They also offer an excellent opportunity to focus on stress and intonation. When using dialogues in the classroom, ensure that you use the Look up & Speak technique, ie students read a line silently, then look at their partners and say the line from memory. This encourages more natural dialogues.

Dialogue practice focusing on stress, rhythm and intonation can be followed by students using the basic format of the dialogue, but altering some of the details. This could then lead on to students being asked to produce similar dialogues without referring to their texts. This also corresponds to the transition from controlled to freer practice (production)

The use of discussion in the EFL classroom

Discussions are a effective and natural way to practise talking freely in English. When you introduce discussions find topics that will appeal to your students. Be clear on the aims of the discussion and the specific skills you wish to focus on. Always introduce the specific language of discussion you wish to develop (eg agreeing, disagreeing, conceding a point, keeping a discussion going by seeking others’ opinions and giving minimal encouragers, expressing disbelief, asking for clarification etc) as the target language prior to the discussion, and elicit or teach important vocabulary.

Topic is seen by most teachers as the central focus of a classroom discussion. However, another very important point to consider is not only what to talk about, but why we need to talk about it. Of course a discussion which has no aim but to discuss the topic may succeed, but often, the discussion gradually subsides until you hear the familiar cry of ‘I can’t think of anything to say.’

What the students who say this often mean is that they have no reason to say anything. This kind of discussion only imitates real conversation for it lacks the purpose of genuine discourse. In short, students need a reason to speak more than they need something to speak about. When a group is given a task to perform through verbal interaction, all speech becomes purposeful, and therefore more interesting. Language also involves thought; and a task involving talking must also involve thinking out.

The use of role-play in the EFL classroom

It is not enough to provide students with the opportunity to speak in English. As teachers, we must encourage students to speak in a number of different situations and help them to develop their confidence in using the language flexibly. One of the most difficult and frustrating things about learning a language is making the transition from the classroom to the real world. Role-play is one of the ways of easing students into the real world by helping us to recreate and enact a wide variety of real life situations in the classroom.
The effective use of role-play adds variety to the kinds of activities we can use in the classroom; it encourages thinking, creativity and spontaneity; it allows students to practise new structures in a relatively safe environment, and it introduces a fun element into the classroom which is conducive to real learning.

Good preparation is essential to success. Effective role-plays shouldn’t be scripted out in detail. They should give a basic description of the scenario and characters while leaving room for creativity. Role-play cards are an effective tool.

eg

STUDENT A                       STUDENT B

Hotel guest booking a room              Hotel receptionist

Elements:                           Elements:
   Book in                             Welcome the guest
   You have a reservation                  Find them a room

Complications:                      Complications:
   You are on your own                     You can’t find the reservation
   You want a shower and breakfast     You only have a double room
   You have an early morning meeting       with bath available
   and mustn’t be late



Always prepare well. Ask questions and elicit ideas first, identify target 
structures and vocabulary. If there are a few students who will be acting out 
the same role, but in different groups, allow them to work together to plan 
ideas and work out key phrases (otherwise allow students time to plan 
individually). Role-plays should generally be carried out at least twice, 
allowing students to change roles. Avoid correction during role-plays; observe 
and debrief at the end. Role-play is an excellent tool but can be difficult to 
manage due to its unpredictability. It is useful therefore to consider 
beforehand the various ways that role-plays may develop so that you are 
more prepared. Encourage students to really get into role by exaggerating 
actions, gestures, tone and stress.
Teaching complete beginners

Teaching complete beginners using the target language only can be a very 
daunting prospect for less experienced teachers. Some questions are likely to 
come to mind, eg ‘Where do I start?’, ‘What can I do when they clearly don’t 
understand me?’ etc.

Here are a few points to consider:

Students obviously will not have sufficient knowledge of the language to enable them to understand anything more than basic instructions, therefore it is essential that we use all the tools at our command, eg voice and intonation, pictures, realia, diagrams, gestures and modeling, written language matched with pictures etc. Be confident. You will be surprised how quickly a group of complete beginners will be able to master some of the basics of the language eg giving basic personal details, distinguishing between different personal pronouns or expressing possession.

Introduce basic classroom language immediately and encourage students to use these. You can demonstrate the meaning of these through the use of modelling, facial expression and pictures. Students will quickly learn the meaning of simple commands / instructions if you reinforce what you say with the use of gestures. For example: cupping your hand to your ear when you say ‘listen’, pointing from your eye to an object when you say ‘look’, putting your palms together then opening your hands when you say ‘open your books’, etc.

Remember that repetition and practice are essential at this stage to enable real language acquisition.

Careful preparation is essential and it is important to consider potential problems and plan how to deal with these if the need arises.

Remember the importance of praise and encouragement; at this level these are even more important than usual, otherwise students will quickly become demotivated.

Be creative and energetic in the classroom.

Encourage self-correction, a valuable skill which will continue to aid students as they progress to higher levels.

Include lots of pairwork and groupwork to provide students with ample opportunity to use the language.
Don’t be afraid to use games in the classroom with adult learners. You may worry that students will find these silly and be reluctant to participate. However, if you are confident in your approach and students can see the communicative value of the games, you shouldn’t experience any problems.

Remember to grade the language that you use to the students’ level.


It is important to introduce the following basic phrases immediately: ‘Sorry / pardon!’, ‘Can you repeat that please?’, ‘Can you speak more slowly please?’, ‘I don’t understand’, ‘I don’t know’ etc.

To model the meaning of ‘sorry / pardon’ you can ask a student their name, then after their reply cup your hand to your ear, shake your head and say ‘pardon?’

To demonstrate the meaning of ‘can you repeat that please’ you could use a simple hand gesture, rolling your hand in a forwards motion. If students do not understand that, you can give an example.

To demonstrate the meaning of ‘can you speak more slowly please’ you can simply say the phrase slowly yourself.

To demonstrate the meaning of ‘I don’t understand’ you can point to your head, shake your head and use a palms-up gesture, and a confused facial expression.

To demonstrate the meaning of ‘I don’t know’ you can point to a picture of a person in the students’ coursebook and ask ‘What is his / her name?’ and reply ‘I don’t know’ while pointing to your head.

These are all basic phrases which are essential in the classroom as they encourage students to use the target language right from the start rather than always reverting to their mother tongue. You can write these phrases on the board and point to them during the lesson to help students to use them. Another way to encourage students to learn and use these phrases is to alternate asking different students basic questions in a very quiet voice or at a very fast pace; you can also ask a few advanced questions that they will not be able to understand, to encourage them to say that they don’t understand. Time spent on these exercises is always time well spent and successfully encourages students to use these basic communication strategies.



Phonology

To be able to speak English well enough for successful communication to occur, our learners need to hear sounds, words and connected speech before they can reproduce them. When given a model and a reason to communicate orally, learners can master the complexities of our sound system, and stress and intonation patterns.

Pronunciation

Certain sounds in English are difficult for foreign learners, especially when those sounds are non-existent in the learner’s first language. It is unrealistic for many students to expect to speak English with R.P. (Received pronunciation) but when pronunciation patterns impede communicative success, remedial work is essential. The Spanish student whose aim is 'to find a yob' (j / y confusion) and the Japanese student who eats 'flied lice' (r / l confusion) obviously need help. Minimal pair exercises and the phonemic script can be helpful here, as can exercises in consonant clusters (str/ cl/) etc.

Stress/Rhythm

Word stress needs to be practised, with rules being given as well as exceptions to the rules (eg photograph, photography). Incorrect syllable stress can confuse the listener, and when coupled with sound confusion, can give completely incorrect information. A student who is asked what deserts he can name and answers 'strawberry ice-cream' clearly got the stress wrong, and this stress error impedes communication.

Intonation

Intonation and sentence stress need to be taught together. Just as the wrong stress alters the meaning of a sentence, the wrong intonation pattern can convey the wrong mood or attitude.

Accuracy versus fluency

The question of whether students should be encouraged to speak freely and fluently, often making grammatical errors which are not immediately dealt with, or whether we should encourage accuracy, is one which continues to evade a solution.  It really depends on what the aim of your oral lesson is, and therefore the emphasis in different lessons or at different stages of a lesson can be placed either on accuracy or on fluency.

If the aim is to build up students' confidence speaking English, encourage them to deal with new communicative situations or to practise informal exchange of information or opinions, then fluency takes precedence. Errors can be monitored and addressed in a follow-up activity or lesson, however during the activity the speakers are not interrupted unless a particular error impedes communication. 
 
If the aim is to practise, for instance, ways of connecting ideas or of paraphrasing (that is to say..) or exemplifying (take, for example....'), or correct tense usage, then accuracy has to be concentrated upon, entrenched errors dealt with systematically and new errors prevented. Correction may still be delayed, however when the target language point is used incorrectly, the teacher may interrupt and correct (or better still, prompt self-correction).  Pair work is helpful as learners prefer to make mistakes with classmates as long as they feel their errors are being monitored. Making notes around the class as you listen can then lead to group / class work and guided correction by the teacher. Choral repetition backchaining and language lab work can all be useful, and practising weak forms (I want to buy a pen) and running words together can aid fluency.

In any case, it is important that whatever language point you decide to focus on for accuracy work, the reasons for choosing that point are communicative. It should not be accuracy for accuracy's sake, but students should be helped to achieve accuracy in communicating meaningful messages to other people.

Getting students to talk

Here are some possible activities which may help to get students talking. Remember, they need a reason to talk to someone, and a reason to listen.


BEGINNERS

Teacher uses flashcard of Peter on board. Elicits information from students with prompts, eg 'He's 22 years old, he comes from London'. Only use present simple of is, has, comes.  Give 8 sentences. Practise via choral repetition. Teacher then elicits question forms.  These are written on the board and practised all together (eg he has 2 sisters, he has a dog called 'Rover' and he comes to Manchester every weekend). The teacher asks individual questions and students give answers.

Pairwork practice - information gap (guided practice)

Students are given information about a famous British person. Each student has to find out details from their partner to fill in an information table. Roles can then be reversed.

Free practice

Students choose a famous person from their own country and their partners ask questions.

INTERMEDIATE  

Aim: To use persuasive language, organise information and use personal knowledge about one's homeland for free oral practice.

Students are asked what they know about Hawaii, or an exotic location elsewhere.  Categories are put on to the board, eg climate / religion / local customs / typical food / places of interest / cultural events etc. These are discussed to check understanding. Students are then told to make notes on these topics with regard to their own country, and to add any categories they feel are important. This can be done in pairs (if monolingual) or groups, or individually if multi-lingual. It's useful to explain that they are going to take on the role of travel agents trying to persuade potential tourists that their own country would be fantastic to visit. Some expressions, such as "you'll love the...", or "you'll really enjoy the...", are useful. When this has been prepared, give them one more task. Explain that tourists can offend the locals if they are not warned of certain 'do's or don'ts' in their host country, for example forming queues in Britain, removing your shoes when entering a Japanese or Korean home, and visiting Catholic churches anywhere wearing shorts.

Students are then paired with a student from another country or region, and the 'travel agent' has to answer questions and give information about his/her homeland.


ADVANCED

At this level, ellipsis is often something learners have never studied and do not use.  For example:

1.   A.  I like apples          B.  (Agreeing)  So do I

2.   A.  I don't like cheese        B.  (Agreeing)   Neither do I

3.   A.  I like milk                B.  (Disagreeing)  Do you?  I don't

4.   A.  I don't like rice          B.  (Disagreeing)  Don't you? I do

Each of these forms would be practised with several examples before moving on to the next one (describing hobbies or types of films etc). All four can then be put together in guided practice. For example, the following cue card can be given to students:

A.  I like..............                 B.  (Agree)

A.  I don't like                      B.  (Agree)

A.  Express preference       B.  (Respond)

This can become more complex

A.   Do you think you'll get married?           I hope to
I'm planning to
I'd like to
I think so
I hope so

A.   Do you think you'll fail your exams?        I hope not
                                                                    I don't think so

A.   Have you ever been to France?          No, I haven't, but I'd like to
      Have you tried Korean food?               No, but I'd love to

Students can then be asked to make a list of about ten questions, paying special attention to pronunciation, stress and rhythm both in asking questions and answering, which is then done with a partner.

This is a simple exercise to practise, yet can make a big difference to making the language learners use sound much more natural and helping them understand everyday speech.

PRACTICAL PROBLEMS 

 Many learners of English fail to pronounce the endings of words, especially past tense regular verbs, which can cause the listener some confusion. Look at these verbs in the past simple tense.

stopped                  asked          listened
played          called          picked
visited         missed
lived           decided

Although they all end in 'ed' the pronunciation of the 'ed' differs. Sometimes it is pronounced /t/, sometimes /d/ and can also be /id/.

Most nationalities will have difficulties with pronunciation due to English spelling. The same spelling can be pronounced in a variety of ways. Look at the following spellings:

    a)                      b)                   c)
cough               bough             ghetto
Different spellings can be pronounced the same way. Look at the following words:
            dish
            station
            racial
                 parachute

Students may find it difficult to know which way to pronounce 'th'.
Sometimes it is voiced as in:
the    then this       although

     Sometimes it is unvoiced as in:
    think      through     threw    tenth



SELF-CHECK 3

Look at Practical Problem No 1. Can you give a rule to help your students distinguish between the different pronunciations of the ending?

Write a short passage incorporating at least 10 of the verbs from Practical Problem No 1 which you could record and play to your class.

Write an exercise for students to do in pairs, based on your passage, to give practice in the above pronunciation.








Part 2. Writing


We have said that to read successfully, training must be given - the same applies to writing, a skill closely linked with reading and often the one learners find most difficult to acquire. This may be because

a) writing requires organisational skills and careful thought: planning first, then checking and rewriting. This makes spontaneity somewhat difficult.  

b) it is usually regarded as a solitary activity.

c) teachers often assume that students know how to organise and develop written work, if only in their first language, but this is often untrue. Just try writing one of the tasks you ask your students to do!

Given these problems, it makes sense to try to offer students help in a variety of ways, for example:

a) By giving plenty of exposure to model texts, both good models and faulty models (clearly indicating which is which) and give checklists for student reference (ie do's and don'ts).

b) By creating a collaborative environment: using pairwork and groupwork to prepare the ground, giving support, ideas and the chance to participate before the actual writing begins.

By ensuring that the writing takes place in a relaxed learning environment.

     d) By actually teaching organisational skills, the way to develop ideas and  
how to reach a logical conclusion, through regular practice.

     e) By making sure that students have clear criteria to work towards - that   
          is, they know what you are looking for and how you are going to mark   
          their work.


Preparing the way

Because of the range of writing tasks students may wish to complete, some basic questions need answering before the student knows how to approach a particular piece of writing. The first 4 things to consider are:

What is the purpose of this piece of writing?  (eg to inform, request, apologise, instruct, etc)

Who will be the reader? (eg a potential employer, a child, the general public, a loved one, etc)

What is the appropriate style and register? (eg very formal and polite, semi-formal and respectful, friendly and informal) 

What is the correct layout or format? (eg positioning of addresses, complementary close, paragraphing, etc)




SELF-CHECK 4

Why is it that students are so often loath to put pen to paper to produce a piece of written work?


Teaching writing skills

In order to write effectively students must be assisted in developing the necessary skills. Just because a student is able to write isolated, grammatically correct sentences, does not necessarily mean that they will be able to organise a written composition.  Through exposure to a range of selected texts and by studying techniques and devices used to achieve particular results, language learners begin to understand how to produce a piece of writing. To teach writing skills effectively, it's important to:

have a regular, weekly slot dedicated to writing (not the last ten minutes of a lesson as written homework is set).

use a short listening to set the scene, with note-taking, group discussion (ie for / against points) with the teacher giving guidance on contrastive devices (however, although) or words of addition (furthermore, in addition), etc. Ideas can then be joined and paragraphs developed on the board.

incorporate a short reading presented as a model. This gives students something to pull apart and learn how to reconstruct. By focusing on tenses, linking devices, reference words, vocabulary items etc, it gradually becomes clear how words, sentences and paragraphs fit together to fulfil a particular task for a particular reader in the appropriate style and register.
Product writing and basic stages of a writing lesson

Effective writing involves much more than grammatical correctness; students need to know how to organise a composition relevant to the genre and target audience. The genres include narrative essays, discursive essays, report writing, formal and informal letter writing, among others. These need to be tackled at the appropriate levels. Whatever the genre, we can outline basic lesson stages:

1) Lead-in / Discussion
Rationale: To elicit learners’ knowledge (activate schemata)
            To develop predictive skills
            To stimulate interest in the topic

2) Stimulus (Model Text)
Rationale: To exhibit the relationship between stimulus and target text 
         type
        To provide model text for analysis - to derive explicit              
        organisational framework

3) Enabling activities for Main Writing Task
Rationale: To work on information structure and language features
            To work on text generation techniques

4) Main Writing Task
Rationale: To go through a process to generate a product similar to the 
         model text

5) Follow-Up: Comparing texts with a partner / Improving texts
Rationale: To exchange texts with partners
            To evaluate own and partner’s text against a Key text
            To improve own text

This planning relies on students following a model text and producing a similar text of their own and is often referred to as product writing.  Product writing focuses on the organisational conventions of particular text types, or genres. At the end of this module, you will find examples of product writing lessons for different levels.

Process writing and writing sub-skills

Another approach to teaching writing is called process writing. Process writing focuses on the skills a writer needs to complete a particular writing task. These are writing sub-skills.

As follows are the 6 writing sub-skills, which are also the 6 consecutive stages of a process writing lesson (after a lead-in/discussion and before a follow-up).

Brainstorming: making notes of all possible things to be included in the text.
Planning: writing a general outline of the text.
Drafting: writing the first version of the text.
Peer evaluation: giving feedback to classmates on their drafts.
Re-drafting: making changes to the text based on the peer feedback and the writer's own analysis – improving the organisation and coherence, correcting language errors.
Proofreading: final reading and checking for minor mistakes, such as punctuation and spelling.

The process is not necessary a linear one though and will vary depending on the writer and the type of correspondence involved. For example, a lot less time on planning will be spent on a simple postcard and there may only be one draft, without the revisiting / editing phase. However, a lot more time will be spent on the planning and editing stages when writing a formal speech. 

As effective teachers we should be incorporating both the product and process approaches to writing into our lessons.


Writing in class and for homework

Writing is a skill that is often overlooked in the classroom because of the potential difficulties involved, but which deserves more attention in this environment.

Writing in class is a helpful exercise as it shows the teacher what the student can produce without the support of textbooks etc, and can be monitored with the most common mistakes dealt with on the board during the lesson.
 
Collaborative writing also offers a lot of benefits to students. Although the ultimate aim is to develop the writing skills of students individually, students can support each other and can benefit from learning from each other’s strengths. It also helps to generate interest and enthusiasm and meaningful discussion which can then benefit the actual writing process.

Writing is also useful for homework. When giving writing tasks as homework, however, it is essential to prepare students thoroughly by providing them with a purpose for writing, a target audience and a model, if appropriate. For example, the homework task ‘Describe your home’ does not give the student any guidelines on how they should approach the task. Should they write a formal, detailed description for an advertisement, or an informal description to a new pen-friend aiming to share personal information, or part of a description to a twin school giving detailed information on life in their own country?

There will be a different focus to writing skills depending on the students’ level. At the lower levels for example, the teacher will be focussing on the linking of simple sentences together and the correct use of basic grammar and vocabulary. At higher levels, however, the teacher will be focussing on much more, eg conventions of formal and informal writing, the use of synonyms, appropriate use of conjunctions etc.

Marking written work 

Judging a piece of writing can be quite difficult for a teacher. The best way to mark a long piece of work is using criteria. Look carefully at the types of errors your students are making. Give credit for what has been got right and highlight points to work on. These can be in any area of writing, for example:

GRAMMAR         Don’t forget your -ed endings on verbs in your story.

WORD CHOICE Try not to use the word ‘nice’ so much.

ORGANISATION    Look again at where the paragraphs should go.

CONTENT                  How did the boy get to the castle? 

MECHANICS       I can’t read this sentence, please work on your handwriting. 


 Make a clear difference between content (what they wrote about), form (the layout and style) and accuracy (language correctness). 

Then when you comment on the students' work, you can make clear the difference between an accurate but rather rude letter and a very polite and sensible letter that has a lot of mistakes. 

Always make the writing something to share and show to others. Make a list of good phrases that students use and go over them in class together. Read little bits from good work. Stick them up on posters and put them on the wall for all to see. Whatever you do, make the students proud of their writing so that they want to make it look beautiful next time.

Another useful way of dealing with errors is for a sheet to be handed out with the students’ mistakes, for example 20 sentences taken from their writing. Let them correct the mistakes – working in pairs or in a whole-class session. It's amazing how many mistakes students themselves are able to correct. They also seem to enjoy this technique.


Examples of writing lessons

1.  Beginner/elementary

An informal letter giving directions

Presentation:  Use flash cards to revise basic building vocabulary (ie The Bank, Cinema, Shop etc). Use flannelboard to present basic directions/location/language. 'On the left, on the right'  'Turn left ......
Go down Low Street ......’  choral repetition.
Highlight imperative forms 

Practice 1:  Oral practice. Question and answer practice. Written reinforcement for sentences on board.

Practice 2:  Pairwork (Information Gap Activity)
Pairs (A+B) have street plans with 4 buildings located on each. They must ask their partner for directions to, and locations of, the 4 buildings they do not have, and give details of those which they do have.

Reading  (Model text):  A short passage giving directions to new students in the town is given out. Students answer 5 questions (general comprehension) and have to locate the school on the plan given in practice 2 above. 
Students check in pairs.
Focus on any new verbs/vocabulary that did not come in the earlier practice, such as ‘roundabout’ or ‘cross over’.

Free Writing  Teacher tells students: "It's your birthday on Friday and you are having a party. You have invited your class to your house. They know where the school is, but not where you live. Write a paragraph giving directions for them to follow from the language school/college to your house".
(NB: If students live some distance away, you may need to pre-teach
take the number four bus from ..... get off at ......, etc) A simple handout with the layout for an informal letter makes this easier. The students simply fill in the actual body of the letter giving directions. 

Encourage the students to draw a small map to go with the directions as this is not only realistic but gives you a chance to check if they are accurate or not.

2. Intermediate

Writing a film review

Stage 1:   Lead-in
    a) Ask students to describe different types of films (ie comedy,  
    science fiction etc). Teacher puts on board.
b) Pairwork - students discuss 'What makes a good film'.  
    Feedback to teacher who puts ideas onto the board (ie plot,   
    soundtrack, actors, photography etc).

Stage 2:  Reading (Model text)
Hand out the text of a film review with general comprehension questions (short exercise). The questions should simply check understanding of characters, plot, etc. Students can also identify which of the characteristics of good films raised in 1b) above were mentioned.

Stage 3:  Grammar
Elicit grammatical features of the model text.
Structures to be used in a review = present tenses
              b)  Sequencing = after that, then, next, etc.
               c)  Passive voice, adjectives and adverbs. For example:

The direction is brilliant. - It is brilliantly directed.
The photography is impressive. - It is impressively photographed.

 Exercises in these three give practice in the grammatical structures   
 commonly found in reviews.

Stage 4:  Pairwork/oral practice
Students describe a film they have recently seen or particularly enjoyed.  Their partner takes notes. All the above should be included and the following basic outline followed.

            a) the background to the film
            b) the plot
            c) a scene the student particularly enjoyed
            d) the students' feelings about it, possible recommendation

Discussing these points together familiarises the group with the language, grammar and organisation. If the teacher takes the opportunity to make a clear collection of notes on the board, this will then provide a plan for paragraphing a piece of writing.
 

Stage 5:  Free Writing
"Write a review of a film you have recently seen".
A word limit of, say, 300-400 words is good as this is realistically how much space they would be allowed in a magazine or newspaper.

NOTES

Before you begin this lesson you should think carefully:

What do my students need to know about how a review is written?

You cannot teach review writing if you have not first understood yourself how one is structured. Be careful about the type of review you choose.

Reviews are often full of cultural references that the students will find difficult. Here is an example:

Pygmalion

Anyone who sees this marvellous production will have to agree that the play is far superior to Lerner and Lowe’s sugar candied musical. It’s a pleasure to see the play that Shaw wrote and to be reminded of his serious purpose back in 1914 when the play caused a scandal. The notion that turning a flowergirl into a lady might actually be a disservice may pack less punch today, but still resonates in these days of the WAG and airhead celeb.

Avoid giving such reviews as models; opt for ones with fewer cultural references, which your students will be able to understand.  

It's useful to prepare this lesson then either take your group to the cinema or watch a movie together and ask them to write a review of the film you have seen together.

3.  Advanced

Advanced students need to recognise how signals are sent to the reader in order to guide them through how a text hangs together. Using listening passages or reading texts, or having other students giving an oral presentation can be used to present model texts for studying linking devices at higher levels.
Composition writing - expressing an opinion

Model text 

Nowadays it is difficult for students in Further and Higher Education to study as much as their course tutors would like them to. Indeed, it is unusual for students to spend anything more than a quarter of the recommended number of hours doing their coursework. Take the case of two students from Bristol University. Because of grant cuts and rising costs, they are forced to take low paid work, for example, bar work, supermarket shelf-stacking and kitchen work. For this, they can be paid less than £5.00 per hour. In other words, an hour's work would earn them just enough to buy a sandwich.


Stage 1  Students read the text and identify

a) phrases which exemplify (for example, take the case of .....)
b) phrases which amplify (indeed…..)
c) language used for rephrasing (in other words…..)


Stage 2  Guided Writing

Once the language for certain functions has been identified, it can be practised. A simple sentence can be given and students can be asked to amplify or exemplify accordingly. Gradually, language can be added with different functions, for example, presenting a balanced argument (While it may be time that . . .  Despite statistics which reveal that . . .  Although one can understand concern about . . .) The language for counterarguments, rejecting these and leading to a final conclusion gives the basics for a composition giving one's opinion.


Stage 3  Preparation for Writing

Give students a plan
1.  Topic sentence - amplify
2.  Main viewpoint  - amplify
                               - rephrase
3.  Counterargument - reject it and say why
4.  Conclusion - your own opinion


Stage 4  Free Writing

"All post-16 education should be free" - Give your opinion in about 100 words. 

TASK 1

Complete the table below using the 10 verbs in the list.
/t/
/d/
/id/































asked               lived
called              played
decided             visited
missed              stopped                                                      
listened                picked




TASK 2

Write 3 words of your own (do not use examples from the module) containing 'gh' pronounced in 3 different ways.


TASK 3

Write 3 words of your own containing the same sound as the sound underlined in Practical Problem No 3, in which it is spelled in 3 different ways.



TASK 4

Write 2 examples of your own for a voiced 'th' sound and 2 examples for an unvoiced 'th' sound. Clearly mark which is which!



TASK 5

Which other skill is closely linked with writing?

What sort of classroom environment is best to encourage your students to write?

What are the 4 basic questions which need answering before the student can tackle a particular piece of writing?

Here are the basic stages of a product writing lesson discussed in the module:

A) Lead-in / Discussion
B) Stimulus (Model Text)
C) Enabling activities for Main Writing Task
D) Main Writing Task
E) Follow-Up

Match the following activities with the stages above: 

1. Reading and checking comprehension.
2. Brainstorming what students know about the topic.
3. Collaboration between and among students in the process of feedback.
4. Solitary work; drafting and redrafting.
5. Identifying grammatical structures and vocabulary appropriate for the writing task; exercises and games based on such structures and vocabulary.

Simply match the letter and number, eg 
C-3 
(NB: this is just an example, NOT a correct answer)

Why is writing in class a useful exercise? Give at least two reasons.