Intesol Learning App.

Check for procfile and make sure to install most recent database file if setting up on your local dev environment

Instructions for setting up dev env\s\s
Requirements\s\s
PHP v 7.0 min\s\s
Install Laravel, Composer, Git, Node and NPM on your machine\s\s

Clone the repository with git clone\s\s
add the most recent sql file to your database\s\s
Copy .env.example file to .env and edit database credentials there\s\s
~~Run Composer Install~~\s\s
Run php artisan key:generate\s\s
~~Run php artisan migrate --seed~~\s\s
Run php artisan serve\s\s
That's it: launch the main URL and login with default credentials\s\s admin@admin.com - password\s\s
\s\s
\s\s
(Once you upload the database to your local server and amend the .env file you shouldn't need to run composer install or php artisan migrate --seed, you should just be able to run 'php artisan serve')
