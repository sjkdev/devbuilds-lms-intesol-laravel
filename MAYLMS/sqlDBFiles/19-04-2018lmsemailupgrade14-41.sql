# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.5.5-10.1.25-MariaDB)
# Database: lmsemailupgrade
# Generation Time: 2018-04-19 13:41:16 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table 120_hours
# ------------------------------------------------------------

DROP TABLE IF EXISTS `120_hours`;

CREATE TABLE `120_hours` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table 120u1m1s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `120u1m1s1s`;

CREATE TABLE `120u1m1s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `u1m1s1q1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q3` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q4` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q5` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q6` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q7` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q8` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q9` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q10` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q11` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q12` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q13` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q14` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q15` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q16` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q17` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q18` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q19` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q20` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q21` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q22` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q23` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table appointments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `appointments`;

CREATE TABLE `appointments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned DEFAULT NULL,
  `employee_id` int(10) unsigned DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `finish_time` datetime DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table canneds
# ------------------------------------------------------------

DROP TABLE IF EXISTS `canneds`;

CREATE TABLE `canneds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table course_student
# ------------------------------------------------------------

DROP TABLE IF EXISTS `course_student`;

CREATE TABLE `course_student` (
  `course_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `rating` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `course_student_course_id_foreign` (`course_id`),
  KEY `course_student_user_id_foreign` (`user_id`),
  CONSTRAINT `course_student_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE,
  CONSTRAINT `course_student_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table course_upload_texts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `course_upload_texts`;

CREATE TABLE `course_upload_texts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table course_upload_videos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `course_upload_videos`;

CREATE TABLE `course_upload_videos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table course_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `course_user`;

CREATE TABLE `course_user` (
  `course_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  KEY `fk_p_54418_54417_user_cou_596eece522b73` (`course_id`),
  KEY `fk_p_54417_54418_course_u_596eece522bee` (`user_id`),
  CONSTRAINT `fk_p_54417_54418_course_u_596eece522bee` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_p_54418_54417_user_cou_596eece522b73` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table courses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `courses`;

CREATE TABLE `courses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `price` decimal(15,2) DEFAULT NULL,
  `course_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `published` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `courses_deleted_at_index` (`deleted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;

INSERT INTO `courses` (`id`, `title`, `slug`, `description`, `price`, `course_image`, `start_date`, `published`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'Oscar Rosenbaum','oscar-rosenbaum','Non qui tempora omnis temporibus. Quia autem a quis illum molestiae. Minima sint impedit est dolore autem omnis excepturi.',39.28,NULL,'2018-03-01',0,'2018-03-16 00:50:28','2018-04-03 21:41:46','2018-04-03 21:41:46'),
	(2,'Izabella Crist','izabella-crist','Neque nihil quia molestias porro ullam. Qui qui perspiciatis blanditiis ex sint dolor dolores blanditiis. Incidunt quia et aut aliquid rerum natus sit.',96.44,NULL,'2018-03-01',0,'2018-03-16 00:50:28','2018-04-03 21:41:50','2018-04-03 21:41:50'),
	(3,'Guadalupe Simonis','guadalupe-simonis','Adipisci culpa saepe iste minima impedit consequuntur. Est perspiciatis dolorem consequatur quo. Sapiente suscipit reprehenderit odit sed ut. Blanditiis dicta neque quasi esse doloribus.',133.23,NULL,'2018-03-01',0,'2018-03-16 00:50:28','2018-04-03 21:41:54','2018-04-03 21:41:54'),
	(4,'Dashawn Lang','dashawn-lang','Similique quaerat sint nam omnis. Quia voluptatem natus pariatur vitae. Sapiente consectetur facere quas sed aliquid reiciendis. Asperiores sint assumenda aut iste non.',65.76,NULL,'2018-03-01',0,'2018-03-16 00:50:28','2018-04-03 21:41:59','2018-04-03 21:41:59'),
	(5,'Diana Sauer','diana-sauer','Rem aliquam totam nisi nesciunt corrupti laudantium. Earum molestias nam sit sed dolores totam et. Veritatis assumenda eos debitis aut beatae recusandae sed id.',13.28,NULL,'2018-03-01',0,'2018-03-16 00:50:28','2018-04-03 21:42:08','2018-04-03 21:42:08'),
	(6,'120 Hour Tesol','120_hour_tesol',NULL,NULL,NULL,'2018-03-21',1,'2018-03-16 09:22:03','2018-03-16 09:22:18',NULL),
	(7,'190 Hour Tesol','190_hour_tesol',NULL,NULL,NULL,'2018-03-16',1,'2018-03-16 09:23:53','2018-03-16 09:23:53',NULL);

/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table create_courses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `create_courses`;

CREATE TABLE `create_courses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table devtests
# ------------------------------------------------------------

DROP TABLE IF EXISTS `devtests`;

CREATE TABLE `devtests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table int120u1m1s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u1m1s1s`;

CREATE TABLE `int120u1m1s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `u1m1s1q1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q3` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q4` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q5` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q6` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q7` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u1m1s1q8` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q9` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q10` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q11` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q12` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q13` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q14` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q15` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q16` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q17` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q18` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q19` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q20` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q21` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q22` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q23` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q24` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q25` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q26` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q27` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q28` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q29` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q30` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u1m1s1q31` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u1m1s1s` WRITE;
/*!40000 ALTER TABLE `int120u1m1s1s` DISABLE KEYS */;

INSERT INTO `int120u1m1s1s` (`id`, `user_id`, `u1m1s1q1`, `u1m1s1q2`, `u1m1s1q3`, `u1m1s1q4`, `u1m1s1q5`, `u1m1s1q6`, `u1m1s1q7`, `created_at`, `updated_at`, `u1m1s1q8`, `u1m1s1q9`, `u1m1s1q10`, `u1m1s1q11`, `u1m1s1q12`, `u1m1s1q13`, `u1m1s1q14`, `u1m1s1q15`, `u1m1s1q16`, `u1m1s1q17`, `u1m1s1q18`, `u1m1s1q19`, `u1m1s1q20`, `u1m1s1q21`, `u1m1s1q22`, `u1m1s1q23`, `u1m1s1q24`, `u1m1s1q25`, `u1m1s1q26`, `u1m1s1q27`, `u1m1s1q28`, `u1m1s1q29`, `u1m1s1q30`, `u1m1s1q31`, `text`)
VALUES
	(1,0,'Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL),
	(2,0,'Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL),
	(3,0,'Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146','Base Table Or View Not Found: 1146',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL),
	(4,0,'test answer version','test answer version','test answer version','test answer version','test answer version','test answer version','test answer version',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL),
	(5,0,'fbGqG4BWWJAC3c7BBp1DFbUclpUEwwwV','fbGqG4BWWJAC3c7BBp1DFbUclpUEwwwV','fbGqG4BWWJAC3c7BBp1DFbUclpUEwwwV','fbGqG4BWWJAC3c7BBp1DFbUclpUEwwwV','fbGqG4BWWJAC3c7BBp1DFbUclpUEwwwV','fbGqG4BWWJAC3c7BBp1DFbUclpUEwwwV','fbGqG4BWWJAC3c7BBp1DFbUclpUEwwwV',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL),
	(6,0,'nullable test','nullable test','nullable test','nullable test','nullable test','nullable test','nullable test',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL),
	(7,0,'random test','random test','random test','random test','random test','random test','random test',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL),
	(8,0,'qwerty','qwerty','qwerty','qwerty','qwerty','qwerty','qwerty',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL),
	(9,0,'new data test','new data test','new data test','new data test','new data test','new data test','new data test',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL),
	(10,0,'new data test','new data test','new data test','new data test','new data test','new data test','new data test',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL),
	(11,4,'','','','','','','',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL),
	(12,0,'user id db test','user id db test','user id db test','user id db test','user id db test','user id db test','user id db test',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL),
	(13,4,'','','','','','','',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL),
	(14,0,'metaData test','metaData test','metaData test','metaData test','metaData test','metaData test','metaData test',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL),
	(15,4,'','','','','','','',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL),
	(16,0,'hidden id test','hidden id test','hidden id test','hidden id test','hidden id test','hidden id test','hidden id test',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL),
	(17,4,'hidden user name 2','hidden user name 2','hidden user name 2','hidden user name 2','hidden user name 2','hidden user name 2','hidden user name 2',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL),
	(18,6,'neo user 120','neo user 120','neo user 120','neo user 120','neo user 120','neo user 120','neo user 120',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL),
	(19,9,'Jay 120 test','Jay 120 test','Jay 120 test','Jay 120 test','Jay 120 test','Jay 120 test','Jay 120 test',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL),
	(20,6,'pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','',NULL),
	(21,6,'','','','','','','',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','pbysovceatn;ovsuaebriv;');

/*!40000 ALTER TABLE `int120u1m1s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u2m1s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u2m1s1s`;

CREATE TABLE `int120u2m1s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `u2m1s1q1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u2m1s1q2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q3` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q4` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q5` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q6` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q7` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q8` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q9` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q10` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q11` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q12` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q13` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q14` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q15` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q16` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q17` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q18` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q19` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q20` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q21` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q23` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q22` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q24` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q25` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q26` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q27` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q28` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q29` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q30` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q31` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q32` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q33` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q34` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q35` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q36` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q37` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q38` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q39` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q40` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q41` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q42` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q43` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q44` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q45` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q46` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q47` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q48` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q49` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q50` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q51` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q52` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q53` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q54` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q55` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q56` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q57` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q58` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q59` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q60` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q61` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q62` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q63` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u2m1s1q64` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u2m1s1s` WRITE;
/*!40000 ALTER TABLE `int120u2m1s1s` DISABLE KEYS */;

INSERT INTO `int120u2m1s1s` (`id`, `user_id`, `u2m1s1q1`, `created_at`, `updated_at`, `u2m1s1q2`, `u2m1s1q3`, `u2m1s1q4`, `u2m1s1q5`, `u2m1s1q6`, `u2m1s1q7`, `u2m1s1q8`, `u2m1s1q9`, `u2m1s1q10`, `u2m1s1q11`, `u2m1s1q12`, `u2m1s1q13`, `u2m1s1q14`, `u2m1s1q15`, `u2m1s1q16`, `u2m1s1q17`, `u2m1s1q18`, `u2m1s1q19`, `u2m1s1q20`, `u2m1s1q21`, `u2m1s1q23`, `u2m1s1q22`, `u2m1s1q24`, `u2m1s1q25`, `u2m1s1q26`, `u2m1s1q27`, `u2m1s1q28`, `u2m1s1q29`, `u2m1s1q30`, `u2m1s1q31`, `u2m1s1q32`, `u2m1s1q33`, `u2m1s1q34`, `u2m1s1q35`, `u2m1s1q36`, `u2m1s1q37`, `u2m1s1q38`, `u2m1s1q39`, `u2m1s1q40`, `u2m1s1q41`, `u2m1s1q42`, `u2m1s1q43`, `u2m1s1q44`, `u2m1s1q45`, `u2m1s1q46`, `u2m1s1q47`, `u2m1s1q48`, `u2m1s1q49`, `u2m1s1q50`, `u2m1s1q51`, `u2m1s1q52`, `u2m1s1q53`, `u2m1s1q54`, `u2m1s1q55`, `u2m1s1q56`, `u2m1s1q57`, `u2m1s1q58`, `u2m1s1q59`, `u2m1s1q60`, `u2m1s1q61`, `u2m1s1q62`, `u2m1s1q63`, `u2m1s1q64`)
VALUES
	(1,6,'test answer',NULL,NULL,'test answer','test answer','test answer','test answer','test answer','test answer','test answer','test answer','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(2,6,'',NULL,NULL,'','','','','','','','','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','Use an example of each of the following in a sentence and underline it.','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(3,6,'Unit 2 | The Study of EnglishUnit 2 | The Study of English',NULL,NULL,'Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(4,6,'',NULL,NULL,'','','','','','','','','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(5,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(6,6,'self check test',NULL,NULL,'self check test','self check test','self check test','self check test','self check test','self check test','self check test','self check test','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(7,6,'',NULL,NULL,'','','','','','','','','task 1 test','task 1 test','task 1 test','task 1 test','task 1 test','task 1 test','task 1 test','task 1 test','task 1 test','task 1 test','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(8,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','task 2 test','task 2 test','task 2 test','task 2 test','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(9,6,'Unit 2 | The Study of English',NULL,NULL,'Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','Unit 2 | The Study of English','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(10,6,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(11,6,'',NULL,NULL,'','','','','','','','','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(12,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(13,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','','','','','','','',''),
	(14,6,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(15,6,'',NULL,NULL,'','','','','','','','','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(16,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(17,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','','','','','','','',''),
	(18,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','','','','','',''),
	(19,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
	(20,6,'vbhskghersvgnlstilbh',NULL,NULL,'vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(21,6,'',NULL,NULL,'','','','','','','','','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(22,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(23,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','','','','','','','','',''),
	(24,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','','','','','',''),
	(25,6,'',NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh','vbhskghersvgnlstilbh');

/*!40000 ALTER TABLE `int120u2m1s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u2m2s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u2m2s1s`;

CREATE TABLE `int120u2m2s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `u2m2s1q1` int(11) DEFAULT NULL,
  `u2m2s1q2` int(11) DEFAULT NULL,
  `u2m2s1q3` int(11) DEFAULT NULL,
  `u2m2s1q4` int(11) DEFAULT NULL,
  `u2m2s1q5` int(11) DEFAULT NULL,
  `u2m2s1q6` int(11) DEFAULT NULL,
  `u2m2s1q7` int(11) DEFAULT NULL,
  `u2m2s1q8` int(11) DEFAULT NULL,
  `u2m2s1q9` int(11) DEFAULT NULL,
  `u2m2s1q10` int(11) DEFAULT NULL,
  `u2m2s1q11` int(11) DEFAULT NULL,
  `u2m2s1q12` int(11) DEFAULT NULL,
  `u2m2s1q13` int(11) DEFAULT NULL,
  `u2m2s1q14` int(11) DEFAULT NULL,
  `u2m2s1q15` int(11) DEFAULT NULL,
  `u2m2s1q16` int(11) DEFAULT NULL,
  `u2m2s1q17` int(11) DEFAULT NULL,
  `u2m2s1q18` int(11) DEFAULT NULL,
  `u2m2s1q19` int(11) DEFAULT NULL,
  `u2m2s1q20` int(11) DEFAULT NULL,
  `u2m2s1q21` int(11) DEFAULT NULL,
  `u2m2s1q22` int(11) DEFAULT NULL,
  `u2m2s1q23` int(11) DEFAULT NULL,
  `u2m2s1q24` int(11) DEFAULT NULL,
  `u2m2s1q25` int(11) DEFAULT NULL,
  `u2m2s1q26` int(11) DEFAULT NULL,
  `u2m2s1q27` int(11) DEFAULT NULL,
  `u2m2s1q28` int(11) DEFAULT NULL,
  `u2m2s1q29` int(11) DEFAULT NULL,
  `u2m2s1q30` int(11) DEFAULT NULL,
  `u2m2s1q31` int(11) DEFAULT NULL,
  `u2m2s1q32` int(11) DEFAULT NULL,
  `u2m2s1q33` int(11) DEFAULT NULL,
  `u2m2s1q34` int(11) DEFAULT NULL,
  `u2m2s1q35` int(11) DEFAULT NULL,
  `u2m2s1q36` int(11) DEFAULT NULL,
  `u2m2s1q37` int(11) DEFAULT NULL,
  `u2m2s1q38` int(11) DEFAULT NULL,
  `u2m2s1q39` int(11) DEFAULT NULL,
  `u2m2s1q40` int(11) DEFAULT NULL,
  `u2m2s1q41` int(11) DEFAULT NULL,
  `u2m2s1q42` int(11) DEFAULT NULL,
  `u2m2s1q43` int(11) DEFAULT NULL,
  `u2m2s1q44` int(11) DEFAULT NULL,
  `u2m2s1q45` int(11) DEFAULT NULL,
  `u2m2s1q46` int(11) DEFAULT NULL,
  `u2m2s1q47` int(11) DEFAULT NULL,
  `u2m2s1q48` int(11) DEFAULT NULL,
  `u2m2s1q49` int(11) DEFAULT NULL,
  `u2m2s1q50` int(11) DEFAULT NULL,
  `u2m2s1q51` int(11) DEFAULT NULL,
  `u2m2s1q52` int(11) DEFAULT NULL,
  `u2m2s1q53` int(11) DEFAULT NULL,
  `u2m2s1q54` int(11) DEFAULT NULL,
  `u2m2s1q55` int(11) DEFAULT NULL,
  `u2m2s1q56` int(11) DEFAULT NULL,
  `u2m2s1q57` int(11) DEFAULT NULL,
  `u2m2s1q58` int(11) DEFAULT NULL,
  `u2m2s1q59` int(11) DEFAULT NULL,
  `u2m2s1q60` int(11) DEFAULT NULL,
  `u2m2s1q61` int(11) DEFAULT NULL,
  `u2m2s1q62` int(11) DEFAULT NULL,
  `u2m2s1q63` int(11) DEFAULT NULL,
  `u2m2s1q64` int(11) DEFAULT NULL,
  `u2m2s1q65` int(11) DEFAULT NULL,
  `u2m2s1q66` int(11) DEFAULT NULL,
  `u2m2s1q67` int(11) DEFAULT NULL,
  `u2m2s1q68` int(11) DEFAULT NULL,
  `u2m2s1q69` int(11) DEFAULT NULL,
  `u2m2s1q70` int(11) DEFAULT NULL,
  `u2m2s1q71` int(11) DEFAULT NULL,
  `u2m2s1q72` int(11) DEFAULT NULL,
  `u2m2s1q73` int(11) DEFAULT NULL,
  `u2m2s1q74` int(11) DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u2m2s1s` WRITE;
/*!40000 ALTER TABLE `int120u2m2s1s` DISABLE KEYS */;

INSERT INTO `int120u2m2s1s` (`id`, `created_at`, `updated_at`, `user_id`, `u2m2s1q1`, `u2m2s1q2`, `u2m2s1q3`, `u2m2s1q4`, `u2m2s1q5`, `u2m2s1q6`, `u2m2s1q7`, `u2m2s1q8`, `u2m2s1q9`, `u2m2s1q10`, `u2m2s1q11`, `u2m2s1q12`, `u2m2s1q13`, `u2m2s1q14`, `u2m2s1q15`, `u2m2s1q16`, `u2m2s1q17`, `u2m2s1q18`, `u2m2s1q19`, `u2m2s1q20`, `u2m2s1q21`, `u2m2s1q22`, `u2m2s1q23`, `u2m2s1q24`, `u2m2s1q25`, `u2m2s1q26`, `u2m2s1q27`, `u2m2s1q28`, `u2m2s1q29`, `u2m2s1q30`, `u2m2s1q31`, `u2m2s1q32`, `u2m2s1q33`, `u2m2s1q34`, `u2m2s1q35`, `u2m2s1q36`, `u2m2s1q37`, `u2m2s1q38`, `u2m2s1q39`, `u2m2s1q40`, `u2m2s1q41`, `u2m2s1q42`, `u2m2s1q43`, `u2m2s1q44`, `u2m2s1q45`, `u2m2s1q46`, `u2m2s1q47`, `u2m2s1q48`, `u2m2s1q49`, `u2m2s1q50`, `u2m2s1q51`, `u2m2s1q52`, `u2m2s1q53`, `u2m2s1q54`, `u2m2s1q55`, `u2m2s1q56`, `u2m2s1q57`, `u2m2s1q58`, `u2m2s1q59`, `u2m2s1q60`, `u2m2s1q61`, `u2m2s1q62`, `u2m2s1q63`, `u2m2s1q64`, `u2m2s1q65`, `u2m2s1q66`, `u2m2s1q67`, `u2m2s1q68`, `u2m2s1q69`, `u2m2s1q70`, `u2m2s1q71`, `u2m2s1q72`, `u2m2s1q73`, `u2m2s1q74`, `text`)
VALUES
	(1,NULL,NULL,6,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,NULL,NULL,6,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,NULL,NULL,6,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(4,NULL,NULL,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'hsdvglb');

/*!40000 ALTER TABLE `int120u2m2s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u2m3s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u2m3s1s`;

CREATE TABLE `int120u2m3s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u2m3s1q1` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q2` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q3` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q4` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q5` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q6` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q7` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q8` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q9` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q10` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q11` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q12` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q13` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q14` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q15` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q16` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q17` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q18` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q19` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q20` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q21` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q22` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q23` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q24` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q25` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q26` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q27` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q28` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q29` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q30` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q31` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q32` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q33` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q34` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q35` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q36` text COLLATE utf8mb4_unicode_ci,
  `u2m3s1q37` text COLLATE utf8mb4_unicode_ci,
  `text` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u2m3s1s` WRITE;
/*!40000 ALTER TABLE `int120u2m3s1s` DISABLE KEYS */;

INSERT INTO `int120u2m3s1s` (`id`, `user_id`, `created_at`, `updated_at`, `u2m3s1q1`, `u2m3s1q2`, `u2m3s1q3`, `u2m3s1q4`, `u2m3s1q5`, `u2m3s1q6`, `u2m3s1q7`, `u2m3s1q8`, `u2m3s1q9`, `u2m3s1q10`, `u2m3s1q11`, `u2m3s1q12`, `u2m3s1q13`, `u2m3s1q14`, `u2m3s1q15`, `u2m3s1q16`, `u2m3s1q17`, `u2m3s1q18`, `u2m3s1q19`, `u2m3s1q20`, `u2m3s1q21`, `u2m3s1q22`, `u2m3s1q23`, `u2m3s1q24`, `u2m3s1q25`, `u2m3s1q26`, `u2m3s1q27`, `u2m3s1q28`, `u2m3s1q29`, `u2m3s1q30`, `u2m3s1q31`, `u2m3s1q32`, `u2m3s1q33`, `u2m3s1q34`, `u2m3s1q35`, `u2m3s1q36`, `u2m3s1q37`, `text`)
VALUES
	(1,6,NULL,NULL,'ghfdshsf','gfsdhsfg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,6,NULL,NULL,'null, [\'class\' => \'form-control\'])','null, [\'class\' => \'form-control\'])','null, [\'class\' => \'form-control\'])','null, [\'class\' => \'form-control\'])','null, [\'class\' => \'form-control\'])',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'null, [\'class\' => \'form-control\'])','null, [\'class\' => \'form-control\'])'),
	(3,6,NULL,NULL,'pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'pbysovceatn;ovsuaebriv;','pbysovceatn;ovsuaebriv;'),
	(4,6,NULL,NULL,'byvxkus;bglsvyzg','byvxkus;bglsvyzg','byvxkus;bglsvyzg','byvxkus;bglsvyzg','byvxkus;bglsvyzg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'byvxkus;bglsvyzg','byvxkus;bglsvyzg');

/*!40000 ALTER TABLE `int120u2m3s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u3m1s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u3m1s1s`;

CREATE TABLE `int120u3m1s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `u3m1s1q1` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u3m1s1q2` text COLLATE utf8mb4_unicode_ci,
  `u3m1s1q3` text COLLATE utf8mb4_unicode_ci,
  `u3m1s1q4` text COLLATE utf8mb4_unicode_ci,
  `u3m1s1q5` tinyint(4) DEFAULT NULL,
  `u3m1s1q6` tinyint(4) DEFAULT NULL,
  `u3m1s1q7` tinyint(4) DEFAULT NULL,
  `u3m1s1q8` tinyint(4) DEFAULT NULL,
  `u3m1s1q9` tinyint(4) DEFAULT NULL,
  `u3m1s1q10` tinyint(4) DEFAULT NULL,
  `u3m1s1q11` tinyint(4) DEFAULT NULL,
  `u3m1s1q12` tinyint(4) DEFAULT NULL,
  `u3m1s1q13` tinyint(4) DEFAULT NULL,
  `u3m1s1q14` tinyint(4) DEFAULT NULL,
  `u3m1s1q15` tinyint(4) DEFAULT NULL,
  `u3m1s1q16` tinyint(4) DEFAULT NULL,
  `u3m1s1q17` tinyint(4) DEFAULT NULL,
  `u3m1s1q18` tinyint(4) DEFAULT NULL,
  `u3m1s1q19` tinyint(4) DEFAULT NULL,
  `u3m1s1q20` tinyint(4) DEFAULT NULL,
  `u3m1s1q21` tinyint(4) DEFAULT NULL,
  `u3m1s1q22` tinyint(4) DEFAULT NULL,
  `u3m1s1q23` tinyint(4) DEFAULT NULL,
  `u3m1s1q24` tinyint(4) DEFAULT NULL,
  `u3m1s1q25` tinyint(4) DEFAULT NULL,
  `u3m1s1q26` tinyint(4) DEFAULT NULL,
  `u3m1s1q27` tinyint(4) DEFAULT NULL,
  `u3m1s1q28` tinyint(4) DEFAULT NULL,
  `u3m1s1q29` tinyint(4) DEFAULT NULL,
  `u3m1s1q30` tinyint(4) DEFAULT NULL,
  `u3m1s1q31` tinyint(4) DEFAULT NULL,
  `u3m1s1q32` tinyint(4) DEFAULT NULL,
  `u3m1s1q33` tinyint(4) DEFAULT NULL,
  `u3m1s1q34` tinyint(4) DEFAULT NULL,
  `u3m1s1q35` tinyint(4) DEFAULT NULL,
  `u3m1s1q36` tinyint(4) DEFAULT NULL,
  `u3m1s1q37` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u3m1s1s` WRITE;
/*!40000 ALTER TABLE `int120u3m1s1s` DISABLE KEYS */;

INSERT INTO `int120u3m1s1s` (`id`, `user_id`, `u3m1s1q1`, `created_at`, `updated_at`, `u3m1s1q2`, `u3m1s1q3`, `u3m1s1q4`, `u3m1s1q5`, `u3m1s1q6`, `u3m1s1q7`, `u3m1s1q8`, `u3m1s1q9`, `u3m1s1q10`, `u3m1s1q11`, `u3m1s1q12`, `u3m1s1q13`, `u3m1s1q14`, `u3m1s1q15`, `u3m1s1q16`, `u3m1s1q17`, `u3m1s1q18`, `u3m1s1q19`, `u3m1s1q20`, `u3m1s1q21`, `u3m1s1q22`, `u3m1s1q23`, `u3m1s1q24`, `u3m1s1q25`, `u3m1s1q26`, `u3m1s1q27`, `u3m1s1q28`, `u3m1s1q29`, `u3m1s1q30`, `u3m1s1q31`, `u3m1s1q32`, `u3m1s1q33`, `u3m1s1q34`, `u3m1s1q35`, `u3m1s1q36`, `u3m1s1q37`)
VALUES
	(1,6,'fdssd',NULL,NULL,'kfjdhtsg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,6,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,0,NULL,NULL,NULL,0,NULL,0,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,0,NULL,NULL,NULL,0,NULL,NULL,NULL);

/*!40000 ALTER TABLE `int120u3m1s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u3m2s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u3m2s1s`;

CREATE TABLE `int120u3m2s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u3m2s1q1` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q2` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q3` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q4` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q5` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q6` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q7` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q8` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q9` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q10` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q11` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q12` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q13` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q14` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q15` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q16` tinyint(4) DEFAULT NULL,
  `u3m2s1q17` tinyint(4) DEFAULT NULL,
  `u3m2s1q18` tinyint(4) DEFAULT NULL,
  `u3m2s1q19` tinyint(4) DEFAULT NULL,
  `u3m2s1q20` tinyint(4) DEFAULT NULL,
  `u3m2s1q21` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q22` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q23` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q24` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q25` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q26` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q27` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q28` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q29` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q30` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q31` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q32` tinyint(4) DEFAULT NULL,
  `u3m2s1q33` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q34` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q35` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q36` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q37` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q38` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q39` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q40` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q41` text COLLATE utf8mb4_unicode_ci,
  `u3m2s1q42` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u3m2s1s` WRITE;
/*!40000 ALTER TABLE `int120u3m2s1s` DISABLE KEYS */;

INSERT INTO `int120u3m2s1s` (`id`, `user_id`, `created_at`, `updated_at`, `u3m2s1q1`, `u3m2s1q2`, `u3m2s1q3`, `u3m2s1q4`, `u3m2s1q5`, `u3m2s1q6`, `u3m2s1q7`, `u3m2s1q8`, `u3m2s1q9`, `u3m2s1q10`, `u3m2s1q11`, `u3m2s1q12`, `u3m2s1q13`, `u3m2s1q14`, `u3m2s1q15`, `u3m2s1q16`, `u3m2s1q17`, `u3m2s1q18`, `u3m2s1q19`, `u3m2s1q20`, `u3m2s1q21`, `u3m2s1q22`, `u3m2s1q23`, `u3m2s1q24`, `u3m2s1q25`, `u3m2s1q26`, `u3m2s1q27`, `u3m2s1q28`, `u3m2s1q29`, `u3m2s1q30`, `u3m2s1q31`, `u3m2s1q32`, `u3m2s1q33`, `u3m2s1q34`, `u3m2s1q35`, `u3m2s1q36`, `u3m2s1q37`, `u3m2s1q38`, `u3m2s1q39`, `u3m2s1q40`, `u3m2s1q41`, `u3m2s1q42`)
VALUES
	(1,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'gfarclbyaergyae;gryl','gfarclbyaergyae;gryl',1,0,1,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'gfarclbyaergyae;gryl','gfarclbyaergyae;gryl',1,0,1,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'gfdagsfdgs','sfdhgsgh',1,1,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(4,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'fdsgrsvgts','gfsdvt',0,1,0,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(5,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor',NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor',0,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolor',NULL);

/*!40000 ALTER TABLE `int120u3m2s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u3m3s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u3m3s1s`;

CREATE TABLE `int120u3m3s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u3m3s1q1` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q2` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q3` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q4` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q5` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q6` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q7` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q8` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q9` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q10` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q11` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q12` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q13` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q14` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q15` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q16` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q17` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q18` text COLLATE utf8mb4_unicode_ci,
  `u3m3s1q19` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u3m3s1s` WRITE;
/*!40000 ALTER TABLE `int120u3m3s1s` DISABLE KEYS */;

INSERT INTO `int120u3m3s1s` (`id`, `user_id`, `created_at`, `updated_at`, `u3m3s1q1`, `u3m3s1q2`, `u3m3s1q3`, `u3m3s1q4`, `u3m3s1q5`, `u3m3s1q6`, `u3m3s1q7`, `u3m3s1q8`, `u3m3s1q9`, `u3m3s1q10`, `u3m3s1q11`, `u3m3s1q12`, `u3m3s1q13`, `u3m3s1q14`, `u3m3s1q15`, `u3m3s1q16`, `u3m3s1q17`, `u3m3s1q18`, `u3m3s1q19`)
VALUES
	(1,6,NULL,NULL,'svsts','cgsrt',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,6,NULL,NULL,'avz','gcrar',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,6,NULL,NULL,NULL,NULL,'htsr htb',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(4,6,NULL,NULL,NULL,NULL,NULL,NULL,'ghkjghkgkgj','gjhkgkvgkbg','gjhkvgkvgk','gvkgkgkgvk','gkvgkvgk','kvgkgkvgk','kvgkvgkvgk','kgkgkgk','gjkfkgkg','gkvgkvgk','gkgvkvk','gkvgkvgkv','kgkvgkvgk','kvgkvgk','gkvgkvgk');

/*!40000 ALTER TABLE `int120u3m3s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u3m4s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u3m4s1s`;

CREATE TABLE `int120u3m4s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table int120u3m5s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u3m5s1s`;

CREATE TABLE `int120u3m5s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u3m5s1q1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q3` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q4` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u3m5s1q5` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u3m5s1s` WRITE;
/*!40000 ALTER TABLE `int120u3m5s1s` DISABLE KEYS */;

INSERT INTO `int120u3m5s1s` (`id`, `user_id`, `created_at`, `updated_at`, `u3m5s1q1`, `u3m5s1q2`, `u3m5s1q3`, `u3m5s1q4`, `u3m5s1q5`)
VALUES
	(1,6,NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"');

/*!40000 ALTER TABLE `int120u3m5s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u4m1s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u4m1s1s`;

CREATE TABLE `int120u4m1s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `u4m1s1q1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m1s1q2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m1s1q3` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m1s1q4` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m1s1q5` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `int120u4m1s1s` WRITE;
/*!40000 ALTER TABLE `int120u4m1s1s` DISABLE KEYS */;

INSERT INTO `int120u4m1s1s` (`id`, `user_id`, `u4m1s1q1`, `u4m1s1q2`, `u4m1s1q3`, `u4m1s1q4`, `u4m1s1q5`, `created_at`, `updated_at`)
VALUES
	(1,9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-04-16 14:26:48',NULL),
	(2,9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','','','','','2018-04-16 14:26:48',NULL),
	(3,9,'seperate  input 1','','','','','2018-04-16 14:26:48',NULL),
	(4,9,'Sint timeam inciderint nam te, mel ut percipit gubergren scriptorem. Eu quando accommodare consequuntur sed, vel quando facilis invidunt te. At nam veri legendos comprehensam, melius detraxit complectitur ne sea. Eum ex volumus dignissim, ut utamur vituperata sit, qui ut periculis consectetuer. Ne lucilius petentium vel, vix ei possim reprehendunt.','Sint timeam inciderint nam te, mel ut percipit gubergren scriptorem. Eu quando accommodare consequuntur sed, vel quando facilis invidunt te. At nam veri legendos comprehensam, melius detraxit complectitur ne sea. Eum ex volumus dignissim, ut utamur vituperata sit, qui ut periculis consectetuer. Ne lucilius petentium vel, vix ei possim reprehendunt.','Sint timeam inciderint nam te, mel ut percipit gubergren scriptorem. Eu quando accommodare consequuntur sed, vel quando facilis invidunt te. At nam veri legendos comprehensam, melius detraxit complectitur ne sea. Eum ex volumus dignissim, ut utamur vituperata sit, qui ut periculis consectetuer. Ne lucilius petentium vel, vix ei possim reprehendunt.','Sint timeam inciderint nam te, mel ut percipit gubergren scriptorem. Eu quando accommodare consequuntur sed, vel quando facilis invidunt te. At nam veri legendos comprehensam, melius detraxit complectitur ne sea. Eum ex volumus dignissim, ut utamur vituperata sit, qui ut periculis consectetuer. Ne lucilius petentium vel, vix ei possim reprehendunt.','Sint timeam inciderint nam te, mel ut percipit gubergren scriptorem. Eu quando accommodare consequuntur sed, vel quando facilis invidunt te. At nam veri legendos comprehensam, melius detraxit complectitur ne sea. Eum ex volumus dignissim, ut utamur vituperata sit, qui ut periculis consectetuer. Ne lucilius petentium vel, vix ei possim reprehendunt.','2018-04-16 14:26:48',NULL),
	(5,9,'hgeio qgrqeug','gr eaget','aerhtuejyethre','tsrhjd rhae','aesht','2018-04-16 14:26:48',NULL),
	(6,9,'hruewigyberso','griow;rghslybgt','f;w toi;shtosg','bgkf;d\' nkh','hbfwgtb sluvofdjb','2018-04-16 14:26:48',NULL),
	(7,6,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"',NULL,NULL);

/*!40000 ALTER TABLE `int120u4m1s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u4m2s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u4m2s1s`;

CREATE TABLE `int120u4m2s1s` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `u4m2s1q1` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q2` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q3` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q4` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q5` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u4m2s1q6` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q7` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q8` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q9` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u4m2s1q10` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `int120u4m2s1s` WRITE;
/*!40000 ALTER TABLE `int120u4m2s1s` DISABLE KEYS */;

INSERT INTO `int120u4m2s1s` (`id`, `user_id`, `u4m2s1q1`, `u4m2s1q2`, `u4m2s1q3`, `u4m2s1q4`, `u4m2s1q5`, `created_at`, `updated_at`, `u4m2s1q6`, `u4m2s1q7`, `u4m2s1q8`, `u4m2s1q9`, `u4m2s1q10`)
VALUES
	(1,9,'','','Id ad tempus bibendum arcu duis.Id ad tempus bibendum arcu duis.','Id ad tempus bibendum arcu duis.Id ad tempus bibendum arcu duis.','Id ad tempus bibendum arcu duis.Id ad tempus bibendum arcu duis.',NULL,NULL,'Id ad tempus bibendum arcu duis.Id ad tempus bibendum arcu duis.','Id ad tempus bibendum arcu duis.Id ad tempus bibendum arcu duis.','Id ad tempus bibendum arcu duis.','Id ad tempus bibendum arcu duis.','Id ad tempus bibendum arcu duis.'),
	(2,9,'','','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,NULL,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Excepteur sint occaecat cupidatat non proident,','Excepteur sint occaecat cupidatat non proident,','Excepteur sint occaecat cupidatat non proident,'),
	(3,6,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','','','',NULL,NULL,'','','','',''),
	(4,6,'','','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"',NULL,NULL,'\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"');

/*!40000 ALTER TABLE `int120u4m2s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int120u5m1s1s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int120u5m1s1s`;

CREATE TABLE `int120u5m1s1s` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `u5m1s1q1` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `u5m1s1q2` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q3` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q4` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q5` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q6` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q7` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q8` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q9` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q10` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q11` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q12` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q13` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q14` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q15` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `u5m1s1q16` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `int120u5m1s1s` WRITE;
/*!40000 ALTER TABLE `int120u5m1s1s` DISABLE KEYS */;

INSERT INTO `int120u5m1s1s` (`id`, `user_id`, `u5m1s1q1`, `created_at`, `updated_at`, `u5m1s1q2`, `u5m1s1q3`, `u5m1s1q4`, `u5m1s1q5`, `u5m1s1q6`, `u5m1s1q7`, `u5m1s1q8`, `u5m1s1q9`, `u5m1s1q10`, `u5m1s1q11`, `u5m1s1q12`, `u5m1s1q13`, `u5m1s1q14`, `u5m1s1q15`, `u5m1s1q16`)
VALUES
	(1,9,'',NULL,NULL,'things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','vthings that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.'),
	(2,9,'',NULL,NULL,'things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','vthings that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.','things that have never been in a way that challenges, and sometimes threatens, adults who work with them.'),
	(3,9,'this is a test input for u5m1s1',NULL,NULL,'','','','','','','','','','','','','','',''),
	(4,9,'',NULL,NULL,'this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheckthis is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck','this is a test input for u5m1s1 selfcheck');

/*!40000 ALTER TABLE `int120u5m1s1s` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table lesson_student
# ------------------------------------------------------------

DROP TABLE IF EXISTS `lesson_student`;

CREATE TABLE `lesson_student` (
  `lesson_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `lesson_student_lesson_id_foreign` (`lesson_id`),
  KEY `lesson_student_user_id_foreign` (`user_id`),
  CONSTRAINT `lesson_student_lesson_id_foreign` FOREIGN KEY (`lesson_id`) REFERENCES `lessons` (`id`) ON DELETE CASCADE,
  CONSTRAINT `lesson_student_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table lessons
# ------------------------------------------------------------

DROP TABLE IF EXISTS `lessons`;

CREATE TABLE `lessons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lesson_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_text` text COLLATE utf8mb4_unicode_ci,
  `full_text` text COLLATE utf8mb4_unicode_ci,
  `position` int(10) unsigned DEFAULT NULL,
  `free_lesson` tinyint(4) DEFAULT '0',
  `published` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `54419_596eedbb6686e` (`course_id`),
  KEY `lessons_deleted_at_index` (`deleted_at`),
  CONSTRAINT `54419_596eedbb6686e` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `lessons` WRITE;
/*!40000 ALTER TABLE `lessons` DISABLE KEYS */;

INSERT INTO `lessons` (`id`, `course_id`, `title`, `slug`, `lesson_image`, `short_text`, `full_text`, `position`, `free_lesson`, `published`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,'Qui dolorem soluta beatae odio.','qui-dolorem-soluta-beatae-odio',NULL,'Fugiat est eaque placeat et. Quibusdam quia saepe in laborum facere. Provident nihil et saepe quia. Amet velit fuga porro accusantium veniam dolores. Enim reprehenderit inventore tenetur accusantium perferendis.','Enim id possimus sit. Nemo et eveniet ut saepe omnis vel.\nQui ab voluptatem ea nihil quas ea et. Voluptas eum nostrum eum aut ut quibusdam. Error aut nobis beatae alias.\nAperiam omnis ipsa placeat. Magni excepturi enim sint consequuntur odio tempore. Assumenda fugiat sit quo minima. Sed dolores qui id quis.\nNihil et unde unde non voluptatum quo consequatur. Dicta doloremque qui dolore nesciunt porro repellat. Eum cumque laudantium cumque placeat doloremque.\nVeniam aliquid in aliquid atque. Aliquid laudantium qui libero similique. Est quo veniam itaque aspernatur officiis soluta. Nisi aut maiores in consequatur.\nCorporis repellendus incidunt mollitia velit distinctio. Aliquam nihil officiis quia. Et sed doloremque dolore quae ipsam dicta commodi. Non tempore quos omnis corporis expedita odio.\nQuas dolorem rerum similique laborum ut enim. Voluptas necessitatibus nam quo eius ut autem accusamus. Quod fugit aperiam eum temporibus.',8,0,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(2,1,'Rerum commodi vel earum modi blanditiis dicta.','rerum-commodi-vel-earum-modi-blanditiis-dicta',NULL,'Amet eaque omnis maiores vitae et. Ab dignissimos ad assumenda rerum quidem ea. Fuga minima dolor autem. Quia corporis quam quo porro.','Enim possimus dolorum expedita. Velit assumenda officiis rerum voluptatem molestiae iste qui. Est accusamus a vel eius rerum aut. Doloribus voluptatem minus nihil doloremque.\nEos et quidem omnis deserunt atque. Sequi temporibus perferendis voluptas et dolor et. Qui et id in labore in et quo.\nOfficiis qui aut eum optio iure quia consequatur. Dolore et delectus sunt ex numquam et aut. Quisquam officia ab tempora dolore architecto doloribus animi. Aut ad maxime nisi et.\nEum et quam ratione debitis. Enim ut corporis et aperiam. Sit voluptatem et nam.\nQuasi qui et quo qui. Molestiae eos earum quia quisquam optio delectus repudiandae quos. Nihil inventore voluptatem accusamus laudantium et.\nDolorum qui sit asperiores adipisci rerum deleniti neque. Repudiandae minima totam sapiente. Non voluptatem veniam eius dolore.\nNesciunt voluptas quo nihil ut. Ullam dolores sit necessitatibus occaecati quae. Suscipit cupiditate labore eos ut reprehenderit. Mollitia vel quos est.',4,0,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(3,1,'Rerum inventore est qui.','rerum-inventore-est-qui',NULL,'Doloribus adipisci eos vitae ea numquam aspernatur. Qui rerum asperiores quia ducimus id tempora. Quos adipisci quia dolorem iusto.','Neque ipsa dolores voluptates consequuntur. Facere sed maxime id occaecati quia est incidunt. Praesentium error aut optio unde sint in ut. Sequi natus placeat debitis id dignissimos.\nVeritatis eos natus rerum. Et eos nihil voluptatem tempore molestias quaerat. Aspernatur a qui eos eaque minima quidem. Quia sit molestiae molestias commodi dignissimos excepturi.\nSit consequatur occaecati quasi quia. Omnis nobis rem animi consequuntur ea corrupti. Nobis qui vero quidem praesentium consectetur nemo quisquam. Dolorem sunt sed voluptate omnis quia totam aut velit.\nHarum excepturi amet fugiat id rerum quis laudantium aut. Molestiae officia ea aut voluptas ut. Aut suscipit voluptas sit omnis odit aspernatur. Et illum consequuntur sint nam magnam id magnam vitae.\nOmnis iusto quia architecto sunt architecto. Eos cumque amet magni quo amet autem odio. Ex ut earum aperiam expedita pariatur nihil expedita minima.',9,1,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(4,1,'Pariatur iure sunt provident et nostrum.','pariatur-iure-sunt-provident-et-nostrum',NULL,'Nemo ipsa aut sit qui qui. Est eveniet rerum reiciendis quibusdam laboriosam. Temporibus aut temporibus dolorem commodi tenetur. Et et eaque voluptatum sint nisi porro.','Quis voluptas est aut qui consequatur voluptates voluptates molestiae. Minus ea molestias et corrupti voluptatem. Assumenda vitae distinctio natus ullam. Vel velit sed vel tempore voluptatem magni.\nQui voluptas odio incidunt saepe facilis id impedit. Debitis culpa dolor eius aspernatur magni ut nulla. Et dolore autem voluptatem id dicta debitis qui.\nAliquam aliquid laboriosam nihil deserunt unde quibusdam. Maiores dolorem fuga a amet. Eveniet ut dolorem ut sunt ea pariatur perferendis. Et rerum nihil autem deleniti est.\nLibero dolorum perferendis impedit et labore. Consectetur eligendi ut sit nisi corporis voluptas molestias. Dolores maiores sint laudantium ratione quam alias aut. Laboriosam ut fugit optio nostrum cupiditate autem.\nProvident atque officia voluptas aut eum et dolores. Officiis nulla sint nobis. Vero nesciunt aut autem incidunt consequatur.',4,0,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(5,1,'Facilis quos ad est.','facilis-quos-ad-est',NULL,'Omnis sed perferendis laboriosam cumque facere non et. Voluptatibus necessitatibus sapiente illo. Eos aut et omnis praesentium in at ullam.','Sit quia vitae est ut eveniet ut dicta. Dignissimos et sunt repudiandae mollitia. Ut fugit harum doloribus. Eos nam voluptatibus explicabo quam eum nesciunt.\nDolores laborum labore esse eaque doloremque aut. Sit iusto inventore magnam omnis incidunt. Facilis quasi et vel maiores aut.\nMaiores sint nemo quaerat quia quia autem. Quisquam possimus odio hic. Ea velit aut provident suscipit ut incidunt vel. Aliquam distinctio et eos vitae nemo eum.\nSuscipit qui ipsum voluptas eaque. Consequatur esse quasi veniam ducimus distinctio. Est recusandae laudantium fugiat nobis.\nNesciunt exercitationem enim et est veniam ut sint nihil. Delectus dolores quia quia minima. Saepe aut porro impedit ratione. Velit quia repudiandae sit eligendi.\nFugiat eum similique aut. Magnam dolorem quam soluta minus. Molestiae perspiciatis accusamus voluptatem eum.\nSed nesciunt molestias omnis incidunt magnam sint deserunt. Molestiae et nemo accusantium magnam. Et earum minus officiis quas sed.',7,1,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(6,1,'Recusandae harum voluptatem omnis sed quia.','recusandae-harum-voluptatem-omnis-sed-quia',NULL,'Omnis in quia ex velit. Sed consequatur rem incidunt necessitatibus et soluta. Eum tempora rerum asperiores.','Voluptas assumenda odit facere quis voluptates. Consectetur quasi officiis doloremque adipisci laboriosam cumque repellendus. Non tempora distinctio id autem omnis suscipit. Velit neque id aspernatur doloribus aut tempore sapiente impedit.\nPerferendis eos assumenda facere voluptatum consequatur. Esse culpa dignissimos sequi consectetur aperiam ducimus sed. Eos vitae officia officia temporibus est aut. Iste eum delectus vero sed repellat.\nNon porro omnis repellat suscipit. Alias deleniti qui blanditiis deleniti. Ut impedit animi fugiat sunt. Rerum quae voluptas explicabo impedit.\nNesciunt accusantium ullam assumenda est. Velit dolor voluptatibus nisi pariatur exercitationem. Ipsam sed nemo error.\nAb facilis et nihil qui. Ex sit ab ea eum. Dolorem sint totam quam suscipit non. Fuga earum ut doloribus ut iusto libero enim eaque.\nProvident et quis commodi quis. Quia molestiae aperiam minima dolore quo est et. Libero dignissimos quo eum iure.',5,1,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(7,1,'Similique tempore consequatur eos.','similique-tempore-consequatur-eos',NULL,'Porro possimus magnam inventore ea aspernatur quia possimus. Facere hic ducimus suscipit. Aliquid possimus odit fugiat quisquam. Exercitationem inventore unde aliquam ab.','Cumque perferendis facere placeat sint sint. Rerum nulla corrupti doloremque aut omnis voluptatibus. Enim quae nesciunt minus ut porro exercitationem dolores magnam.\nSed accusantium esse ullam. Quia atque quia eaque qui pariatur aut. Ipsum magnam saepe quia accusantium iste debitis voluptas laborum.\nDicta quibusdam pariatur modi iure eligendi. Voluptates et quo nam est quis in. Tempore consequatur exercitationem non consequuntur nulla corporis dolores.\nEnim et ipsum dolorem qui. Rerum ut qui doloribus in. Provident perferendis debitis non quis atque quia. Eum eaque nemo consequatur quam.\nId corrupti facere aut est molestiae praesentium eius. Impedit cupiditate ut omnis est sunt quia ut. Nostrum nostrum non ea alias. Qui modi ducimus eum consequatur eius dolor voluptas.\nAtque omnis non ut. Nulla rem quaerat aut odio quia enim adipisci et. Enim numquam occaecati numquam. Et nostrum distinctio repellat repellat.',2,1,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(8,1,'Laboriosam omnis sapiente quia.','laboriosam-omnis-sapiente-quia',NULL,'Id deleniti sit laborum doloribus mollitia expedita et. Sed quos dolores at ipsa. Molestiae velit voluptas qui non ex sed.','Maiores non id et fugiat ut nostrum. Voluptatum dolorum vel sunt. Rerum veritatis natus rerum dicta non.\nQui est natus recusandae repudiandae sit laudantium sed. Doloribus molestiae aliquam quisquam officia. Ut aspernatur soluta at soluta debitis delectus illum. Sed natus libero praesentium libero velit numquam eum.\nIpsa odit ullam ea recusandae itaque maiores illo. Consequuntur et voluptatem accusantium expedita quibusdam. Et veritatis quas atque incidunt repellat eum praesentium. Dolorum et ut libero atque consequatur repudiandae. Laudantium et dolores ut fuga aut et velit.\nCulpa eos nihil nam. Aliquam quis ut qui iste omnis necessitatibus. Ut voluptas magni nostrum ea sapiente nulla impedit.\nMaiores quo suscipit qui quo nemo aperiam nobis. Enim id veniam amet eius cum alias nihil voluptatem. Nisi blanditiis excepturi provident non porro modi. Quo dolorem quae sequi sit eveniet rem odit aut.',10,1,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(9,1,'Et doloribus et tempora quisquam vitae.','et-doloribus-et-tempora-quisquam-vitae',NULL,'Aliquid molestias nihil accusantium cumque aut id. Voluptatem cumque nemo voluptate sed voluptatem eveniet laborum. Dolorum blanditiis consequatur amet cum animi enim est. Blanditiis corporis doloremque voluptas repellendus maxime.','Voluptate modi architecto eligendi non dolores et. Ea quia provident nam ut eos. Praesentium quia enim laboriosam fugit.\nMolestias ut facilis dignissimos consequatur. Et et magnam aut et et explicabo distinctio. Ratione maiores officiis voluptatum. Eius adipisci non ipsam aut et.\nNon quasi tenetur distinctio provident aut facere sunt. Quia ipsa optio odit doloremque nihil. Ducimus quos natus possimus maxime omnis aspernatur asperiores praesentium. Animi architecto enim accusamus repellendus voluptates id sint. Enim omnis pariatur fugiat placeat.\nNihil est non quos ut nihil accusantium vero harum. Et temporibus odio deleniti at ut voluptatibus sint.\nNam rem minima aut nam. Delectus ab et cupiditate inventore consequatur et ut.\nTemporibus molestiae debitis repellat qui vel quae. Natus libero saepe blanditiis. Asperiores aut quo ipsa ratione odit laboriosam. Ea eos ipsum aperiam.',6,0,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(10,1,'Est et earum quidem repellat dolorem est.','est-et-earum-quidem-repellat-dolorem-est',NULL,'Pariatur quas quia nam temporibus quis consequatur nisi. Voluptas velit aut velit deserunt quas voluptatibus. Earum et iusto tempore rerum minus tempora nihil.','Cupiditate consequatur tenetur voluptatem. Iure harum voluptas aliquam eum sequi facere quisquam. Iusto necessitatibus voluptas eum error. Quo nulla ut nisi est vel esse.\nEt voluptas est sed voluptatum suscipit a quaerat. Dolorem voluptatum quia ex iure quis dolorem. Sint aut magnam corrupti similique. Voluptatem dolorem voluptatibus esse libero eius odio.\nEum eos beatae ipsam. Adipisci sint sit quod iste unde. Rerum qui ut voluptate ut vero impedit.\nFacere unde illum dolores nisi animi dolorem tenetur. Expedita ut laboriosam ducimus est. Id fugit neque nihil ipsum ea. Maxime cupiditate est quos sit iste id saepe qui.\nTenetur maxime voluptate id libero. Sit debitis aperiam ea veritatis omnis optio. Aut ut sed sunt nobis deserunt mollitia.\nEt et quia ullam aliquam ducimus magni et. Officia voluptatem numquam deserunt deserunt delectus. Eum ipsa occaecati sit voluptatem aliquam ad.\nSint enim et at aut ut ut. Quasi quia accusamus officia similique.',9,1,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(11,2,'Minima quia nulla iste sed possimus qui.','minima-quia-nulla-iste-sed-possimus-qui',NULL,'Ipsa sint nemo quo alias aliquid. Eum quas porro in repellendus.','Ipsum voluptate labore aperiam qui. Nulla qui quo excepturi saepe illo cupiditate modi.\nEt quia deserunt voluptatem. Harum unde sit facere consectetur dolor et sequi.\nEt odio est aliquam eius ut vel. A qui optio laborum. Quos atque velit consequuntur vel iure amet.\nOmnis perferendis ut ut perspiciatis exercitationem. Nihil et velit dicta molestiae animi. Quibusdam inventore cum ut omnis eos et dolores ut.\nNeque magnam iure quibusdam. In sed praesentium dolor quo nobis sed dolores. Cum quidem eos accusamus. Aspernatur voluptas voluptates placeat saepe totam.\nSoluta eligendi qui consequatur unde. Esse et beatae odit pariatur. Voluptatem sint sapiente dolorem molestiae magni. Totam eos officiis adipisci est quas qui eum.\nVelit incidunt perferendis deserunt amet sed consectetur eveniet ducimus. Mollitia aut aut ut dolores repudiandae et.',5,1,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(12,2,'Ut in ipsum inventore quis.','ut-in-ipsum-inventore-quis',NULL,'Aut aliquid itaque quis repellendus et. Fugiat impedit ullam recusandae nihil deleniti officia est. Sed sit quisquam cum nesciunt ut quod dolores aperiam.','Deserunt quia sunt et quo blanditiis voluptatum. Atque eaque quisquam eveniet hic consequatur ea. Maxime non consequuntur non id.\nNostrum voluptatibus dolorem et praesentium aperiam dolores ullam. Commodi aperiam ipsam dolores quia dicta ipsa. Ratione soluta id cum.\nVoluptas quisquam quia laboriosam. Et laboriosam ea quasi sunt quaerat dolores.\nRepudiandae officiis non dolor debitis a error est. Nobis quo aut eos commodi dolorem. Autem qui sed magni dolores. Quasi quisquam voluptatum beatae voluptatibus dignissimos.\nEst ex voluptates recusandae sed sed ipsum. Ipsa dolores dolores totam dolorem. Vero iusto quo labore repudiandae aperiam officia dolorum.\nIllo quod ipsam incidunt vitae rerum. Soluta et autem non. Officia sed ipsum nemo odit voluptas beatae. Numquam at non ex officia.',4,0,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(13,2,'Debitis quas nihil alias aut.','debitis-quas-nihil-alias-aut',NULL,'Asperiores consequuntur ab soluta sit iure qui. Quia vero quo ipsa totam rem quia. Qui illo voluptas eos est aut saepe officiis.','Error necessitatibus et et id asperiores et. Distinctio nulla omnis fugit error ut beatae. Consequatur sunt architecto tempora consequatur.\nReiciendis cumque aut repellat nulla quasi. Et quis nihil reprehenderit officiis. Dicta hic doloribus nihil qui aut provident. Omnis et possimus iste repellat.\nNeque qui enim dolorum dolore aut cupiditate. Ea voluptas perspiciatis sit voluptatem vero est. Blanditiis nam facere deserunt facere deleniti.\nVoluptatem sit quam facere sequi quam. Sed consequatur rem velit nisi aut consectetur dolorem. Quaerat et dolores sunt temporibus.\nQuod assumenda facere aspernatur facilis earum accusamus necessitatibus vel. Fugiat quos unde incidunt ducimus nihil voluptatibus. Quo similique eveniet hic officia ut. Omnis sint harum nam quasi accusantium consectetur excepturi voluptatum.\nSint dolorem libero quia voluptate. Corrupti earum eos qui aliquam esse. Similique qui dicta praesentium architecto. Nemo ea veritatis qui aut expedita fugit.',6,0,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(14,2,'Quasi quos perferendis fugit iusto corrupti.','quasi-quos-perferendis-fugit-iusto-corrupti',NULL,'Consequatur voluptate modi ex at aliquid eveniet. Voluptates vero tempora voluptatem veritatis at rem ducimus. Ea voluptas culpa iure nobis voluptas ut. Blanditiis cum et sunt odit ut.','Occaecati non necessitatibus ut assumenda et. Sint neque itaque commodi maiores recusandae tempore voluptatem. In eos non quos veniam quaerat consequatur.\nEt autem ut rerum id unde. Accusamus et culpa veritatis nemo et nihil. Sit commodi illo iste ipsa nostrum magni consequatur totam.\nEius porro quia necessitatibus perferendis soluta sed et. Consectetur sunt aut aperiam quo et. Expedita et fuga magni explicabo. Autem aspernatur ullam saepe autem dolore voluptas mollitia.\nMaiores non veniam exercitationem non aut quaerat neque. Sint autem qui voluptas culpa magni aut voluptas. Doloribus vitae eligendi molestias. Illum et dolorum sapiente laudantium.\nQuos ea accusamus enim maxime blanditiis placeat. Suscipit voluptate ducimus accusantium. Explicabo qui dignissimos voluptate expedita. Facilis omnis atque fugit odit.',4,0,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(15,2,'Voluptatum exercitationem est suscipit earum.','voluptatum-exercitationem-est-suscipit-earum',NULL,'Consequatur blanditiis voluptatem consectetur aliquam nisi ex dolorum. Placeat est eum quis porro adipisci modi. Similique repellat aut hic qui consequuntur ipsa. Autem aut harum nemo et quas.','Facilis aliquam modi corporis quisquam quidem voluptatem. Tenetur ut veritatis natus eius consequuntur. Nostrum aspernatur reiciendis libero omnis. Architecto ab nesciunt qui et. Voluptatem corporis similique qui voluptas.\nMinus animi sunt et dolor magnam. Architecto ratione accusantium accusamus iste occaecati sint. Autem vitae consequatur voluptas a consectetur sit dolorem.\nAperiam reprehenderit molestiae numquam maxime. Voluptas veritatis qui modi facere officia nisi. Sed voluptatem quia quos ea ducimus voluptas fuga.\nEt debitis qui quia doloremque. Et unde quaerat enim consequatur optio similique. Laudantium est dolore officiis non vel vero reiciendis.\nA rem commodi ut commodi eum. Laboriosam laborum voluptatibus aspernatur et doloribus. Atque quam velit ut aperiam.\nDeserunt impedit dolores voluptates pariatur. Consectetur doloremque consequatur quia et quo et. Esse harum dolorem qui qui.',6,0,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(16,2,'Nihil quae eos voluptates odio consequatur.','nihil-quae-eos-voluptates-odio-consequatur',NULL,'Minus aut dolorem aspernatur et aut. Iusto veritatis fugiat rerum possimus enim quasi quia. Cupiditate ducimus distinctio sit id sed placeat fuga. Laudantium amet qui eos fugiat illo vel id soluta.','Modi ipsum maiores minus eos sapiente quidem sed. Molestiae repellat illo quidem voluptas corporis. Sit quod adipisci rerum ut repellendus repellendus sed.\nUnde cum sapiente error aut qui quod eveniet nostrum. Aut qui consequatur ipsum quae vel autem eveniet incidunt. Quisquam voluptate dolor voluptatem consectetur cupiditate iste sunt.\nCupiditate enim ut et. Libero et molestiae cupiditate quis mollitia. Sit est nisi omnis voluptatem facere. Aut error iure in reprehenderit omnis culpa et.\nSunt excepturi deserunt expedita aperiam quas. Consequatur et praesentium officia ex culpa eligendi molestiae. Maxime possimus at doloribus sint quo aut expedita.\nSimilique exercitationem mollitia libero saepe sit aspernatur quas impedit. Qui et et voluptatem fugiat nulla rerum.\nAdipisci qui voluptatem omnis ab earum ut omnis. Repellendus id consectetur veniam dolor. Sint assumenda possimus nobis aut accusamus quia sit. Aut ratione sequi libero velit error vitae hic consequatur.',5,1,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(17,2,'Non et facere dolorem est molestiae.','non-et-facere-dolorem-est-molestiae',NULL,'Dolore harum ut culpa architecto aut suscipit. Architecto consequatur nostrum et omnis doloremque dicta eius dignissimos. Maiores culpa repellat et ut occaecati fugit est. Officiis omnis dolor illum consequatur id perferendis cupiditate necessitatibus.','Voluptates alias eaque doloremque commodi suscipit nam quaerat. Rerum corrupti aut nulla dignissimos accusantium fugit.\nConsectetur rem aliquam necessitatibus dignissimos fuga dolorum. Sit accusantium non dolorem itaque alias omnis. Molestiae soluta est esse dolorum provident temporibus illo repellendus. Consequatur culpa consequuntur ex.\nDicta mollitia quo et fuga accusantium rerum eos. Dicta cupiditate molestias veniam. Recusandae voluptatem molestias quod dolorem. Minima adipisci velit impedit minus est dicta.\nOfficia quia aliquid deserunt. Rerum quos non quis ut. Maxime fugiat qui reiciendis repellendus eos est.\nRem aperiam aut aliquam ipsam et. Error minus fuga velit fuga possimus. Eveniet officia nobis pariatur quasi quibusdam.\nConsequatur tempora dolores repellendus mollitia quas. Enim impedit debitis harum aut optio aut voluptas.\nConsequatur et vel id est. Inventore ducimus et dolorum qui repellendus. Itaque voluptas eius repellat est nostrum dolores. Magnam sed sint omnis.',5,0,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(18,2,'Eum quia deleniti sed sunt.','eum-quia-deleniti-sed-sunt',NULL,'Quis qui ut aperiam earum. Maxime blanditiis hic tenetur ad impedit ipsam culpa. Corporis aperiam perspiciatis asperiores molestiae itaque quo sed. Ipsum voluptatem sed qui voluptas magni et et consequatur.','Labore cum sapiente iure sunt totam temporibus. Eveniet laborum eos facere voluptatibus eaque non at. Praesentium consectetur nisi maxime sed dolor.\nEst molestiae beatae fugiat aut adipisci fugiat neque. Quis et quis non.\nVoluptatem magni similique molestias dolor ullam libero reiciendis. Ducimus accusantium mollitia cumque dignissimos. Sit expedita cumque doloribus dolores quia magni illo voluptates.\nOmnis tempore repudiandae at et repudiandae qui sed. Iste occaecati quibusdam necessitatibus. Ut accusamus quisquam eos necessitatibus ad odit. Est sunt odio ea.\nBeatae quis quam aperiam id iure error. Voluptatem dolore ex eos est. Voluptatem totam voluptatibus iure dignissimos.\nNemo ipsum quia eveniet. Voluptatum animi voluptatem fuga ex autem. Cupiditate qui aliquam molestiae officiis. Sunt voluptatem dolores corrupti veniam deserunt necessitatibus.',5,1,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(19,2,'Dolorem ut nihil quo quos maxime vel.','dolorem-ut-nihil-quo-quos-maxime-vel',NULL,'Omnis eveniet accusantium facere iusto rerum aspernatur et veniam. Atque omnis accusamus et est alias debitis occaecati. Ut voluptatibus mollitia omnis natus veritatis est. Quibusdam consequatur et error et et iure vel.','Distinctio nostrum accusamus autem qui. Quia omnis soluta perferendis. Animi autem mollitia earum voluptatibus officia et debitis. Error commodi fuga maiores enim modi nostrum beatae.\nFugiat quia aut dicta consectetur est repellat totam. Quia corrupti accusantium eos ex consectetur modi maxime. Magnam et ipsam quia non nam ipsum.\nMagni praesentium neque ipsam reprehenderit totam ut temporibus. Error explicabo quia facilis. Ut blanditiis nemo maiores aut quis quo quidem. Laborum doloremque sint ad mollitia est quis maiores.\nEligendi quia ipsum consequatur quia ratione nihil. Neque delectus id et velit qui at ea asperiores. Earum facilis inventore exercitationem debitis beatae. Sapiente nisi reprehenderit quo sapiente earum omnis.\nOmnis eaque assumenda dolorum exercitationem at. Quisquam maiores consequuntur inventore quia iusto et a. Omnis dolorem consectetur quam distinctio aliquam alias vel. Et unde ullam reiciendis et.',1,0,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(20,2,'Corporis odio vero iusto est ipsum voluptatibus.','corporis-odio-vero-iusto-est-ipsum-voluptatibus',NULL,'Eos quod aut mollitia sapiente sint eius eveniet iste. Quo quod et tempora aut. Enim quia consectetur dolor dolores ab.','Fugiat repellendus possimus sunt nesciunt blanditiis. Omnis nisi aliquid et qui dolore et. Eos voluptates ea ullam accusantium inventore unde dolorem.\nOmnis natus quaerat et omnis non nesciunt velit. Quibusdam aut eligendi laborum pariatur autem est et eveniet. Laborum amet eaque optio consequuntur eos accusantium. Quasi animi optio et delectus qui provident. Repellendus consequuntur sunt quia iure aut eos nulla.\nUt consequuntur voluptatem voluptatem modi repellat. Qui quos et eligendi non molestiae culpa. Voluptas earum rerum at id quidem perspiciatis.\nQuisquam vero cum ut quasi id recusandae sapiente. Error odio eos velit quas magnam a est. Ea vero sit expedita aperiam placeat nesciunt. Est et ut distinctio rem facere.\nAspernatur reprehenderit accusantium excepturi vel saepe molestiae. Laborum ut voluptate molestiae iure voluptatem. Iste quam qui nemo fugiat.',6,1,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(21,3,'Est enim consectetur quod enim.','est-enim-consectetur-quod-enim',NULL,'Qui ut maxime sed voluptas. Animi assumenda ex aspernatur et quia. Totam quisquam asperiores quod eum dolores fugiat ipsum. Sit laboriosam autem est repudiandae dolores voluptas.','Veritatis beatae dignissimos sequi voluptatum tenetur numquam repellat. Et vero laudantium repellendus voluptates. Et non facilis non.\nSapiente laborum consequatur iusto aliquid sit. Assumenda nostrum laboriosam iure sequi vero voluptatem perspiciatis.\nSed perspiciatis nemo ab ipsum. Architecto consequatur ipsum excepturi quod. Iste nisi quod possimus eligendi tempore dolorem.\nVoluptatem repellat et excepturi modi eos amet. Voluptas tempora omnis at maiores. Accusantium officia aperiam et quia vel quia et qui. Cupiditate nam placeat hic porro provident dolore corrupti numquam.\nVelit nobis est accusamus impedit itaque error laboriosam. Non nostrum dolorum molestias dolorem quaerat. Nulla culpa ad sunt voluptatem nobis.\nDicta quae vero aliquam voluptas sit illo et. Dolorem a itaque modi ipsum odit est praesentium. Sint libero quia et et libero ex. Qui aperiam inventore beatae et hic provident. Perferendis nihil at est quas vero cumque.',10,0,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(22,3,'Ab quod doloremque vel. Velit ut ab maxime.','ab-quod-doloremque-vel-velit-ut-ab-maxime',NULL,'Ipsam expedita voluptas mollitia ut. Voluptatibus quis rem repellat voluptas commodi nihil. Et sed qui rerum qui a maxime dicta. Ipsum natus aut architecto aut qui mollitia fugiat occaecati.','Facilis itaque consequatur autem quidem atque amet. Omnis assumenda odit et dolorum.\nQuis in minus reprehenderit cumque deleniti nisi. Aliquid praesentium sed rerum mollitia delectus aut illum. Magnam provident perspiciatis quidem molestias. Sunt corporis ex quo quos eum aut omnis ut.\nExcepturi rerum ut inventore quisquam. In voluptas et doloremque reprehenderit. Repudiandae et et laborum. Qui voluptatem numquam possimus commodi quasi tenetur maxime.\nIusto voluptatum quasi sunt exercitationem. Pariatur et et iure ut. Harum nemo qui aliquid illum fugiat repellendus voluptas quis.\nOccaecati suscipit et aut sunt voluptate est quibusdam. Fugit dicta illo minima. Quibusdam vel rerum sed eum perspiciatis qui qui. Cupiditate est unde eum et repudiandae a ea. Numquam at ipsa deserunt.\nTenetur quia et maxime ratione. Aut repellendus reprehenderit corrupti totam. Aliquid soluta et quos id porro.',10,0,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(23,3,'Eveniet cumque nihil doloremque.','eveniet-cumque-nihil-doloremque',NULL,'Iusto sit tempore maxime labore voluptatem beatae dicta. Ut quia cumque consequatur similique veniam aut et.','Accusantium et modi labore. Ut et excepturi est cumque molestiae et modi. Et et odio modi eaque saepe. Ducimus explicabo sed excepturi omnis veniam.\nOdio eos qui voluptatem aut ex. Earum et aliquam fugiat exercitationem rem optio. Laboriosam vitae nulla quidem et voluptas consequatur dolor. Quaerat in corrupti est aut placeat consequatur.\nNihil reprehenderit doloribus aspernatur odit maiores sed accusamus voluptas. Reiciendis rerum saepe et eaque aspernatur. Asperiores ut sed mollitia officia sed incidunt ratione necessitatibus. Maxime repellendus aperiam repellat aspernatur voluptatem aspernatur delectus iure. Praesentium sed ipsa eligendi et voluptas ut.\nFuga iusto autem ut reiciendis repudiandae id voluptatem. Quia voluptas modi quo qui maxime ex. Optio culpa incidunt porro. Dolor labore consequatur architecto et et maiores saepe.\nAspernatur sequi est qui suscipit aliquid. Natus nobis voluptatem nisi laboriosam quos itaque.',3,1,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(24,3,'Repudiandae magnam unde et et molestias.','repudiandae-magnam-unde-et-et-molestias',NULL,'Doloremque deleniti aut in doloremque impedit. Et quis vel quae omnis eius maiores culpa eius. Consectetur corrupti laboriosam ullam ut quod.','Sunt soluta maxime eveniet possimus. Aut soluta et est dolor et. Magni ut voluptatem optio minus laboriosam. Eveniet dolorem nisi excepturi non eos ab id explicabo.\nLabore culpa eum cupiditate laboriosam. Illo illum possimus natus enim nam. Vel omnis quos provident assumenda cum et nemo.\nDoloribus officiis voluptas sapiente dolor vel. Explicabo sed velit molestias porro sequi molestiae sunt. Dicta quasi repudiandae tempore architecto beatae.\nQuidem quis dolores eos rerum rerum. Aliquid numquam repellendus consequatur consequatur nisi consequatur possimus. Minus inventore est veritatis possimus placeat vitae eaque.\nConsequatur voluptatibus tempora beatae aut. Amet aperiam autem quia esse error adipisci. Quia nostrum error blanditiis sit distinctio laborum omnis. Tempore est vel et et occaecati molestiae ab corrupti.\nSunt dicta quis quos ad voluptate. Laboriosam sit ut incidunt ipsum facere debitis. Assumenda ut enim rem quis repudiandae labore pariatur doloribus.',9,1,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(25,3,'Reprehenderit sunt nesciunt non qui qui eius.','reprehenderit-sunt-nesciunt-non-qui-qui-eius',NULL,'Voluptas omnis saepe eos fugiat magnam qui rerum veritatis. Doloremque ipsa sint eos odit minima modi. Voluptatem dolores labore nesciunt fugiat est a.','Aut nihil beatae dolores est. Sequi sed aut sint eos ut. Quisquam quos nam sunt doloremque.\nQuaerat voluptatem vel tenetur sed. Reprehenderit ipsam corrupti rerum qui ab ad. Non voluptate iusto nemo ut ut tempore.\nFuga dolorem est ea aperiam. Doloremque omnis tempore maxime velit. Voluptatum nihil aspernatur optio qui nam nam nihil.\nVel iste officia odio et molestiae omnis exercitationem. Fugiat reprehenderit itaque commodi molestias in quod. Voluptates non in nemo fuga perferendis in.\nNihil temporibus debitis nihil perferendis. Est voluptatum quisquam quia iure voluptatem voluptas cum.\nAtque nostrum impedit fugit labore non quis. Quis rerum repellat neque praesentium. Aut quia porro atque ullam. Voluptatem repellat in accusamus non voluptas sit.\nQui doloribus placeat ad. Veritatis ut earum cumque quia est.',5,0,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(26,3,'Qui inventore saepe omnis molestiae.','qui-inventore-saepe-omnis-molestiae',NULL,'Culpa cum error deserunt alias et. Dicta quis consequuntur recusandae nulla qui. Voluptatem vero nobis eveniet sunt voluptatibus sunt. Impedit quod ut itaque adipisci aliquid quam illum.','Deleniti molestiae est et iusto nesciunt. Autem vero reprehenderit est earum.\nAutem facilis ea ut aut voluptas assumenda nesciunt. Ut commodi sit minus aspernatur sed mollitia in. Delectus sapiente ipsam harum quia voluptas dicta. Quidem dolorum cumque vel culpa minus.\nDolor minima iste modi. Et voluptatum unde saepe explicabo. Quia qui ut aut dignissimos nemo.\nDoloremque temporibus debitis soluta nisi omnis doloremque. Cum ea quos nemo corrupti quibusdam aspernatur id.\nLaborum voluptas quibusdam occaecati voluptatum. Quae nobis quia odit.\nCupiditate consequatur magni qui earum. Vel facere et illum placeat cum quia quam. Expedita rerum illum tempora dolorem itaque excepturi et.\nQui delectus ut fugit officiis possimus voluptates tempore. Quam et excepturi eos aperiam ad laboriosam voluptatem. Nihil qui corporis labore illo. Reiciendis soluta adipisci omnis corrupti.\nHic temporibus quis dolorem consequatur qui sint earum. Laudantium minima quos voluptas debitis qui quod laborum.',7,1,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(27,3,'Est molestias a porro commodi ipsa.','est-molestias-a-porro-commodi-ipsa',NULL,'Ut vitae sed veniam enim nobis neque. Sed accusantium nostrum cumque sint et.','Assumenda deserunt nisi quos voluptatibus veritatis id. Esse veritatis eligendi consequatur.\nAliquid ipsam molestiae perspiciatis repudiandae. Veniam sunt voluptas vel fugiat. Enim quia ea sit suscipit.\nAliquam nesciunt ut in necessitatibus et dolor. Aspernatur temporibus asperiores sint iusto repudiandae ipsam. Autem natus at aut repudiandae quasi. Aliquam optio accusamus iure ullam totam consequuntur dolores.\nAt et consectetur ipsa numquam dolorem. Voluptatibus aut adipisci nobis omnis suscipit assumenda. Minus facere magni necessitatibus officia.\nNecessitatibus nostrum delectus sunt saepe eligendi molestiae. Sed asperiores et cum itaque. Reiciendis quas adipisci aut fuga dolores perferendis occaecati.\nSaepe cumque ad dolor aut ut. Ex quia aliquid minima velit vel sed consectetur dolor. A ut repellat voluptatum inventore quo nemo hic quos.',4,0,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(28,3,'Quia facere ex enim sapiente qui.','quia-facere-ex-enim-sapiente-qui',NULL,'Quod culpa ad id in. Possimus qui et omnis ratione. Non cum id rerum. Accusantium maiores voluptas velit sunt nemo iure vero.','Exercitationem provident quia rem tenetur velit exercitationem qui minus. Consequatur id facere est nulla. Sed quia aliquam vero omnis quia quibusdam maxime. Facilis aspernatur ut architecto repellendus sapiente.\nAutem quibusdam velit perferendis excepturi aut. Asperiores cumque enim ipsa non et perferendis quia commodi. Explicabo dolorum aut maxime deleniti qui quisquam quibusdam. Officiis ut incidunt rerum consectetur voluptatum.\nEx distinctio ea deserunt maiores. Aperiam optio aperiam ut impedit omnis. Qui quo facere magnam adipisci distinctio ut. Ea pariatur exercitationem qui sint voluptatum praesentium sapiente.\nQuia commodi ipsum et error quo nulla iste. Voluptatem dignissimos aspernatur explicabo blanditiis ut. Hic corrupti tempore quis voluptatum aut voluptatem ad. Architecto minima dolores quia saepe.\nEa aut et omnis laboriosam error fugiat a. Odit ut culpa et autem aspernatur praesentium quia. Exercitationem molestiae dolorem repellat ut corrupti.',7,0,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(29,3,'Provident rerum ea quasi animi aut.','provident-rerum-ea-quasi-animi-aut',NULL,'Amet mollitia consequatur animi harum quos qui voluptas. Ipsa officiis deserunt fugit qui. Suscipit illum quas temporibus ea rerum facere quo reiciendis.','Ab ducimus voluptas ut quod nisi. Id ut doloremque quasi. Quia consequatur et possimus dolorum qui impedit est.\nPariatur quidem quo earum est ut rerum. Iusto eaque animi aut perspiciatis laboriosam. Aut nostrum autem molestiae.\nEa optio officiis vitae aliquam incidunt alias. Cum itaque voluptate hic qui doloribus impedit. Qui vero ut sit qui aut quas nemo quod.\nError ipsam cumque vitae architecto eligendi. Molestiae ut atque veritatis esse sint. Veniam facere architecto aspernatur ipsam consequatur praesentium.\nQui libero nulla minus at consequatur et alias. Non voluptatibus maiores et ad a. Magni asperiores laudantium sunt natus dolorem quisquam sint.\nSimilique dolorem vel et voluptatum et. At est quos eaque sit est. Saepe dignissimos quas error porro earum optio eligendi. Perferendis accusantium consequatur consectetur earum pariatur rerum.',4,0,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(30,3,'Qui placeat unde accusamus voluptas aut.','qui-placeat-unde-accusamus-voluptas-aut',NULL,'Ipsa et enim distinctio et placeat porro. Qui consequatur ut et nam sit expedita laudantium. Consequatur et animi nihil sint. Veniam tempora natus repellat assumenda temporibus.','Voluptatem ut non voluptatem necessitatibus in alias. Iusto aut placeat iusto quo quo. Culpa vel earum eius error doloribus eos. Assumenda exercitationem laboriosam libero ullam repellat aut rerum.\nTotam sequi et voluptatem enim asperiores. Sapiente assumenda aut et doloribus sed. Similique sunt veniam consequuntur illum expedita repellendus.\nAt sed voluptatum eaque eveniet. Minus officiis amet quaerat. Porro voluptate possimus non excepturi ipsam quod inventore amet.\nDolor rerum rerum fuga assumenda deleniti quas. Corrupti quis sunt molestias est. Molestiae nam ea voluptatem itaque expedita.\nEst eligendi aut non aut voluptas nesciunt voluptas. Suscipit voluptas sequi distinctio ut qui delectus. Veritatis quo reprehenderit quidem expedita autem. Voluptas eveniet consequatur et dolore.',2,1,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(31,4,'Facere mollitia odit et.','facere-mollitia-odit-et',NULL,'Est ullam aliquid unde rerum omnis. Qui ut dolores culpa enim corrupti deleniti.','Exercitationem exercitationem qui quae. Nam qui eaque et. Velit deserunt ut occaecati voluptatem facere.\nNostrum consequatur explicabo quae nostrum repellat aut quas. Enim dolorum mollitia nostrum et et. Culpa consequatur dolores sint dolorem qui cupiditate. Ea quisquam hic quod sit blanditiis in.\nAut adipisci aliquam voluptas dicta sed blanditiis non. Dolor nobis ut iure est tempora tempore aperiam illo. Nam at deleniti recusandae aliquam recusandae. Eos ab soluta aliquam nostrum.\nConsequatur iste officiis quaerat laudantium doloribus similique magnam. Culpa odit accusamus animi temporibus. Et sit ad quia et qui iusto id. Omnis praesentium molestiae earum quae dolorem voluptatem.\nRerum labore magnam velit sit ut itaque et ab. Accusamus consectetur voluptatem et et. Amet et ea excepturi ea vitae.\nAutem consequatur placeat occaecati. Est praesentium officiis quia asperiores et. Nihil nemo eos est eos voluptatem id.',4,0,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(32,4,'Voluptas velit inventore sint ipsum iure rerum.','voluptas-velit-inventore-sint-ipsum-iure-rerum',NULL,'Et sed eum fuga nesciunt. Ipsam qui dignissimos officia iste. Consequuntur eum quis explicabo adipisci officia. Sint qui temporibus blanditiis quia dolorum autem.','Voluptatem sed soluta hic molestiae sed qui aut aut. Amet qui praesentium et est neque quia delectus. Facilis est recusandae molestiae eius ipsum. Temporibus saepe dignissimos fugit dolore et recusandae quidem fuga.\nSit similique autem nemo esse laboriosam molestias. Repellat ut dolores ut eos. Deserunt a quos veritatis dolores autem vel hic. Incidunt libero dignissimos qui ipsa. Aut repellendus sit modi consequatur vel tenetur adipisci.\nVoluptatum nam perferendis natus rerum fugit. Eius quidem recusandae vel occaecati quaerat. Omnis vitae reprehenderit molestias eligendi at.\nMollitia earum laudantium eos ipsam odit. Esse laboriosam similique minus corporis eligendi eveniet. Qui et ut quos minima ratione sunt voluptas. Molestiae deleniti qui ipsum possimus excepturi quibusdam.\nTempora porro molestiae doloribus distinctio eum quasi ullam. Qui animi est quia reiciendis illum. Debitis enim asperiores et iure alias officia. Adipisci ea minima tempora eum sed exercitationem.',9,0,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(33,4,'Itaque illum repellat odit cumque dignissimos.','itaque-illum-repellat-odit-cumque-dignissimos',NULL,'Officia deserunt iste rerum vero adipisci ipsam. Mollitia tempora fugiat ab. Temporibus aliquam sit at nemo. Voluptates porro unde ex atque voluptas itaque eaque.','Dolore sunt eum porro quia dicta veritatis ut. Qui modi minus doloremque ad totam. Illum similique fuga aut. Assumenda et aspernatur alias ut aut itaque.\nVeniam laudantium consequatur exercitationem molestias voluptatibus explicabo quam. Consequatur cum illum asperiores nulla. Unde dignissimos quia omnis maiores deleniti.\nReprehenderit commodi deleniti fuga odit quia dolor sed vel. Fugiat est quae quae debitis est beatae aut. Illo fugiat debitis qui iste.\nVel eos totam ullam voluptas et commodi dolore qui. Qui asperiores nemo sit excepturi doloremque aut voluptatum tempore. Recusandae ut id in aliquam. Dolores quo qui itaque et necessitatibus deleniti.\nEst velit nesciunt eius est vitae odit. Nesciunt perspiciatis veniam perspiciatis non atque quis velit quia. Sit suscipit assumenda quia et rerum expedita cumque.',5,0,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(34,4,'Est est in occaecati ullam sunt.','est-est-in-occaecati-ullam-sunt',NULL,'Ipsa quod non sed et et quia corrupti. Voluptatem iste dolores reprehenderit est. Eum laborum et aut placeat soluta. Tempora sit et minus architecto nihil inventore.','Placeat voluptas neque eum vitae in id. Dolor molestiae occaecati dolor praesentium ut voluptate omnis. Repellat saepe ullam dolor ullam expedita.\nRepellat suscipit quam molestias id asperiores dolore ut. Illo consectetur velit omnis. Voluptatem molestias aliquid vel aut reiciendis velit.\nAtque esse delectus eos vero rerum ducimus. Aut expedita quis quas error quia consequatur. Consequuntur ad asperiores voluptas voluptatem in perferendis et. Tempore minus ut et at.\nDeleniti cupiditate quisquam ut iste. Architecto vel vitae vel adipisci dolore. Libero hic consequatur sunt deserunt et delectus consequatur aut. Fugit mollitia itaque eos provident et sit.\nIn facere eveniet eum omnis suscipit. Aut aut delectus incidunt reprehenderit quos ab rerum tempora. Maiores ipsum natus et. Est quia aliquid omnis voluptates.\nIncidunt eos nam odio sint. Placeat reprehenderit ab inventore possimus molestiae consequatur laudantium. Unde voluptates vel nesciunt maiores quod.',8,0,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(35,4,'Minus ut eos voluptatem enim rerum adipisci quos.','minus-ut-eos-voluptatem-enim-rerum-adipisci-quos',NULL,'Voluptas aut totam eos et molestias. Dolor voluptas quibusdam cum hic voluptates cum. Voluptatem id exercitationem qui assumenda.','Accusantium ipsam et omnis quod voluptas voluptatem occaecati consequatur. In ut ut ducimus ullam id doloremque adipisci. Nemo praesentium quis et sint placeat eum. Quaerat eos sunt odit quasi sapiente beatae vel.\nIpsam quis doloremque repudiandae sunt. Est consequatur et quo nihil aperiam nihil suscipit. Est accusamus amet perspiciatis. Suscipit illo sed facere culpa eos dolorem et commodi. Laboriosam quod voluptatem et tempore.\nVeritatis nemo magni amet voluptatem molestiae eveniet est sit. Harum fugit temporibus est est. Error dolores dolorum aut quas numquam neque laboriosam.\nSint esse voluptas nostrum quaerat est. Rerum officiis saepe error omnis quia. Explicabo error quasi iste vel praesentium reprehenderit. Dolore nihil qui suscipit.\nAut consequuntur eveniet a ipsa. Possimus nobis qui qui et animi laudantium fugiat. Sequi suscipit et odit et est nihil. Quos modi repudiandae quis.',5,1,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(36,4,'Porro ex et dolor qui autem non fugiat.','porro-ex-et-dolor-qui-autem-non-fugiat',NULL,'Accusantium labore temporibus ut et. Cumque expedita tempore voluptatem quisquam magnam in maiores omnis.','Consequatur suscipit voluptate et tempora neque. Sapiente velit dolorum quod aut aut. Possimus animi et eos quaerat aut explicabo. A et corporis unde qui est vitae non.\nVoluptas maiores aut nihil repellat ullam ad. Et dignissimos magnam error quia quia laborum.\nOmnis harum sapiente tenetur id. Nostrum quo asperiores nobis dicta. Sit modi modi consectetur voluptas. Mollitia voluptates at iure.\nOccaecati nesciunt omnis dicta quia debitis unde omnis fuga. Vitae ex est corporis velit recusandae cum. Iusto tenetur voluptas eum quaerat ut quae doloribus. Eaque vero qui rerum eos occaecati.\nAperiam non molestias amet aspernatur voluptas. Voluptatibus ut minima fugiat explicabo velit modi distinctio rerum. Omnis voluptas tempora incidunt et similique sed error et. Rem repudiandae facere fugiat explicabo ut molestiae.\nUt et molestiae quidem. Nostrum et et officia. Ea dolorum nihil nostrum est incidunt. Inventore dolorem natus molestiae illo perspiciatis ipsam.',4,0,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(37,4,'Sapiente at quia aut et ipsam soluta ut.','sapiente-at-quia-aut-et-ipsam-soluta-ut',NULL,'Saepe dolore quia ut dolores hic aut. Molestiae autem ullam ullam molestiae velit ipsum sit. Id voluptatem voluptatum iure. Est aspernatur quia autem minima.','Aut eum beatae natus sed. Dolor sint enim amet. Veniam repellendus possimus aut quo ut qui magnam.\nDucimus sapiente ad dolores non repudiandae dolorum quidem nihil. Facilis et aut nostrum. Eligendi quae sequi minima. Ad tenetur quia rem cum eligendi.\nSaepe excepturi vero iusto aut ad. Odit porro suscipit voluptatibus. Qui veritatis minima veniam ipsa sed. Enim rerum error quae.\nVero nisi reprehenderit ex consequuntur nam sit quisquam. Temporibus corrupti soluta atque deserunt. Esse libero at molestiae sunt rerum rerum porro.\nExplicabo aut omnis eveniet. Aut vel quis sit est ducimus. Possimus fugit et voluptas facilis aut.\nNon omnis occaecati exercitationem veniam temporibus et maxime. Accusantium vitae nam voluptates facilis velit. Assumenda expedita sunt minus iusto eaque. Consectetur quia a quidem sunt.',4,0,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(38,4,'Nam maxime quae aperiam ut nobis et.','nam-maxime-quae-aperiam-ut-nobis-et',NULL,'Consectetur atque sint distinctio ipsam modi quod. Perferendis vero dignissimos et incidunt temporibus libero consequatur. Non eum vel accusantium fugit doloremque inventore. Voluptas est et illo aperiam vero consequatur.','Nam velit voluptas mollitia architecto quia qui. Ducimus enim mollitia laudantium reiciendis incidunt. Nostrum non aut reprehenderit sequi. Adipisci aut fugiat est ut possimus maxime.\nReiciendis accusamus labore deleniti excepturi nemo qui. In molestiae laboriosam autem molestiae.\nAut hic eius nihil et dignissimos. Non doloribus harum quis blanditiis. Tempora non et nam maxime doloribus nostrum.\nVeritatis ex tempore modi. Corrupti est optio tempore eum in iusto cum rem.\nEius unde doloremque unde sunt est quaerat sunt. Alias quasi et voluptatem. Minus nihil rerum officia. Et animi quis aspernatur aliquam est debitis.\nEt minima voluptas rerum cupiditate. Illum nemo recusandae quia doloribus mollitia quasi. Ullam eos qui aut provident.\nQui nihil sed quibusdam explicabo modi animi maxime. Velit aut modi quo repudiandae rem soluta consectetur. Iusto ut officiis autem est rerum. Maxime quo et possimus iure doloribus optio.',2,0,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(39,4,'Veritatis repellat modi odit.','veritatis-repellat-modi-odit',NULL,'Aut doloremque in consequuntur. Dicta odit nostrum dignissimos perspiciatis quo perspiciatis. Sit adipisci ducimus dolores enim doloribus perferendis. Et voluptatem amet eaque atque harum. Qui quibusdam ut quia facere ipsum.','Et voluptatum quas numquam reiciendis dolores. Deserunt libero ut debitis non aliquam repellat magni. Eum sequi totam praesentium assumenda qui voluptas quisquam quisquam.\nAlias totam expedita modi dolore. Atque provident id doloribus blanditiis. Voluptatem molestiae repudiandae et voluptatum atque voluptates. Hic sunt ut consequatur esse voluptatum assumenda.\nEnim dicta aliquam voluptas. Exercitationem laboriosam amet ut est voluptas autem. Ut quia sed exercitationem et quia eos. Eos voluptates eveniet mollitia perferendis libero aut.\nVoluptatibus est molestias quibusdam et aperiam quasi. Quo id impedit et possimus non voluptatum dolor. Omnis ipsa id molestias asperiores repellat tenetur aut et. Ut voluptas optio sunt consectetur ut necessitatibus. Ipsam deleniti velit voluptatum totam.\nIncidunt esse tenetur repudiandae tempora quam ipsa sed. Aut eum aspernatur qui qui distinctio eaque. Molestias nihil qui et esse.',7,1,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(40,4,'Voluptatem sunt eveniet suscipit iure.','voluptatem-sunt-eveniet-suscipit-iure',NULL,'Error vel quibusdam excepturi quos itaque. Perferendis recusandae sit voluptas consequuntur. Sed qui qui voluptatem optio. Totam qui iste voluptatem voluptatem.','Tempore debitis rerum iure at eligendi. Rerum quia autem maiores dolor. Eius aspernatur velit reiciendis quia.\nQuisquam quia laudantium aliquid dolorum deleniti autem. Aut facere cupiditate hic et. Quis nobis esse vero velit cum qui.\nPossimus sunt omnis accusamus et. Earum reprehenderit error error in tempore qui sunt. Et illum et perferendis odit quisquam.\nSed asperiores dolorem magnam vel. Eos ut eius qui temporibus. Nihil at ipsa placeat libero id. Omnis incidunt itaque consectetur est rerum accusantium.\nNecessitatibus molestiae et quia laudantium earum. Aliquid consequatur aut in esse aperiam quia ullam. Natus at recusandae recusandae aspernatur culpa.\nQuidem quo quia necessitatibus omnis. Autem non dolore aut eveniet voluptatem. Nulla quis voluptas et natus molestias quam architecto. Esse assumenda voluptatem enim consequatur rerum et at ut.\nVel quas consequatur quia deserunt commodi repellat. Aliquid quae qui at blanditiis. Qui sequi illum aut ipsum ipsa.',8,0,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(41,5,'Id ut aut quia nobis. Odio ex rerum id.','id-ut-aut-quia-nobis-odio-ex-rerum-id',NULL,'Et tempore dolores est sit est non. Quia maiores aut ut voluptatem soluta. Iusto temporibus quaerat nihil voluptatem earum libero ex aut. Autem assumenda aperiam repudiandae quod esse velit aut ut.','Sunt eos numquam sapiente id ullam. Voluptatem quia voluptates deserunt mollitia fuga. Nam quaerat distinctio error tenetur. Et et aut et eaque voluptatibus voluptas.\nCorrupti aut pariatur voluptatem numquam omnis tempore. Vitae quos eos perspiciatis. Aspernatur assumenda corporis facilis deleniti. Reiciendis nulla velit et modi porro.\nQuisquam aut rerum architecto corporis velit necessitatibus sit. Eos vitae debitis alias explicabo qui animi itaque. Perferendis ipsum consequatur ipsum sequi voluptatem. Saepe corrupti error eum.\nConsequatur in aut repellat modi veritatis dicta. Ullam sed voluptatum tenetur dolores qui fuga eligendi. Quam quis voluptate similique enim.\nConsequatur est corporis placeat amet enim et ut. Ad in voluptas accusamus vel. Omnis quia ea impedit fuga saepe qui. Consectetur atque sit voluptatem et sit nihil enim. Eveniet tempore porro hic.',3,0,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(42,5,'Id esse ducimus tempora repellendus tempora.','id-esse-ducimus-tempora-repellendus-tempora',NULL,'Aut quae dolores hic corrupti autem quibusdam quo et. Dolorem reprehenderit necessitatibus et corrupti. Veritatis molestias magni sunt iusto et mollitia eligendi. Est nisi consequatur similique sed odit porro non itaque.','Ad ut aliquid perspiciatis est. Saepe ducimus labore corporis facilis. Dolores non doloremque ut qui.\nLaboriosam dignissimos impedit et. In sed ea et aut voluptatem omnis laborum. Nihil iste quos architecto labore.\nMinima commodi sit illo saepe eum ut rem. Accusantium consequuntur sit repellat aliquid impedit.\nVero hic minima aut impedit. Incidunt quisquam iusto sint occaecati molestiae. Tempore sit libero est aperiam eveniet velit.\nQuam est modi officiis delectus illo amet dignissimos. Atque reiciendis eaque et aut. Minima sint est itaque fugiat. Blanditiis asperiores quo labore ut aut necessitatibus reiciendis. Recusandae repellendus voluptatibus aliquid vel vero blanditiis eos.\nEst temporibus eveniet magnam quod aut voluptatem. Consectetur fugit omnis voluptatem sunt. Pariatur aut ab odit animi minus quam.\nSit exercitationem voluptatem quia reprehenderit omnis praesentium. Dolore reprehenderit voluptate asperiores non et. Aut consectetur qui nam.',5,1,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(43,5,'Tenetur fuga beatae mollitia ipsam debitis optio.','tenetur-fuga-beatae-mollitia-ipsam-debitis-optio',NULL,'Fugit dolorem itaque quasi quidem voluptas dolorem quo. Ut maxime necessitatibus odit dolorem. Voluptatem aliquid est earum fugit. Facere et soluta et minus.','Odio quod animi rerum. Dignissimos dignissimos voluptas animi rerum. Labore eaque ratione aut nemo saepe sint.\nCumque expedita quas in qui sapiente quasi officiis. Similique omnis quasi molestiae fuga dolore. Iste et quas ut aspernatur in earum laudantium sunt. Quo eum quos et ab.\nIllum itaque voluptatem eius at sapiente. Cumque earum culpa omnis eos quia. Laborum eos voluptatum fugit et cum aut explicabo. Earum aut velit repudiandae quia a sint dolor.\nDelectus dolores sit quia suscipit et dolores. Animi cumque vel quod placeat nisi hic ipsam. Accusamus explicabo vel rerum impedit error sit rerum consequatur. Natus tempore molestiae minus provident.\nUt molestiae nulla quo aspernatur pariatur. Dignissimos doloribus sed aperiam rem id est. Nesciunt nisi sint eius. Cumque quis quia qui distinctio qui.',6,1,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(44,5,'Sit non occaecati veniam voluptate.','sit-non-occaecati-veniam-voluptate',NULL,'Blanditiis ex veniam iste praesentium aut itaque. Natus qui velit quia dolores. Quasi vitae facere esse quis. Exercitationem explicabo tenetur voluptatem quibusdam temporibus et possimus reprehenderit.','Et laudantium doloribus dolores dolor dolore sed. Rem voluptatum sint ut. Nobis at et error qui. Atque amet vitae blanditiis praesentium excepturi labore sapiente non.\nExpedita unde vitae in et distinctio id. Deleniti incidunt eum architecto pariatur id perferendis. Libero sunt omnis alias cum.\nIure commodi illo et ab. Deleniti ea error natus quia molestiae rerum ut.\nQuia unde quisquam rerum et autem architecto. Qui ipsa voluptas voluptatum. Necessitatibus sapiente repellat similique et dolorem amet. Pariatur incidunt voluptatibus laboriosam maxime ipsam optio.\nEt ratione laborum dolores et maiores architecto. Nam et rerum praesentium reprehenderit cum cupiditate. Sapiente repellat a suscipit dolorem eum tempora.\nTemporibus voluptatem nostrum fugit accusamus quam. Asperiores non placeat praesentium voluptatem. Est laboriosam dolore mollitia tempore. Ipsum est eligendi praesentium non.',4,0,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(45,5,'Ipsam cum blanditiis excepturi occaecati.','ipsam-cum-blanditiis-excepturi-occaecati',NULL,'At qui reiciendis adipisci reiciendis ut dolor est. Nemo enim saepe eum dignissimos. Nam voluptas qui quo quia eum dolor aut. Repellendus delectus tempora quibusdam neque minus officiis eum voluptatem.','Qui dolor eum veritatis fuga ullam quam corporis. Culpa tenetur fugit et et iure repellendus omnis facilis. Quam tenetur quis reiciendis saepe odit illum.\nMagni voluptate minus praesentium explicabo quasi rerum. Voluptatum ea aliquid odio exercitationem. Architecto est omnis ex possimus. Illo quis quo praesentium.\nModi quod error ut voluptatem qui delectus excepturi. Tempora in labore facere sit fugit laborum. Cum aspernatur consequuntur et quia qui iste.\nQuis tenetur quis qui magni consequatur voluptas tenetur quod. Doloremque fuga aspernatur nobis et consectetur consequatur quidem. Velit dolorum qui nostrum corrupti et et. Et ab minima ullam.\nIncidunt alias ut quo suscipit aut voluptate voluptatem. Hic rem et architecto. Incidunt at enim et.\nEt velit ea culpa placeat autem nobis illo. Et aut fuga ad ex modi. Iusto doloribus autem est eligendi.',6,1,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(46,5,'Incidunt tempora magnam dolorem debitis.','incidunt-tempora-magnam-dolorem-debitis',NULL,'Maxime est commodi ratione quia. Fugit ut impedit voluptate repellat. Vel consectetur sapiente saepe soluta perspiciatis magni vitae voluptas.','Et eum numquam aspernatur. Iste error velit alias dolore. Aut eum hic nemo et excepturi iure consequuntur.\nEx asperiores ut ratione minus. Nulla optio incidunt est odit. Facilis officiis sed aspernatur ducimus sit itaque eveniet ex.\nPlaceat vero optio animi in at doloremque quia non. Voluptas sint est est adipisci facilis dolor sit et.\nEt quo ut voluptatum aut velit inventore quo. Ut sint hic velit minus animi porro. Dolor ab voluptas quia unde.\nNihil nobis culpa ipsam voluptatum occaecati. Laborum deleniti quo minima veniam quas. Reprehenderit cum nemo voluptas nam. Quam velit quis quos reiciendis eum.\nSunt necessitatibus ipsum enim aut animi id ex doloribus. Pariatur quas molestias qui optio nam. Et provident voluptate accusantium quas ut.\nNobis doloremque voluptatem est vero. Velit ducimus odio dolorem non provident qui. Iusto quis laudantium quibusdam repellendus quo est magni sed.',9,0,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(47,5,'Placeat voluptate aliquam voluptas non.','placeat-voluptate-aliquam-voluptas-non',NULL,'Autem eius explicabo sint et sit iure natus a. Numquam suscipit ea dolor reiciendis consequatur molestiae omnis ab. Facilis sed voluptatum autem harum occaecati.','Accusamus ipsa vero in at. Similique autem ipsum rerum est ab autem reiciendis. Laudantium pariatur et neque minima voluptatum quis. Voluptas aut cupiditate corrupti soluta.\nEt architecto et quidem nam sit veritatis. Voluptatem labore facere soluta quam tempora ut. Aut neque ducimus eius rem nesciunt animi placeat. Fugit aut officia aut saepe.\nFacere omnis reprehenderit soluta eos fugiat vero error. Culpa alias dolor error fuga vel dolores. Delectus neque aut similique officiis qui soluta.\nQuas non eum officia possimus. Nisi quia eum voluptas sint eius animi. Eos cupiditate eius dolorem dicta facilis provident culpa. Non velit quod saepe qui sint sit.\nPossimus totam quis voluptas. Sint cum quia ex qui ea ullam aut. Aliquid inventore reprehenderit numquam vero. Quia voluptatum architecto aut. Et aperiam perferendis non ut quibusdam numquam.',3,0,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(48,5,'Numquam consequuntur facere est et rem omnis.','numquam-consequuntur-facere-est-et-rem-omnis',NULL,'Error laborum cumque similique nobis. Commodi nisi aperiam est pariatur facilis. Neque earum iusto ut culpa eius.','Ut corporis est sunt non. Sint qui itaque aperiam ut explicabo aut nisi. Modi ut aut animi quam sunt nam.\nVoluptatum ea facere dicta corporis quo ipsum velit. Deserunt explicabo impedit mollitia enim ut. Corrupti sunt vero numquam ex cupiditate quasi ratione. Magnam commodi in harum odit necessitatibus officiis.\nNemo sed reiciendis consectetur ut tempora saepe ea. Sed atque ut et possimus voluptate corporis ab. Quidem est maxime minima consectetur.\nMaiores enim assumenda minus nemo. Autem eos omnis libero voluptate rem aut. Voluptatem dicta provident velit. Nostrum qui ad atque fugit et dignissimos sunt iste.\nDeleniti et eius illo ex. Rem quis a autem tenetur architecto autem.\nMagnam sequi a dolorem quidem. Vel reiciendis quia quo aperiam dolorem. Deleniti modi sint et sed suscipit delectus ea. Totam numquam sit ducimus omnis sint ea commodi ut. Error eum ex tenetur.',2,0,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(49,5,'Tenetur natus consequatur sed autem quo.','tenetur-natus-consequatur-sed-autem-quo',NULL,'Sint nobis rerum cum nisi. Reprehenderit velit non sit laboriosam sunt sunt. Sint veritatis blanditiis enim ad.','Optio ut a autem. Consequatur eum qui et. Qui et est magni. Voluptatem et sapiente dolorem at perspiciatis odit rerum cum.\nSoluta dicta vel nihil cumque eveniet. Tempora tempora neque doloribus numquam. Aliquam vel consequatur harum. Ad qui iste nesciunt.\nSaepe ut delectus incidunt. Repellat magni doloremque libero. Saepe tenetur aut quibusdam dicta labore optio. Et hic aut soluta in id perferendis deleniti.\nAnimi nemo inventore voluptatem alias maxime est ut laboriosam. Ut non eligendi reiciendis sit non et sequi. Vero voluptatem nemo quia et et molestiae officia. Consequuntur rem aut reiciendis.\nQuaerat reprehenderit fugit repudiandae magnam et ut itaque. Ratione quae commodi veniam nam facere maiores delectus laboriosam. Qui eos ab illo eos.\nEum corrupti odio aliquam tempora voluptas. Ipsum ullam voluptas quia et inventore dolore. At qui et magnam est quam quis sint.',1,0,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(50,5,'Necessitatibus harum sint et quam.','necessitatibus-harum-sint-et-quam',NULL,'Assumenda nulla temporibus et. Quaerat reprehenderit facilis minus quos eos natus beatae. Ut et facere facilis vel sunt accusamus voluptas nisi.','Quibusdam sint dolorum molestiae debitis assumenda ea est nisi. Id architecto aut et numquam. Aspernatur veritatis ad est fugiat odio maxime cum quisquam. Quam est nesciunt deserunt in ut odit.\nEst tenetur quia fugiat. Sed voluptatem non placeat consequatur placeat quisquam est nulla.\nUt voluptatem laboriosam quisquam iure autem. Similique tempora sunt sunt eum. Quibusdam molestiae sapiente voluptatem id explicabo sapiente laborum. Voluptates quisquam beatae dolor fugit.\nModi nisi veniam quia fuga sapiente. Suscipit quae odit cum eum eum. Ut dolores et autem qui. Non nulla iusto consectetur sed sed tempore.\nAdipisci soluta hic ut repellat. Alias dicta cumque et sint eum voluptatibus. Esse sed modi eos veritatis at. Quo magni fuga dolor aut rem repellendus exercitationem unde.\nOfficiis consequatur vitae aut cumque illo. Maxime qui omnis cupiditate tempore voluptatum fugit. Ea ratione quod quisquam aut quas illum. Quia non voluptatem aut.',7,1,0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL);

/*!40000 ALTER TABLE `lessons` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table lmsaprils
# ------------------------------------------------------------

DROP TABLE IF EXISTS `lmsaprils`;

CREATE TABLE `lmsaprils` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table media
# ------------------------------------------------------------

DROP TABLE IF EXISTS `media`;

CREATE TABLE `media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model_id` int(10) unsigned DEFAULT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collection_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `disk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) unsigned NOT NULL,
  `manipulations` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_properties` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_column` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table messages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mid` int(11) DEFAULT NULL,
  `sid` int(11) DEFAULT NULL,
  `trid` int(11) DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `mdate` date DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `messages_deleted_at_index` (`deleted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table messenger_messages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `messenger_messages`;

CREATE TABLE `messenger_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `topic_id` int(10) unsigned NOT NULL,
  `sender_id` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sent_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `messenger_messages_topic_id_foreign` (`topic_id`),
  CONSTRAINT `messenger_messages_topic_id_foreign` FOREIGN KEY (`topic_id`) REFERENCES `messenger_topics` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `messenger_messages` WRITE;
/*!40000 ALTER TABLE `messenger_messages` DISABLE KEYS */;

INSERT INTO `messenger_messages` (`id`, `topic_id`, `sender_id`, `content`, `sent_at`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,5,'hi there','2018-03-28 13:44:27','2018-03-28 12:44:27','2018-03-28 12:44:27',NULL),
	(2,2,5,'Hi','2018-03-28 13:44:49','2018-03-28 12:44:49','2018-03-28 12:44:49',NULL),
	(3,3,5,'hello again','2018-03-28 13:45:50','2018-03-28 12:45:50','2018-03-28 12:45:50',NULL),
	(4,4,5,'hello again','2018-03-28 13:46:10','2018-03-28 12:46:10','2018-03-28 12:46:10',NULL),
	(5,5,1,'hello','2018-03-28 13:46:46','2018-03-28 12:46:46','2018-03-28 12:46:46',NULL),
	(6,6,1,'test 2','2018-03-28 13:49:17','2018-03-28 12:49:17','2018-03-28 12:49:17',NULL),
	(7,7,1,'test message','2018-03-28 21:20:07','2018-03-28 20:20:07','2018-03-28 20:20:07',NULL),
	(8,1,1,'test replay','2018-03-28 21:20:20','2018-03-28 20:20:20','2018-03-28 20:20:20',NULL),
	(9,2,4,'Hi 190hr Trainee','2018-03-28 21:21:30','2018-03-28 20:21:30','2018-03-28 20:21:30',NULL),
	(10,2,5,'hello back','2018-03-28 21:22:21','2018-03-28 20:22:21','2018-03-28 20:22:21',NULL),
	(11,8,1,'test message','2018-03-28 22:51:44','2018-03-28 21:51:44','2018-03-28 21:51:44',NULL),
	(12,8,1,'test back to myself','2018-03-28 22:52:04','2018-03-28 21:52:04','2018-03-28 21:52:04',NULL),
	(13,9,1,'new test message','2018-03-28 23:23:14','2018-03-28 22:23:14','2018-03-28 22:23:14',NULL),
	(14,10,1,'another test','2018-03-28 23:24:10','2018-03-28 22:24:10','2018-03-28 22:24:10',NULL),
	(15,11,5,'test email','2018-03-28 23:24:53','2018-03-28 22:24:53','2018-03-28 22:24:53',NULL),
	(16,12,4,'2903 message test','2018-03-29 10:28:44','2018-03-29 09:28:44','2018-03-29 09:28:44',NULL),
	(17,13,1,'2903 message','2018-03-29 10:31:58','2018-03-29 09:31:58','2018-03-29 09:31:58',NULL),
	(18,14,1,'Hello This is a test. \r\n\r\nHappy Easter Sunday','2018-04-01 21:44:11','2018-04-01 20:44:11','2018-04-01 20:44:11',NULL),
	(19,15,7,'Hi I am the new 120 hr trainee','2018-04-04 10:25:44','2018-04-04 09:25:44','2018-04-04 09:25:44',NULL),
	(20,16,1,'test email','2018-04-19 10:55:39','2018-04-19 09:55:39','2018-04-19 09:55:39',NULL),
	(21,17,2,'hello admin test 19042018','2018-04-19 10:56:34','2018-04-19 09:56:34','2018-04-19 09:56:34',NULL),
	(22,18,6,'hello','2018-04-19 11:43:25','2018-04-19 10:43:25','2018-04-19 10:43:25',NULL),
	(23,19,6,'test 1904 noon','2018-04-19 11:43:46','2018-04-19 10:43:46','2018-04-19 10:43:46',NULL),
	(24,20,1,'hi neo 1904','2018-04-19 11:46:17','2018-04-19 10:46:17','2018-04-19 10:46:17',NULL),
	(25,20,1,'reply','2018-04-19 14:12:34','2018-04-19 13:12:34','2018-04-19 13:12:34',NULL),
	(26,1,1,'reply','2018-04-19 14:14:34','2018-04-19 13:14:34','2018-04-19 13:14:34',NULL),
	(27,21,1,'hello neo','2018-04-19 14:29:06','2018-04-19 13:29:06','2018-04-19 13:29:06',NULL),
	(28,21,4,'howdy','2018-04-19 14:30:41','2018-04-19 13:30:41','2018-04-19 13:30:41',NULL),
	(29,18,6,'this is a response','2018-04-19 14:39:53','2018-04-19 13:39:53','2018-04-19 13:39:53',NULL);

/*!40000 ALTER TABLE `messenger_messages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table messenger_topics
# ------------------------------------------------------------

DROP TABLE IF EXISTS `messenger_topics`;

CREATE TABLE `messenger_topics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `sent_at` timestamp NULL DEFAULT NULL,
  `sender_read_at` timestamp NULL DEFAULT NULL,
  `receiver_read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `messenger_topics` WRITE;
/*!40000 ALTER TABLE `messenger_topics` DISABLE KEYS */;

INSERT INTO `messenger_topics` (`id`, `subject`, `sender_id`, `receiver_id`, `sent_at`, `sender_read_at`, `receiver_read_at`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'hi',5,1,'2018-04-19 13:14:34','2018-04-04 09:56:43','2018-04-19 13:20:05','2018-03-28 12:44:27','2018-04-19 13:20:05',NULL),
	(2,'Hello there',5,4,'2018-03-28 20:22:21','2018-04-04 09:48:40','2018-03-28 20:22:24','2018-03-28 12:44:49','2018-04-04 09:48:40',NULL),
	(3,'Hello again',5,0,'2018-03-28 12:45:50','2018-03-28 12:45:50',NULL,'2018-03-28 12:45:50','2018-03-28 12:45:50',NULL),
	(4,'Hello again',5,0,'2018-03-28 12:46:10','2018-03-28 12:46:10',NULL,'2018-03-28 12:46:10','2018-03-28 12:46:10',NULL),
	(5,'hello 190',1,3,'2018-03-28 12:46:46','2018-03-28 12:46:46',NULL,'2018-03-28 12:46:46','2018-03-28 12:46:46',NULL),
	(6,'test one',1,0,'2018-03-28 12:49:17','2018-03-28 12:49:17',NULL,'2018-03-28 12:49:17','2018-03-28 12:49:17',NULL),
	(7,'test',1,3,'2018-03-28 20:20:07','2018-03-28 20:20:07',NULL,'2018-03-28 20:20:07','2018-03-28 20:20:07',NULL),
	(8,'trainee test message',1,1,'2018-03-28 21:52:04','2018-04-19 13:14:40',NULL,'2018-03-28 21:51:44','2018-04-19 13:14:40',NULL),
	(9,'new message test',1,3,'2018-03-28 22:23:14','2018-03-28 22:23:14',NULL,'2018-03-28 22:23:14','2018-03-28 22:23:14',NULL),
	(10,'another test',1,3,'2018-03-28 22:24:10','2018-03-28 22:28:38',NULL,'2018-03-28 22:24:10','2018-03-28 22:28:38',NULL),
	(11,'hi admin',5,0,'2018-03-28 22:24:53','2018-03-28 22:24:53',NULL,'2018-03-28 22:24:53','2018-03-28 22:24:53',NULL),
	(12,'2903 message test',4,0,'2018-03-29 09:28:44','2018-03-29 09:28:44',NULL,'2018-03-29 09:28:44','2018-03-29 09:28:44',NULL),
	(13,'Good Morning email test',1,2,'2018-03-29 09:31:58','2018-03-29 09:31:58','2018-04-19 09:56:58','2018-03-29 09:31:58','2018-04-19 09:56:58',NULL),
	(14,'Hello New Student',1,4,'2018-04-01 20:44:11','2018-04-01 20:44:11','2018-04-19 10:41:27','2018-04-01 20:44:11','2018-04-19 10:41:27',NULL),
	(15,'hello test',7,1,'2018-04-04 09:25:44','2018-04-04 09:41:01','2018-04-19 13:13:03','2018-04-04 09:25:44','2018-04-19 13:13:03',NULL),
	(16,'test',1,0,'2018-04-19 09:55:39','2018-04-19 09:59:19',NULL,'2018-04-19 09:55:39','2018-04-19 09:59:19',NULL),
	(17,'hello admin',2,0,'2018-04-19 09:56:34','2018-04-19 09:56:34',NULL,'2018-04-19 09:56:34','2018-04-19 09:56:34',NULL),
	(18,'hello',6,2,'2018-04-19 13:39:53','2018-04-19 13:40:08','2018-04-19 12:53:44','2018-04-19 10:43:25','2018-04-19 13:40:08',NULL),
	(19,'test 1904 noon',6,0,'2018-04-19 10:43:46','2018-04-19 10:43:46',NULL,'2018-04-19 10:43:46','2018-04-19 10:43:46',NULL),
	(20,'hi neo',1,4,'2018-04-19 13:12:34','2018-04-19 13:12:34','2018-04-19 13:31:01','2018-04-19 10:46:17','2018-04-19 13:31:01',NULL),
	(21,'hello',1,4,'2018-04-19 13:30:41','2018-04-19 13:30:45','2018-04-19 13:31:18','2018-04-19 13:29:06','2018-04-19 13:31:18',NULL);

/*!40000 ALTER TABLE `messenger_topics` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(94,'2014_10_12_100000_create_password_resets_table',1),
	(95,'2017_07_19_082005_create_1500441605_permissions_table',1),
	(96,'2017_07_19_082006_create_1500441606_roles_table',1),
	(97,'2017_07_19_082009_create_1500441609_users_table',1),
	(98,'2017_07_19_082347_create_1500441827_courses_table',1),
	(99,'2017_07_19_082723_create_1500442043_lessons_table',1),
	(100,'2017_07_19_082724_create_media_table',1),
	(101,'2017_07_19_082929_create_1500442169_questions_table',1),
	(102,'2017_07_19_083047_create_1500442247_questions_options_table',1),
	(103,'2017_07_19_083236_create_1500442356_tests_table',1),
	(104,'2017_07_19_120427_create_596eec08307cd_permission_role_table',1),
	(105,'2017_07_19_120430_create_596eec0af366b_role_user_table',1),
	(106,'2017_07_19_120808_create_596eece522a6e_course_user_table',1),
	(107,'2017_07_19_121657_create_596eeef709839_question_test_table',1),
	(108,'2017_08_14_085956_create_course_students_table',1),
	(109,'2017_08_17_051131_create_tests_results_table',1),
	(110,'2017_08_17_051254_create_tests_results_answers_table',1),
	(111,'2017_08_18_054622_create_lesson_student_table',1),
	(112,'2017_08_18_060324_add_rating_to_course_student_table',1),
	(113,'2018_03_15_170944_create_trainee_messages_table',1),
	(114,'2018_03_15_170953_create_trainer_messages_table',1),
	(115,'2018_03_15_171005_create_notifications_table',1),
	(116,'2018_03_15_171016_create_canneds_table',1),
	(117,'2018_03_15_210331_create_1521140611_messages_table',1),
	(118,'2018_03_16_104411_create_120_hours_table',2),
	(119,'2018_03_16_104504_create_one_twenties_table',2),
	(120,'2018_03_16_104515_create_one_nineties_table',2),
	(121,'2018_03_16_113510_create_create_courses_table',2),
	(122,'2018_03_16_132915_create_trainee_lists_table',2),
	(123,'2018_03_16_134821_create_course_upload_videos_table',2),
	(124,'2018_03_16_135106_create_course_upload_texts_table',2),
	(125,'2018_03_19_110245_create_model190hr_unit_one_module_one_section_ones_table',2),
	(126,'2018_03_19_110307_create_model190hr_unit_one_module_one_section_twos_table',2),
	(127,'2018_03_19_110317_create_model190hr_unit_one_module_one_section_threes_table',2),
	(128,'2018_03_19_110323_create_model190hr_unit_one_module_one_section_fours_table',2),
	(129,'2018_03_19_110329_create_model190hr_unit_one_module_one_section_fives_table',2),
	(130,'2018_03_19_110335_create_model190hr_unit_one_module_one_section_sixes_table',2),
	(131,'2018_03_19_110356_create_model190hr_unit_one_module_one_section_sevens_table',2),
	(132,'2018_03_19_110404_create_model190hr_unit_one_module_one_section_eights_table',2),
	(133,'2018_03_19_110412_create_model190hr_unit_one_module_one_section_nines_table',2),
	(134,'2018_03_19_110420_create_model190hr_unit_one_module_one_section_tens_table',2),
	(135,'2018_03_19_110429_create_model190hr_unit_ones_table',2),
	(136,'2018_03_19_110437_create_model190hr_unit_twos_table',2),
	(137,'2018_03_19_110502_create_model190hr_unit_two_module_one_section_ones_table',2),
	(138,'2018_03_19_111534_create_model190hr_unit_two_module_one_section_twos_table',2),
	(139,'2018_03_19_111547_create_model190hr_unit_two_module_one_section_threes_table',2),
	(140,'2018_03_19_111554_create_model190hr_unit_two_module_one_section_fours_table',2),
	(141,'2018_03_19_111600_create_model190hr_unit_two_module_one_section_fives_table',2),
	(142,'2018_03_19_111607_create_model190hr_unit_two_module_one_section_sixes_table',2),
	(143,'2018_03_19_111613_create_model190hr_unit_two_module_one_section_sevens_table',2),
	(144,'2018_03_19_111620_create_model190hr_unit_two_module_one_section_eights_table',2),
	(145,'2018_03_19_111626_create_model190hr_unit_two_module_one_section_nines_table',2),
	(146,'2018_03_19_111633_create_model190hr_unit_two_module_one_section_tens_table',2),
	(147,'2018_03_19_111647_create_model190hr_unit_threes_table',2),
	(148,'2018_03_19_111702_create_model190hr_unit_three_module_one_section_ones_table',2),
	(149,'2018_03_19_111710_create_model190hr_unit_three_module_one_section_twos_table',2),
	(150,'2018_03_19_111716_create_model190hr_unit_three_module_one_section_threes_table',2),
	(151,'2018_03_19_111723_create_model190hr_unit_three_module_one_section_fours_table',2),
	(152,'2018_03_19_111730_create_model190hr_unit_three_module_one_section_fives_table',2),
	(153,'2018_03_19_111735_create_model190hr_unit_three_module_one_section_sixes_table',2),
	(154,'2018_03_19_111740_create_model190hr_unit_three_module_one_section_sevens_table',2),
	(155,'2018_03_19_111747_create_model190hr_unit_three_module_one_section_eights_table',2),
	(156,'2018_03_19_111753_create_model190hr_unit_three_module_one_section_nines_table',2),
	(157,'2018_03_19_111800_create_model190hr_unit_three_module_one_section_tens_table',2),
	(158,'2018_03_19_111813_create_model190hr_unit_fours_table',2),
	(159,'2018_03_19_111826_create_model190hr_unit_four_module_one_section_ones_table',2),
	(160,'2018_03_19_111832_create_model190hr_unit_four_module_one_section_threes_table',2),
	(161,'2018_03_19_111840_create_model190hr_unit_four_module_one_section_twos_table',2),
	(162,'2018_03_19_111901_create_model190hr_unit_four_module_one_section_fours_table',2),
	(163,'2018_03_19_111908_create_model190hr_unit_four_module_one_section_fives_table',2),
	(164,'2018_03_19_111915_create_model190hr_unit_four_module_one_section_sixes_table',2),
	(165,'2018_03_19_111921_create_model190hr_unit_four_module_one_section_sevens_table',2),
	(166,'2018_03_19_111928_create_model190hr_unit_four_module_one_section_eights_table',2),
	(167,'2018_03_19_111935_create_model190hr_unit_four_module_one_section_nines_table',2),
	(168,'2018_03_19_111942_create_model190hr_unit_four_module_one_section_tens_table',2),
	(169,'2018_03_19_112028_create_model120hr_unit_one_module_one_section_ones_table',2),
	(170,'2018_03_19_112038_create_model120hr_unit_ones_table',2),
	(171,'2018_03_19_112046_create_model120hr_unit_one_module_one_sectiontwos_table',2),
	(172,'2018_03_19_112059_create_model120hr_unit_one_module_one_section_threes_table',2),
	(173,'2018_03_19_112106_create_model120hr_unit_one_module_one_section_fours_table',2),
	(174,'2018_03_19_112112_create_model120hr_unit_one_module_one_section_fives_table',2),
	(175,'2018_03_19_112119_create_model120hr_unit_one_module_one_section_sixes_table',2),
	(176,'2018_03_19_112130_create_model120hr_unit_one_module_one_section_sevens_table',2),
	(177,'2018_03_19_112137_create_model120hr_unit_one_module_one_section_eights_table',2),
	(178,'2018_03_19_112143_create_model120hr_unit_one_module_one_section_n_ines_table',2),
	(179,'2018_03_19_112156_create_model120hr_unit_one_module_one_section_tens_table',2),
	(180,'2018_03_19_112209_create_model120hr_unit_twos_table',2),
	(181,'2018_03_19_112224_create_model120hr_unit_two_module_one_section_ones_table',2),
	(182,'2018_03_19_112302_create_model120hr_unit_two_module_one_section_twos_table',2),
	(183,'2018_03_19_112307_create_model120hr_unit_two_module_one_section_threes_table',2),
	(184,'2018_03_19_112313_create_model120hr_unit_two_module_one_section_fours_table',2),
	(185,'2018_03_19_112319_create_model120hr_unit_two_module_one_section_fives_table',2),
	(186,'2018_03_19_112326_create_model120hr_unit_two_module_one_section_sixes_table',2),
	(187,'2018_03_19_112333_create_model120hr_unit_two_module_one_section_sevens_table',2),
	(188,'2018_03_19_112339_create_model120hr_unit_two_module_one_section_eights_table',2),
	(189,'2018_03_19_112346_create_model120hr_unit_two_module_one_section_nines_table',2),
	(190,'2018_03_19_112353_create_model120hr_unit_two_module_one_section_tens_table',2),
	(191,'2018_03_19_112403_create_model120hr_unit_threes_table',2),
	(192,'2018_03_19_112424_create_model120hr_unit_three_module_one_section_ones_table',2),
	(193,'2018_03_19_112431_create_model120hr_unit_three_module_one_section_twos_table',2),
	(194,'2018_03_19_112436_create_model120hr_unit_three_module_one_section_threes_table',2),
	(195,'2018_03_19_112443_create_model120hr_unit_three_module_one_section_fours_table',2),
	(196,'2018_03_19_112449_create_model120hr_unit_three_module_one_section_fives_table',2),
	(197,'2018_03_19_112455_create_model120hr_unit_three_module_one_section_sixes_table',2),
	(198,'2018_03_19_112501_create_model120hr_unit_three_module_one_section_sevens_table',2),
	(199,'2018_03_19_112509_create_model120hr_unit_three_module_one_section_eights_table',2),
	(200,'2018_03_19_112515_create_model120hr_unit_three_module_one_section_nines_table',2),
	(201,'2018_03_19_112521_create_model120hr_unit_three_module_one_section_tens_table',2),
	(202,'2018_03_19_112533_create_model120hr_unit_fours_table',2),
	(203,'2018_03_19_112548_create_model120hr_unit_four_module_one_section_ones_table',2),
	(204,'2018_03_19_112554_create_model120hr_unit_four_module_one_section_twos_table',2),
	(205,'2018_03_19_112559_create_model120hr_unit_four_module_one_section_threes_table',2),
	(206,'2018_03_19_112605_create_model120hr_unit_four_module_one_section_fours_table',2),
	(207,'2018_03_19_112611_create_model120hr_unit_four_module_one_section_fives_table',2),
	(208,'2018_03_19_112617_create_model120hr_unit_four_module_one_section_sixes_table',2),
	(209,'2018_03_19_112648_create_model120hr_unit_four_module_one_section_sevens_table',2),
	(210,'2018_03_19_112655_create_model120hr_unit_four_module_one_section_eights_table',2),
	(211,'2018_03_19_112702_create_model120hr_unit_four_module_one_section_nines_table',2),
	(212,'2018_03_19_112708_create_model120hr_unit_four_module_one_section_tens_table',2),
	(213,'2018_03_20_121537_create_unit_one_controllers_table',3),
	(214,'2018_03_20_121622_create_unit_ones_table',3),
	(215,'2018_03_20_121636_create_unit_twos_table',3),
	(216,'2018_03_20_121645_create_unit_threes_table',3),
	(217,'2018_03_20_121654_create_unit_fours_table',3),
	(218,'2018_03_20_161503_create_onetwentyhourhomepages_table',3),
	(219,'2018_03_20_161517_create_oneninetyhourhomepages_table',3),
	(220,'2014_10_28_175635_create_threads_table',4),
	(221,'2014_10_28_180224_create_participants_table',5),
	(222,'2014_11_03_154831_add_soft_deletes_to_participants_table',5),
	(223,'2014_12_04_124531_add_softdeletes_to_threads_table',5),
	(224,'2018_03_19_111534_create_model190hr_unit_two_module_two_section_ones_table',6),
	(225,'2018_03_19_111710_create_model190hr_unit_three_module_two_section_ones_table',6),
	(226,'2018_03_19_111716_create_model190hr_unit_three_module_three_section_ones_table',6),
	(227,'2018_03_19_111723_create_model190hr_unit_three_module_four_section_ones_table',6),
	(228,'2018_03_19_111730_create_model190hr_unit_three_module_five_section_ones_table',6),
	(229,'2018_03_19_112302_create_model120hr_unit_two_module_two_section_ones_table',6),
	(230,'2018_03_19_112307_create_model120hr_unit_two_module_three_section_ones_table',6),
	(231,'2018_03_19_112431_create_model120hr_unit_three_module_two_section_ones_table',6),
	(232,'2018_03_19_112436_create_model120hr_unit_three_module_three_section_ones_table',6),
	(233,'2018_03_19_112443_create_model120hr_unit_three_module_four_section_ones_table',6),
	(234,'2018_03_19_112449_create_model120hr_unit_three_module_five_section_ones_table',6),
	(235,'2018_03_19_112554_create_model120hr_unit_four_module_two_section_ones_table',6),
	(236,'2018_03_21_114739_create_unit_fives_table',6),
	(237,'2018_03_21_115911_create_model190hr_online_teaching_practices_table',6),
	(238,'2018_03_21_121155_create_trainer_lists_table',6),
	(239,'2018_03_22_134917_create_online_teaching_practice_table',6),
	(240,'2018_03_22_152847_create_model190hr_unit_five_module_one_section_ones_table',6),
	(241,'2018_03_22_153944_create_model190hr_unit_five_module_two_section_ones_table',6),
	(242,'2018_03_19_104144_create_messenger_topics_table',7),
	(243,'2018_03_19_104145_create_messenger_messages_table',7),
	(244,'2018_03_19_103754_create_1521448674_properties_table',8),
	(245,'2017_05_11_074334_create_1494477814_appointments_table',9),
	(246,'2018_04_11_080557_create_120u1m1s1s_table',9),
	(247,'2018_04_11_080638_create_int120u1m1s1s_table',9),
	(248,'2018_04_11_080739_create_int120u2m1s1s_table',9),
	(249,'2018_04_11_080745_create_int120u2m2s1s_table',9),
	(250,'2018_04_11_080751_create_int120u2m3s1s_table',9),
	(251,'2018_04_11_080801_create_int120u3m1s1s_table',9),
	(252,'2018_04_11_080807_create_int120u3m2s1s_table',9),
	(253,'2018_04_11_080812_create_int120u3m3s1s_table',9),
	(254,'2018_04_11_080820_create_int120u3m4s1s_table',9),
	(255,'2018_04_11_080825_create_int120u3m5s1s_table',9),
	(256,'2018_04_11_131809_create_devtests_table',9),
	(257,'2018_04_12_195510_create_test_d_bs_table',9),
	(258,'2018_04_12_195557_create_lmsaprils_table',9),
	(259,'2018_04_12_201603_add_userid_to_int120u1m1s1s',10),
	(260,'2018_04_12_202732_add_question_ids_to_int120u1m1s1s',11);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table model120hr_unit_four_module_one_section_eights
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_four_module_one_section_eights`;

CREATE TABLE `model120hr_unit_four_module_one_section_eights` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_four_module_one_section_fives
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_four_module_one_section_fives`;

CREATE TABLE `model120hr_unit_four_module_one_section_fives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_four_module_one_section_fours
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_four_module_one_section_fours`;

CREATE TABLE `model120hr_unit_four_module_one_section_fours` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_four_module_one_section_nines
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_four_module_one_section_nines`;

CREATE TABLE `model120hr_unit_four_module_one_section_nines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_four_module_one_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_four_module_one_section_ones`;

CREATE TABLE `model120hr_unit_four_module_one_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_four_module_one_section_sevens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_four_module_one_section_sevens`;

CREATE TABLE `model120hr_unit_four_module_one_section_sevens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_four_module_one_section_sixes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_four_module_one_section_sixes`;

CREATE TABLE `model120hr_unit_four_module_one_section_sixes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_four_module_one_section_tens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_four_module_one_section_tens`;

CREATE TABLE `model120hr_unit_four_module_one_section_tens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_four_module_one_section_threes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_four_module_one_section_threes`;

CREATE TABLE `model120hr_unit_four_module_one_section_threes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_four_module_one_section_twos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_four_module_one_section_twos`;

CREATE TABLE `model120hr_unit_four_module_one_section_twos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_four_module_two_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_four_module_two_section_ones`;

CREATE TABLE `model120hr_unit_four_module_two_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_fours
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_fours`;

CREATE TABLE `model120hr_unit_fours` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_one_module_one_section_eights
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_one_module_one_section_eights`;

CREATE TABLE `model120hr_unit_one_module_one_section_eights` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_one_module_one_section_fives
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_one_module_one_section_fives`;

CREATE TABLE `model120hr_unit_one_module_one_section_fives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_one_module_one_section_fours
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_one_module_one_section_fours`;

CREATE TABLE `model120hr_unit_one_module_one_section_fours` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_one_module_one_section_n_ines
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_one_module_one_section_n_ines`;

CREATE TABLE `model120hr_unit_one_module_one_section_n_ines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_one_module_one_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_one_module_one_section_ones`;

CREATE TABLE `model120hr_unit_one_module_one_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_one_module_one_section_sevens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_one_module_one_section_sevens`;

CREATE TABLE `model120hr_unit_one_module_one_section_sevens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_one_module_one_section_sixes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_one_module_one_section_sixes`;

CREATE TABLE `model120hr_unit_one_module_one_section_sixes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_one_module_one_section_tens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_one_module_one_section_tens`;

CREATE TABLE `model120hr_unit_one_module_one_section_tens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_one_module_one_section_threes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_one_module_one_section_threes`;

CREATE TABLE `model120hr_unit_one_module_one_section_threes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_one_module_one_sectiontwos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_one_module_one_sectiontwos`;

CREATE TABLE `model120hr_unit_one_module_one_sectiontwos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_ones`;

CREATE TABLE `model120hr_unit_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_three_module_five_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_three_module_five_section_ones`;

CREATE TABLE `model120hr_unit_three_module_five_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_three_module_four_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_three_module_four_section_ones`;

CREATE TABLE `model120hr_unit_three_module_four_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_three_module_one_section_eights
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_three_module_one_section_eights`;

CREATE TABLE `model120hr_unit_three_module_one_section_eights` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_three_module_one_section_fives
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_three_module_one_section_fives`;

CREATE TABLE `model120hr_unit_three_module_one_section_fives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_three_module_one_section_fours
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_three_module_one_section_fours`;

CREATE TABLE `model120hr_unit_three_module_one_section_fours` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_three_module_one_section_nines
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_three_module_one_section_nines`;

CREATE TABLE `model120hr_unit_three_module_one_section_nines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_three_module_one_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_three_module_one_section_ones`;

CREATE TABLE `model120hr_unit_three_module_one_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_three_module_one_section_sevens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_three_module_one_section_sevens`;

CREATE TABLE `model120hr_unit_three_module_one_section_sevens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_three_module_one_section_sixes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_three_module_one_section_sixes`;

CREATE TABLE `model120hr_unit_three_module_one_section_sixes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_three_module_one_section_tens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_three_module_one_section_tens`;

CREATE TABLE `model120hr_unit_three_module_one_section_tens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_three_module_one_section_threes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_three_module_one_section_threes`;

CREATE TABLE `model120hr_unit_three_module_one_section_threes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_three_module_one_section_twos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_three_module_one_section_twos`;

CREATE TABLE `model120hr_unit_three_module_one_section_twos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_three_module_three_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_three_module_three_section_ones`;

CREATE TABLE `model120hr_unit_three_module_three_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_three_module_two_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_three_module_two_section_ones`;

CREATE TABLE `model120hr_unit_three_module_two_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_threes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_threes`;

CREATE TABLE `model120hr_unit_threes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_two_module_one_section_eights
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_two_module_one_section_eights`;

CREATE TABLE `model120hr_unit_two_module_one_section_eights` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_two_module_one_section_fives
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_two_module_one_section_fives`;

CREATE TABLE `model120hr_unit_two_module_one_section_fives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_two_module_one_section_fours
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_two_module_one_section_fours`;

CREATE TABLE `model120hr_unit_two_module_one_section_fours` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_two_module_one_section_nines
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_two_module_one_section_nines`;

CREATE TABLE `model120hr_unit_two_module_one_section_nines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_two_module_one_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_two_module_one_section_ones`;

CREATE TABLE `model120hr_unit_two_module_one_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_two_module_one_section_sevens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_two_module_one_section_sevens`;

CREATE TABLE `model120hr_unit_two_module_one_section_sevens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_two_module_one_section_sixes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_two_module_one_section_sixes`;

CREATE TABLE `model120hr_unit_two_module_one_section_sixes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_two_module_one_section_tens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_two_module_one_section_tens`;

CREATE TABLE `model120hr_unit_two_module_one_section_tens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_two_module_one_section_threes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_two_module_one_section_threes`;

CREATE TABLE `model120hr_unit_two_module_one_section_threes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_two_module_one_section_twos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_two_module_one_section_twos`;

CREATE TABLE `model120hr_unit_two_module_one_section_twos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_two_module_three_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_two_module_three_section_ones`;

CREATE TABLE `model120hr_unit_two_module_three_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_two_module_two_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_two_module_two_section_ones`;

CREATE TABLE `model120hr_unit_two_module_two_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model120hr_unit_twos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model120hr_unit_twos`;

CREATE TABLE `model120hr_unit_twos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_online_teaching_practices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_online_teaching_practices`;

CREATE TABLE `model190hr_online_teaching_practices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_five_module_one_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_five_module_one_section_ones`;

CREATE TABLE `model190hr_unit_five_module_one_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_five_module_two_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_five_module_two_section_ones`;

CREATE TABLE `model190hr_unit_five_module_two_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_four_module_one_section_eights
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_four_module_one_section_eights`;

CREATE TABLE `model190hr_unit_four_module_one_section_eights` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_four_module_one_section_fives
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_four_module_one_section_fives`;

CREATE TABLE `model190hr_unit_four_module_one_section_fives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_four_module_one_section_fours
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_four_module_one_section_fours`;

CREATE TABLE `model190hr_unit_four_module_one_section_fours` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_four_module_one_section_nines
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_four_module_one_section_nines`;

CREATE TABLE `model190hr_unit_four_module_one_section_nines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_four_module_one_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_four_module_one_section_ones`;

CREATE TABLE `model190hr_unit_four_module_one_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_four_module_one_section_sevens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_four_module_one_section_sevens`;

CREATE TABLE `model190hr_unit_four_module_one_section_sevens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_four_module_one_section_sixes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_four_module_one_section_sixes`;

CREATE TABLE `model190hr_unit_four_module_one_section_sixes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_four_module_one_section_tens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_four_module_one_section_tens`;

CREATE TABLE `model190hr_unit_four_module_one_section_tens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_four_module_one_section_threes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_four_module_one_section_threes`;

CREATE TABLE `model190hr_unit_four_module_one_section_threes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_four_module_one_section_twos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_four_module_one_section_twos`;

CREATE TABLE `model190hr_unit_four_module_one_section_twos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_fours
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_fours`;

CREATE TABLE `model190hr_unit_fours` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_one_module_one_section_eights
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_one_module_one_section_eights`;

CREATE TABLE `model190hr_unit_one_module_one_section_eights` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_one_module_one_section_fives
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_one_module_one_section_fives`;

CREATE TABLE `model190hr_unit_one_module_one_section_fives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_one_module_one_section_fours
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_one_module_one_section_fours`;

CREATE TABLE `model190hr_unit_one_module_one_section_fours` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_one_module_one_section_nines
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_one_module_one_section_nines`;

CREATE TABLE `model190hr_unit_one_module_one_section_nines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_one_module_one_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_one_module_one_section_ones`;

CREATE TABLE `model190hr_unit_one_module_one_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_one_module_one_section_sevens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_one_module_one_section_sevens`;

CREATE TABLE `model190hr_unit_one_module_one_section_sevens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_one_module_one_section_sixes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_one_module_one_section_sixes`;

CREATE TABLE `model190hr_unit_one_module_one_section_sixes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_one_module_one_section_tens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_one_module_one_section_tens`;

CREATE TABLE `model190hr_unit_one_module_one_section_tens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_one_module_one_section_threes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_one_module_one_section_threes`;

CREATE TABLE `model190hr_unit_one_module_one_section_threes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_one_module_one_section_twos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_one_module_one_section_twos`;

CREATE TABLE `model190hr_unit_one_module_one_section_twos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_ones`;

CREATE TABLE `model190hr_unit_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_three_module_five_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_three_module_five_section_ones`;

CREATE TABLE `model190hr_unit_three_module_five_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_three_module_four_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_three_module_four_section_ones`;

CREATE TABLE `model190hr_unit_three_module_four_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_three_module_one_section_eights
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_three_module_one_section_eights`;

CREATE TABLE `model190hr_unit_three_module_one_section_eights` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_three_module_one_section_fives
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_three_module_one_section_fives`;

CREATE TABLE `model190hr_unit_three_module_one_section_fives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_three_module_one_section_fours
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_three_module_one_section_fours`;

CREATE TABLE `model190hr_unit_three_module_one_section_fours` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_three_module_one_section_nines
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_three_module_one_section_nines`;

CREATE TABLE `model190hr_unit_three_module_one_section_nines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_three_module_one_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_three_module_one_section_ones`;

CREATE TABLE `model190hr_unit_three_module_one_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_three_module_one_section_sevens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_three_module_one_section_sevens`;

CREATE TABLE `model190hr_unit_three_module_one_section_sevens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_three_module_one_section_sixes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_three_module_one_section_sixes`;

CREATE TABLE `model190hr_unit_three_module_one_section_sixes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_three_module_one_section_tens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_three_module_one_section_tens`;

CREATE TABLE `model190hr_unit_three_module_one_section_tens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_three_module_one_section_threes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_three_module_one_section_threes`;

CREATE TABLE `model190hr_unit_three_module_one_section_threes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_three_module_one_section_twos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_three_module_one_section_twos`;

CREATE TABLE `model190hr_unit_three_module_one_section_twos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_three_module_three_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_three_module_three_section_ones`;

CREATE TABLE `model190hr_unit_three_module_three_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_three_module_two_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_three_module_two_section_ones`;

CREATE TABLE `model190hr_unit_three_module_two_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_threes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_threes`;

CREATE TABLE `model190hr_unit_threes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_two_module_one_section_eights
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_two_module_one_section_eights`;

CREATE TABLE `model190hr_unit_two_module_one_section_eights` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_two_module_one_section_fives
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_two_module_one_section_fives`;

CREATE TABLE `model190hr_unit_two_module_one_section_fives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_two_module_one_section_fours
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_two_module_one_section_fours`;

CREATE TABLE `model190hr_unit_two_module_one_section_fours` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_two_module_one_section_nines
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_two_module_one_section_nines`;

CREATE TABLE `model190hr_unit_two_module_one_section_nines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_two_module_one_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_two_module_one_section_ones`;

CREATE TABLE `model190hr_unit_two_module_one_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_two_module_one_section_sevens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_two_module_one_section_sevens`;

CREATE TABLE `model190hr_unit_two_module_one_section_sevens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_two_module_one_section_sixes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_two_module_one_section_sixes`;

CREATE TABLE `model190hr_unit_two_module_one_section_sixes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_two_module_one_section_tens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_two_module_one_section_tens`;

CREATE TABLE `model190hr_unit_two_module_one_section_tens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_two_module_one_section_threes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_two_module_one_section_threes`;

CREATE TABLE `model190hr_unit_two_module_one_section_threes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_two_module_one_section_twos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_two_module_one_section_twos`;

CREATE TABLE `model190hr_unit_two_module_one_section_twos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_two_module_two_section_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_two_module_two_section_ones`;

CREATE TABLE `model190hr_unit_two_module_two_section_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table model190hr_unit_twos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model190hr_unit_twos`;

CREATE TABLE `model190hr_unit_twos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table notifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table one_nineties
# ------------------------------------------------------------

DROP TABLE IF EXISTS `one_nineties`;

CREATE TABLE `one_nineties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table one_twenties
# ------------------------------------------------------------

DROP TABLE IF EXISTS `one_twenties`;

CREATE TABLE `one_twenties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table oneninetyhourhomepages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oneninetyhourhomepages`;

CREATE TABLE `oneninetyhourhomepages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table onetwentyhourhomepages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `onetwentyhourhomepages`;

CREATE TABLE `onetwentyhourhomepages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table participants
# ------------------------------------------------------------

DROP TABLE IF EXISTS `participants`;

CREATE TABLE `participants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `thread_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `last_read` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table permission_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permission_role`;

CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned DEFAULT NULL,
  `role_id` int(10) unsigned DEFAULT NULL,
  KEY `fk_p_54415_54416_role_per_596eec08308d0` (`permission_id`),
  KEY `fk_p_54416_54415_permissi_596eec0830986` (`role_id`),
  CONSTRAINT `fk_p_54415_54416_role_per_596eec08308d0` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_p_54416_54415_permissi_596eec0830986` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;

INSERT INTO `permission_role` (`permission_id`, `role_id`)
VALUES
	(1,1),
	(2,1),
	(3,1),
	(4,1),
	(5,1),
	(6,1),
	(7,1),
	(8,1),
	(9,1),
	(10,1),
	(11,1),
	(12,1),
	(13,1),
	(14,1),
	(15,1),
	(16,1),
	(17,1),
	(18,1),
	(19,1),
	(20,1),
	(21,1),
	(22,1),
	(23,1),
	(24,1),
	(25,1),
	(26,1),
	(27,1),
	(28,1),
	(29,1),
	(30,1),
	(31,1),
	(32,1),
	(33,1),
	(34,1),
	(35,1),
	(36,1),
	(37,1),
	(38,1),
	(39,1),
	(40,1),
	(41,1),
	(42,1),
	(43,1),
	(44,1),
	(45,1),
	(1,2),
	(21,2),
	(22,2),
	(23,2),
	(24,2),
	(26,2),
	(27,2),
	(28,2),
	(29,2),
	(31,2),
	(32,2),
	(33,2),
	(34,2),
	(36,2),
	(37,2),
	(38,2),
	(39,2),
	(40,2),
	(41,2),
	(42,2),
	(43,2),
	(44,2),
	(45,2),
	(1,3),
	(21,3),
	(24,3),
	(26,3),
	(29,3),
	(31,3),
	(34,3),
	(36,3),
	(37,3),
	(38,3),
	(39,3),
	(40,3),
	(41,3),
	(44,3),
	(21,4),
	(24,4),
	(26,4),
	(29,4),
	(31,4),
	(34,4),
	(36,4),
	(39,4),
	(21,5),
	(22,5),
	(23,5),
	(24,5),
	(46,1),
	(47,4),
	(48,5),
	(49,1),
	(51,4),
	(50,5),
	(52,6),
	(53,7);

/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;

INSERT INTO `permissions` (`id`, `title`, `created_at`, `updated_at`)
VALUES
	(1,'user_management_access','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(2,'user_management_create','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(3,'user_management_edit','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(4,'user_management_view','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(5,'user_management_delete','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(6,'permission_access','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(7,'permission_create','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(8,'permission_edit','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(9,'permission_view','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(10,'permission_delete','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(11,'role_access','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(12,'role_create','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(13,'role_edit','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(14,'role_view','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(15,'role_delete','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(16,'user_access','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(17,'user_create','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(18,'user_edit','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(19,'user_view','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(20,'user_delete','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(21,'course_access','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(22,'course_create','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(23,'course_edit','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(24,'course_view','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(25,'course_delete','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(26,'lesson_access','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(27,'lesson_create','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(28,'lesson_edit','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(29,'lesson_view','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(30,'lesson_delete','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(31,'question_access','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(32,'question_create','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(33,'question_edit','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(34,'question_view','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(35,'question_delete','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(36,'questions_option_access','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(37,'questions_option_create','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(38,'questions_option_edit','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(39,'questions_option_view','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(40,'questions_option_delete','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(41,'test_access','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(42,'test_create','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(43,'test_edit','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(44,'test_view','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(45,'test_delete','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(46,'admin_menu_title','2018-03-16 01:49:05','2018-03-16 01:49:05'),
	(47,'trainee_menu_title','2018-03-16 01:49:19','2018-03-16 01:49:19'),
	(48,'trainer_menu_title','2018-03-16 01:49:31','2018-03-16 01:49:31'),
	(49,'admin_meta_access','2018-03-19 22:37:06','2018-03-19 22:37:06'),
	(50,'trainer_meta_access','2018-03-19 22:37:17','2018-03-19 22:37:17'),
	(51,'trainee_meta_access','2018-03-19 22:37:25','2018-03-19 22:37:25'),
	(52,'120hr_trainee_meta_access','2018-03-19 22:37:52','2018-03-19 22:37:52'),
	(53,'190hr_trainee_meta_access','2018-03-19 22:37:59','2018-03-19 22:37:59');

/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table properties
# ------------------------------------------------------------

DROP TABLE IF EXISTS `properties`;

CREATE TABLE `properties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `properties_deleted_at_index` (`deleted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table question_test
# ------------------------------------------------------------

DROP TABLE IF EXISTS `question_test`;

CREATE TABLE `question_test` (
  `question_id` int(10) unsigned DEFAULT NULL,
  `test_id` int(10) unsigned DEFAULT NULL,
  KEY `fk_p_54420_54422_test_que_596eeef70992f` (`question_id`),
  KEY `fk_p_54422_54420_question_596eeef7099af` (`test_id`),
  CONSTRAINT `fk_p_54420_54422_test_que_596eeef70992f` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_p_54422_54420_question_596eeef7099af` FOREIGN KEY (`test_id`) REFERENCES `tests` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `question_test` WRITE;
/*!40000 ALTER TABLE `question_test` DISABLE KEYS */;

INSERT INTO `question_test` (`question_id`, `test_id`)
VALUES
	(1,43),
	(2,41),
	(3,35),
	(4,15),
	(5,9),
	(6,36),
	(7,43),
	(8,23),
	(9,10),
	(10,9),
	(11,24),
	(12,49),
	(13,49),
	(14,49),
	(15,42),
	(16,40),
	(17,48),
	(18,14),
	(19,15),
	(20,25),
	(21,31),
	(22,36),
	(23,4),
	(24,38),
	(25,43),
	(26,43),
	(27,17),
	(28,45),
	(29,7),
	(30,14),
	(31,41),
	(32,8),
	(33,5),
	(34,12),
	(35,29),
	(36,23),
	(37,18),
	(38,25),
	(39,12),
	(40,31),
	(41,38),
	(42,36),
	(43,26),
	(44,36),
	(45,39),
	(46,43),
	(47,38),
	(48,36),
	(49,38),
	(50,6);

/*!40000 ALTER TABLE `question_test` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table questions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `questions`;

CREATE TABLE `questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `question_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `questions_deleted_at_index` (`deleted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;

INSERT INTO `questions` (`id`, `question`, `question_image`, `score`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'Cumque officia qui et aspernatur et.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(2,'Ut minus asperiores illo eum enim praesentium.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(3,'Adipisci eaque voluptatem dolor.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(4,'Consequatur repellat placeat atque eaque.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(5,'Ut ut nesciunt doloremque a.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(6,'Quis ab iure in architecto ex.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(7,'Excepturi et voluptas corporis non et.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(8,'Totam vel eos hic quia earum est et.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(9,'Sunt id reiciendis et. Et sapiente laborum enim.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(10,'Nobis voluptas ut amet id ad numquam.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(11,'Et iusto qui vel quae quia.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(12,'Nulla quos deleniti voluptatem temporibus.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(13,'Soluta totam quis quo ipsam aut.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(14,'Voluptate ut et consectetur iste.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(15,'Repudiandae rerum porro enim deserunt.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(16,'Est voluptatem corporis adipisci est.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(17,'Qui sit labore distinctio porro aut nostrum.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(18,'Est nisi velit debitis.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(19,'A illum nulla nulla voluptatem non.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(20,'Ipsam ut ad aliquam alias placeat laudantium.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(21,'Optio corporis dicta unde accusamus vel.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(22,'Nisi sint et quia qui quia.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(23,'Ut eum et vel.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(24,'Autem et quisquam voluptatem possimus.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(25,'Delectus voluptas sint eum ea.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(26,'Deleniti adipisci rerum aut ut vero eveniet.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(27,'Vel suscipit et ut ducimus.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(28,'Animi et placeat aliquam natus.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(29,'Deleniti odio quae beatae.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(30,'At aspernatur et nobis fugit neque et.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(31,'Aut non nesciunt velit tenetur commodi.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(32,'Modi voluptas nobis rerum earum quia.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(33,'Dolorem aut hic quae officiis nam.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(34,'Facere dolorum earum velit ut suscipit.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(35,'Optio et ut occaecati tempora.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(36,'Enim et eveniet ut recusandae.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(37,'Est optio facilis labore delectus nam.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(38,'Sed porro beatae provident aut aliquid.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(39,'Nostrum accusamus aut consequatur consectetur.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(40,'Quas ut ex sit sit.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(41,'Aut alias qui quisquam eaque voluptatem.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(42,'Voluptas ipsum est nesciunt inventore totam est.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(43,'Blanditiis assumenda voluptatem maxime eaque.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(44,'Nam aperiam et nostrum.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(45,'Quam odio nihil odio velit et ipsam.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(46,'Beatae ea quisquam animi iusto ducimus sed.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(47,'Dignissimos minus optio nostrum sit accusantium.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(48,'Consequatur veritatis iusto iste vitae.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(49,'Ipsam sed qui placeat.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(50,'Est voluptatibus laudantium vel magni.?',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL);

/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table questions_options
# ------------------------------------------------------------

DROP TABLE IF EXISTS `questions_options`;

CREATE TABLE `questions_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(10) unsigned DEFAULT NULL,
  `option_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `correct` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `54421_596eee8745a1e` (`question_id`),
  KEY `questions_options_deleted_at_index` (`deleted_at`),
  CONSTRAINT `54421_596eee8745a1e` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `questions_options` WRITE;
/*!40000 ALTER TABLE `questions_options` DISABLE KEYS */;

INSERT INTO `questions_options` (`id`, `question_id`, `option_text`, `correct`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,'Et placeat ut error sed molestias consequuntur.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(2,1,'Nihil deserunt qui ducimus rem deleniti.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(3,1,'Natus consequatur necessitatibus ad.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(4,1,'Vero voluptas nostrum sed aut.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(5,2,'Eos fugit esse voluptates totam cupiditate et.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(6,2,'Laboriosam omnis perspiciatis sapiente.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(7,2,'Harum reiciendis possimus tenetur ipsam.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(8,2,'Numquam ut est aliquam consequatur.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(9,3,'Officia adipisci exercitationem modi odit ullam.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(10,3,'Tempora asperiores et maiores molestias.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(11,3,'Voluptatem in provident quidem optio quia.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(12,3,'Sint sunt aut sit ut voluptas quos totam.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(13,4,'Corporis sapiente pariatur fuga.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(14,4,'Optio dolorem ut maiores eaque explicabo sit.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(15,4,'Deserunt dolor id iste nemo ut eum temporibus.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(16,4,'Assumenda a itaque quis facere saepe.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(17,5,'Qui provident et sed autem cumque.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(18,5,'Culpa mollitia est quam labore qui.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(19,5,'Est qui aut vel aut velit.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(20,5,'Est a optio autem iusto optio animi expedita.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(21,6,'Rerum est accusantium voluptatibus dolor.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(22,6,'Ut omnis voluptates veniam.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(23,6,'Id et facilis optio illo praesentium.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(24,6,'Consequatur sint nam dolor ducimus consectetur.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(25,7,'Blanditiis minus libero et delectus quia laborum.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(26,7,'Voluptatem iste at iste nisi harum culpa quas.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(27,7,'Voluptatem ullam deserunt ut reiciendis.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(28,7,'Tenetur dolore laborum a et.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(29,8,'Ad itaque enim et enim ratione voluptas.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(30,8,'Non praesentium quis dolorum est.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(31,8,'Commodi quia exercitationem id neque officia.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(32,8,'Nihil vel nisi sit sint numquam numquam aperiam.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(33,9,'Laudantium quae eos facere eum.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(34,9,'Laboriosam sit minima dolores et.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(35,9,'Nobis laborum aut ea qui enim sit et quae.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(36,9,'Earum maxime totam eius ratione alias.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(37,10,'Ut ipsum aliquam quam accusantium et.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(38,10,'Qui dolorem et placeat praesentium aut vero.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(39,10,'In doloremque maiores qui magnam sed.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(40,10,'Ea eligendi labore error quam.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(41,11,'Consequatur perferendis quia ut.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(42,11,'Ab quia sit aspernatur nostrum omnis.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(43,11,'Fugiat sed rerum excepturi nam.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(44,11,'Sapiente nam sunt nulla quia itaque.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(45,12,'Rerum labore ad nam eius.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(46,12,'Quo et id qui et eum.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(47,12,'Vel est natus quia ullam cupiditate occaecati.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(48,12,'Esse et distinctio id et autem enim.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(49,13,'Consectetur molestias architecto hic dolorem.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(50,13,'Aut eius delectus vero magnam.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(51,13,'Error rerum non ipsam amet nesciunt.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(52,13,'Saepe ex aut maxime voluptatem vitae.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(53,14,'Ex libero voluptatem et similique est quia et.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(54,14,'Architecto ut sint quo veritatis.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(55,14,'Voluptatem sit aut quo.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(56,14,'Vero et ipsum aut.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(57,15,'Repellendus est est aspernatur odit atque atque.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(58,15,'Voluptatum explicabo consequatur nisi voluptas.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(59,15,'Doloribus distinctio voluptas sed aliquam.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(60,15,'Saepe nihil ut quas.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(61,16,'Minus nemo eos eius quaerat beatae.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(62,16,'Quibusdam eum inventore eos qui.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(63,16,'Earum et magnam id recusandae sint incidunt.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(64,16,'Atque eos vitae et odit modi.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(65,17,'Facilis totam quidem odio.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(66,17,'Commodi illum eum numquam.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(67,17,'Omnis qui necessitatibus ut quae ipsa.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(68,17,'Libero blanditiis voluptas deserunt.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(69,18,'Quasi inventore dolorem hic quam aut.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(70,18,'Esse cupiditate assumenda fuga repellendus.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(71,18,'Ex quod tenetur qui possimus voluptates.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(72,18,'Vero sapiente nihil in autem.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(73,19,'Alias dicta saepe quo ipsa ducimus.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(74,19,'Quia ipsum mollitia aut modi consequuntur.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(75,19,'Nesciunt ab et ex voluptas officiis fuga.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(76,19,'Exercitationem omnis quis voluptatem qui.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(77,20,'Aut dolorem ea aut soluta aliquam.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(78,20,'Vel ea maiores quidem quo hic qui et reiciendis.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(79,20,'Quia sunt dicta facere.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(80,20,'Et esse ullam repudiandae natus nulla ut.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(81,21,'Consectetur consequatur eaque id blanditiis.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(82,21,'Dignissimos sint debitis qui et eius nihil.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(83,21,'Quis sed harum assumenda natus.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(84,21,'Possimus esse delectus aut dolores.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(85,22,'Tempora ut quia error natus voluptatum optio.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(86,22,'Est veniam ut nam nostrum.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(87,22,'Officia ut et voluptas sit optio aut ipsa.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(88,22,'Impedit commodi aspernatur in sunt fugit.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(89,23,'Qui in nostrum quibusdam qui amet nam.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(90,23,'Qui et nostrum eius.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(91,23,'Voluptates enim ea aut animi facilis aut.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(92,23,'Ea aut accusantium magnam aliquid.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(93,24,'Accusamus culpa impedit nobis vero ea omnis.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(94,24,'Dolore commodi minima est ratione rem aut non.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(95,24,'Quae earum eveniet expedita ducimus ex.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(96,24,'Dolore eum facilis est est facere.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(97,25,'Sed est sint maiores qui a omnis blanditiis et.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(98,25,'Delectus iste a porro sequi at.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(99,25,'Et similique vel non hic et voluptate rerum.?',1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(100,25,'Quia doloremque illum cupiditate quo ut.?',0,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(101,26,'Fuga possimus qui ea explicabo earum.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(102,26,'Et et aspernatur voluptas deleniti libero.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(103,26,'Rerum molestiae est esse ad labore sint rerum.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(104,26,'Minus neque voluptas sit dolorem.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(105,27,'Ab et voluptas qui optio est ad qui suscipit.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(106,27,'Quibusdam officia fugit odio dolores rerum est.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(107,27,'Doloribus sint in qui libero corrupti.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(108,27,'Unde aut magni harum tenetur quis.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(109,28,'Qui ut dicta voluptatum ut ea.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(110,28,'Qui et totam quo iure vel.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(111,28,'Hic vero error sit ipsa veritatis voluptatem.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(112,28,'Dolores asperiores vel est sit.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(113,29,'Eos consequuntur ut qui similique maiores.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(114,29,'Eaque ullam laudantium cupiditate quia a ut.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(115,29,'Voluptate rem non porro animi ea incidunt rerum.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(116,29,'Architecto eius sit quia aliquam esse molestiae.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(117,30,'Possimus incidunt veritatis praesentium aut.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(118,30,'Fugit excepturi dolorem sequi totam tempore.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(119,30,'Sint quod natus quisquam eos veniam.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(120,30,'Facere ducimus aut et.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(121,31,'Facilis tempore rerum blanditiis.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(122,31,'Est aut et iure dolorem distinctio omnis et.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(123,31,'Eveniet qui omnis libero voluptatum non eligendi.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(124,31,'In maiores magnam veniam quas qui architecto.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(125,32,'Non hic tempore tenetur eveniet quo.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(126,32,'Deleniti voluptatem similique ducimus.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(127,32,'Laboriosam non culpa aut.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(128,32,'Sequi voluptates eligendi unde magni.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(129,33,'Est consequatur consectetur minus aut harum.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(130,33,'Voluptas neque nesciunt sed inventore non.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(131,33,'Ut quis libero omnis aut et quia.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(132,33,'Tempore ipsam at laudantium voluptates eos autem.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(133,34,'Et odit ad id.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(134,34,'Est sunt recusandae rerum fugiat rem culpa.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(135,34,'Velit voluptatem eum animi aliquam.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(136,34,'Eveniet voluptatem qui iure libero.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(137,35,'Nam quasi est delectus dolor.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(138,35,'Accusantium voluptas dolor corrupti non.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(139,35,'Vel non odio rerum dolores.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(140,35,'Laboriosam magni qui deserunt sit.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(141,36,'Ullam numquam qui quo impedit.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(142,36,'Sed dolore amet sunt aperiam.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(143,36,'Qui voluptatem fugit corporis dolor.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(144,36,'Aperiam et est ea et repudiandae assumenda.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(145,37,'Et fuga at eum quis nostrum sed.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(146,37,'Ab nisi perferendis sapiente occaecati et.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(147,37,'Eum vel minus magni officiis pariatur tempora.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(148,37,'Accusantium qui nostrum eaque accusamus.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(149,38,'Et laboriosam iste ut magni accusantium non.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(150,38,'Qui officiis sequi eaque suscipit rerum.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(151,38,'Ad et qui quia porro.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(152,38,'Enim et ut molestias consequatur consequatur.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(153,39,'Eligendi magnam totam dolorem.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(154,39,'Inventore vero aut aut ipsam.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(155,39,'Qui iste dolores explicabo velit.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(156,39,'Excepturi sit beatae voluptatem voluptatibus.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(157,40,'Id impedit aut autem molestiae.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(158,40,'Magni et id aut voluptatem magni sed.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(159,40,'Aut saepe quod voluptas minima facere.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(160,40,'Autem fuga quod quod quia natus non illum.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(161,41,'Quod recusandae dolores fuga ad.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(162,41,'Amet qui non dolorum.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(163,41,'Et non suscipit ea distinctio.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(164,41,'Qui quia nemo dolorum totam voluptas.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(165,42,'Esse quod possimus quidem qui soluta ut.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(166,42,'Autem nulla id voluptatem itaque necessitatibus.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(167,42,'Doloremque cupiditate vitae sunt nisi.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(168,42,'Dicta dolor eveniet qui sit maiores.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(169,43,'Asperiores earum voluptate recusandae aut.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(170,43,'Accusamus ut dolorem et impedit ut omnis ut.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(171,43,'Consectetur voluptatem dolore expedita a.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(172,43,'Voluptatibus error aut ducimus sed ex.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(173,44,'Et enim atque quae hic placeat saepe.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(174,44,'Optio rerum maxime quaerat.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(175,44,'Velit eius possimus ut unde dignissimos et.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(176,44,'Dolores excepturi aliquid minus et qui.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(177,45,'Ullam id illo quas numquam quis sed dolores.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(178,45,'Vel quia odit est omnis quas dolores ducimus.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(179,45,'Suscipit aut provident unde omnis et laboriosam.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(180,45,'Rem in a deleniti aut enim.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(181,46,'Dicta sit et placeat sit.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(182,46,'Eius magnam et molestiae dignissimos labore.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(183,46,'Et consectetur sunt quo dolores.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(184,46,'Mollitia nisi minima voluptatem quam ea est.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(185,47,'Qui saepe reiciendis voluptatem unde et id.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(186,47,'Dolores quam neque pariatur ex.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(187,47,'Nisi et dicta amet velit totam dolorum provident.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(188,47,'Doloribus quia ut iure.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(189,48,'In sapiente recusandae aliquam aut nam.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(190,48,'Ut sed sed debitis quos ut vel.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(191,48,'Accusamus iure veniam nobis quia nihil aut sunt.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(192,48,'Eos sint quas sed dicta qui optio facere eos.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(193,49,'Aut blanditiis debitis vitae aut.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(194,49,'Et in accusamus esse.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(195,49,'Laudantium ut quia rerum.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(196,49,'Similique porro a illum ut minima et.?',1,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(197,50,'Rerum aut culpa minima labore.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(198,50,'Dolores accusamus quae ex placeat.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(199,50,'Id amet est voluptas ipsum fuga dolor sit.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL),
	(200,50,'Ea quia fugit aut consequuntur illo.?',0,'2018-03-16 00:50:30','2018-03-16 00:50:30',NULL);

/*!40000 ALTER TABLE `questions_options` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table role_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `role_user`;

CREATE TABLE `role_user` (
  `role_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  KEY `fk_p_54416_54417_user_rol_596eec0af3746` (`role_id`),
  KEY `fk_p_54417_54416_role_use_596eec0af37c1` (`user_id`),
  CONSTRAINT `fk_p_54416_54417_user_rol_596eec0af3746` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_p_54417_54416_role_use_596eec0af37c1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;

INSERT INTO `role_user` (`role_id`, `user_id`)
VALUES
	(1,1),
	(5,2),
	(4,3),
	(6,4),
	(7,5),
	(6,6),
	(6,7),
	(7,8),
	(6,9);

/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`)
VALUES
	(1,'Administrator (can create other users)','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(2,'Teacher','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(3,'Student','2018-03-16 00:50:28','2018-03-16 00:50:28'),
	(4,'Trainee','2018-03-16 00:52:47','2018-03-16 00:52:47'),
	(5,'Trainer','2018-03-16 00:53:14','2018-03-16 00:53:14'),
	(6,'120 Hour Trainee','2018-03-19 22:39:21','2018-03-19 22:39:21'),
	(7,'190 Hour Trainee','2018-03-19 22:39:32','2018-03-19 22:40:59');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table test_d_bs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `test_d_bs`;

CREATE TABLE `test_d_bs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table tests
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tests`;

CREATE TABLE `tests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(10) unsigned DEFAULT NULL,
  `lesson_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `published` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `54422_596eeef514d00` (`course_id`),
  KEY `54422_596eeef53411a` (`lesson_id`),
  KEY `tests_deleted_at_index` (`deleted_at`),
  CONSTRAINT `54422_596eeef514d00` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE,
  CONSTRAINT `54422_596eeef53411a` FOREIGN KEY (`lesson_id`) REFERENCES `lessons` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `tests` WRITE;
/*!40000 ALTER TABLE `tests` DISABLE KEYS */;

INSERT INTO `tests` (`id`, `course_id`, `lesson_id`, `title`, `description`, `published`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,NULL,1,'Labore cupiditate nemo harum non et.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(2,NULL,2,'Maxime molestiae voluptatem dolore aliquam.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(3,NULL,3,'Et nulla aliquid ut.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(4,NULL,4,'Laboriosam dignissimos id molestias.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(5,NULL,5,'Et veritatis minima quo aut unde.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(6,NULL,6,'Qui quod et reiciendis voluptatem quos.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(7,NULL,7,'Accusamus nisi numquam ut minima.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(8,NULL,8,'Possimus quam a pariatur eaque aut.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(9,NULL,9,'Molestias modi illo officia nesciunt aut omnis.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(10,NULL,10,'Ab qui similique qui quos voluptas iste.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(11,NULL,11,'Rerum aut quam sed vel et eaque.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(12,NULL,12,'Quos vitae occaecati at aut.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(13,NULL,13,'Dolorum ex quo adipisci esse tempore sit.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(14,NULL,14,'Molestiae laboriosam voluptas illum eaque.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(15,NULL,15,'Inventore neque autem est aperiam vel nulla quae.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(16,NULL,16,'Et perspiciatis cum quia maxime quia.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(17,NULL,17,'Earum quisquam nostrum eveniet id id facere.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(18,NULL,18,'Odio blanditiis sit modi et non id.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(19,NULL,19,'Incidunt et est dolorem optio.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(20,NULL,20,'Id possimus omnis qui qui voluptates.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(21,NULL,21,'Saepe inventore illo modi.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(22,NULL,22,'Odio omnis libero rerum ad.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(23,NULL,23,'Et magnam sit porro eos at.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(24,NULL,24,'Nesciunt ea deleniti est.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(25,NULL,25,'Consequuntur atque dolorem nostrum.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(26,NULL,26,'Harum debitis debitis optio.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(27,NULL,27,'Quas aut pariatur maiores doloribus.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(28,NULL,28,'Omnis eum quas id sint consequatur non.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(29,NULL,29,'Aut alias nihil et exercitationem omnis omnis.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(30,NULL,30,'Sed quis cumque pariatur et repellat.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(31,NULL,31,'Fugiat fuga exercitationem dignissimos omnis.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(32,NULL,32,'Sed sit officia quia dolor.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(33,NULL,33,'Eum non vel eligendi ad.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(34,NULL,34,'Rerum exercitationem atque quia fugit.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(35,NULL,35,'Quae quis voluptas rerum reiciendis et adipisci.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(36,NULL,36,'Consequatur rerum quia et.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(37,NULL,37,'Odio natus impedit modi aspernatur illum.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(38,NULL,38,'Quae inventore nulla autem architecto nihil.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(39,NULL,39,'Qui nisi quidem reiciendis odit itaque quaerat.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(40,NULL,40,'In delectus incidunt cum sunt nihil.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(41,NULL,41,'Similique maxime a aliquid aliquid quia a vitae.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(42,NULL,42,'Maxime ea qui aut velit.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(43,NULL,43,'Id veniam id enim esse et.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(44,NULL,44,'Iusto enim eius asperiores quasi non.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(45,NULL,45,'Et cum minima dicta dolor excepturi.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(46,NULL,46,'Et aut porro fugiat.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(47,NULL,47,'Dolore ipsum eos velit ipsum maxime sunt.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(48,NULL,48,'Dolorum qui iure porro sunt dolorum.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(49,NULL,49,'Tempora laboriosam aperiam non.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL),
	(50,NULL,50,'Impedit qui sunt voluptates.',NULL,1,'2018-03-16 00:50:29','2018-03-16 00:50:29',NULL);

/*!40000 ALTER TABLE `tests` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tests_results
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tests_results`;

CREATE TABLE `tests_results` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `test_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `test_result` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tests_results_test_id_foreign` (`test_id`),
  KEY `tests_results_user_id_foreign` (`user_id`),
  CONSTRAINT `tests_results_test_id_foreign` FOREIGN KEY (`test_id`) REFERENCES `tests` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tests_results_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table tests_results_answers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tests_results_answers`;

CREATE TABLE `tests_results_answers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tests_result_id` int(10) unsigned DEFAULT NULL,
  `question_id` int(10) unsigned DEFAULT NULL,
  `option_id` int(10) unsigned DEFAULT NULL,
  `correct` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tests_results_answers_tests_result_id_foreign` (`tests_result_id`),
  KEY `tests_results_answers_question_id_foreign` (`question_id`),
  KEY `tests_results_answers_option_id_foreign` (`option_id`),
  CONSTRAINT `tests_results_answers_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `questions_options` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tests_results_answers_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tests_results_answers_tests_result_id_foreign` FOREIGN KEY (`tests_result_id`) REFERENCES `tests_results` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table threads
# ------------------------------------------------------------

DROP TABLE IF EXISTS `threads`;

CREATE TABLE `threads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table trainee_lists
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trainee_lists`;

CREATE TABLE `trainee_lists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table trainee_messages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trainee_messages`;

CREATE TABLE `trainee_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table trainer_lists
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trainer_lists`;

CREATE TABLE `trainer_lists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table trainer_messages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trainer_messages`;

CREATE TABLE `trainer_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table unit_fives
# ------------------------------------------------------------

DROP TABLE IF EXISTS `unit_fives`;

CREATE TABLE `unit_fives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table unit_fours
# ------------------------------------------------------------

DROP TABLE IF EXISTS `unit_fours`;

CREATE TABLE `unit_fours` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table unit_one_controllers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `unit_one_controllers`;

CREATE TABLE `unit_one_controllers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table unit_ones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `unit_ones`;

CREATE TABLE `unit_ones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table unit_threes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `unit_threes`;

CREATE TABLE `unit_threes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table unit_twos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `unit_twos`;

CREATE TABLE `unit_twos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `user_id`)
VALUES
	(1,'Admin','admin@admin.com','$2y$10$l4MghrLnKXTRUDlR07XQeesKHRIaAe7WzDf90g751BEf70AwnJ5m.','qM7jem7TmtMZXNZpLrVaNl7hZRVH9NjAegHRP2ufbJ3JJaPwhDHifCF82KAZ','2018-03-16 00:50:28','2018-03-16 00:50:28',0),
	(2,'Jay Trainer','trainer@trainer.com','$2y$10$JzAdrqQTve.BwRQkOZ910eFpWgeKa8w7gZrzq5Yc7CmrUxKgPAwYq','4VGcy8IAk9eeDwcmX7VhVumX0a18Jw8zpMgQDK5e9AeGwR4oceorZLUaUtSK','2018-03-16 00:53:57','2018-03-16 00:53:57',0),
	(3,'Jay Trainee','trainee@trainee.com','$2y$10$0QrSLe8fA8ZPijlP.XycS.g9L74L4t0yJ/ZdMWglihsh2qXi88xty','Qlqj6a65iiK0CNM1Tr1A08fUtibbe1FAnxki4MfoWZtRwSRQHLM5GmwocB5s','2018-03-16 00:54:21','2018-03-16 00:54:21',0),
	(4,'120 Hour Trainee','120hr@120hr.com','$2y$10$IzZWfB2xM/O4oKsDL5cpK.csTVEvgccEe1u2OWQs3l/5OFLnmGozq','odgRyAGUpuc7Q6SsnM7ppltK0LETjnDhSSVO4uWJUpNtikxDd1mW2VRNB1py','2018-03-19 22:40:12','2018-03-19 22:40:12',0),
	(5,'190 Hour Trainee','190hr@190hr.com','$2y$10$jwO6Y/L8U1WY8uo8lmj3SeD5GFNyCLNW2zQv33DCEo0I89U4uijQW','3P7cLperfuRF2ARVih1Lc9qPZpoWhPu9CtsvNif34fjA3kjd7fnZGWylsYF9','2018-03-19 22:41:33','2018-03-19 22:41:33',0),
	(6,'New Student','neo@neo.com','$2y$10$EHsgZOTU0YZd6UnEjGs4Iuq9.L9xWnVocb2UPy1fZShYxYKNVtZAC','JkosWSwdreJK45Ue8IZHbV7tqv73BzkUEz7hr3TlAJfcG3hkmNm3G90A3vXb','2018-03-21 11:23:16','2018-03-21 11:23:16',0),
	(7,'new 120','new120@new120.com','$2y$10$Uv5lNQoIjh6FlzNSoKTGDuiNtEOrdOy0z586.97qrSVJa6NFNl9OK',NULL,'2018-04-04 09:09:28','2018-04-04 09:09:28',0),
	(8,'new 190','new190@new190.com','$2y$10$0H4TdFfcOkrs/EtVpw9.HuSxQTM4slXRlgnYKedGwmdJWxJ.w1l/.',NULL,'2018-04-04 09:09:52','2018-04-04 09:09:52',0),
	(9,'jay120','jay120@jay120.com','$2y$10$nhBfUtb/lDJPK2gAZz5kMOQdnvHrVG48DbUYapBlF68W5Ba2t7ZIm',NULL,'2018-04-16 07:34:33','2018-04-16 07:34:33',0);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
