<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassRoomInfo extends Model
{
    protected $table = 'classroomInfo';

    protected $fillable = [
    	'id',
    	'course_user',
    	'course_student',
    	'class_id',
    	'course_id',
    	'title',
    ];


    public function classroomInfo()
    {
    	return $this->belongsToMany(User::class, 'course_student', 'course_user');
    }


    public function courseStudentLevel()
    {
    	return $this->belongsToMany(User::class, 'course_student_level', 'course_student_status');
    }
}
