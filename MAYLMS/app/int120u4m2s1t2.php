<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class int120u4m2s1t2 extends Model
{
    protected $table = 'int120u4m2s1t2s';
    protected $fillable = [
        'user_id',
    	'u4m2s1q1',
    	'u4m2s1q2',
    	'u4m2s1q3',
    	'u4m2s1q4',
    	'u4m2s1q5',
    	'u4m2s1q6',
    	'u4m2s1q7',
    	'u4m2s1q8',
    	'u4m2s1q9',
    	'u4m2s1q10',
    ];
}
