<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TraineeList extends Model
{
    public function studentdata(){
    	 // $users = DB::table('users')->where('role_name', 'Trainee')->first();
    	 

        return $this->role()->where('role_name', 'Trainee')->first();
    }
}
