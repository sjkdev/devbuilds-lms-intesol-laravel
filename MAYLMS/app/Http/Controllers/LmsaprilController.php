<?php

namespace App\Http\Controllers;

use App\lmsapril;
use Illuminate\Http\Request;

class LmsaprilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\lmsapril  $lmsapril
     * @return \Illuminate\Http\Response
     */
    public function show(lmsapril $lmsapril)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\lmsapril  $lmsapril
     * @return \Illuminate\Http\Response
     */
    public function edit(lmsapril $lmsapril)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\lmsapril  $lmsapril
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, lmsapril $lmsapril)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\lmsapril  $lmsapril
     * @return \Illuminate\Http\Response
     */
    public function destroy(lmsapril $lmsapril)
    {
        //
    }
}
