<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\User;
use App\Notifications;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationsController extends Controller
{
    public function index(){

        $dinstinct = DB::table('users')->distinct()->first();

        $studentinfo = DB::table('users')->where('users.role_name', 'Trainee')->get();

        $notifications = DB::table('notifications')->get();
    	
        return view ('admin.notifications.index', compact('notifications', 'studentinfo', 'column', 'distinct'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.notifications.create', compact('notificationscreate'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $data = request()->except(['_token']);

        DB::table('notifications')->insert($data);

        return redirect()->route('admin.notifications.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TraineeList  $traineeList
     * @return \Illuminate\Http\Response
     */
    public function show(Notifications $notifications)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TraineeList  $traineeList
     * @return \Illuminate\Http\Response
     */
    public function edit(Notifications $notifications)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TraineeList  $traineeList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notifications $notifications)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TraineeList  $traineeList
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notifications $notifications)
    {
        //
    }
}
