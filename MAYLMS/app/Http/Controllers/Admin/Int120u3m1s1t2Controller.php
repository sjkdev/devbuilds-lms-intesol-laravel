<?php

namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use App\int120u3m1s1t2;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Int120u3m1s1t2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.onetwentyhours.unit-three.module-one.section-one.index', compact('U3M1S1'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.onetwentyhours.unit-three.module-one.section-one.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->except(['_token']);
 
        DB::table('int120u3m1s1t2s')->insert($data);

        return redirect()->route('admin.onetwentyhours.unit-three.module-one.section-one.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\int120u3m1s1  $int120u3m1s1
     * @return \Illuminate\Http\Response
     */
    public function show(int120u3m1s1t2 $int120u3m1s1t2)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\int120u3m1s1t2  $int120u3m1s1t2
     * @return \Illuminate\Http\Response
     */
    public function edit(int120u3m1s1t2 $int120u3m1s1t2)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\int120u3m1s1t2  $int120u3m1s1t2
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int120u3m1s1t2 $int120u3m1s1t2)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\int120u3m1s1t2  $int120u3m1s1t2
     * @return \Illuminate\Http\Response
     */
    public function destroy(int120u3m1s1t2 $int120u3m1s1t2)
    {
        //
    }
}
