<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\User;
use App\TraineeList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TraineeListController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // $courses = DB::table('courses')->get();

        $studentinfo = DB::table('users')->where('users.role_name', 'Trainee')->get();

        $studenttestdata = DB::table('users')->where('role_name', 'Trainee')->first();

        // $students = \App\User::whereHas('role', function ($q) { $q->where('role_id', 3); } )->get()->pluck('name', 'id', 'role_name');
        // $trainee = \App\User::whereHas('role', function ($q) { $q->where('role_id', 5); } )->get()->pluck('name', 'id', 'role_name');
        // $trainee120 = \App\User::whereHas('role', function ($q) { $q->where('role_id', 6); } )->get()->pluck('name', 'id', 'role_name');
        // $trainee190 = \App\User::whereHas('role', function ($q) { $q->where('role_id', 7); } )->get()->pluck('name', 'id', 'role_name');
        return view('admin.traineelist.index', compact( 'studentinfo', 'traineelist', 'students', 'trainee', 'traine120', 'trainee190', 'studenttestdata', ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TraineeList  $traineeList
     * @return \Illuminate\Http\Response
     */
    public function show(TraineeList $traineeList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TraineeList  $traineeList
     * @return \Illuminate\Http\Response
     */
    public function edit(TraineeList $traineeList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TraineeList  $traineeList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TraineeList $traineeList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TraineeList  $traineeList
     * @return \Illuminate\Http\Response
     */
    public function destroy(TraineeList $traineeList)
    {
        //
    }

    public function traineeList()
    {
        // $traineelist = Trainee::all();
        $user = Users::all();

        return view('admin.traineelist.index', ['traineeList' => $traineeList]);
    }

    // public function isTrainee()
    // {
    //     return $this->hasRole('Trainee');
    // }

    // public function isTrainee()
    // {

    //     $users->hasRole('Trainee');
    //     foreach ($this->roles()->get() as $role)
    //     {
    //         if ($role->name == 'Trainee')
    //         {
    //             return true;
    //         }
    //     }

    //     return false;
    // }
    // 
    public function traineeUser(){
        $users = User::role('trainee')->get(); 
    }
}
