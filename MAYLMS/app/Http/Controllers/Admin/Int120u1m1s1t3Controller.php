<?php

namespace App\Http\Controllers\Admin;

use Auth;
use DB;
use App\int120u1m1s1t3;
// use Illuminate\Http\Request;
use Request;
use App\Http\Controllers\Controller;

class Int120u1m1s1t3Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.onetwentyhours.unit-one.module-one.section-one.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.onetwentyhours.unit-one.module-one.section-one.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = request()->except(['_token']);
 
        DB::table('int120u1m1s1t3s')->insert($data);

        return redirect()->route('admin.onetwentyhours.unit-one.module-one.section-one.index');

    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\int120u1m1s1  $int120u1m1s1
     * @return \Illuminate\Http\Response
     */
    public function show(int120u1m1s1t3 $int120u1m1s1t3)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\int120u1m1s1t3  $int120u1m1s1t3
     * @return \Illuminate\Http\Response
     */
    public function edit(int120u1m1s1t3 $int120u1m1s1t3)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\int120u1m1s1t3  $int120u1m1s1t3
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int120u1m1s1t3 $int120u1m1s1t3)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\int120u1m1s1t3  $int120u1m1s1t3
     * @return \Illuminate\Http\Response
     */
    public function destroy(int120u1m1s1t3 $int120u1m1s1t3)
    {
        //
    }
}
