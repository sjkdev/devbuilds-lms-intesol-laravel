<?php

namespace App\Http\Controllers\Admin;

use App\Model120hrUnitOneModuleOneSectionOne;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Model120hrUnitOneModuleOneSectionOneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.onetwentyhours.unit-one.module-one.section-one.index');
    }
   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.onetwentyhours.unit-one.module-one.section-one.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Request::all();

        return $input;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model120hrUnitOneModuleOneSectionOne  $model120hrUnitOneModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function show(Model120hrUnitOneModuleOneSectionOne $model120hrUnitOneModuleOneSectionOne)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model120hrUnitOneModuleOneSectionOne  $model120hrUnitOneModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function edit(Model120hrUnitOneModuleOneSectionOne $model120hrUnitOneModuleOneSectionOne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model120hrUnitOneModuleOneSectionOne  $model120hrUnitOneModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model120hrUnitOneModuleOneSectionOne $model120hrUnitOneModuleOneSectionOne)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model120hrUnitOneModuleOneSectionOne  $model120hrUnitOneModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model120hrUnitOneModuleOneSectionOne $model120hrUnitOneModuleOneSectionOne)
    {
        //
    }
}
