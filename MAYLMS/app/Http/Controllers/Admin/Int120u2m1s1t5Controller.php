<?php

namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use App\int120u2m1s1t5;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class Int120u2m1s1t5Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.onetwentyhours.unit-two.module-one.section-one.index', compact ('U2M1S1'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->except(['_token']);
 
        DB::table('int120u2m1s1t5s')->insert($data);

        return redirect()->route('admin.onetwentyhours.unit-two.module-one.section-one.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\int120u2m1s1  $int120u2m1s1
     * @return \Illuminate\Http\Response
     */
    public function show(int120u2m1s1t5 $int120u2m1s1t5)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\int120u2m1s1t5  $int120u2m1s1t5
     * @return \Illuminate\Http\Response
     */
    public function edit(int120u2m1s1t5 $int120u2m1s1t5)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\int120u2m1s1t5  $int120u2m1s1t5
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int120u2m1s1t5 $int120u2m1s1t5)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\int120u2m1s1t5  $int120u2m1s1t5
     * @return \Illuminate\Http\Response
     */
    public function destroy(int120u2m1s1t5 $int120u2m1s1t5)
    {
        //
    }
}
