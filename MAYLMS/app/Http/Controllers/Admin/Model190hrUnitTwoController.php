<?php

namespace App\Http\Controllers;

use App\Model190hrUnitTwo;
use Illuminate\Http\Request;

class Model190hrUnitTwoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model190hrUnitTwo  $model190hrUnitTwo
     * @return \Illuminate\Http\Response
     */
    public function show(Model190hrUnitTwo $model190hrUnitTwo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model190hrUnitTwo  $model190hrUnitTwo
     * @return \Illuminate\Http\Response
     */
    public function edit(Model190hrUnitTwo $model190hrUnitTwo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model190hrUnitTwo  $model190hrUnitTwo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model190hrUnitTwo $model190hrUnitTwo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model190hrUnitTwo  $model190hrUnitTwo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model190hrUnitTwo $model190hrUnitTwo)
    {
        //
    }
}
