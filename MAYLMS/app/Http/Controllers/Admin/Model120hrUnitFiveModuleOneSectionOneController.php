<?php

namespace App\Http\Controllers\Admin;

use App\Model120hrUnitFiveModuleOneSectionOne;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Model120hrUnitFiveModuleOneSectionOneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.onetwentyhours.unit-five.module-one.section-one.index', compact('U5M1S1'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model120hrUnitFiveModuleOneSectionOne  $model120hrUnitFiveModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function show(Model120hrUnitFiveModuleOneSectionOne $model120hrUnitFiveModuleOneSectionOne)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model120hrUnitFiveModuleOneSectionOne  $model120hrUnitFiveModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function edit(Model120hrUnitFiveModuleOneSectionOne $model120hrUnitFiveModuleOneSectionOne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model120hrUnitFiveModuleOneSectionOne  $model120hrUnitFiveModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model120hrUnitFiveModuleOneSectionOne $model120hrUnitFiveModuleOneSectionOne)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model120hrUnitFiveModuleOneSectionOne  $model120hrUnitFiveModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model120hrUnitFiveModuleOneSectionOne $model120hrUnitFiveModuleOneSectionOne)
    {
        //
    }
}
