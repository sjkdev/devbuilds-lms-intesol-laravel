<?php

namespace App\Http\Controllers\Admin;

use App\Model120hrUnitThreeModuleThreeSectionOne;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Model120hrUnitThreeModuleThreeSectionOneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.onetwentyhours.unit-three.module-three.section-one.index', compact('U3M3S1'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model120hrUnitThreeModuleOneSectionThree  $model120hrUnitThreeModuleOneSectionThree
     * @return \Illuminate\Http\Response
     */
    public function show(Model120hrUnitThreeModuleThreeSectionOne $model120hrUnitThreeModuleThreeSectionOne)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model120hrUnitThreeModuleOneSectionThree  $model120hrUnitThreeModuleOneSectionThree
     * @return \Illuminate\Http\Response
     */
    public function edit(Model120hrUnitThreeModuleThreeSectionOne $model120hrUnitThreeModuleThreeSectionOne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model120hrUnitThreeModuleOneSectionThree  $model120hrUnitThreeModuleOneSectionThree
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model120hrUnitThreeModuleThreeSectionOne $model120hrUnitThreeModuleThreeSectionOne)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model120hrUnitThreeModuleOneSectionThree  $model120hrUnitThreeModuleOneSectionThree
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model120hrUnitThreeModuleThreeSectionOne $model120hrUnitThreeModuleThreeSectionOne)
    {
        //
    }
}
