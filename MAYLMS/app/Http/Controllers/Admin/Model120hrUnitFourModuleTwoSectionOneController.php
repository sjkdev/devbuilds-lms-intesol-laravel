<?php

namespace App\Http\Controllers\Admin;

use App\Model120hrUnitFourModuleTwoSectionOne;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Model120hrUnitFourModuleTwoSectionOneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.onetwentyhours.unit-four.module-two.section-one.index',compact('U4M2S1'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model120hrUnitFourModuleOneSectionTwo  $model120hrUnitFourModuleOneSectionTwo
     * @return \Illuminate\Http\Response
     */
    public function show(Model120hrUnitFourModuleTwoSectionOne $model120hrUnitFourModuleTwoSectionOne)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model120hrUnitFourModuleOneSectionTwo  $model120hrUnitFourModuleOneSectionTwo
     * @return \Illuminate\Http\Response
     */
    public function edit(Model120hrUnitFourModuleTwoSectionOne $model120hrUnitFourModuleTwoSectionOne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model120hrUnitFourModuleOneSectionTwo  $model120hrUnitFourModuleOneSectionTwo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model120hrUnitFourModuleTwoSectionOne $model120hrUnitFourModuleTwoSectionOne)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model120hrUnitFourModuleOneSectionTwo  $model120hrUnitFourModuleOneSectionTwo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model120hrUnitFourModuleTwoSectionOne $model120hrUnitFourModuleTwoSectionOne)
    {
        //
    }
}
