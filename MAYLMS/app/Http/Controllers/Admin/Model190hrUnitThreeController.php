<?php

namespace App\Http\Controllers;

use App\Model190hrUnitThree;
use Illuminate\Http\Request;

class Model190hrUnitThreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model190hrUnitThree  $model190hrUnitThree
     * @return \Illuminate\Http\Response
     */
    public function show(Model190hrUnitThree $model190hrUnitThree)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model190hrUnitThree  $model190hrUnitThree
     * @return \Illuminate\Http\Response
     */
    public function edit(Model190hrUnitThree $model190hrUnitThree)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model190hrUnitThree  $model190hrUnitThree
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model190hrUnitThree $model190hrUnitThree)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model190hrUnitThree  $model190hrUnitThree
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model190hrUnitThree $model190hrUnitThree)
    {
        //
    }
}
