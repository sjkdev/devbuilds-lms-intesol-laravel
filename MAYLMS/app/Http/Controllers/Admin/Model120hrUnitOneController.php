<?php

namespace App\Http\Controllers;

use App\Model120hrUnitOne;
use Illuminate\Http\Request;

class Model120hrUnitOneController extends Controller
{

    /**
     * Display a listing of Permission.
     *
     * @return \Illuminate\Http\Response
     */

        public function index()
    {
        //if (! Gate::allows('onetwenty_access')) {
          //  return abort(401);
        //}


        //  else {
        //     $tests = Test::all();
        // }

        return view('admin.twentyhours.index', compact('onetwentyhours'));
    }








    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexbakup()
    {
        // return view ('admin.onetwenyhours.index'), compact('onetwentyhours');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model120hrUnitOne  $model120hrUnitOne
     * @return \Illuminate\Http\Response
     */
    public function show(Model120hrUnitOne $model120hrUnitOne)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model120hrUnitOne  $model120hrUnitOne
     * @return \Illuminate\Http\Response
     */
    public function edit(Model120hrUnitOne $model120hrUnitOne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model120hrUnitOne  $model120hrUnitOne
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model120hrUnitOne $model120hrUnitOne)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model120hrUnitOne  $model120hrUnitOne
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model120hrUnitOne $model120hrUnitOne)
    {
        //
    }
}
