<?php

namespace App\Http\Controllers\Admin;

use App\Model190hrUnitFourModuleOneSectionOne;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Model190hrUnitFourModuleOneSectionOneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.oneninetyhours.unit-four.module-one.section-one.index', compact('U4M1S1_L'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model190hrUnitFourModuleOneSectionOne  $model190hrUnitFourModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function show(Model190hrUnitFourModuleOneSectionOne $model190hrUnitFourModuleOneSectionOne)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model190hrUnitFourModuleOneSectionOne  $model190hrUnitFourModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function edit(Model190hrUnitFourModuleOneSectionOne $model190hrUnitFourModuleOneSectionOne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model190hrUnitFourModuleOneSectionOne  $model190hrUnitFourModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model190hrUnitFourModuleOneSectionOne $model190hrUnitFourModuleOneSectionOne)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model190hrUnitFourModuleOneSectionOne  $model190hrUnitFourModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model190hrUnitFourModuleOneSectionOne $model190hrUnitFourModuleOneSectionOne)
    {
        //
    }
}
