<?php

namespace App\Http\Controllers;

use App\UnitOneController;
use Illuminate\Http\Request;

class UnitOneControllerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UnitOneController  $unitOneController
     * @return \Illuminate\Http\Response
     */
    public function show(UnitOneController $unitOneController)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UnitOneController  $unitOneController
     * @return \Illuminate\Http\Response
     */
    public function edit(UnitOneController $unitOneController)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UnitOneController  $unitOneController
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UnitOneController $unitOneController)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UnitOneController  $unitOneController
     * @return \Illuminate\Http\Response
     */
    public function destroy(UnitOneController $unitOneController)
    {
        //
    }
}
