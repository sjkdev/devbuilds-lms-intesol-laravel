<?php

namespace App\Http\Controllers\Admin;

use App\Model190hrUnitOneModuleOneSectionOne;
use Illuminate\Http\Request;
use App\Http\controllers\Controller;

class Model190hrUnitOneModuleOneSectionOneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.oneninetyhours.unit-one.module-one.section-one.index', compact('U1M1S1_L'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model190hrUnitOneModuleOneSectionOne  $model190hrUnitOneModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function show(Model190hrUnitOneModuleOneSectionOne $model190hrUnitOneModuleOneSectionOne)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model190hrUnitOneModuleOneSectionOne  $model190hrUnitOneModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function edit(Model190hrUnitOneModuleOneSectionOne $model190hrUnitOneModuleOneSectionOne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model190hrUnitOneModuleOneSectionOne  $model190hrUnitOneModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model190hrUnitOneModuleOneSectionOne $model190hrUnitOneModuleOneSectionOne)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model190hrUnitOneModuleOneSectionOne  $model190hrUnitOneModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model190hrUnitOneModuleOneSectionOne $model190hrUnitOneModuleOneSectionOne)
    {
        //
    }
}
