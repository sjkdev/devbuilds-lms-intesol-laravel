<?php

namespace App\Http\Controllers\Admin;

use App\devtest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DevtestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.devtest.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\devtest  $devtest
     * @return \Illuminate\Http\Response
     */
    public function show(devtest $devtest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\devtest  $devtest
     * @return \Illuminate\Http\Response
     */
    public function edit(devtest $devtest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\devtest  $devtest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, devtest $devtest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\devtest  $devtest
     * @return \Illuminate\Http\Response
     */
    public function destroy(devtest $devtest)
    {
        //
    }
}
