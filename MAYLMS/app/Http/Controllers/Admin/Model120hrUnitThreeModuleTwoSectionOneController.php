<?php

namespace App\Http\Controllers\Admin;

use App\Model120hrUnitThreeModuleTwoSectionOne;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Model120hrUnitThreeModuleTwoSectionOneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.onetwentyhours.unit-three.module-two.section-one.index', compact('U3M2S1'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model120hrUnitThreeModuleOneSectionTwo  $model120hrUnitThreeModuleOneSectionTwo
     * @return \Illuminate\Http\Response
     */
    public function show(Model120hrUnitThreeModuleTwoSectionOne $model120hrUnitThreeModuleTwoSectionOne)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model120hrUnitThreeModuleOneSectionTwo  $model120hrUnitThreeModuleOneSectionTwo
     * @return \Illuminate\Http\Response
     */
    public function edit(Model120hrUnitThreeModuleTwoSectionOne $model120hrUnitThreeModuleTwoSectionOne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model120hrUnitThreeModuleOneSectionTwo  $model120hrUnitThreeModuleOneSectionTwo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model120hrUnitThreeModuleTwoSectionOne $model120hrUnitThreeModuleTwoSectionOne)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model120hrUnitThreeModuleOneSectionTwo  $model120hrUnitThreeModuleOneSectionTwo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model120hrUnitThreeModuleTwoSectionOne $model120hrUnitThreeModuleTwoSectionOne)
    {
        //
    }
}
