<?php

namespace App\Http\Controllers;

use App\OneNinety;
use Illuminate\Http\Request;

class OneNinetyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return view('admin.190hours.index'), compact('oneninety');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OneNinety  $oneNinety
     * @return \Illuminate\Http\Response
     */
    public function show(OneNinety $oneNinety)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OneNinety  $oneNinety
     * @return \Illuminate\Http\Response
     */
    public function edit(OneNinety $oneNinety)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OneNinety  $oneNinety
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OneNinety $oneNinety)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OneNinety  $oneNinety
     * @return \Illuminate\Http\Response
     */
    public function destroy(OneNinety $oneNinety)
    {
        //
    }
}
