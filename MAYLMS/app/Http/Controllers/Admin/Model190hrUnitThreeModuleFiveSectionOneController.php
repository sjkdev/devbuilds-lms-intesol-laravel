<?php

namespace App\Http\Controllers\Admin;

use App\Model190hrUnitThreeModuleFiveSectionOne;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Model190hrUnitThreeModuleFiveSectionOneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.oneninetyhours.unit-three.module-five.section-one.index', compact('U3M5S1_L'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model190hrUnitThreeModuleOneSectionFive  $model190hrUnitThreeModuleOneSectionFive
     * @return \Illuminate\Http\Response
     */
    public function show(Model190hrUnitThreeModuleFiveSectionOne $model190hrUnitThreeModuleFiveSectionOne)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model190hrUnitThreeModuleOneSectionFive  $model190hrUnitThreeModuleOneSectionFive
     * @return \Illuminate\Http\Response
     */
    public function edit(Model190hrUnitThreeModuleFiveSectionOne $model190hrUnitThreeModuleFiveSectionOne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model190hrUnitThreeModuleOneSectionFive  $model190hrUnitThreeModuleOneSectionFive
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model190hrUnitThreeModuleFiveSectionOne $model190hrUnitThreeModuleFiveSectionOne)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model190hrUnitThreeModuleOneSectionFive  $model190hrUnitThreeModuleOneSectionFive
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model190hrUnitThreeModuleFiveSectionOne $model190hrUnitThreeModuleFiveSectionOne)
    {
        //
    }
}
