<?php

namespace App\Http\Controllers\Admin;

use App\Model190hrUnitThreeModuleTwoSectionOne;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Model190hrUnitThreeModuleTwoSectionOneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.oneninetyhours.unit-three.module-two.section-one.index', compact('U3M2S1_L'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model190hrUnitThreeModuleOneSectionTwo  $model190hrUnitThreeModuleOneSectionTwo
     * @return \Illuminate\Http\Response
     */
    public function show(Model190hrUnitThreeModuleTwoSectionOne $model190hrUnitThreeModuleTwoSectionOne)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model190hrUnitThreeModuleOneSectionTwo  $model190hrUnitThreeModuleOneSectionTwo
     * @return \Illuminate\Http\Response
     */
    public function edit(Model190hrUnitThreeModuleTwoSectionOne $model190hrUnitThreeModuleTwoSectionOne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model190hrUnitThreeModuleOneSectionTwo  $model190hrUnitThreeModuleOneSectionTwo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model190hrUnitThreeModuleTwoSectionOne $model190hrUnitThreeModuleTwoSectionOne)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model190hrUnitThreeModuleOneSectionTwo  $model190hrUnitThreeModuleOneSectionTwo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model190hrUnitThreeModuleTwoSectionOne $model190hrUnitThreeModuleTwoSectionOne)
    {
        //
    }
}
