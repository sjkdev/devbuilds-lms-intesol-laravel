<?php

namespace App\Http\Controllers;

use App\Model120hrUnitTwo;
use Illuminate\Http\Request;

class Model120hrUnitTwoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model120hrUnitTwo  $model120hrUnitTwo
     * @return \Illuminate\Http\Response
     */
    public function show(Model120hrUnitTwo $model120hrUnitTwo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model120hrUnitTwo  $model120hrUnitTwo
     * @return \Illuminate\Http\Response
     */
    public function edit(Model120hrUnitTwo $model120hrUnitTwo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model120hrUnitTwo  $model120hrUnitTwo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model120hrUnitTwo $model120hrUnitTwo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model120hrUnitTwo  $model120hrUnitTwo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model120hrUnitTwo $model120hrUnitTwo)
    {
        //
    }
}
