<?php

namespace App\Http\Controllers\Admin;

use Course;
use User;
use Auth;
use DB;
use App\trainerHome;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TrainerHomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // $user = new \App\User;

        $users = DB::table('users')->get();

        $trainees = DB::table('course_student')->get();

        $classroomInfo = DB::table('classroomInfo')->get();
        // dd($trainees);

        return view ('admin.trainerhome.index', compact('trainerhome', 'users', 'trainees', 'classroomInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->except(['_token']);

        DB::table('trainerhome')->insert($data);

        return redirect()->route('admin.trainerhome.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\trainerHome  $trainerHome
     * @return \Illuminate\Http\Response
     */
    public function show(trainerHome $trainerHome)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\trainerHome  $trainerHome
     * @return \Illuminate\Http\Response
     */
    public function edit(trainerHome $trainerHome)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\trainerHome  $trainerHome
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, trainerHome $trainerHome)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\trainerHome  $trainerHome
     * @return \Illuminate\Http\Response
     */
    public function destroy(trainerHome $trainerHome)
    {
        //
    }
}
