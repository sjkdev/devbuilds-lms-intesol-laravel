<?php

namespace App\Http\Controllers\Admin;

use App\Model120hrUnitFourModuleOneSectionOne;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Model120hrUnitFourModuleOneSectionOneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.onetwentyhours.unit-four.module-one.section-one.index', compact('U4M1S1'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model120hrUnitFourModuleOneSectionOne  $model120hrUnitFourModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function show(Model120hrUnitFourModuleOneSectionOne $model120hrUnitFourModuleOneSectionOne)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model120hrUnitFourModuleOneSectionOne  $model120hrUnitFourModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function edit(Model120hrUnitFourModuleOneSectionOne $model120hrUnitFourModuleOneSectionOne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model120hrUnitFourModuleOneSectionOne  $model120hrUnitFourModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model120hrUnitFourModuleOneSectionOne $model120hrUnitFourModuleOneSectionOne)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model120hrUnitFourModuleOneSectionOne  $model120hrUnitFourModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model120hrUnitFourModuleOneSectionOne $model120hrUnitFourModuleOneSectionOne)
    {
        //
    }
}
