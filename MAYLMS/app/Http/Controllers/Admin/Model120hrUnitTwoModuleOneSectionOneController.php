<?php

namespace App\Http\Controllers\Admin;

use App\Model120hrUnitTwoModuleOneSectionOne;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Model120hrUnitTwoModuleOneSectionOneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.onetwentyhours.unit-two.module-one.section-one.index', compact ('U2M1S1'));
    }

    public function indexTwo()
    {
        return view ('admin.onetwentyhours.unit-two.module-one.section-two.index', compact ('U2M1S2'));
    }

   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model120hrUnitTwoModuleOneSectionOne  $model120hrUnitTwoModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function show(Model120hrUnitTwoModuleOneSectionOne $model120hrUnitTwoModuleOneSectionOne)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model120hrUnitTwoModuleOneSectionOne  $model120hrUnitTwoModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function edit(Model120hrUnitTwoModuleOneSectionOne $model120hrUnitTwoModuleOneSectionOne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model120hrUnitTwoModuleOneSectionOne  $model120hrUnitTwoModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model120hrUnitTwoModuleOneSectionOne $model120hrUnitTwoModuleOneSectionOne)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model120hrUnitTwoModuleOneSectionOne  $model120hrUnitTwoModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model120hrUnitTwoModuleOneSectionOne $model120hrUnitTwoModuleOneSectionOne)
    {
        //
    }
}
