<?php

namespace App\Http\Controllers\Admin;

use App\Model190hrUnitThreeModuleThreeSectionOne;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Model190hrUnitThreeModuleThreeSectionOneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.oneninetyhours.unit-three.module-three.section-one.index', compact('U3M3S1_L'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model190hrUnitThreeModuleOneSectionThree  $model190hrUnitThreeModuleOneSectionThree
     * @return \Illuminate\Http\Response
     */
    public function show(Model190hrUnitThreeModuleThreeSectionOne $model190hrUnitThreeModuleThreeSectionOne)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model190hrUnitThreeModuleOneSectionThree  $model190hrUnitThreeModuleOneSectionThree
     * @return \Illuminate\Http\Response
     */
    public function edit(Model190hrUnitThreeModuleThreeSectionOne $model190hrUnitThreeModuleThreeSectionOne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model190hrUnitThreeModuleOneSectionThree  $model190hrUnitThreeModuleOneSectionThree
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model190hrUnitThreeModuleThreeSectionOne $model190hrUnitThreeModuleThreeSectionOne)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model190hrUnitThreeModuleOneSectionThree  $model190hrUnitThreeModuleOneSectionThree
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model190hrUnitThreeModuleThreeSectionOne $model190hrUnitThreeModuleThreeSectionOne)
    {
        //
    }
}
