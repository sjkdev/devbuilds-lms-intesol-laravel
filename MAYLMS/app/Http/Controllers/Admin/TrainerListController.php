<?php

namespace App\Http\Controllers\Admin;

use App\TrainerList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TrainerListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.trainerlist.index', compact('trainerlist'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.trainerlist.create', compact('trainerlist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TrainerList  $trainerList
     * @return \Illuminate\Http\Response
     */
    public function show(TrainerList $trainerList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TrainerList  $trainerList
     * @return \Illuminate\Http\Response
     */
    public function edit(TrainerList $trainerList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TrainerList  $trainerList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TrainerList $trainerList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TrainerList  $trainerList
     * @return \Illuminate\Http\Response
     */
    public function destroy(TrainerList $trainerList)
    {
        //
    }
}
