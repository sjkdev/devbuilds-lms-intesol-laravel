<?php

namespace App\Http\Controllers;

use App\Model190hrUnitOne;
use Illuminate\Http\Request;

class Model190hrUnitOneController extends Controller
{
    /**
     * Display a listing of Permission.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('hello 120 hour Course page!');
        // if (! Gate::allows('permission_access')) {
        //     return abort(401);
        // }


        //         $permissions = Permission::all();

        // return view('admin.permissions.index', compact('permissions'));  

    }  



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexbakup()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model190hrUnitOne  $model190hrUnitOne
     * @return \Illuminate\Http\Response
     */
    public function show(Model190hrUnitOne $model190hrUnitOne)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model190hrUnitOne  $model190hrUnitOne
     * @return \Illuminate\Http\Response
     */
    public function edit(Model190hrUnitOne $model190hrUnitOne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model190hrUnitOne  $model190hrUnitOne
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model190hrUnitOne $model190hrUnitOne)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model190hrUnitOne  $model190hrUnitOne
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model190hrUnitOne $model190hrUnitOne)
    {
        //
    }
}
