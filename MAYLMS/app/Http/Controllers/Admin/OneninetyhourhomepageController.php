<?php

namespace App\Http\Controllers\Admin;

use App\oneninetyhourhomepage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OneninetyhourhomepageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.oneninetyhours.index', compact('oneninetyhours'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\oneninetyhourhomepage  $oneninetyhourhomepage
     * @return \Illuminate\Http\Response
     */
    public function show(oneninetyhourhomepage $oneninetyhourhomepage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\oneninetyhourhomepage  $oneninetyhourhomepage
     * @return \Illuminate\Http\Response
     */
    public function edit(oneninetyhourhomepage $oneninetyhourhomepage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\oneninetyhourhomepage  $oneninetyhourhomepage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, oneninetyhourhomepage $oneninetyhourhomepage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\oneninetyhourhomepage  $oneninetyhourhomepage
     * @return \Illuminate\Http\Response
     */
    public function destroy(oneninetyhourhomepage $oneninetyhourhomepage)
    {
        //
    }
}
