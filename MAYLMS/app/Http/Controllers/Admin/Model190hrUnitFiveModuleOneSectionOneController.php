<?php

namespace App\Http\Controllers\Admin;

use App\Model190hrUnitFiveModuleOneSectionOne;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Model190hrUnitFiveModuleOneSectionOneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.oneninetyhours.unit-five.module-one.section-one.index', compact('U5M1S1_L'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model190hrUnitFiveModuleOneSectionOne  $model190hrUnitFiveModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function show(Model190hrUnitFiveModuleOneSectionOne $model190hrUnitFiveModuleOneSectionOne)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model190hrUnitFiveModuleOneSectionOne  $model190hrUnitFiveModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function edit(Model190hrUnitFiveModuleOneSectionOne $model190hrUnitFiveModuleOneSectionOne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model190hrUnitFiveModuleOneSectionOne  $model190hrUnitFiveModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model190hrUnitFiveModuleOneSectionOne $model190hrUnitFiveModuleOneSectionOne)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model190hrUnitFiveModuleOneSectionOne  $model190hrUnitFiveModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model190hrUnitFiveModuleOneSectionOne $model190hrUnitFiveModuleOneSectionOne)
    {
        //
    }
}
