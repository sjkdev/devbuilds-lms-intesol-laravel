<?php

namespace App\Http\Controllers;

use App\Model120hrUnitFour;
use Illuminate\Http\Request;

class Model120hrUnitFourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model120hrUnitFour  $model120hrUnitFour
     * @return \Illuminate\Http\Response
     */
    public function show(Model120hrUnitFour $model120hrUnitFour)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model120hrUnitFour  $model120hrUnitFour
     * @return \Illuminate\Http\Response
     */
    public function edit(Model120hrUnitFour $model120hrUnitFour)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model120hrUnitFour  $model120hrUnitFour
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model120hrUnitFour $model120hrUnitFour)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model120hrUnitFour  $model120hrUnitFour
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model120hrUnitFour $model120hrUnitFour)
    {
        //
    }
}
