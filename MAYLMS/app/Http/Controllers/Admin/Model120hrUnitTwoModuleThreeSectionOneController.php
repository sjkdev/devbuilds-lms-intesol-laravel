<?php

namespace App\Http\Controllers\Admin;

use App\Model120hrUnitTwoModuleThreeSectionOne;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Model120hrUnitTwoModuleThreeSectionOneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.onetwentyhours.unit-two.module-three.section-one.index', compact ('U2M3S1'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model120hrUnitTwoModuleOneSectionThree  $model120hrUnitTwoModuleOneSectionThree
     * @return \Illuminate\Http\Response
     */
    public function show(Model120hrUnitTwoModuleThreeSectionOne $model120hrUnitTwoModuleThreeSectionOne)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model120hrUnitTwoModuleOneSectionThree  $model120hrUnitTwoModuleOneSectionThree
     * @return \Illuminate\Http\Response
     */
    public function edit(Model120hrUnitTwoModuleThreeSectionOne $model120hrUnitTwoModuleThreeSectionOne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model120hrUnitTwoModuleOneSectionThree  $model120hrUnitTwoModuleOneSectionThree
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model120hrUnitTwoModuleThreeSectionOne $model120hrUnitTwoModuleThreeSectionOne)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model120hrUnitTwoModuleOneSectionThree  $model120hrUnitTwoModuleOneSectionThree
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model120hrUnitTwoModuleThreeSectionOne $model120hrUnitTwoModuleThreeSectionOne)
    {
        //
    }
}
