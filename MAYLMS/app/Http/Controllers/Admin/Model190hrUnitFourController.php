<?php

namespace App\Http\Controllers\Admin;

use App\Model190hrUnitFour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Model190hrUnitFourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model190hrUnitFour  $model190hrUnitFour
     * @return \Illuminate\Http\Response
     */
    public function show(Model190hrUnitFour $model190hrUnitFour)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model190hrUnitFour  $model190hrUnitFour
     * @return \Illuminate\Http\Response
     */
    public function edit(Model190hrUnitFour $model190hrUnitFour)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model190hrUnitFour  $model190hrUnitFour
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model190hrUnitFour $model190hrUnitFour)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model190hrUnitFour  $model190hrUnitFour
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model190hrUnitFour $model190hrUnitFour)
    {
        //
    }
}
