<?php

namespace App\Http\Controllers\Admin;

use App\Model190hrOnlineTeachingPractice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Model190hrOnlineTeachingPracticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.oneninetyhours.onlineteachingpractice.index', compact('onlineteachingpractice'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model190hrOnlineTeachingPractice  $model190hrOnlineTeachingPractice
     * @return \Illuminate\Http\Response
     */
    public function show(Model190hrOnlineTeachingPractice $model190hrOnlineTeachingPractice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model190hrOnlineTeachingPractice  $model190hrOnlineTeachingPractice
     * @return \Illuminate\Http\Response
     */
    public function edit(Model190hrOnlineTeachingPractice $model190hrOnlineTeachingPractice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model190hrOnlineTeachingPractice  $model190hrOnlineTeachingPractice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model190hrOnlineTeachingPractice $model190hrOnlineTeachingPractice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model190hrOnlineTeachingPractice  $model190hrOnlineTeachingPractice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model190hrOnlineTeachingPractice $model190hrOnlineTeachingPractice)
    {
        //
    }
}
