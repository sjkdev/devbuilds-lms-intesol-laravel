<?php

namespace App\Http\Controllers\Admin;

use App\Model190hrUnitTwoModuleThreeSectionOne;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Model190hrUnitTwoModuleThreeSectionOneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.oneninetyhours.unit-two.module-three.section-one.index', compact('U2M3S1_L'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model190hrUnitTwoModuleOneSectionThree  $model190hrUnitTwoModuleOneSectionThree
     * @return \Illuminate\Http\Response
     */
    public function show(Model190hrUnitTwoModuleThreeSectionOne $model190hrUnitTwoModuleThreeSectionOne)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model190hrUnitTwoModuleOneSectionThree  $model190hrUnitTwoModuleOneSectionThree
     * @return \Illuminate\Http\Response
     */
    public function edit(Model190hrUnitTwoModuleThreeSectionOne $model190hrUnitTwoModuleThreeSectionOne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model190hrUnitTwoModuleOneSectionThree  $model190hrUnitTwoModuleOneSectionThree
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model190hrUnitTwoModuleThreeSectionOne $model190hrUnitTwoModuleThreeSectionOne)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model190hrUnitTwoModuleOneSectionThree  $model190hrUnitTwoModuleOneSectionThree
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model190hrUnitTwoModuleThreeSectionOne $model190hrUnitTwoModuleThreeSectionOne)
    {
        //
    }
}
