<?php

namespace App\Http\Controllers;

use App\CourseUploadText;
use Illuminate\Http\Request;

class CourseUploadTextController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CourseUploadText  $courseUploadText
     * @return \Illuminate\Http\Response
     */
    public function show(CourseUploadText $courseUploadText)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CourseUploadText  $courseUploadText
     * @return \Illuminate\Http\Response
     */
    public function edit(CourseUploadText $courseUploadText)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CourseUploadText  $courseUploadText
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CourseUploadText $courseUploadText)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CourseUploadText  $courseUploadText
     * @return \Illuminate\Http\Response
     */
    public function destroy(CourseUploadText $courseUploadText)
    {
        //
    }
}
