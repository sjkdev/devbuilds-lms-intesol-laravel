<?php

namespace App\Http\Controllers\Admin;

use App\Model190hrUnitFiveModuleTwoSectionOne;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Model190hrUnitFiveModuleTwoSectionOneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.oneninetyhours.unit-five.module-one.section-one.index', compact('U5M2S1_L'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model190hrUnitFiveModuleTwoSectionOne  $model190hrUnitFiveModuleTwoSectionOne
     * @return \Illuminate\Http\Response
     */
    public function show(Model190hrUnitFiveModuleTwoSectionOne $model190hrUnitFiveModuleTwoSectionOne)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model190hrUnitFiveModuleTwoSectionOne  $model190hrUnitFiveModuleTwoSectionOne
     * @return \Illuminate\Http\Response
     */
    public function edit(Model190hrUnitFiveModuleTwoSectionOne $model190hrUnitFiveModuleTwoSectionOne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model190hrUnitFiveModuleTwoSectionOne  $model190hrUnitFiveModuleTwoSectionOne
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model190hrUnitFiveModuleTwoSectionOne $model190hrUnitFiveModuleTwoSectionOne)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model190hrUnitFiveModuleTwoSectionOne  $model190hrUnitFiveModuleTwoSectionOne
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model190hrUnitFiveModuleTwoSectionOne $model190hrUnitFiveModuleTwoSectionOne)
    {
        //
    }
}
