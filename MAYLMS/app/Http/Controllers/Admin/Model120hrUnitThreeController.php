<?php

namespace App\Http\Controllers\Admin;

use App\Model120hrUnitThree;
use Illuminate\Http\Request;

class Model120hrUnitThreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model120hrUnitThree  $model120hrUnitThree
     * @return \Illuminate\Http\Response
     */
    public function show(Model120hrUnitThree $model120hrUnitThree)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model120hrUnitThree  $model120hrUnitThree
     * @return \Illuminate\Http\Response
     */
    public function edit(Model120hrUnitThree $model120hrUnitThree)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model120hrUnitThree  $model120hrUnitThree
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model120hrUnitThree $model120hrUnitThree)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model120hrUnitThree  $model120hrUnitThree
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model120hrUnitThree $model120hrUnitThree)
    {
        //
    }
}
