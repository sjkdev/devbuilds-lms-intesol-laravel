<?php

namespace App\Http\Controllers\Admin;
// namespace App\Http\Controllers\Admin\Form;
// namespace App\Http\Controllers\Admin\Model120hrUnitThreeModuleOneSectionOneController;


use App\Model120hrUnitThreeModuleOneSectionOne;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use App\Http\Controllers\Admin\Model120hrUnitThreeModuleOneSectionOneController;

class Model120hrUnitThreeModuleOneSectionOneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.onetwentyhours.unit-three.module-one.section-one.index', compact('U3M1S1'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $questions = new Question;
        $questions->question = $request->input('u3m1q1');
        $questions->question = $request->input('u3m1q2');
        $questions->question = $request->input('u3m1q3');
        $questions->question = $request->input('u3m1q4');
        $questions->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model120hrUnitThreeModuleOneSectionOne  $model120hrUnitThreeModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function show(Model120hrUnitThreeModuleOneSectionOne $model120hrUnitThreeModuleOneSectionOne)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model120hrUnitThreeModuleOneSectionOne  $model120hrUnitThreeModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function edit(Model120hrUnitThreeModuleOneSectionOne $model120hrUnitThreeModuleOneSectionOne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model120hrUnitThreeModuleOneSectionOne  $model120hrUnitThreeModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model120hrUnitThreeModuleOneSectionOne $model120hrUnitThreeModuleOneSectionOne)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model120hrUnitThreeModuleOneSectionOne  $model120hrUnitThreeModuleOneSectionOne
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model120hrUnitThreeModuleOneSectionOne $model120hrUnitThreeModuleOneSectionOne)
    {
        //
    }

    // public function radioTextBox()
    // {
    //     $radioTextBox = Form::checkbox('name', 'value');

    // }
}
