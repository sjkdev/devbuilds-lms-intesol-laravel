<?php

namespace App\Http\Controllers\Admin;

use App\Model120hrUnitThreeModuleFourSectionOne;
use Illuminate\Http\Request;
use App\Http\controllers\Controller;

class Model120hrUnitThreeModuleFourSectionOneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.onetwentyhours.unit-three.module-four.section-one.index', compact('U3M4S1'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model120hrUnitThreeModuleOneSectionFour  $model120hrUnitThreeModuleOneSectionFour
     * @return \Illuminate\Http\Response
     */
    public function show(Model120hrUnitThreeModuleFourSectionOne $model120hrUnitThreeModuleFourSectionOne)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model120hrUnitThreeModuleOneSectionFour  $model120hrUnitThreeModuleOneSectionFour
     * @return \Illuminate\Http\Response
     */
    public function edit(Model120hrUnitThreeModuleFourSectionOne $model120hrUnitThreeModuleFourSectionOne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model120hrUnitThreeModuleOneSectionFour  $model120hrUnitThreeModuleOneSectionFour
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model120hrUnitThreeModuleFourSectionOne $model120hrUnitThreeModuleFourSectionOne)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model120hrUnitThreeModuleOneSectionFour  $model120hrUnitThreeModuleOneSectionFour
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model120hrUnitThreeModuleFourSectionOne $model120hrUnitThreeModuleFourSectionOne)
    {
        //
    }
}
