<?php

namespace App\Http\Controllers;

use App\int140u1m1s1;
use Illuminate\Http\Request;

class Int140u1m1s1Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\int140u1m1s1  $int140u1m1s1
     * @return \Illuminate\Http\Response
     */
    public function show(int140u1m1s1 $int140u1m1s1)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\int140u1m1s1  $int140u1m1s1
     * @return \Illuminate\Http\Response
     */
    public function edit(int140u1m1s1 $int140u1m1s1)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\int140u1m1s1  $int140u1m1s1
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int140u1m1s1 $int140u1m1s1)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\int140u1m1s1  $int140u1m1s1
     * @return \Illuminate\Http\Response
     */
    public function destroy(int140u1m1s1 $int140u1m1s1)
    {
        //
    }
}
