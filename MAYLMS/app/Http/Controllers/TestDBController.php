<?php

namespace App\Http\Controllers;

use App\testDB;
use Illuminate\Http\Request;

class TestDBController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\testDB  $testDB
     * @return \Illuminate\Http\Response
     */
    public function show(testDB $testDB)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\testDB  $testDB
     * @return \Illuminate\Http\Response
     */
    public function edit(testDB $testDB)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\testDB  $testDB
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, testDB $testDB)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\testDB  $testDB
     * @return \Illuminate\Http\Response
     */
    public function destroy(testDB $testDB)
    {
        //
    }
}
