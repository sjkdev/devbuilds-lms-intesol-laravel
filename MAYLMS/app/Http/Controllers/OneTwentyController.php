<?php

namespace App\Http\Admin\Controllers;

use App\OneTwenty;
use Illuminate\Http\Request;

class OneTwentyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.twentyhours.index', compact('onetwentyhours'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OneTwenty  $oneTwenty
     * @return \Illuminate\Http\Response
     */
    public function show(OneTwenty $oneTwenty)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OneTwenty  $oneTwenty
     * @return \Illuminate\Http\Response
     */
    public function edit(OneTwenty $oneTwenty)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OneTwenty  $oneTwenty
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OneTwenty $oneTwenty)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OneTwenty  $oneTwenty
     * @return \Illuminate\Http\Response
     */
    public function destroy(OneTwenty $oneTwenty)
    {
        //
    }
}
