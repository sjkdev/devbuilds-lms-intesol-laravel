<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Model120hrUnitThreeModuleOneSectionOne extends Model
{
    protected $fillable = ['u3m1q1', 'u3m1q2', 'u3m1q3', 'u3m1q4'];
}
