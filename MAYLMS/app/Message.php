<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Message
 *
 * @package App
 * @property integer $mid
 * @property integer $sid
 * @property integer $trid
 * @property string $subject
 * @property text $message
 * @property string $mdate
 * @property string $status
*/
class Message extends Model
{
    use SoftDeletes;

    protected $fillable = ['mid', 'sid', 'trid', 'subject', 'message', 'mdate', 'status'];
    
    

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setMidAttribute($input)
    {
        $this->attributes['mid'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setSidAttribute($input)
    {
        $this->attributes['sid'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setTridAttribute($input)
    {
        $this->attributes['trid'] = $input ? $input : null;
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setMdateAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['mdate'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        } else {
            $this->attributes['mdate'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getMdateAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], config('app.date_format'));

        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d', $input)->format(config('app.date_format'));
        } else {
            return '';
        }
    }
    
}
