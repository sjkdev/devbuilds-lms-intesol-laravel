<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseStudent extends Model
{
    protected $table = 'course_student';

    protected $fillable = [
    	'course_id',
    	'course_student_level',
    	'course_student_status',
    	'user_id',
    ];
}
