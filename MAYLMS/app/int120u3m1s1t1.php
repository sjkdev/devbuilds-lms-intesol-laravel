<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class int120u3m1s1t1 extends Model
{
    protected $table = 'int120u3m1s1t1s';
    protected $fillable = [
        'user_id',
    	'u3m1s1q1', 
    	'u3m1s1q2', 
    	'u3m1s1q3', 
    	'u3m1s1q4',
    	'u3m1s1q5',
    	'u3m1s1q6',
    	'u3m1s1q7'
    	'u3m1s1q8',
    	'u3m1s1q9',
    	'u3m1s1q10',
    	'u3m1s1q11',
    	'u3m1s1q12',
    	'u3m1s1q13',
    	'u3m1s1q14',
    	'u3m1s1q15',
    	'u3m1s1q16',
    	'u3m1s1q17',
    	'u3m1s1q18',
    	'u3m1s1q19',
    	'u3m1s1q20',
    	'u3m1s1q21',
    	'u3m1s1q22',
    	'u3m1s1q23',
        'u3m1s1q24',
        'u3m1s1q25',
        'u3m1s1q26',
        'u3m1s1q27',
        'u3m1s1q28',
        'u3m1s1q29',
        'u3m1s1q30',
        'u3m1s1q31',
        'u3m1s1q32',
        'u3m1s1q33',
        'u3m1s1q34',
        'u3m1s1q35',
        'u3m1s1q36',
        'u3m1s1q37',
        'u3m1s1q38',
    ];
}
