<?php
namespace App;

use Cmgmyr\Messenger\Traits\Messagable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Hash;

/**
 * Class User
 *
 * @package App
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
*/
class User extends Authenticatable
{

    use Messagable, Notifiable;
    use Notifiable;
    protected $fillable = ['name', 'email', 'password', 'address', 'dob', 'phone', 'nationality', 'remember_token'];
    
    
    /**
     * Hash password
     * @param $input
     */
    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }
    
    
    public function role()
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }

    public function classroomInfo()
    {
        return $this->belongsToMany(ClassRoomInfo::class, 'classroomInfo');
    }

    public function isAdmin()
    {
        return $this->role()->where('role_id', 1)->first();
    }

    public function students()
    {
        return $this->role()->where('role_id', 3)->first();
    }

    public function trainee()
    {
        return $this->role()->where('role_id', 5)->first();
    }

    public function trainee120()
    {
        return $this->role()->where('role_id', 6)->first();
    }

    public function trainee190()
    {
        return $this->role()->where('role_id', 7)->first();
    }

    public function isTrainer()
    {
        return $this->role()->where('role_id', 4)->first();
    }

     public function isTrainee()
    {
        return $this->role()->where('role_id', 5)->first();
    }

    public function lessons()
    {
        return $this->belongsToMany('App\Lesson', 'lesson_student');
    }

    // email stuff
    public function topics() {
        return $this->hasMany(MessengerTopic::class, 'receiver_id')->orWhere('sender_id', $this->id);
    }

    public function inbox()
    {
        return $this->hasMany(MessengerTopic::class, 'receiver_id');
    }

    public function outbox()
    {
        return $this->hasMany(MessengerTopic::class, 'sender_id');
    }

    // identifying roles
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function hasRole()
    {
        return $this->belongsToMany('App\Role');
    }


    public function isStudent()
    {
        foreach ($this->roles()->get() as $role)
        {
            if ($role->name == 'Student')
            {
                return true;
            }
        }

        return false;
    }


    public function isTrainee120()
    {
        foreach ($this->roles()->get() as $role)
        {
            if ($this->role()->where('role_id', 6)->first())
            {
                return true;
            }
        }

        return false;
    }

    public function isTrainee190()
    {
        foreach ($this->roles()->get() as $role)
        {
            if ($this->role()->where('role_id', 7)->first())
            {
                return true;
            }
        }

        return false;
    }
    

    public function course()
    {
        return $this->belongsToMany('App\Course');
    }

  

    public function userList()
    {
        // $users = User::with('roles.permissions')->get();
        // $users = User::all();
        $users = User::all()->except($currentUser->id);

        return view ('admin.createcourses.index', compact('users'));
    }

    public function getStartDateAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], config('app.date_format'));

        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d', $input)->format(config('app.date_format'));
        } else {
            return '';
        }
    }

}
