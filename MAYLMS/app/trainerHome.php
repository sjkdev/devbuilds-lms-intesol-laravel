<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainerHome extends Model
{
    protected $table = 'trainerhome';

    protected $fillable = [
    	'id',
    	'classroom_id',
    	'course_student',
    	'course_student_level',
    	'course_student_status',
    	'canned',
    	'notifications',
    ];


    // public function trainees()
    // {
    //     // return $this->belongsToMany(Auth::User, 'isTrainee');
    //     return $this->belongsToMany(User::whereHas('role', function ($q) { $q->where('role_id', 5); } )->get()->pluck('name', 'id'));
    // }

//      public function trainees()
//     {
//         return $this->belongsToMany(User::class, 'course_student');
//     }
// }
