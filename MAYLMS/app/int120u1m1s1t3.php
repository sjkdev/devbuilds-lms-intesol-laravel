<?php

namespace App;
d
use Illuminate\Database\Eloquent\Model;

class int120u1m1s1t3 extends Model
{
    protected $table = 'int120u1m1st3s';

    protected $fillable = [
        'user_id',
    	'u1m1s1q1', 
    	'u1m1s1q2', 
    	'u1m1s1q3', 
    	'u1m1s1q4',
    	'u1m1s1q5',
    	'u1m1s1q6',
    	'u1m1s1q7'
    	'u1m1s1q8',
    	'u1m1s1q9',
    	'u1m1s1q10',
    	'u1m1s1q11',
    	'u1m1s1q12',
    	'u1m1s1q13',
    	'u1m1s1q14',
    	'u1m1s1q15',
    	'u1m1s1q16',
    	'u1m1s1q17',
    	'u1m1s1q18',
    	'u1m1s1q19',
    	'u1m1s1q20',
    	'u1m1s1q21',
    	'u1m1s1q22',
    	'u1m1s1q23',
        'u1m1s1q24',
        'u1m1s1q25',
        'u1m1s1q26',
        'u1m1s1q27',
        'u1m1s1q28',
        'u1m1s1q29',
        'u1m1s1q30',
        'u1m1s1q31',
    ]
};
