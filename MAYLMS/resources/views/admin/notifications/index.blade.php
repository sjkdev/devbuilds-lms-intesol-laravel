@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('content')


<div class="panel panel-default">
	<div class="panel-heading">Notifications</div>
	<div class="panel-body">
		

		<table class="table table-bordered table-striped {{ count($notifications) > 0 ? 'datatable' : '' }}">
			
			<thead>
				<th>NID</th>
				<th>SID</th>
				<th>TRID</th>
				<th>Task</th>
				<th>Status</th>
				<th>Date</th>
			</thead>
			
			<tbody>
				
				@foreach($notifications as $notification)
				<tr>
					<td>{{ $notification->NID}}</td>
					<td>{{ $notification->SID}}</td>
					<td>{{ $notification->TRID}}</td>
					<td>{{ $notification->TASK}}</td>
					<td>{{ $notification->NSTATUS}}</td>
					<td>{{ $notification->NDATE}}</td>
					@endforeach
				</tr>
				
			</tbody>
		</table>
		
		
		{{-- @endif --}}
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">User info</div>
	<div class="panel-body">
		@foreach($studentinfo as $info)
				{{-- <li>Rolename : {{ $info->role_name}}</li>
				<li>ID : {{ $info->id}}</li>
				<li>Name : {{ $info->name}}</li> --}}
				<li>{{ $info->role_name }}</li>
				<li>{{ $info->name }}</li>
				<hr>
			</ul>
		@endforeach
	</div>
</div>

{{-- <div class="panel panel-default">
	<div class="panel-heading">Dinstict output query</div>
	<div class="panel-body">
		@foreach($distinct as $data)
		<ul>
			<li>{{ $data }}</li>
		</ul>
		@endforeach
	</div>
</div> --}}

{{-- <div class="panel panel-default">
	<div class="panel-heading">results test</div>
	<div class="panel-body">
		@foreach($resultsv4 as $result)
		<ul>
			<li>{{ $result }}</li>
		</ul>
		@endforeach
	</div>
</div> --}}

{{-- <div class="panel panel-default">
	<div class="panel-heading">Student list</div>
	<div class="panel-body">
		@foreach( $studentdata as $studentdatee )
		<ul>
			<li>{{ $studentdatee }}</li>
		</ul>
		@endforeach
	</div>
</div> --}}

<div class="panel panel-default">
	<div class="panel-heading">Create New Notification</div>
	<div class="panel-body">
		
		{!! Form::open(['action' => 'Admin\NotificationsController@store', 'method' => 'POST']) !!}
		<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">
		{!! Form::label('label', 'Message Title')!!}
		{!! Form::text('newnotificationtitle')!!}
		<div class="form-group">
							{!! Form::label('Type message here', 'Type message here:') !!}
							{!! Form::textarea('newnotification', null, ['class' => 'form-control'])!!}
							
						</div>

		{!! Form::submit('Create Notification', ['class' => 'btn btn-info form-control'])!!}
									{!! Form::close() !!}
		
	</div>
</div>


<div class="panel panel-default">
	<div class="panel-heading">List of notifications</div>
	<div class="panel-body">

		<table class="table table-dark">

  <tbody>
    <tr>
    
    @foreach($notifications as $notification)
		<td><b>{{ $notification->newnotificationtitle}}</b></td>
		<td>{{ $notification->newnotification}}</td>
		@endforeach
    </tr>
   
  </tbody>
</table>
		
		
	</div>
</div>


<div class="panel panel-default">
	<div class="panel-heading">Select Notifications</div>
	<div class="panel-body">
		 <{{-- div class="col-xs-12 form-group">
            {!! Form::label('notifications', 'Notifications', ['class' => 'control-label']) !!}
            {!! Form::select('notifications[]', $notificationsTitle, old('notifications'), ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}
            <p class="help-block"></p>
            @if($errors->has('notifications'))
                <p class="help-block">
                    {{ $errors->first('notifications') }}
                </p>
            @endif
        </div>
 --}}



        {{-- <div class="col-xs-12 form-group">
            {!! Form::label('notifications', 'Notifications', ['class' => 'control-label']) !!}
            {!! Form::select('notifications[]', $notifications, old('notifications') ? old('notifications') : $notifications->notification->pluck('newnotificationtitle')->toArray(), ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}
            <p class="help-block"></p>
            @if($errors->has('notifications'))
                <p class="help-block">
                    {{ $errors->first('notifications') }}
                </p>
            @endif
        </div> --}}
	</div>
</div>
@endsection