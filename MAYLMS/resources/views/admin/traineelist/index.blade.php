@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('global.traineelist.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list') | List of Trainees
        </div>
        <div class="panel-body">
          {{--   @foreach($studentinfo as $student)
            <ul>
                <li>{{ $student }}</li>
            </ul>
            @endforeach
            {{dd($studentinfo)}}   --}}          
        </div>
    </div>


    @foreach ($users as $user)
    {{ $user->username }}
    @endforeach


<div class="panel panel-default">
    <div class="panel-heading">Trainee list</div>
    <div class="panel-body">
      {{--   <table>
            <tr>
                <th>Role</th>
                <th>Name</th>
            </tr>

            @foreach($studentinfo as $info)
            <tr>
                <td>{{ $info->role_name }}</td>
                <td>{{ $info->name }}</td>
            </tr>
            @endforeach
            <tr>
                <td></td>
                <td></td>
            </tr>
        </table> --}}
    </div>
</div>

{{--  <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($courses) > 0 ? 'datatable' : '' }} @can('course_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('course_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        @if (Auth::user()->isAdmin())
                            <th>@lang('global.courses.fields.teachers')</th>
                        @endif
            
                            <th>@lang('global.courses.fields.students')</th>
                        <th>@lang('global.courses.fields.title')</th>
                        <th>Class ID</th>
                        <th>Course Level</th>
                        
                         @if (Auth::user()->isAdmin())
                        <th>@lang('global.courses.fields.slug')</th>
                        <th>@lang('global.courses.fields.description')</th>
                       
                        <th>@lang('global.courses.fields.price')</th>

                        <th>@lang('global.courses.fields.course-image')</th>
                        @endif
                       
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($courses) > 0)
                        @foreach ($courses as $course)
                            <tr data-entry-id="{{ $course->id }}">
                                @can('course_delete')
                                    @if ( request('show_deleted') != 1 )<td></td>@endif
                                @endcan

                                @if (Auth::user()->isAdmin())
                                <td>
                                    @foreach ($course->teachers as $singleTeachers)
                                        <span class="label label-info label-many">{{ $singleTeachers->name }}</span>
                                    @endforeach
                                </td>
                                @endif
                                <td>
                                    @foreach ($course->students as $singleStudents)
                                        <span class="label label-info label-many">{{ $singleStudents->name }}</span>
                                    @endforeach
                                </td>
                                <td>{{ $course->title }}</td>
                                <td>{{$course->class_id}}</td>
                                <td>{{ $course->level}}</td>
                                
                                <td>{{ $course->slug }}</td>
                                <td>{!! $course->description !!}</td>
                                <td>{{ $course->price }}</td>
                                <td>@if($course->course_image)<a href="{{ asset('uploads/' . $course->course_image) }}" target="_blank"><img src="{{ asset('uploads/thumb/' . $course->course_image) }}"/></a>@endif</td>
                                <td>{{ $course->start_date }}</td>
                                <td>{{ Form::checkbox("published", 1, $course->published == 1 ? true : false, ["disabled"]) }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.courses.restore', $course->id])) !!}
                                    {!! Form::submit(trans('global.app_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.courses.perma_del', $course->id])) !!}
                                    {!! Form::submit(trans('global.app_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('course_view')
                                    <a href="{{ route('admin.lessons.index',['course_id' => $course->id]) }}" class="btn btn-xs btn-primary">@lang('global.lessons.title')</a>
                                    @endcan
                                    @can('course_edit')
                                    <a href="{{ route('admin.courses.edit',[$course->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                    @endcan
                                    @can('course_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.courses.destroy', $course->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="12">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div> --}}
@endsection


