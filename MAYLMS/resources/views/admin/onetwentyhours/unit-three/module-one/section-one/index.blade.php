@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('content')

<div class="panel panel-default">
	<div class="panel-body">Unit Three Module One</div>
</div>
<div class="panel panel-group">
	<div class="panel-heading"><h3>Unit 3 | The Teaching and Learning of EFL</h3></div>
	<div class="panel-heading"><h4>Module 1 | The Basic Principles of TEFL</h4></div>


	<div class="panel-body" id="exTab1">
		<ul  class="nav nav-pills">

			<li class="active"><a  href="#1a" data-toggle="tab" style="background-color:#fff;">Info</a></li>
			<li><a href="#2a" data-toggle="tab" style="background-color:#fff;">Basic Principles</a></li>
			<li><a href="#3a" data-toggle="tab" style="background-color:#fff;">How do we bring about success</a></li>
			<li><a href="#4a" data-toggle="tab" style="background-color:#fff;">Exams</a></li>
			<li><a href="#5a" data-toggle="tab" style="background-color:#fff;" class="orangeText">Self Check 1</a></li>
			<li><a href="#6a" data-toggle="tab" style="background-color:#fff;" class="orangeText">Self Check 2</a></li>
			<li><a href="#7a" data-toggle="tab" style="background-color:#fff;">Answers to Self Checks</a></li>
			<li><a href="#8a" data-toggle="tab" style="background-color:#fff;">Grammatical Approaches</a></li>
			<li><a href="#9a" data-toggle="tab" style="background-color:#fff;">Pair and Group Work</a></li>
			<li><a href="#10a" data-toggle="tab" style="background-color:#fff;">Teaching Grammar and Vocabulary in Context</a></li>
			<li><a href="#11a" data-toggle="tab" style="background-color:#fff;" class="blueText">Task</a></li>

		</ul>

	</div>
</div>


<div class="tab-content clearfix">
	{{-- tab 1 --}}
	<div class="tab-pane" id="1a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Info</h3>
						</div>
						<div class="panel-body">
							<h4>At the end of this module you will:-</h4>
							<ul class="cleanList">
								<li>a) have a basic understanding of the principles of good EFL teaching</li>
								<li>b) understand the importance of a caring environment in the classroom</li>
								<li>c) know why lack of consideration of these principles could hinder language production and progress</li>
								<li>d) understand the need for communicative teaching</li>
								<li>e) have some idea of the concept of communication versus correctness</li>
							</ul>


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	{{-- tab 2 --}}
	<div class="tab-pane" id="2a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Basic Principles of TEFL</h3>
						</div>
						<div class="panel-body">
							<p>
								One of the basic principles of good EFL teaching is to make sure that our students CAN succeed. In order to do this we should consider the following questions at the beginning of every new group/class. 
							</p>
							<ul class="cleanList">
								<li>How do I involve the students?</li>
								<li>How do I make students want to learn and look forward to their lessons?</li>
								<li>How do I make them work hard?</li>
								<li>How do I keep them happy?</li>
							</ul>
							<br>
							<h4 class="strUpper text-center strBold">Unhappy Students learn very little</h4>
							<br>
							<h4 class="strUpper text-center strBold">We need our students to be happy</h4>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">Relationships</div>
						<div class="panel-body">
							<p>
								Over the years there have been many varied approaches to teaching English eg 'structural', 'notional', 'communicative' etc. and each promises to be the best, the new way to faster success. None will work in isolation. Successful English teaching is not only a question of method, but also of student / teacher relationships. You must be interested in your students. The student must need/want to learn AND the teacher must also need/want the student to learn. Encouragement is of great importance. Students need to feel they are progressing; this feeling comes, on the whole, from the teacher. (More about this later in this module).
							</p>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">Look after your students</div>
						<div class="panel-body">
							<p>
								In order to achieve our aim of ensuring that our students are happy (and therefore able to learn), we not only teach them and give them opportunities to learn, we must also look after them.
							</p>
							<br>
							<ul>
								<li><i>Students need to feel at ease</i></li>
								<li><i>Students need to feel comfortable</i></li>
								<li><i>Students need to welcome</i></li>
							</ul>
							
							<br>
							<p>
								We, as teachers, need to settle them in and while doing so try to 'suss out' any 'hang-ups' they have which will prevent them from learning. For example they may have old family prejudices against our race; they may have been convinced that they will find English too difficult; they may have met untrained teachers who turned them off classes almost for life! Now they are your students - you must help them over these barriers and make them feel relaxed and happy enough to succeed.
							</p>

						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">Are you a caring person?</div>
						<div class="panel-body">
							<p>
								Throughout the course you will be shown how to prepare for a career in TEFL. This will involve planning lessons; preparing materials; getting to grips with grammar; setting out your classroom; understanding the students' problems. We hope to make you a fully-prepared professional who can bring about success in any EFL situation. We cannot make you a caring person, but if you are not going to care about your students' success, you will not succeed as a teacher. If you are not going to be concerned about their happiness you will not be happy and neither you nor your students will be successful.								
							</p>
							<p>
								From now on we assume that you are a caring person who wants to bring about success.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	{{--  tab 3 --}}
	<div class="tab-pane" id="3a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>HOW DO WE BRING ABOUT SUCCESS?</h3>
						</div>

					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Involving the student</h3>
						</div>
						<div class="panel-body">
							<p>
								Everyone likes to talk about him/herself. Likewise students like to hear about their classmates / fellow students. When this is allowed in class you will find that your students involve themselves far more than if they are talking about a fictional character from a coursebook.
							</p>
							<p>
								The teacher shows interest in his/her students by getting to know about their interests and backgrounds.								
							</p>
							<p>
								Get to know the area the students come from so that relevant discussions can be set up.
							</p>
							<p>
								Allow your students to express their opinions about their own environment and about their experiences in England, USA etc.
							</p>
							<p>
								<b>
									If all your students are desperately putting their hands up to tell you something, you have achieved your aim of involving them.
								</b>
							</p>



						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Giving the student confidence</h3>
						</div>
						<div class="panel-body">
							<p>
								Role-play, dialogues, sketches - however you do it, acting out involves both the performing students and the listeners. They will be less self-conscious about the English as they concentrate on the character or acting role which they are required to carry out. They may be embarrassed or nervous at first, but hopefully when they have got through that they will be assured that they can also get over the 'fear' of speaking English.
							</p>
							<p>
								Building up the students' confidence in any situation means encouraging them at every opportunity and never 'putting them down'. (More about this later in the module)
							</p>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Using realistic language</h3>
						</div>
						<div class="panel-body">
							<p>Hopefully, gone are the days of courses which taught useless language <br>
								<blockquote>"Does your grandmother wear a red dress?"</blockquote><br>
								<blockquote>"Is this a pen? Yes, this is a pen."</blockquote>
							are not phrases which students will not need, if at all.</p>
							<p>
								Real situations and examples which the student will meet outside the classroom should be used as much as possible; equally the language should be taught in context.
							</p>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Shortened forms</h3>
						</div>
						<div class="panel-body">
							<p>
								Use them! I'd, she's, weren't etc. are normal English. In their full form they are not used unless you want to sound like a foreigner!
							</p>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Accent</h3>
						</div>
						<div class="panel-body">
							<p>
								Teachers of EFL do not all have the same accent. On the whole students will expect this and readily accept regional accents as well as American, Canadian, Australian etc. 
							</p>
							<p>
								Do not be afraid to use your normal accent, trying to change it and being inconsistent can be confusing. So long as you do not use very colloquial English or colloquialisms which are grammatically unacceptable, relax in the knowledge that you can be a model in the classroom whether you come from Glasgow or Oxford.
							</p>
							<p>
								However, it is essential that your students are given the opportunity to hear as many different accents as you can bring into the classroom. This way they will not be shocked when they hear other accents outside the classroom, and they will not find themselves unable to understand anyone but you.
							</p>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h3>
							Keeping the students' interest
						</h3></div>
						<div class="panel-body">
							<p>
								So you've involved the students, they are gaining confidence and desperately wanting to join in the lesson. How do you keep this level of interest?
							</p>
							<p>
								Varying the activities is imperative. Monotony will send your students to sleep. Change the activity regularly, even if you are teaching the same language point. During the course you will learn many different ways to teach and exploit a language point, remember to use them. Don't be known as 'the teacher who always does a reading passage' or 'the one who always uses a tape' - this makes for disinterested students even before they have reached the lesson.
							</p>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>L.T.T.T.</h3>
						</div>
						<div class="panel-body">
							<p>
								During your TEFL career you will often hear L.T.T.T. - Limit Teacher Talking Time. This is important in keeping the students interested and motivated.
							</p>
							<p>
								You will keep their interest by ensuring that they are allowed to produce a lot of language in lessons. They will <b>not</b> learn how to speak English if <b>you</b> do all the speaking.
							</p>
						</div>
					</div>
					<div class="panel panel-deafult">
						<div class="panel-heading">
							<h3>Correction</h3>
						</div>
						<div class="panel-body">
							<p>When correcting spoken English remember to be encouraging, even completely incorrect answers need recognition that the student has made the effort (though you must make sure you do not patronise). There are ways of saying that the answer is not right without putting the student off trying again. (More on this in Unit 4)</p>
							<p>
								When correcting written work, again it is important to acknowledge that the student has tried. It is demotivating to see a page full of red pen, especially when it may have taken the student hours to do it. Choose the most relevant points (ie relevant to the teaching point), do not correct every mistake.
							</p>
							<p>
								It is important that the students can read and understand the corrections which you make on their work. A chart showing the abbreviations which you use is a good idea. You might also suggest that they write on alternate lines so that your corrections can be seen. It is really up to you what method of correction and abbreviation you use but it is essential that this is understood between you and the students and that once a method has been established that you stick to it. (More on correction of work in Module 5 Error)
							</p>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Visual Aids</h3>
						</div>
						<div class="panel-body">
							<p>
								Don't become the school magician - something different to pull out of the hat every day, and a lesson built around it. Aids aid your lesson, they are not the lesson! If you are a technical whizz-kid, good for you, but you are not there to impress your students with your technical know-how, you are there to teach and as such you should use aids to assist you in your task. (More about this in Module 4 Visual Aids)
							</p>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Levels</h3>
						</div>
						<div class="panel-body">
							<p>
								Classes should, in theory be made up of students who are at exactly the same level of language learning. Those levels are as follows:-
							</p>
							<ul class="cleanList">
								<li>Beginners</li>
								<li>Elementary</li>
								<li>Intermediate</li>
								<li>Advanced</li>
							</ul>
							<p>
								As you can see this in itself would cause problems for a very small school which can only offer 2 or 3 levels. These problems can be overcome through clever timetabling and you should not be faced with mixed levels in your class.
							</p>
							<p>
								If, however, you look at the following, more extensive list, you will see that there could be a need for a much larger number of classes:-
							</p>
							<ul class="cleanList">
								<li>true beginners</li>
								<li>false beginners</li>
								<li>elementary</li>
								<li>post elementary / lower intermediate</li>
								<li>intermediate</li>
								<li>post intermediate</li>
								<li>advanced</li>
							</ul>
							<p>You will rarely see the number of students' levels taken account of in this way, so it is obvious that you will, to a certain extent, have mixed levels in your classes.</p>
							<p>
								You will, of course, also have mixed abilities. You will always have students studying for the same level whose language aptitude is far from the same. You must take account of this and closely monitor those who need extra help. (More about this later in this module).
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	{{-- tab 4 --}}
	<div class="tab-pane" id="4a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Exams</h3>
						</div>
						<div class="panel-body">
							<p>
								Most classes of EFL students are working towards exams. They use these qualifications to further their studies and/or their careers so they are very important to them.
							</p>
							<p>
								As teachers we must have an understanding of the stress which exam entry exerts on many students, especially those who need to pass in order to further their careers.
							</p>
							<h4 class="strUpper">Teaching for exams</h4>
							<p>
								Teaching for ESOL international examinations constitutes an important part of an ESOL teacher’s career. Any English teacher can be asked by the school, institution, or private persons to prepare students for these examinations. All teachers will encounter these exams at some time in their teaching career.
							</p>
							<p><b>Why are students taking these exams?</b></p>
							<p>The reasons are extremely diverse:</p>
							<ul>
								<li>To have an internationally recognized language certificate</li>
								<li>To work and/or live in a country where English is the language of communication </li>
								<li>To seek further education</li>
								<li>To improve employment prospects</li>
							</ul>
							<p><b>What are the most sought after ESOL examinations?</b></p>
							<h4>1. Cambridge ESOL examinations</h4>
							<p>
								The most prestigious, and widely known examination board is Cambridge ESOL, which is part of the University of Cambridge. Cambridge ESOL (English for Speakers of Other Languages) examinations are the world’s leading range of certificates for learners of English. They are recognized by a huge number of employers and educational institutions all over the world. Since around 2 million people from 130 countries take them yearly, the demand for teaching for these exams is continuously growing.  
							</p>
							<p>
								Cambridge ESOL is part of the world-famous University of Cambridge. Its English language exams are linked to the Common European Framework for Modern Languages, published by the Council of Europe.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

{{-- tab --}}
<div class="tab-pane" id="5a">
	<div class="#">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>Self Check 1</h3>
					</div>
					<div class="panel-body">
						<p>Follow this link http://www.coe.int/t/dg4/linguistic/Cadre1_en.asp  to find out:</p>
						<ol>
							{!! Form::open(['action' => 'Admin\Int120u3m1s1t1Controller@store', 'method' => 'POST']) !!}
							<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">
								{{ Form::label('Whats does CEFR stand for?')}}<br>
								{{ Form::text('u3m1s1q1', null, ['class' => ' form-control'])}} 

								

								<br>
								<li>What is CEFR?<br>{{ Form::text('u3m1s1q2', 'question', ['class' => ' form-control'])}}   </li><br>
								<li>Is it only used in Europe? <br>
									{{ Form::text('u3m1s1q3', null, ['class' => ' form-control'])}}
									   </li><br>
									<li>Does it refer to measuring the proficiency in the English language exclusively? <br>
										{{ Form::text('u3m1s1q4', null, ['class' => ' form-control'])}}   </li>
										
									</ol>

									
									{!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}
									{!! Form::close() !!}
									

								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3>1.1 The most popular Cambridge ESOL exams:</h3>
								</div>
								<div class="panel-body u3m1selfCheckTable">
									<table>
										<tr>
											<th>Examinations</th>
											<th>Description</th>
											<th>use</th>
										</tr>
										<tr>
											<td>Young Learners of English <br>YLE</td>
											<td>Three levels: YLE Starters <br> YLE MOvers, and YLE Flyers</td>
											<td>These exams give a consistent measure of how well 7–12 year–olds are doing in the skills of listening, speaking, reading and writing. Tests are designed to make learning fun. They train the young learners to work towards certificates. There are no grades but the pupils earn learning ‘shields’ that record their progress, which encourage children to do better.</td>
										</tr>
										<tr>
											<td>Key English Test <br>KET<br>- general<br>-for schools
											</td>
											<td>- the general KET is for adults, while KET for schools is directed towards pupils.
												There are three papers: 1. Reading and Writing; 2. Listening; 3. Speaking (interactive) <br>Final grades are correlated with CEFR (common European Framework) at A2 level (Basic user): pass, pass with merit, and pass with distinction 
											</td>
											<td>KET is a basic level qualification that shows that the learners can use English to communicate in simple situations and have achieved a good foundation in learning English.
											</td>
										</tr>
										<tr>
											<td>Preliminary English Test <br>PET
											</td>
											<td>- the format is similar to KET.
												<br>- paper-based and computer-based exam options
												<br>Final grades are correlated with CEFR (common European Framework) at B1 level (pass) or B2 (pass with distinction).
											</td>
											<td>- an intermediate level qualification in English that opens the doors to opportunities for work, study and travel.</td>
										</tr>
										<tr>
											<td>Certificate English <br> FCE
											</td>
											<td>This exam uses real-life situations 
												There are 5 Papers: Reading, Writing, Use of English (grammar and vocabulary), Listening, and Speaking (interactive).
											Final grades are correlated with CEFR at B1 level. Passing grades are C, B, and A as the highest grade.</td>
											<td>-accepted by thousands of employers, universities and government departments around the world as a qualification in upper-intermediate English. A wide range of educational institutions for study purposes also accepts it.</td>
										</tr>
										<tr>
											<td>Certificate in Advanced English
												CAE
											</td>
											<td>This is an examination in academic English.
												There are 5 Papers: Reading, Writing, English in Use (grammar, vocabulary, register), Listening, and Speaking (interactive)
												Final grades are correlated with CEFR at C1 level. The passing grades are, in order from lowest to highest, C, B, and A.
											</td>
											<td>CAE the ideal qualification to prove that that students have mastered English at such a level that will enable them to deal with complicated academic and professional tasks in English: following academic courses, carrying out challenging research, communicating appropriately in diverse social and cultural contexts.
												It is accepted by a large number of universities on all continents. 
											</td>
										</tr>
										<tr>
											<td>Certificate of Proficiency in English
												CPE
											</td>
											<td>This is the most advanced exam, for learners who have achieved an extremely high level of skill in the English language.
												There are 5 Papers: Reading, Writing, Use of English (grammar, vocabulary), Listening, and Speaking
												Final grades are correlated with CEFR at C2 level. The passing grades are, in order from lowest to highest, C, B, and A.
											</td>
											<td>CPE exam uses real-life situations that are especially designed to help learners communicate effectively and learn the language skills they need to function at the very highest levels of professional and academic life (native or near native academic and professional language proficiency)
												It is accepted by a much larger number of universities and employers than the CAE all over the world.
											</td>
										</tr>
									</table>


								</div>
								<div class="panel-body">
									<p>The English tested in these examinations covers a variety of accents from different English speaking countries. They are British examinations but the English tested is not restricted to the accents in the British Isles. In speaking or writing, for example, American English is accepted as long as the candidates are consistent in using it. There are two examination periods per year: June and December. These language certificates are valid for life.</p>
								</div>

							</div>

							<div class="panel panel-default">
								<div class="panel-heading"><h3>1.2. Other Cambridge ESOL examinations:
								</h3></div>
								<div class="panel-body">
									<p>
										<b>IELTS</b> (International English Language Testing System) is perhaps the most widely recognized language qualification in the world. 
										IELTS is jointly owned by the British Council, IDP: IELTS (International Development Program of Australian Universities and Colleges) Australia, and the University of Cambridge. These ESOL Examinations are administered through more than 800 test centres and locations in over 130 countries on all continents. IELTS scores are valid for two years.

									</p>
									<div class="panel-body u3m1selfCheckTable">
										<table>
											<tr>
												<th>Examination</th>
												<th>Description</th>
												<th>Use</th>
											</tr>
											<tr>
												<td>IELTS</td>
												<td><p>IELTS is of two types: general and academic.
												</p>
												<p>Four Papers: Listening, Reading, Writing, and Speaking (interview).
													This is not a pass or fail test. The scores range from 0 to 9 and half scores is also awarded (e.g. 6.5). 
													Note: To have an idea about the correlation between the IELTS scores, the other Cambridge examination scores and CEFR, see the table at the end of the section on Cambridge examinations. 
												</p></td>
												<td><b>General Training module</b> is for those who choose wish to migrate to an English-speaking country, (e.g. Australia, Canada, New Zealand, UK) or to train or study at below degree level.
													<b>Academic module</b> is for those who wish to study at undergraduate or postgraduate levels, or seek professional registration, e.g. doctors and nurses.</td>
												</tr>
											</table>
										</div>

										<div class="panel-body">
											<p>The language tested uses different accents of English and the topics are diverse and are not connected to any culture in particular. This certificate is valid for two years. If a person needs an IELTS certificate after the 2-year period expires, he/she has to take it again. It can be taken any time of the year.</p>
										</div>

									</div>
								</div>

								<div class="panel panel-default">
									<div class="panel-heading">
										<h3>Professional English Examinations</h3>
									</div>
									<div class="panel-body u3m1selfCheckTable">
										<table>
											<tr>
												<th>Examinations</th>
												<th>Descriptions</th>
												<th>Use</th>
											</tr>
											<tr>
												<td>Business English Cambridge <br>BEC</td>
												<td>Three level from low to high: <br>
													<ul class="cleanList">
														<li>Preliminary</li>
														<li>Vantage</li>
														<li>Higher</li>
													</ul>
													<br>
													<i>Note: see the correlation with CEFR in the table below.</i>
												</td>
												<td>All BEC’s are recognised by employers, ministries, government bodies and professional organisations throughout the world. This valuable suite of three qualifications provides clear proof that you have the English skills to make you an asset to your employer.</td>
											</tr>
											<tr>
												<td>Business Language Testing Service <br>BULATS</td>
												<td>It tests all the skills and grammar/vocabulary.<br>
													More information at: 
													http://www.bulats.org/ 
												</td>
												<td><b>BULATS</b> is set of language benchmarking, training and assessment tools for measuring the language skills of the workforce, job applicants and students wishing to study business-related subjects. It provides an economical, flexible and easy-to-use approach to specifying and assessing language skills for business and industry. It is available in English, French, German and Spanish.</td>
											</tr>
											<tr>
												<td>Cambridge English: Financial <br>ICFE
												</td>
												<td>ICFE gives an in-depth assessment of learners’ ability to operate in a financial context at levels B2 and C1 of the CEFR– the internationally accepted system for describing language ability.
													The exam takers are tested on all the four skills.
												</td>
												<td>It is directed towards improving the learners’ skills to seek employment or promotion in a finance context, study towards an accounting or finance qualification, improve ability to conduct business internationally or with international clients and colleagues, improve exam and classroom performance in finance and accountancy exams.</td>
											</tr>
											<tr>
												<td>Cambridge English: Legal <br>ILEC</td>
												<td>ILEC is an in-depth assessment of one’s ability to operate in a legal context at levels B2 and C1 of the CEFR. <br>
												The examination covers all four language skills: Reading, Writing, Listening and Speaking. Each test is based on realistic texts, tasks and topics similar to those expected to be encountered when working as a lawyer.</td>
												<td>This is a high-level qualification that will help exam takers to demonstrate that they have the language skills for a successful career in international law.</td>
											</tr>
											<tr>
												<td>Teaching Knowledge Test <br>TKT</td>
												<td>TKT is divided into four separate modules that can be taken all, or just the one/ones that meet the person’s needs. There’s total flexibility in how and when the modules are taken  and a certificate for each one completed will be received.</td>
												<td>TKT has been developed for people who are already teaching, but would like to take an internationally recognised qualification to gain formal recognition for their experience, want to enhance their career opportunities by broadening their teaching experience into specialist areas, or want to keep their teaching skills up to date.</td>
											</tr>
											<tr>
												<td>In-service Certificate for English Language Teaching <br>ICELT</td>
												<td>This is a language and teaching skills certificate.
													ICELT is divided into two modules. Module 1 (Language for Teachers) can be taken as a standalone module or together with Module 2 (Teaching and Methodology) to achieve the full ICELT qualification.
												</td>
												<td>ICELT is ideal for teachers who are already teaching English in a specific context, e.g. teaching adults or young learners in the private sector, teaching within a university environment, teaching primary or secondary school learners. This qualification can help teachers to deepen their knowledge and develop their ability to reflect on and improve your teaching.
												</td>
											</tr>
										</table>
										<br>
										<p>For more in-depth information about each examination, follow this link: http://www.cambridgeesol.org/exams/
										</p>
										<p>These exams are of great importance in a large part of the world and are usually administered by The British Council.
										</p>


									</div>
								</div>

								<div class="panel panel-default">
									<div class="panel-heading">
										<h3>2. Other English language proficiency tests</h3>
									</div>
									<div class="panel-body u3m1selfCheckTable">
										<h4>2.1 TOEFL</h4>
										<br>
										<p>
											The Test of English as a Foreign Language has long been the favourite English language proficiency qualification for foreign students who wanted to study anywhere abroad where English is the language of instruction. More than 8,500 colleges, universities and agencies in more than 130 countries, including Australia, Canada, the U.K. and the U.S.A, recognize it.
										</p>
										<br>
										<table>
											<tr>
												<th>Examination</th>
												<th>Description</th>
												<th>Use</th>
											</tr>
											<tr>
												<td>TOEFL</td>
												<td><p> The TOEFL test measures the candidates’ ability to use and understand English at the university level. It evaluates how well the candidates combine their listening, reading, speaking and writing skills to perform academic tasks.</p>
													<p>There are two formats for the TOEFL® test. The candidates choose the format depending on the location of the test center. Most test takers take the <b>TOEFL iBT (internet-based testing).</b> Test centers that do not have Internet access offer the <b>PBT (Paper-based Test).</b></p>
													<p>Each test component gets a score of maximum 30 in TOEFL iBT test. The maximum total score is 120. </p>
													<p>The TOEFL PBT papers are: Listening: Structure and Written Expression, and Reading, with a total score between 310 and 677. Writing is tested separately in a test called TWE (Test of Written English). There is no speaking test in this format.
														This link will clarify the major differences between these two formats of TOEFL: <br>
														http://www.interface.edu.pk/tests/toefl/toefl-test-formats.asp
													</p>
												</td>
												<td>The TOEFL test is usually taken by students planning to study at a higher education institution. Scholarships and certification candidates, English-language learners who want to track their progress and students or workers applying for visas, also use it.</td>
											</tr>
										</table>
									</div>
									<div class="panel-body">
										<p>TOEFL PBT is soon going to be replaced completely by its iBT format. TOEFL is exclusively a test of American English and culture, although we can see elements of more diverse cultural topics in the reading paper. Its scores are valid for two years.</p>
									</div>
								</div>

								<div class="panel panel-default">
									<div class="panel-heading">
										<h4>2.2. TOEIC (Test Of English for International Communication)</h4>
									</div>
									<div class="panel-body">
										<p>The TOEIC tests directly measure the ability of non native speakers of English to listen, read, speak and write in English in the global workplace. The TOEIC tests are used by over 9,000 organizations around the globe in more than 90 countries to assess English proficiency in the workplace. If you teach in Japan, it is very likely that you will be asked to prepare the students for this examination. Like IELTS and TOEFL, the scores are valid for two years.</p>
									</div>

								</div>
								<div class="panel panel-default">
									<div class="panel-heading"><h4>2.3. TSE (Test of Spoken English)</h4></div>
									<div class="panel-body">
										<p>The Test of Spoken English / TSE examines the "American English" language skills, just like the TOEFL and the TWE. It is required for admission to most of the universities in the US. The TSE tests your oral skills in university simulated surroundings.</p>
										<p>Other English Language examinations include ECPE Michigan, Trinity College London ESOL, Pearson Test of English - General and Academic (former London Test of English), etc. </p>
										<p>Different countries and language schools have a preference for one or another examination depending on its popularity among the students and their parents, on the degree of “difficulty” and not necessarily on the needs of the learners. For example, one of the most popular examinations with Greek students is ECPE Michigan although this qualification is of no real value in Europe. That is why it is crucially important for the English teachers to get detailed information about each of these examinations and the students’ future study plans so that they can provide the best advice on the most appropriate English language qualifications that the students need for their future careers or life in general.</p>
									</div>
								</div>

								<div class="panel panel-default">
									<div class="panel-heading"><h4>3. Comparing the most popular English examinations.</h4></div>
									<div class="panel-body">
										<p>The following table gives an approximate comparison between the different exams. The exams all use the CEFR proficiency levels.</p>
										<br>
										{!! Form::image('https://i.pinimg.com/originals/2f/ba/a5/2fbaa52ae2a666e1147a9828605f7e7c.jpg') !!}
									</div>
								</div>
{{-- problem area --}}
							</div>
						</div>
					</div>
				</div>

			{{-- tab --}}
<div class="tab-pane" id="6a">
	<div class="#">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>Self Check 2</h3>
					</div>
					<div class="panel-body">
						<p>
							What examination would you recommend to the following students?
						</p>
						<ol>
							<li>Mihai is a 10 year old student who has been learning English for four years at school. He and his parents would like to know his level of English in a formal manner so that they know where to go from here. </li>
							<li>Boris is a high school graduate and wants to study Physics at Berkeley College, U.S.A.</li>
							<li>Carmen is a Brazilian nurse who wants to emigrate to Australia.</li>
							<li>Massimo is a lawyer already specialized in International Law. He would like to work at the International Criminal Court in the Hague and he has found an opportunity to achieve his dream.</li>
						</ol>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading"><b>4. What are the differences between teaching general English and teaching for examinations?</b></div>
					<div class="panel-body">
						<p>Normally, there should be no huge difference between the two but given the constraints of time, responsibility, and teaching strategies teaching (or rather training) for examinations has its own specific features. <br><b>Motivation.</b> One of the biggest differences is the level of the students’ motivation, which is usually very high because the stakes are high.
							There is also the financial aspect: the students or their parents have to pay for these examinations and some of them make huge sacrifices to be able to get these language qualifications. 
						</p>
						<p><b>Teacher's responsibility</b> It is not an understatement to say that the candidates’ whole future can depend on getting the right certificate with the right score. Under these circumstances, the teacher has a huge responsibility to help their students achieve their goals. Usually, the teacher is held responsible for the students’ results. A teacher will be evaluated according to his/her students’ results in these examinations so his/her reputation will be directly connected with these results. <br>
							<b>Time:</b> The preparation courses for examinations are usually short. Generally, they take four hours per week for three months, i.e. about 56 hours, whereas the general English courses can vary a lot, from years to short crash courses. Exam preparation courses are usually held at the weekends or in the evenings, so sometimes the teacher needs to teach two two-hour sessions twice a week, or one four-hour session once a week.
						</p>
						<p><b>Tasks and activities:</b> There is a lot of constraint in choosing activities that will engage students emotionally, cognitively, kinesthetically, and so on. Although they might be beneficial for the students, any activity that is not directly connected to the test items in the examination is regarded as useless and a waste of time. In general English courses, the teachers can make use of all their imagination and creativity to make the lessons fun as well as effective, but examination students can become ‘hooked’ on their goal of passing the exams and can become quite stressed if introduced to anything outside the exam syllabus. </p>
							<p><b>Administrative responsibilities.</b> Unlike regular English courses, exam courses may require some administrative duties from the part of the teacher. The teacher may be responsible for the enrollment of the students in the examinations, keeping the students informed about examination locations, dates, times, organization of transportation to the exam site, regulations of the exam itself, etc.</p>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading"><b>5. How to teach for examinations?</b></div>
					<div class="panel-body">
						<ul class="cleanList">
							<li>a) The teachers’ first responsibility is to know the examination they are going to teach for inside out. First, the teachers need to know the use of the examinations to be able to give the prospective candidates the best advice on which best suits their needs. They also need to get informed about the types of papers (exam components) and their number, how long each paper takes, the times when these examinations are taken, the closest locations, the administrative bodies (e.g. British Council, a university). Information about how each paper is graded and the rubrics for both speaking and writing have to become a teaching tool.</li>
							<li>b) Besides reading about the examination on the official websites and talking to colleagues who have taught for it, the teachers need to take a practice exam themselves under exam conditions to get familiar with the format of the exam and try to predict the students’ main difficulties with it (e.g. reading in a very limited time).</li>
							<li>) Before the students enroll on such a course, it is best for them to take a “mock” examination that will serve as a diagnostic/prognostic test. Thus, the teacher will know the chances a particular student has to at least get a pass score.  For some students, it might be important that they get the highest possible score in that exam. For example, the university they want to study at requires a CAE with an A, not a B or a C, which are pass scores. What is important to know is that a 56-hour course can bring a student who got a C in the mock test to a B, but not to an A. So, if that student needs an A, he or she will have to take two preparation courses. Naturally, there are exceptions to the rule, but it is better to be safe than sorry. 
							A mid-course mock test is also advisable for both the students and the teacher to measure the progress made and to identify areas that need more attention. A mock exam at the end of the course before the examination itself is compulsory to draw all the strings together again.
							</li>
							<li>d) The teacher must know the grammar of English very well to be able to explain any point clearly and briefly when asked by a student, besides teaching it both formally and sometimes informally during the grammar slots of a lesson. Grammar must be taught in the c) ontext offered by the units in the course book.</li>
							<li>e) The teacher should guide students to learn independently. They should encourage the students to read extensively, to watch quality movies (a list of such movies would not be a bad idea), to listen to the radio in English (recommended list of channels would help). The students need a very good dictionary such as Oxford Advanced English Dictionary, or Longman Dictionary of Contemporary English. The students who have iPads or iPhones can buy the applications for these dictionaries and many other study materials, including Oxford A-Z Grammar and Punctuation and professional dictionaries. One of the most stunning dictionary applications is Oxford Wordflex Touch Dictionary, which combines an interactive dictionary with a thesaurus in the form of mind maps organized around topics.
</li>
							<li>f) There is a lot more feedback and “grading” involved than in regular courses, so the teachers need to be prepared to spend time outside class. Because of the amount of preparation, “mock” testing, and one-to-one feedback, the groups must be kept small. An average of six students per group would be ideal.  One-to-one exam teaching should be avoided, if possible, but pairs are acceptable. One reason for this would be the speaking papers in the Cambridge ESOL examination, which have an interactive part (the speaking examination is done with pairs of students who, besides the long turns, have to interact to “solve a problem” using English in their discussion).</li>
							<li>g) Last but not least, besides language teaching, the students need training in examination taking skills. This will be done on a regular basis through exercises and tasks that replicate the test items but also through specific activities that will train the students to cope with the difficulties that might arise during the exam (e.g. not remembering a key word during the speaking examination, or not being able to decide between two possible answers in a multiple-choice reading task). The ones who take the computer-based examinations or the online ones need special training as well.</li>
						</ul>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading"><h4><b>Websites with information about different international English examinations</b></h4></div>
					<div class="panel-body">
						<p>Cambridge ESOL Examinations: <br>http://www.cambridgeesol.org/index.html <br>TOEFL and TOEIC plus a number of other American examinations for college placements (SAT, GRE) <br> http://www.ets.org/tests_products
						</p>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading"><h4><b>Course books for different examinations:</b></h4></div>
					<div class="panel-body">
						<p>
							Course books for Cambridge ESOL examinations: <br>
							You can use this link to find the right course books for your examination classes: <br>
							http://www.cambridgeesol.org/exam-preparation/books-study.php <br>
							Follow this link with recommendations for TOEFL preparation: <br>
							http://gmatclub.com/forum/best-toefl-preparation-books-resources-reviews-comments-79121.html

						</p>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading"><h4><b>Preparation for examinations websites that can be used by the students independently</b></h4></div>
					<div class="panel-body">
						<p>http://www.flo-joe.co.uk/</p>
						<p>http://www.examenglish.com/ (free practice tests)</p>
						<p>There is a wealth of exam preparation sites on the Internet. The ones mentioned above stand out among many others.</p>
						<p>There are also a huge number of iPad and iPhone applications for examination preparation. The free ones are fine but the best are the ones that cost some money but they might be worth the investment.</p>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
{{-- tab --}}
<div class="tab-pane" id="7a">
	<div class="#">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>Answers to Self Checks</h3>
					</div>
					
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Self Check 1</h4>
					</div>
					<div class="panel-body">
						<ol>
							<li>Common European Framework of Reference for Modern languages</li>
							<li>CEFR is framework of reference designed to provide a transparent, coherent and comprehensive basis for the elaboration of language syllabuses and curriculum guidelines, the design of teaching and learning materials, and the assessment of foreign language proficiency. </li>
							<li>It is used in Europe but also in other continents.</li>
							<li>No. As the name suggests, it provides a framework to measuring the proficiency in a number of other languages learnt by the students as second or foreign languages all over the world.</li>
						</ol>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Self Check 2</h4>
					</div>
					<div class="panel-body">
						<ol>
							<li>Mihai: one of YLE examination (Movers) or KET for Schools.</li>
							<li>Boris: IELTS Academic, TOEFL iBT, or CPE. High scores are crucial.</li>
							<li>Carmen: IELTS Academic.</li>
							<li>Massimo: Cambridge English: Legal (ILEC).</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
{{-- tab --}}
<div class="tab-pane" id="8a">
	<div class="#">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>Grammatical Approcahes</h3>
					</div>
					
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Use of Mother Tongue</h4>
					</div>
					<div class="panel-body">
						<p>One of the most frequently asked questions posed by those considering a career in TEFL is - 'Do I have to speak their language?' The stock answer is 'Only for your personal life and to get about in the country'. We also believe that learning your students' mother tongue when you are in their country shows that you are serious about living and teaching there and that you do not expect everyone to speak your mother tongue whilst making no attempt to learn theirs.
						</p>
						<p>In class however you do not need to speak the students' language. This would, in fact, be an impossibility if you were teaching a mixed nationality class such as a summer school in which you may have 10 or more different nationalities.</p>
						<p>Having said that, if you are teaching higher levels and you are finding it difficult to get the meaning of a word across to your students, why waste
						time miming, drawing pictures etc? If you know the word in their language tell them, it's quicker!
						</p>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading"><h4>Grammar</h4></div>
					<div class="panel-body">
						<p>If you are a native speaker, you 'know' the grammar of English perfectly - that is, you are able to produce an infinite number of correctly formed sentences. Your problem is to see it as a foreign language; to know about and be able to explain its most characteristic systems and classes and to understand the problems they pose to a foreign learner.</p>
						<p>Do not depress yourself with the idea that there is somewhere one ultimate set of rules which all experienced EFL teachers and applied linguists 'know' and which you must learn. This is not so. There are many descriptions of the grammar, fashions change, and one description may be better than another in some areas.</p>
						<p>However, you must know your stuff. You don't want the students to know more than you do or you will lose credibility and they will eventually not have enough confidence to succeed.
						</p>
						<p>Beginners do not need the terminology, advanced classes do and they will ask for it. More important than the terminology however is the teacher's ability to explain and to give examples on request.</p>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading"><h4>Grammatical Approaches</h4></div>
					<div class="panel-body">
					<p>
						Grammar Translation as a Teaching Method - The use of the word 'grammar' in the grammar translation method, to a large extent means the grammar of words. Tables showing:- walk, walks, walked, and boy, boys, boy's, boys' are presented. Words are classified into parts of speech and functional categories are defined eg subject, object etc. Then rules are given re: the usage and combination of these elements into clauses and sentences. This is not very helpful to the learner of a foreign language who tries to translate parts of a text, using pieces of grammatical information - he / she may eventually know what was meant but does not acquire any amount of fluency in speaking, listening or writing the language. Grammar-translation has its place but is not widely used today in teaching foreign languages.
					</p>
					<p>The Structural Approach - Grammar also plays a leading part here, the structure being referred to, being a grammatical structure. Course writers list what they consider to be the most important grammatical elements. Here the terminology is unimportant, but the patterns of the grammar are, of course, very important. The grammatical elements are placed in a practical order for teaching purposes and each is taught, practised and drilled. This method is still widely used, though when used alone can become repetitive and boring. However, beginners are taught by this method as no other has been proven, at this level, to be as good.</p>
					<p>Command of grammatical structure is essential to complete understanding and any form of meaningful communication in ESOL. Good teachers and materials writers do not use mindless drills (which are also boring) but include situational practice, always checking the student's understanding of what he is repeating.</p>
					<p>As language teachers, how much of this theory do we need to know? Unless you are going to continue your studies, fortunately very little. The last sentence of each paragraph however is of utmost importance if we are to do the job well! We are teachers of COMMUNICATION, our aim therefore is to facilitate this communication, this will not happen through translation or through mindless repetition of grammatical structures. Students need to know not only the correct grammatical structure but also where and how to use it.</p>
					<p>The communicative approach - It is important to keep reminding ourselves that we are teaching communication. We rarely need to teach declensions and verb lists as in the Latin or even French classes some of you may remember from your schooldays. We are teaching non-native speakers of English to effectively communicate with native speakers of English or other non-native speakers of English. (As an essential language of the world of commerce, English may necessarily be the language of communication between different nationalities none of whom have any other language.)</p>
					<p>If you, yourself have not had the experience of being in a position in which you HAD TO communicate in a foreign language (one in which you are not fluent) you will doubtlessly have observed others in that position. They adopt different styles according to the situation, the audience, their own personality etc. Some seem to relish the task and throw themselves into speaking with as much 'fluency' as they can achieve with the string of mistakes they make along the way. Others freeze, too afraid to speak in case the listener cannot understand due to the mistakes they fear they will make.
					</p>
					<p>Whichever way they tackle the task, we know that most students of EFL want more than simply to communicate. They want to communicate in good English - why otherwise would they bother with classes year after year? They do not want to be laughed at, they do not want to have to resort to paralinguistics (facial expression and body language) or to have to point and mime. They want their English to be good enough to allow them to communicate effectively in correct English.</p>
					<p>As teachers of EFL we are happy with the concept of 'correct' English. It is our job to know what is correct and what is incorrect, and to know that when our students are not making correct utterances, they should be made aware of this fact. However, we must allow students to communicate - a balance must be struck between effective communication and correctness. We cannot be seen to accept an incorrect sentence offered by student A, when student B, or in fact student A after further thought or a look in his book, is likely to challenge your acceptance. Similarly we must prevent students from continuing to make the same mistakes which an examiner will mark wrong. On the other hand, mistakes are inevitable, even from the most advanced students, but to constantly respond each time a mistake is made would be very off-putting for your students, and fluency would be impaired.</p>
					<p>Classroom technique is of immense importance to good, effective, communicative teaching. This is dealt with in Unit 4, Module 1.</p>
					<p>Striking the balance between communication and correctness can be difficult, but the following features should be kept in mind if we are to effectively teach communication.
					</p>
					<ol>
						<li>Communication is passing information to somebody.</li>
						<li>Communication is saying what you want to say, rather than what you  
						    are told to say.
						</li>
						<li>Communication is saying what is true and meaningful rather than 
						     what is linguistically correct.
						</li>
						<li>Communication is producing authentic English rather than textbook              
						    English.
						</li>
						<li>Communication is paying attention to what people mean rather than 
						     how they express themselves.
						</li>
					</ol>
					<p>A great deal of research and work in this field has been going on over the last 20-30 years. The above features of the 'communicative movement' are included here for you to incorporate in your thinking when planning lessons, though not to the exclusion of all else, including correctness.</p>
					<p>The 'communicative movement', we may feel have overdone it a bit in their concern with communication, but this persistent concern has produced a technique which has useful applications. Communication, by definition, is the transfer of information from source to receiver; that information, by definition, is not already known to the receiver. A communicative exercise, therefore, can be set up by ensuring that student A has some information which student B does not have, and then prompting an exchange of information.</p>
					<p>The simplest way to do this is to get the students to elicit information from you, the teacher. You set the scene - ie tell them what they must elicit from you, they can then ask questions at their own level:-  
					eg  -  beginners.
					</p>
					<p>Get the students to find out your hobby by asking simple questions which require yes/no answers (you are less likely to get mistakes from beginners if you use this restriction, otherwise they are likely to get carried away in their eagerness to find out about you).</p>

					<p>Possible questions</p>
					<ol>

						<li>Do you like to swim?</li>
						<li>Do you do it inside?</li>
						<li>Do you make soomething?</li>
						<li>Are you tired afterwards?</li>
						<li>Is it a sport? etc.</li>
					</ol>
					<p>Intermediate and advanced students can cope with something far more complex and with 'free' questions in order to elicit the required information.</p>
					<p>Another way is to get the students to elicit information from each other. This is more complex to set up, and needs planning, but will stimulate your students, will liven up a dull class, and will often prompt the students to ask for further information from each other - this is invaluable in a mixed nationality class.
					</p>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
{{-- tab --}}
<div class="tab-pane" id="9a">
	<div class="#">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>Teaching Skills and Techniques</h3>
					</div>
					
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Modelling</h4>
					</div>
					<div class="panel-body">
						Clear presentation is essential in the classroom. Instructions can be effectively reinforced by showing students what is expected of them, ie modelling. When you model an activity, everyone can clearly see what is expected of them. Modelling can be much more effective than lengthy instructions which can be confusing.
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading"><h4>Chorusing</h4></div>
					<div class="panel-body"><p>
						Chorusing is simply the repetition of words and expressions.It plays two important roles in the classroom: 1) It provides students with a clear model for correct pronunciation and stress, and 2) It helps students to memorise new vocabulary. Just because a student hears a word modelled correctly (ie correct pronunciation & stress), does not mean that he will be able to produce it correctly if he is not given the opportunity to practise.
					</p></div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading"><h4>Chorus, Isolate, Confirm (C.I.C.)</h4></div>
					<div class="panel-body"><p>
						This is a method of chorusing entire sentences with the aim of promoting sentence fluency. It focuses on fluency, intonation and liaison (liaison is the running together of words during normal speech, eg ‘What do you do?’ becomes, ’Whada-yado?’)
					</p>
					<p>
						Firstly, the entire sentence is chorused once, and then working backwards from the end of the sentence individual sounds rather than individual words are chorused until the sentence is built back up. The complete sentence is then chorused again. There should be a marked improvement. This is an important tool for dialogue practice and can be used at any level.
					</p></div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading"><h4>Drilling</h4></div>
					<div class="panel-body"><p>Drilling is a controlled practice exercise allowing controlled practice of new structures before moving on to freer practice. There are many different ways of drilling; it’s aim is to ensure that students are able to use new structures correctly. Here is an example: after teaching the question, ’What’s your favourite…?’, the teacher substitutes a variety of pre-taught nouns, eg food, fruit, animals etc.
						Then the teacher provides students with the opportunity to practise the structure by repetitively asking and eliciting responses from the group until they are able to use the structure fluently.
					</p>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading"><h4>Boardwork</h4></div>
				<div class="panel-body"><p>As teachers we should not forget the importance of the whiteboard in the classroom. It is an important visual aid which serves to reinforce the language and instructions and when used effectively can greatly aid teaching. As well as presenting structures and vocabulary, diagrams and pictures can be drawn to clarify meaning. Pictures and diagrams can assist us in the same way as modelling does; a simple picture can be a lot more effective in demonstrating meaning than a long and complex explanation. A good teacher develops his/her own style and layout which should be the same for every class. It is useful to divide the board up into different areas, eg new vocabulary, target language, exemplifications etc.</p></div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading"><h4>Instructions</h4></div>
				<div class="panel-body"><p>This is one of the most important teaching skills, but is an area where many teachers experience difficulties.
				</p>
				<p>Good instructions should be brief; longer and longer instructions given in the attempt to overcome confusion often have the reverse effect and merely lead to further confusion. Avoid unnecessary language.</p>
				<p>The teacher should a use a set of instruction vocabulary which doesn’t vary; students should be introduced to these at the start of any course. Examples of basic classroom instruction vocabulary are: sit, stand, listen, ask, answer, open your books, work in pairs etc. When introducing these instructions it is often beneficial to use them in conjunction with gestures to demonstrate meaning.</p>
				<p>It is important to prepare and check your instructions before lessons to avoid any potential pitfalls.</p>
				<p>Remember to never use pidgin English in the classroom; we should never model bad English to our students.
				</p>
				<p>Attention signals are effective time saving devices which can be used in the classroom to convey instructions. For example, attention signals can be used for: repeat, be quiet, stand up/sit down, stop, listen, write etc.
					Gestures can also be invaluable in the classroom. They are different from attention signals in that they do not convey instructions, but rather add meaning to the spoken word. Gestures can be used to express encouragement, confusion, praise etc. For example the thumbs up signal to express ‘good’, open hands facing up to express that you don’t know, or don’t want to say, raised eyebrows to express surprise etc.
				</p></div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading"><h4>Concept Checking Questions (C.C.Q.’s)</h4></div>
				<div class="panel-body"><p>
					We use CCQ’s to establish meaning and check student understanding. They can be applied both to grammar (structures / tenses/functional language), and vocabulary (words / phrases). They look at such issues as concept, context, use (eg function, style, relationships between people etc) and time reference as applicable. CCQ’s should check and develop students’ understanding by the asking of carefully planned specific questions.
				</p>
				<p>eg  Vocabulary: witty;  ‘He’s a witty person’</p>
				<table>
					<tr>
						<td>‘Does he make people laugh?’ </td>
						<td>YES</td>
					</tr>
					<tr>
						<td>‘Would he be a good guest at a party?’ </td>
						<td>YES</td>
					</tr>
					<tr>
						<td> ‘Is that because he’s clever or silly?’   </td>
						<td>CLEVER</td>
					</tr>
					<br>
					<tr>
						<td>What, when, why, how, where, which questions</td>
						<td> (ie open questions)</td>
					</tr>
					<tr>
						<td>How does he make people feel? </td>
						<td>HAPPY</td>
					</tr>
					<tr>
						<td>What does he make people do?  </td>
						<td>Laugh</td>
					</tr>
				</table>
				<p>Once you have verified the students’ comprehension you can ask
					students to personalize the vocabulary/structure.
				</p>
				<br>
				<ul class="cleanList">
					<li>Do you like witty people?’ (why / why not?)</li>
					<li>Do people think you are witty?</li>
					<li>Can you think of a witty person you know?</li>
				</ul>
				<p>eg Grammar: used to.. ; ‘I used to smoke’.</p>
				<table>
					<tr>
						<td>Are we talking about the past, present or future?</td>
						<td>PAST</td>
					</tr>
					<tr>
						<td> Did I smoke before?  </td>
						<td>YES</td>
					</tr>
					<tr>
						<td>Did I smoke regularly? </td>
						<td>YES</td>
					</tr>
					<tr>
						<td>Do I smoke now? </td>
						<td>I GAVE UP</td>
					</tr>
				</table>
				<p>Personalized questions once understanding has been established:</p>
				<ul class="cleanList">
					<li>Have you ever smoked?</li>
					<li>When did you give up?</li>
					<li>How difficult was it?</li>
					<li>What did you use to do 5 years ago that you don’t do now?</li>
				</ul>
				<br>
				<p>
					We should avoid questions that waste time and that don’t focus on important issues or clarify meaning. We should not use the target language in our questions, except when practising, ie personalizing.)
				</p>
			</div>
		</div>
	</div>
</div>
</div>
</div>



{{-- tab --}}

<div class="tab-pane" id="9a">
	<div class="#">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>Pairwork and Groupwork</h3>
					</div>
					<div class="panel-body">
						<p>
							It has been argued that language acquisition will only occur when students are exposed to, and have ample opportunity to use the target structures of any lesson in a meaningful and interactive way. It is necessary to provide students with such opportunities through the use of communicative, task-based activities in the EFL classroom; this necessitates the use of pair work and groupwork. Pair work and groupwork can be used for controlled practice of vocabulary and structures, or to provide opportunities for authentic, communicative language use. Any pair work activity is part of a continuum, with initial controlled practice activities leading to more communicative freer practice. Many task-based pair work activities fall somewhere between the two.
						</p>
						<p>
							There are many possibilities for how groupwork and pair work can be used in the classroom; here are a few examples of commonly used activities:
						</p>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading"><h4>Predicting</h4></div>
					<div class="panel-body"><p>
						During group lead-ins, predictive exercises in pairs or groups can be used to introduce a topic and to encourage students to think about what they might encounter in upcoming exercises.
					</p></div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading"><h4>Brainstorming</h4></div>
					<div class="panel-body"><p>
						 Brainstorming is useful for activating prior knowledge and ideas. It can be used to generate vocabulary or ideas and interest in a topic. It ensures that all students are actively involved. A competitive element can be introduced to promote fun and motivation. The information generated is then fed back to the whole group.
					</p></div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading"><h4>Information-gap & Jigsaw Activities</h4></div>
					<div class="panel-body"><p>
						Information-gap exercises can be structured to practise very controlled language structures or to provide real communicative opportunities. Jigsaw activities are a special from of information-gap exercise where key information required to complete a task is divided between two students. The students must pool the information in order to successfully complete the task; they must both give and seek information.

					</p></div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading"><h4>Opinion and Exchange</h4></div>
					<div class="panel-body"><p>
						Students are asked to give and discuss opinions. When carried out in pairs it ensures that all students have the opportunity to express themselves and avoids discussions being dominated by more vocal students. It is also less threatening and can thus help to build student confidence.
					</p>
					<p>Role-Plays: Students are given situations and roles and a task or objective to be accomplished. They are a way of recreating real life situations in the classroom, and can be both effective and enjoyable.</p></div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading"><h4>Discussion</h4></div>
					<div class="panel-body"><p>Discussions can be carried out in pairs or groups, and provide good interactive language opportunities.</p></div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading"><h4>Games</h4></div>
					<div class="panel-body"><p>Games are usually carried out in groups, but can sometimes be adapted to pair work. They can provide students with opportunities to practise language and vocabulary in a fun and relaxed way, and they can add variety to lessons. They should have a purpose and be used to meet the objectives of the lesson.</p></div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading"><h4>Projects</h4></div>
					<div class="panel-body"><p>
						Giving students projects to work on in pairs or small groups can be very effectively used in the classroom and can provide useful opportunities to develop a variety of communicative skills, eg negotiating, agreeing / disagreeing etc. 
					</p></div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading"><h4>Pair Work</h4></div>
					<div class="panel-body"><p>
						Pair work needs to be carefully set up in the classroom. Students need to understand the objectives of the activity, the language to be used, their roles and who they are to work with. To make pair work successful the teacher should prepare and model the activity first. The teacher should carefully monitor students during pair work activities ready to give feedback.
					</p>
					<p>The teacher should consider the fact that different students work at different rates, which can cause a problem if not carefully handled. For example, students who have finished an exercise early may become noisy and disruptive or feel neglected if they are left waiting for other students to finish the activity. Therefore, it is important to plan extension activities which students can work on while waiting for other students to finish.</p>
					<p>Pair work activities which mimic real communication and have a purpose or goal provide students with meaningful communicative opportunities which prepare them for the ‘real world’ outside the classroom. Communicative pair work exercises which allow for choices of what to say promote active listening and appropriate responses thus mimicking real conversation.
					</p>
					<p>
					Pair work helps to build class rapport and relationships within the classroom. For students who are shy, pair work provides them with a safe environment in which they are given the opportunity to contribute while not being the centre of attention.
					</p>
					<p>
					Pair work provides the opportunity to practise a variety of communicative skills, eg turn-taking, negotiating, agreeing and disagreeing with others opinions, etc.
					</p>
					<p>
					Pair work provides the opportunity to cater to different student levels within the same class.
					</p>
					<p>
					Pair work is student centred rather than teacher centred. The amount of speaking practice dramatically increases when pair work and groupwork are used in the classroom.
					</p></div>
				</div>

			</div>
		</div>
	</div>
</div>
{{-- tab --}}

<div class="tab-pane" id="10a">
	<div class="#">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h4>Teaching Grammar and Vocabulary in Context</h4></div>
					<div class="panel-body">
						<p>
						It is important to ensure that all language learning is effective and meaningful. This means that new words and grammar should always be introduced in the context of meaningful, realistic interactions. If grammar and vocabulary are taught in isolation without showing how they are used in context, students will not be able to learn how to use them in any meaningful way. When writing new vocabulary on the board for example, always write the word within the context of a sentence and underline the word.
						</p>
						<p>Of course teaching grammar plays a central role in the EFL classroom. However, it is not just a case of explaining grammar rules to the students; teaching grammar effectively is more complicated than that. First you need to consider the objectives of the class, the students’ learning styles, student age group, materials and resources available, students’ current knowledge etc. In other words, each class is going to have different grammar needs and goals, and it is up to the teacher to determine those goals and provide the means with which to meet them.</p>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading"><h4>Inductive and Deductive Grammar Teaching</h4></div>
					<div class="panel-body">
						<p>Inductive is known as the ‘bottom up’ approach. In other words, students discovering the grammar rules while working through exercises.
						eg following a reading comprehension incorporating the simple past and present perfect tenses, the teacher could ask questions such as, ’Has she ever been to London?’; ’When did she go?’ etc. Then to help students inductively understand the difference between the two tenses, these questions could be followed by asking the students to identify which questions spoke about a definite time in the past and which questions spoke about a person’s general experience.
						</p>
						<p>Deductive is known as the ‘top down’ approach. This is the standard teaching approach where the teacher introduces and explains the grammar rules to the student. eg the teacher starts by explaining that the present perfect tense is made up of the auxiliary verb ‘have’, plus the past participle, and continues to explain that it is used to explain an action that has begun in the past and continues to the present moment…..etc.
						An inductive approach means that the teacher is working to facilitate learning.  
						</p>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">One to One</div>
					<div class="panel-body"><p>
						One to one offers a unique teaching situation with great potential. It offers a real challenge to the teacher, but also an unrivalled opportunity. The content of a one to one teaching session is determined to a large extent by the student. The teacher becomes less of a controller or manager and needs more to respond to changes in demands imposed by the student.
					</p>
					<p>It is however still an artificial situation. Two individuals who have never met and may have little in common spend a lot of time together in a confined space. It is essential that the teacher is aware of this and is able to create space for learning and not expect the student to be totally active. Silence can be difficult in any teaching, but never more so than in a one to one situation. Students need time to read through work, formulate answers etc. and the teacher needs to create the necessary space for this to happen and also to take account of the learner's style.</p>
					<br>
					<p><i>Different expectations</i></p>
					<p>One to one teaching in a business/language school setting is often in a small, cramped room and comes with certain constraints and expectations as it is often billed as 'intensive'.</p>
					<p>One to one in a homestay / private teaching setting is much more relaxed. The host teacher chooses the room and the student determines the pace.</p>
					<p>It is true to say that there is ‘no escape’ in one to one teaching. It is demanding and can be very tiring, but the advantages for the student are many:</p>
					<ol>
						<li>The individual learning style, personality and level of the student           
      can be taken into consideration
</li>
						<li>The student can set the pace, this is not only possible, but   
           necessary
</li>
						<li>The teacher can offer a choice of pattern/timetable in the working  
      day
</li>
						<li>Breaks can be taken when the time suits
</li>
						<li>There are fewer time constraints on the length of tasks and    
           lessons
Tailor-made materials can be prepared and revised to suit the 
      individual
</li>
						<li>The student can have choice and can even supply input material</li>
						<li>The teacher can arrange visits and set up situations to suit the      
           individual
</li>
						<li>Teaching aids can be ‘hands-on’</li>
						<li>The teacher can constantly monitor and feedback on the progress  
           of the student
</li>
						<li>Communication is authentic at all times</li>
						
					</ol>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="tab-pane" id="11a">
	<div class="#">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>Task</h3></div>
					
				</div>
				<div class="panel panel-default">
				
					<div class="panel-heading"><h4>Choose the correct answer or answers in the following:</h4></div>
					<div class="panel-body">
						{!! Form::open(['action' => 'Admin\Int120u3m1s1t2Controller@store', 'method' => 'POST']) !!}
						<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">
						
						<p>1 Which approach does this course recommend?</p>
						<ul class="cleanList">
							<li>a) Notional <span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q5', 0, false) }}</li>
							<li>b) Communicative <span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q6', 0, false) }}</li>
							<li>c) Grammar-translation <span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q7', 0, false) }}</li>
							<li>d) Structural <span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q8', 0, false) }}</li>
						</ul>
						<p>2 Which of the following is least important in your teaching?</p>
						<ul class="cleanList">
							<li>a) Fluency in your student's language <span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q9', 0, false) }}</li>
							<li>b) Planning lessons <span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q10', 0, false) }}</li>
							<li>c) Knowing more about English grammar than your students do <span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q11') }}</li>
							<li>d) Making your students relax <span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q12', 0, false) }}</li>
						</ul>
						<br>
						<p>3 Which of the following activities should all ESL teachers be able to use effectively?</p>
						<ul class="cleanList">
							<li>Pair work <span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q13', 0, false) }}</li>
							<li>Role play<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q14', 0, false) }}</li>
							<li>Translation<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q15', 0, false) }}</li>
							<li>Eliciting student's response<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q16', 0, false) }}</li>
						</ul>
						<br>
						<p>4 Which of the following will keep the students interested?</p>
						<ul class="cleanList">
							<li>Varying your activities<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q17', 0, false) }}</li>
							<li>Getting the students talking<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q18', 0, false) }}</li>
							<li>Using a listening activity in every lesson<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q19', 0, false) }}</li>
							<li>Involving the students<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q20', 0, false) }}</li>
						</ul>
						<br>
						<p>5 Which of the following is correct?</p>
						<ul class="cleanList">
							<li>You should always mark with a red pen<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q21', 0, false) }}</li>
							<li>You should always acknowledge effort<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q22', 0, false) }}</li>
							<li>You should correct all mistakes<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q23', 0, false) }}</li>
							<li>You should explain your correction code to your students<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q24', 0, false) }}</li>
						</ul>
						<br>
						<p>6 Which of the following is most important in communicative language teaching?</p>
						<ul class="cleanList">
							<li>Knowledge of the theory behind TEFL<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q25', 0, false) }}</li>
							<li>Students knowing the grammatical terminology<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q26', 0, false) }}</li>
							<li>Facilitating communication<span class="tab-space"></span>   {{ Form::checkbox('u3m1s127') }}</li>
							<li>Knowledge of all grammar before you start teaching<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q28', 0, false) }}</li>
						</ul>
						<p>7 Which of the following is an essential requirement of the Communicative Approach to language teaching?</p>
						<ul class="cleanList">
							<li>Knowing your students' language<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q29', 0, false) }}</li>
							<li>Use of authentic English<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q30', 0, false) }}</li>
							<li>Knowledge of grammatical terminology<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q31', 0, false) }}</li>
							<li>A good textbook<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q32', 0, false) }}</li>
						</ul>
						<p>8 Which of the following are advantages for one to one students?</p>
						<ul class="cleanList">
							<li>communication is authentic<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q33', 0, false) }}</li>
							<li>the student sets the pace<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q34', 0, false) }}</li>
							<li>the individual learning style is immaterial<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q35', 0, false) }}</li>
							<li>materials can be tailor-made<span class="tab-space"></span>   {{ Form::checkbox('u3m1s1q36', 0, false) }}</li>
						</ul>
					{!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}
												{!! Form::close() !!}
					
				</div>
			</div>
		</div>
	</div>
</div>
{{-- end section --}}
</div>


@endsection