@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('content')

<div class="panel panel-default">
	<div class="panel-body">Unit Three Module Five</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading"><h3>Unit 3 | The Teaching and Learning of EFL</h3></div>
	<div class="panel-body"><h4>Module 5 | Error</h4></div>


	<div class="panel-body" id="exTab1">
		<ul  class="nav nav-pills">

			<li><a  href="#1a" data-toggle="tab" style="background-color:#fff;">Info</a></li>

			<li><a href="#2a" data-toggle="tab" style="background-color:#fff;">Psycholinguistics</a></li>
			<li><a href="#3a" data-toggle="tab" style="background-color:#fff;">Attitudes towards error</a></li>
			<li><a href="#4a" data-toggle="tab" style="background-color:#fff;" class="orangeText">Self Check</a></li>
			<li><a href="#5a" data-toggle="tab" style="background-color:#fff;">Preparing remedial exercises</a></li>
			<li><a href="#6a" data-toggle="tab" style="background-color:#fff;" class="blueText">Task</a></li>		
		</ul>

	</div>
</div>

<div class="tab-content clearfix">

	<div class="tab-pane" id="1a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">Info</div>
						<div class="panel-body">
							<h4>At the end of this unit you will:-</h4>
							<ul>
								<li>a) recognise errors</li>
								<li>b) have some understanding of why students make them</li>
								<li>c) be able to prepare useful remedial exercises</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="2a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>PSYCHOLINGUISTICS</h4>
						</div>
						<div class="panel-body">
							<p>
								Psycholinguistics is the study of language acquisition. It brings together the theories of psychology and linguistics in order to carry out this study. This is truly an interdisciplinary field. Linguists study the structure of language, ie sounds and meanings, and the grammar to which they relate, and they come together with psychologists who investigate how people acquire the structures and functions, and use them in speech and understanding.</p>
								<p>
								A knowledge of the difficulties of learning a foreign language is built into some degree courses in EFL and Applied Linguistics by the syllabus including learning a foreign language from scratch in order to study the process by which we are taught and by which we learn (psycholinguistic study). We are not proposing this here, but if you have ever learned a foreign language, draw on your own experiences when you are teaching. How did you learn? How did you not learn?</p>
								<p>
								This knowledge of the difficulties in learning a foreign language, and consideration of the possible causes of error should lead you, as a teacher of EFL, to develop a helpful attitude towards your students. Your students need to be confident enough, and 'uninterrupted' enough to be fluent, while at the same time, they need to know that they will be corrected and not allowed to continue making the same mistakes. Fluency and accuracy are the aims, effective communication, the ultimate goal, being achieved by a blend of the two.

							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="3a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Error</h4>
						</div>
						<div class="panel-body">						
						<h4>ATTITUDES TOWARDS ERROR</h4>
						<p>People from different walks of life have different attitudes towards error</p>
						<p>a) You can react as a linguist, ie "This is interesting, maybe we should not call this an error, but a stage in the learning process."</p>

						<p>b) You can react as a teacher, ie "Oh no, what will he achieve in his exam?"</p>

						<p>c) You can react as a member of the public, ie "This English is very poor", "This person is illiterate", "What do they teach in schools these days?"</p>

						<p>(Many members of the public just assume that foreigners are not so intelligent because they do not speak THE language, ie our language, English. We have indeed been somewhat spoiled by others needing to learn English as the language of commerce.)
						</p>
					</div>
					<br>
					<div class="panel-body">
					<h4>ERROR ANALYSIS</h4>	
					<p>Error analysis involves collecting and classifying errors, and suggesting possible causes.</p>


					<p>We, as teachers of EFL, do not normally need to formally analyse these errors unless we become involved in English Language Research. However, small scale surveys may help us to see patterns which, ultimately, will help us to help our students to avoid making these errors.
					</p>
					<p>
					Teachers study errors in order to give relevant help and to aid planning for future lessons. A study of students' errors shows the students' current problems and helps us to plan remedial work. This may be carried out informally, based on written tasks, for example - many of your students are confusing simple past and past perfect tenses - you notice this in their homework and devise an exercise which will hopefully help to eliminate the problem.</p>


					<p>More formal collection of data may reveal that only a small section of your class is actually making this mistake. In this case, take that group separately, or write them a homework exercise so as not to bore the rest of the class.</p>

					<p>
					Often EFL teachers have no time to formally count the number of students making particular errors, however a survey of the errors of one class may save you time by helping you to predict the most likely areas of difficulty for a parallel or future class. It is, of course, essential to remember that the setting, the class mixture, the age, the native language and teaching background will have an effect on the learning of the class, but it is also useful to know that students with similar backgrounds, nationalities etc. are likely to have similar problems with particular areas of language.</p>

					<p>
					Analysis of errors may indicate areas of language which require special attention or extra practice. It may also reveal teaching techniques which are not working as well as they might, or an order of presentation which may need adjustment.

					</p>
					</div>
					<div class="panel-body">
						<h4>DATA</h4>
						<p>
							Some theorists name the collection of data for analysis as 'Performance Analysis' rather than 'Error Analysis'. The term 'Performance Analysis' refers to the collection of errors and of examples of correct usage. Error Analysis, in theory, should also include examples of correct usage because we need, as teachers, to consider the overall performance of our students and not just where they are going wrong. However, I say 'in theory' because teachers being engaged in any sort of formal collection of data is unusual, but teachers with time to carry out complete, formal 'performance analysis' are very rare indeed, unless they are becoming involved in linguistic research.
						</p>
					</div>
					<div class="panel-body">
						<h4>INTERPRETATION OF ERRORS</h4>
						<p>For our purposes, we only need look at the work of our students, note common errors and begin to look at:</p>
						<ul>
							<li>a) by whom they are being made</li>
							<li>b) why they are being made</li>
							<li>c) possible remedial exercises</li>
							<li>d) how we can help future students to avoid these errors</li>
						</ul>
						<p>This is, in fact, often referred to as 'interpretation of error', not analysis, again another linguistic argument could develop here!</p>
					</div>
					<div class="panel-body">
						<h4>CLASSIFICATION OF ERRORS</h4>
						<p>Here is a method of classifying written errors which is simple yet comprehensive and can be understood by the students. <br>
						Mark the errors with the following classification:- 
						</p>
						<table>
							<tr>
								<td>G</td>
								<td class="tab-space">grammar</td>
							</tr>
							<tr>
								<td>SP</td>
								<td class="tab-space">spelling</td>
							</tr>
							<tr>
								<td>WO</td>
								<td class="tab-space">word order</td>
							</tr>
							<tr>
								<td>P</td>
								<td class="tab-space">punctuation</td>
							</tr>
							<tr>
								<td>V</td>
								<td class="tab-space">vocabulary</td>
							</tr>
							<tr>
								<td>PR</td>
								<td class="tab-space">prepositions</td>
							</tr>
							<tr>
								<td>T</td>
								<td class="tab-space">tense</td>
							</tr>
						</table>
						<p>Underline the error and write the appropriate abbreviation in the margin. It is useful to get students into the habit of writing on alternate lines - it is often easier to read their script this way, and it is easier to mark.</p>
					</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>ERROR CORRECTION DURING SPEAKING ACTIVITIES</h4>
						</div>
						<div class="panel-body">
							<p>FLUENCY VERSUS ACCURACY</p>
							<p>It is important to get the balance between under correction and over connection right. If, as teachers, we do not give enough correction, it can lead to fossilisation of errors and the feeling among the students that they are not learning anything. If, on the other hand, we over correct it can seriously affect the relationship between students and teacher and can seriously affect student confidence.</p>
							<p>It can be a particularly difficult balance to get right at upper levels due to the fact that classes are usually less structured, and texts may be less focused on specific structures and functions. As a result it can be difficult to know when, why, what and how to correct.</p>
							<br>
							<p>WHY TO CORRECT</p>
							<p>Correction is an essential and core part of the learning process because it helps students to learn how to speak the target language. If student errors are not brought to their attention they will not have the opportunity to learn the correct form and their language abilities will not progress.   </p>
							<br>
							<p>WHAT TO CORRECT</p>
							<p>It may be useful to consider a distinction between mistakes and errors made in ‘A Practical Guide to English Language Teaching’ by Jeremy Harmer:</p>
							<p>A Mistake: occurs when students know the correct language but make a mistake when retrieving it from memory.</p>
							<p>An Error: occurs when students have learned the incorrect language or don’t know the correct language.</p>
							<p>During structured activities, correction should be limited to the target structures being practiced. When deciding what to correct consider the following points:</p>
							<ul>
								<li>Was communication hindered?</li>
								<li>Was the information structured as efficiently as possible?</li>
								<li>Was the message conveyed, the message the student intended?</li>
							</ul>
							<br>
							<p>WHEN TO CORRECT</p>
							<p>The timing of correction and feedback can be a difficult skill to master. There are three main options amongst others:</p>
							<ul>
								<li>a) immediate correction</li>
								<li>b) note errors and feedback later</li>
								<li>c) encourage students to correct each other if they have a clear understanding of the language that they are correcting</li>
							</ul>
							<p>During controlled practice it is best to correct immediately otherwise the structure being taught will be internalized incorrectly and the student will not be able to use it correctly during freer practice. <br>
							During the production phase (less structured practice of structures), we should not interrupt the flow of conversation unless there has been a real breakdown in communication. Rather, make a note of common mistakes and give feedback to the students at the end of the activity.
							</p>
							<br>
							<p>CORRECTION METHODS</p>
							<p>The amount of language correction required depends on the language level and on the stage of the lesson. Always encourage self-correction in order to enable students to develop invaluable self-monitoring skills and independence.</p>
							<p>During Controlled Practice:</p>
							<p>It is useful to consider the following process for dealing with errors during controlled practice:</p>
							<ul>
								<li>a) note there’s a mistake</li>
								<li>b) point out where the mistake is</li>
								<li>c) point out why it’s a mistake</li>
								<li>d) correct the mistake</li>
							</ul>
							<p>Here are some examples of correction techniques (from the least explicit to the most explicit):</p>
							<p>Note the error</p>
							<ul>
								<li>use facial expression or gestures (eg raised eyebrows)</li>
								<li>sentence completion <br>
									S: ‘He work yesterday’<br>
									T: ‘He…?’ using questioning intonation.</li>
								<li>T: ‘Try again’, or ‘Are you sure?’</li>
								<li>T: ‘I don’t understand’</li>
							</ul>
							<br>
								<p>Point out where the error is</p>
								<ul>
									<li>Echo <br>
								S: ‘He work yesterday’ <br>
								T: ‘Work?’</li>
									<li>Stress <br>
								S: ‘He work yesterday’ <br>
								T: ‘He work yesterday?’</li>
									<li>T: ‘What do you mean by . . .?’</li>
								</ul>
								<br>
								<p>Point out why it’s an error</p>
								<ul>
									<li>Analysis <br>
								S: ‘He work yesterday’ <br>
								T: ‘Simple past’</li>
								</ul>
								
								<br>
								<p>How to correct the mistake when the students are unable to do so themselves</p>
								<ul>
									<li>Suggestion <br>
									S: ‘He work yesterday’ <br>
									T: ‘Do you mean “He worked yesterday”?’</li>
									<li>Repetition <br>
								S: ‘He work yesterday’ <br>
								T: ‘He worked. Again.’</li>
								</ul>
								<br>
								<p>Reward success</p>
								<ul>
									<li>S: ‘He worked yesterday’ <br>
										T: Great</li>
									
								</ul>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="4a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Self Check</h4>
						</div>
						<div class="panel-body">
							<p>Look at the following pieces of written work. In each case write a list of all the errors which have been made. Mark on the list, which type of error they are, according to the simple classification above. Where you have marked the error G, WO, PR or T write the correct version underneath eg I am getting up at 7am every day.</p>
							<p> get up at 7 a.m. every day</p>
							<br>
							

						</div>

						<div class="panel-body">
							<p>AUTHENTIC STUDENT ERRORS</p>
							<p>STUDENT 1</p>
							<p>I am 46 years old and married for twenty years. We have got three children, one soon and two daughters. There are six, eight and eleven years old. I am an electrical engineer and worked for a company who manufactured machines for processing automatically. <br>
							At first, I am going to learn English in Great Britain. It's very important for my job. Then, I would like to have a little compagniy with about ten co-worker for myself. If it isn't possible, I'll manage a department who developed high technology machines. <br>
							I became the test sheets for complete and return directly at you. You phoned at your agent about my course at your school in Bristol. Very thanks for your trouble. I'm happy to come at your school and I'm sure to learn better English. I don't know my flight number on the 5st March. I would let you as soon as possible.
							</p>
							<br>
							{!! Form::open(['action' => 'Admin\Int120u3m5s1Controller@store', 'method' => 'POST']) !!}
							<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">
								{{ Form::label('u3m5s1q1', 'STUDENT 1 ERRORS')}}<br>
								{{ Form::text('u3m5s1q1', null, ['class' => '  form-control'])}} 
							
								

								
						{{-- {!! Form::close() !!} --}}
						</div>

						<div class="panel-body">
						
							<p>STUDENT 2</p>
							<p>I'm fourty years old. My height is 1.8m and my hair is blond. My hobbys are walking and clymbing in the mountain. My job is Quality-Manager. I live ten years at Russikon. I weake up at seven o'chlock in the morning. I go for breakfast in the kitchen. After the breakfast and I drive to work by car. In the office there is a lot of work. I have a break at ten o'clock and I drink a tea. 12 o'clock I go home for lunch. <br>
							In the future I will a good life with my family. I hope I get a lot of money. I will learn better english for my job. I hope you will be happy with my history.
							</p>
							<br>
							{{-- {!!Form::open(array('route' => 'admin.onetwentyhours.unit-three.module-three.section-one.store')) !!} --}}
								{{ Form::label('u3m5s1q2', 'STUDENT 2 ERRORS')}}<br>
								{{ Form::text('u3m5s1q2', null, ['class' => '  form-control'])}} 
							
								

								
						{{-- {!! Form::close() !!} --}}
						</div>

						<div class="panel-body">
						
							<p>STUDENT 3</p>
							<p>Yesterday I arrived in Bath. In this town there is a college where I'll study English for two weeks. When we arrived at the college we went to our bedroom. I stay in bed-room with my best friend and I'm very happy for this.
							At eight o'clock we had dinner and then we have visited the college. It is beautiful and there is also a swimming pool. Then we caome back to our bed-room wher I've chatted whit my friend for a long time. We both happy and we both hope to have a good time in this college. Unfortunately the weather isn't very good yesterday rained and today too  but I think that the weather isn't very important.</p>
							<br>
							
								{{ Form::label('u3m5s1q3', 'STUDENT 3 ERRORS')}}<br>
								{{ Form::text('u3m5s1q3', null, ['class' => '  form-control'])}} 
								
								

								
						{{-- {!! Form::close() !!} --}}
						</div>

						<div class="panel-body">
						
							<p>STUDENT 4</p>
							<p>I had been in London for about four days, I saw the Tower of London, the Tower Bridge the Big Ben and the other famous places of this city. To arrive in Bath, I brought a coach. The journey was long but I'm very happy to arrive in this town. I didn't stay here before, but all my friends who saw Bath tell me that it is very beautiful. I hope I enjoy myself very much.</p>
							<br>
						
								{{ Form::label('u3m5s1q4', 'STUDENT 4 ERRORS')}}<br>
								{{ Form::text('u3m5s1q4', null, ['class' => '  form-control'])}} 
							
								

								
						{{-- {!! Form::close() !!} --}}
						</div>
						<div class="panel-body">
							<p>STUDENT 5</p>
							{{ Form::label('u3m5s1q5', 'STUDENT 5 ERRORS')}}<br>
								{{ Form::text('u3m5s1q5', null, ['class' => '  form-control'])}} 
								<br>
							<p>
								a) Thank you for the informations. <br><br>
								{{ Form::text('u3m5s1q6', null, ['class' => '  form-control'])}} 
								<br>

								b) I stand up at 6 o'clock in the morning. <br><br>
								{{ Form::text('u3m5s1q7', null, ['class' => '  form-control'])}} 
								<br>

								c) When I am ready I am eating my meal.<br><br>
								{{ Form::text('u3m5s1q8', null, ['class' => '  form-control'])}} 
								<br>

								d) I passed a good week with you.<br><br>
								{{ Form::text('u3m5s1q9', null, ['class' => '  form-control'])}} 
								<br>

								e) I want saying you thank you.<br><br>
								{{ Form::text('u3m5s1q10', null, ['class' => '  form-control'])}} 
								<br>

								f) I had very nice lunch and wine and it made me very full up and I'm no    
								   more hungry.<br><br>
								   {{ Form::text('u3m5s1q11', null, ['class' => '  form-control'])}} 
								<br>

								g) Where I stay the wife is a very good cooker.<br><br>
								{{ Form::text('u3m5s1q12', null, ['class' => '  form-control'])}} 
								<br>

								h) The father is very humouristic.<br><br>
								{{ Form::text('u3m5s1q13', null, ['class' => '  form-control'])}} 
								<br>

								i) I'm deceived for answer as quickly.<br><br>
								{{ Form::text('u3m5s1q14', null, ['class' => '  form-control'])}} 
								<br>

								j) My friend don't let me any place.<br><br>
								{{ Form::text('u3m5s1q15', null, ['class' => '  form-control'])}} 
								<br>

								k) The weather gets warmer in summer isn't it?<br><br>
								{{ Form::text('u3m5s1q16', null, ['class' => '  form-control'])}} 
								<br>

								l) My partner gives the answer don't he?<br><br>
								{{ Form::text('u3m5s1q17', null, ['class' => '  form-control'])}} 
								<br>

								m) The weather will remember me England.<br><br>
								{{ Form::text('u3m5s1q18', null, ['class' => '  form-control'])}} 
								<br>

								n) We put there our luggage.<br><br>
								{{ Form::text('u3m5s1q19', null, ['class' => '  form-control'])}} 
								<br>

								o) On five o'clock I want to eat my tea. <br><br>
								{{ Form::text('u3m5s1q20', null, ['class' => '  form-control'])}} 
								<br>

							</p>
							<br>
							
								

								<br>
								{!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}
								{!! Form::close() !!}
								
						</div>


					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="5a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Preparing remedial exercises</h4>
						</div>
						<div class="panel-body">
							<p>Once an error has been identified, inform the student of the specific area of language he/she needs to work on. Review the relevant rule together. Once that has been done, give the student a remedial exercise to establish that the language point has been understood and the error pattern is no longer repeated.</p>

							<p>The aim of a remedial exercise is to focus the students’ attention on a single point of error in order to help them with understanding how it works. 'Remedial' means fixing or making better.  </p>
							<p>For example, for the student who has written “I am getting up at 7am every day” in an essay above, you will need a remedial exercise focusing on the difference in meaning between the present simple and present continuous tenses.</p>
							<p>First, correct the error or, if you can, encourage the student to self-correct:</p>
							<br>
							<ul>
								<li>INCORRECT: I am getting up at 7 am every day.</li>
								<li>CORRECT: I get up at 7 am every day.</li>
							</ul>


							<p>Then review the rule together. The student needs to understand why the correction had to be made:</p>
							<ul>
								<li>Rule: To talk about actions that happen regularly, we use the present simple.</li>
								<li>To talk about actions that are going on right now, we use the present continuous.</li>
							</ul>
							<p>Then you can give the student your remedial exercise to check understanding. <br>Begin with concise and clear instructions stating what they need to do. </p>
							<ul>
								<li>Decide whether the present simple or present continuous tense needs to be used in each of the following sentences. Fill the gap with the correct form of the verb in brackets and an auxiliary verb if necessary.</li>
							</ul>

							<p>This is followed by a worked example, ie one sentence done for the student.</p>
							<ul>
								<li>Could you stop talking so loudly – I ________________ (try) to do my 
							     homework!</li>
								<li>Could you stop talking so loudly – I ____am trying______ to do my   
							     homework!</li>
							</ul>
							<p>Finally, there will be some sentences for the students to work with. Since the focus is on the difference in meaning between the present simple and present continuous, these will include both tenses.</p>
							<ol>
								<li>I _______________ (go) by bus today because my car is broken.</li>
								<li>Phil sometimes _________________ (take) a taxi to get to work.</li>
								<li>Julie __________________ (watch) sitcoms every evening.</li>
								<li>I can't talk now, I _____________________ (watch) the Champions' League final. Call me later!</li>
								<li>We __________________ (not meet) very often, but we are still big friends.</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane" id="6a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">Task</div>
						<div class="panel-body">
							<p>From the errors coming under the classifications G, WO, PR and T which you have identified in the students' essays in the lesson Self-check, choose one from each category.</p>

							<p>Now prepare 4 remedial exercises (one for each of the 4 chosen errors) which will focus on the identified problem areas. Follow the steps outlined in the section 'Preparing remedial exercises' at the end of the module.</p>

							<p>Your answer must have five parts:</p>

							<p>1. Write the category, the error, and the correction.</p>
							<p>For example: </p>
							<p>T. </p>
							<p>INCORRECT: I haven’t gone to school yesterday.</p>
							<p>CORRECT: I didn't go to school yesterday.</p>  

							<p>2. Briefly state the rule which explains why there is an error. For example: “If the time of a past action is stated or implied in the sentence, use past simple. If the time is not stated or implied, use present perfect.”</p>

							<p>3. Write clear and simple instructions for the student explaining what they need to do in your remedial exercise. It could be a 'correct the mistake' exercise, a gap-fill exercise, a true / false exercise, etc. Make sure that your four exercises are not all the same type.</p>

							<p>4. Write a worked example, ie one question done correctly to reinforce your instructions.</p>

							<p>5. Prepare a 5-item exercise for your student, eg it may contain 5 gapped sentences, 5 true/false questions, 5 words to put into the specified form, etc.</p>
							 
							<p>Do this for each of the four chosen errors: G, WO, PR and T.  Do not prepare exercises for any other type of error.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection