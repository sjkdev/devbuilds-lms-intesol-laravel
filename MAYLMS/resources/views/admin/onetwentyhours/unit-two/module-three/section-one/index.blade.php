@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('content')


<div class="panel panel-default">
  <div class="panel-body">Unit Two Module Three</div>
</div>
<div class="panel panel-group">

  <div class="panel-heading"><h3>Unit 2 | The Study of English</h3></div>
  <div class="panel-heading"><h4>Module 3  Lexis</h4></div>

  <div class="panel-body" id="exTab1">
    <ul  class="nav nav-pills">

      <li class="active"><a href="#0a" data-toggle="tab" style="background-color:#fff;">Info</a></li>

      <li><a  href="#1a" data-toggle="tab" style="background-color:#fff;">Lexis</a></li>

      <li><a href="#2a" data-toggle="tab" style="background-color:#fff;">Parts of Speech</a></li>

      <li><a href="#3a" data-toggle="tab" style="background-color:#fff;">Synonyms & Antonyms</a></li>

      <li><a href="#4a" data-toggle="tab" style="background-color:#fff;">Appropriate Language</a></li>

      <li><a href="#5a" data-toggle="tab" style="background-color:#fff;">Teaching Vocabulary</a></li>

      <li><a href="#6a" data-toggle="tab" style="background-color:#fff;" class="blueText">Self Check</a></li>

    </ul>
    
  </div>
</div>

<div class="tab-content clearfix">{{-- begin nav sections --}}

	<div class="tab-pane" id="0a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						 <div class="panel-heading">
			              <h3>Info</h3></div>
			              <div class="panel-body">
			                <ul>
			                  <li>At the end of this module you will:-</li>
			                  <li>a) have more understanding of the difficulties EFL students face in their choice of vocabulary
</li>
			                  <li>b) understand the importance of teaching appropriate language</li>
			                  <li>c) have a basic understanding of the formation of words and their relationships</li>
			                  <li>d) have begun to think more deeply about the language you are going to teach</li>

			                </ul>

			              </div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="1a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>LEXIS</h4>
						</div>
						<div class="panel-body">
							<p>
								 Lexis refers to the vocabulary or words that are a vitally important part of learning a language. A student acquires vocabulary in two ways: the first is by discovery, through interaction/conversation or reading materials which the student has chosen for himself / herself; the second is by having words presented to him/her in the classroom in a more formal manner. The teacher has various ways of achieving this: (s)he can point to objects in the classroom, draw pictures, mime an action or make a gesture etc.
							</p>
						</div>
						</div>
						<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Selection</h4>
						</div>
						<div class="panel-body">
							<p>The vocabulary you introduce may to a large extent depend on the coursebook your school uses, but you are free to introduce other areas of vocabulary when you feel that it is relevant to your teaching. However it is essential that your students do not "drown" in too much new vocabulary as, if this is the case, they will retain little of it. Long lists of vocabulary without the structure, relevant context, form and idiom will not bring about language competence.</p>
							<p>Not only is it important not to introduce too much, and particularly not in unrelated lists, but the selection of the vocabulary is of great importance.</p>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Formation</h4>
							</div>
						<div class="panel-body">
							<p>Students need to know the facts about the formation of words and how different forms fit different grammatical contexts - for example take the verb 'smile'. 'Smile' can also be a noun, and the present participle 'smiling' can also be used as an adjective:</p>
							<ul class="cleanList">
								<li>The children never <span class="underline">smile</span> at the teacher.</li>
								<li>The teacher looked angry then he gave me a <span class="underline">smile.</span></li>
								<li>Happy children are always <span class="underline">smiling.</span></li>
								<li>The <span class="underline">smiling</span> of the Mona Lisa looks down on the crowd.</li>
							</ul>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Word Stems</h4>
						</div>
						<div class="panel-body">
							<p>Students need to know the relationship between words with the same stem:
								for example - 
							</p>
							<p>
							    growth - grown - growing - grow - grew
							</p>
						</div>
					</div>
					
					</div>
					
					
				</div>
			</div>
		</div>
	

	<div class="tab-pane" id="2a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Parts of Speech</h4>
						</div>
						<div class="panel-body">
							<p>More advanced students may also find it useful to know how the components of lexical items are put together. The component parts of
multi-word items may themselves be different parts of speech, </p>
							<ul class="cleanList">
								<li>perhaps a combination of two nouns making a single compound noun | eg lampshade</li><br>
								<li>a noun and a gerund making one item of two words | eg - skating rink</li><br>
								<li>two hyphenated words making one item | eg play-off</li><br>
								
							</ul>
							<br>
							<p>Students also need to know common prefixes:-</p>
							<table>
								
								<tr>
								<td>a- </td>
								<td> auto- </td>
								<td> dis- </td>
								<td> pre- </td>
								</tr>
								<tr>
								<td>ab- </td>
								<td> co- </td>
								<td> ex- </td>
								<td> re- </td>
								</tr>
								<tr>
								<td>ante- </td>
								<td> con- </td>
								<td> mis- </td>
								<td> sub-</td>
								</tr>
								<tr>
								<td>an- </td>
								<td> non- </td>
								<td> per- </td>
								<td> un-</td>
								</tr>
								
							</table>

							<br>
							<p>and common suffixes:-</p>
							<table>
								
								<tr>
								<td>-able </td>
								<td> -ism </td>
								<td> -er </td>
								<td> -ist </td>
								</tr>
								<tr>
								<td> -or </td>
								<td> -ose </td>
								<td> -ic </td>
								<td> -ness </td>
								</tr>
								<tr>
								<td> -ify </td>
								<td> -tion </td>
								</tr>
							
								
							</table>

							<div class="panel-body">
								<p>... and how they work</p><br>
								<p>eg making words opposite in meaning:</p>
								<ul class="cleanList">
									<li>mis + understand		=	misunderstand</li><br>
									<li>un  +  happy			=	unhappy</li><br>
									<li>eg forming a noun from an adjective</li><br>
									<li>happy - y + i + ness = happiness</li>
								</ul>
							</div>
							<div class="panel-body">
								<p>Students also need to know how the words are spelt and how they sound. Of particular importance is the way these words are stressed as this can alter the meaning and make them incomprehensible</p>
									<p>
									eg potent + im  = impotent, put the stress on the wrong syllable and it will sound like  - important.
									</p>
							</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

	<div class="tab-pane" id="3a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						
						<div class="panel-heading">
							<h4>Synonyms and Antonyms</h4>
						</div>
						<div class="panel-body">
							<p>Students also need to know how the meaning of one lexical item relates to another.</p>
							<p>Items which mean the same or almost the same are synonyms -</p>
							<p>eg - sizeable and large are synonyms of big</p><br>
							<p>Items which mean the opposite are antonyms -</p>
							<p>eg - sad is an antonym of happy</p>
						</div>
						<div class="panel-body">
							<p>Please note that when a student is translating from his/her mother tongue that he/she will choose one word but that there may be many synonyms. Two words cannot be exactly the same in meaning, otherwise there would be only one word.</p>
						</div>
					
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="4a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Appropriate Language</h3>
						</div>
						<div class="panel-body">
							<p>
								Here, of course, style and register also play a very important part in helping us to decide which word to choose.</p>
							<p>
							eg  synonyms of 'angry' may be elicited as 'cross', which is acceptable in most situations, or 'pissed off' which often isn't. It is important to ensure that students are taught, and know how to choose, appropriate language for particular situations. Lack of awareness in students can cause offence and confusion and can lead to students not being taken seriously.</p>
							<p>
							A non-native speaker of English may impress an everyday speaker with his/her skill in manipulating language as in his / her appropriate use of tenses or correct word order and all things grammatical, but may not know a vitally important word which will satisfy his/her needs at the time.</p>
							<p>
							eg  "I would like some lighter fuel"  Say "lighter fuel" only and you would get what you want. "I would like" and "please" are simply                               politenesses and could be excluded.</p>
							<p>
							When teaching lexis the teacher has to make a judgement on which words could be left until a later stage of learning or for the student to discover for him/herself.
							</p>
							<p>
							eg  'table' must be taught early on in the student's language learning because of the frequency of its use:- dining table, dressing table, bedside table, scientific table, frequency table etc.

														</p>

						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Collocation</h3>
						</div>
						<div class="panel-body">
							<p>When teaching lexis, students need to be aware of the fact that certain words go together, ie collocation. Collocations are clusters of two or three words that occur regularly together in English. <br>
							eg It is correct to say, ’wide awake’, but you can not say ‘wide asleep’ (you would say, ’fast asleep’). When teaching lexis, it is important to tell students which words the items of lexis you are teaching collocate with. Look at some of the common types of collocations below, and try to think of some more examples for each type :</p>
							
							<ul>
								<li><strong>Noun + Noun:</strong>pay rise / money box</li>
								<li><strong>Verb + Noun:</strong>take a break / throw a party</li>
								<li><strong>Verb + Adjective + Noun: turn over a new leaf / lead a healthy lifestyle</strong></li>
								<li><strong>Adjective + preposition:</strong>upset about / embarrassed about / guilty of
</li>
								<li><strong>Adjective + Noun:</strong>square meal / healthy interest</li>
								<li><strong>Adverb +  Verb:</strong>strongly recommend / cautiously suggest</li>
								<li><strong>Adverb + Adjective:</strong>completely exhausted / utterly amazed</li>
								<li><strong>Adverb + Adjective + Noun:</strong>totally unacceptable behaviour</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane" id="5a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Teaching Vocabulary</h3>
						</div>
						<div class="panel-body">
							<p>
								It is important not to overestimate or underestimate your students. Don’t assume that students know the vocabulary, but on the other hand don’t assume that they don’t know the language. When you plan a lesson, always fully familiarize yourself with the materials you will be presenting and prepare how you are going to deal with any new items of vocabulary.
Do not teach words in isolation, and always check comprehension.
With hundreds of thousands of words in the English language, teaching lexis can seem to be a very daunting prospect. However, remember that the average native speaker only uses around five thousand words in everyday speech. Also, remember that your students will not need to produce every word they learn; some they will just need to recognize. Selecting what to teach is therefore essential.
							</p>
							<br>
							<p>You need to ensure that you give your students the information they need in order to use the words you are teaching:</p>
							<ul class="cleanList">
								<li><span>Meaning</span> - including limits (eg ripe: can this word also be used in reference to a person?) It is vital to get across the meaning clearly and to check comprehension by asking questions.</li>
								<li><span>Pronunciation</span> - how to say it (eg bough and trough) This can be a problematic area because there is often no clear relationship between spelling and pronunciation. The phonemic chart can be useful here. Also problematic pronunciations can be drilled, and word stress can be highlighted.</li>
								<li><span>Form</span> - how to use it. Students need to know if it is a verb, noun, adjective etc in order to be able to use it effectively.</li>
								<li><span>Spelling</span> - how to write it. Pronunciation should be clarified before the written form is introduced.</li>
								<li><span>Collocation</span> - what does it go with? (eg young, free and single) You can describe things in ‘great’ detail, not ‘big’ detail, and to ask a question you ‘raise’ your hand, not ‘lift’ your hand. Students need to know this in order to avoid mistakes in usage later.
</li>
								<li><span>Register</span> - is it formal/informal/neutral? (eg Hang on!)</li>
								<li><span>Connotation</span> - does it sound positive/negative/neutral? eg ‘bachelor’ is a neutral / positive word, whereas ‘spinster’ has negative connotations.</li>
								<li><span>How the word is related</span> - to others eg synonyms, antonyms. </li>
							</ul>
							<div class="panel-body">
								<p>Which of the above you choose to highlight will depend on the item you are teaching and the level of your students.</p>
								<ul>Other things to consider:-
									<li>Review the words you teach through games & activities, and encourage students to do the same at home</li>
									<li>Encourage autonomy in your students by encouraging them to read, watch films, and listen to songs and to note the useful words. Encourage students to purchase a good dictionary, and ensure that they know how to use it.</li>
									<li>Have a section on the board to note down new items of vocabulary as they arise. You could try using colour to highlight stress.</li>
									<li>Teach students the grammatical names of the parts of speech, and how to use the phonemic script.</li>
									<li>Always keep a good dictionary in class.</li>
									<li>It is a good idea to teach words with associated meanings together.</li>
								</ul>
								<br>
								<p>Acquiring and retaining new vocabulary is not easy for students, and unless they have the opportunity to use and review the new vocabulary, it will quickly be forgotten. Therefore, it is important to continually recycle learned vocabulary in the classroom.</p>
								<p>One useful tip is to keep a <strong>Word Bank</strong> for each group: the students or teacher choose important vocabulary from each unit throughout the duration of the course. These should be written on separate cards and placed in a depot (a box or large envelope). These can then be used for a variety of activities for time-fill exercises and warm-ups etc. Here are a few ideas for vocabulary activities:</p>

								<ul>
									<li><strong>STORY TELLING:</strong>In small groups students write their names three times on separate pieces of paper. These are shuffled and placed face down on the desk. The teacher writes the beginning of a story on the board, and each student is given five words from the word bank. A name card is drawn from the desk and that student must continue the story using one of their words. The process continues until all the words have been used.</li>
									<li><strong>PICK A WORD:</strong>Each student picks a word from the Word Bank; they must then formulate a sentence using the word, and write it on the board. Once all the sentences have been written on the board, ask the students to find any errors, and elicit corrections.</li>
									<li><strong>SYNONYMS & ANTONYMS:</strong>Choose five to ten words from the Word Bank. In pairs or small groups ask the students to think of as many synonyms and antonyms for those words as they can within a time limit.</li>
									<li><strong>SLAM:</strong>Spread twenty cards from the Word Bank on a desk. The teacher calls out a definition and the first student to touch the correct card wins the card. Repeat until all the cards are taken. The winner is the student with the most cards.</li>
									<li><strong>TOPICAL CONVERSATIONS:</strong>Make small groups and give each student three words from the Word Bank. Write a topic on the board. Set a time limit, eg three minutes. The students then try to lose all their cards by using their words naturally in a conversation.</li>
									<li><strong>BOARD BINGO:</strong>Play bingo using the Word Bank. Write up some words from the Word Bank on the board. Each student chooses nine and writes them on a bingo style grid. The teacher calls out the definitions and the students cross off the appropriate words.</li>
									<li><strong>MATCHING DEFINITIONS:</strong>The students must match the Word Bank words with the correct definitions (written on separate cards). You can divide the class into groups to work together, or you can give each person words or definitions and have the students walk around the room to find partners.</li>
									<li><strong>WHAT’S THE WORD:</strong>Each student chooses three or four words from the day’s lesson and makes a Word Bank card. On pieces of paper they should write a sentence incorporating the words. In small groups the students take it in turns to read out their sentences, omitting the target words (they should whistle or say a set word in its place). The first student to guess the word keeps the card. The winner is the person with the most cards at the end of the game.
</li>
									<li><strong>WORD GAME:</strong>Give each pair a pile of words from the Word bank. One student takes a card and gives hints to their partner. Students receive one point for each word correctly guessed.</li>
								</ul>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="6a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Self Check</h3>
						</div>
						<div class="panel-body">
							{!! Form::open(['action' => 'Admin\Int120u2m3s1Controller@store', 'method' => 'POST']) !!}
							<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">
							<p>
								Find at least 2 other different forms from the following nouns. Name the forms and demonstrate them in sentences. Please note this task is not about synonyms.
							</p>
							<h5>repentance</h5>
							<div class="panel-body">
							  {{ Form::text('u2m3s1q1', null, ['class' => 'form-control'])  }}
							  
							</div>
							<br>
							<h5>grief</h5>
							<div class="panel-body">
							  {{ Form::text('u2m3s1q2', null, ['class' => 'form-control'])  }}
							  
							</div>
							<br>
							{{-- {!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}

						{!! Form::close() !!} --}}
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Task 1</h3>
						</div>
						<div class="form">
							{{-- {!! Form::open(['action' => 'Admin\Int120u2m3s1Controller@store', 'method' => 'POST']) !!}
							<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}"> --}}
						<div class="panel-body">
							<p>Find 2 other different forms from each of the following three nouns. Name the forms (noun, verb, adjective etc) and then use each of them appropriately in a sentence. For example: care - careful (adj), carelessly (adv), carer (noun).</p>
						</div>
						<div class="panel-body">
							<h4>cleanliness</h4>
							  {{ Form::text('u2m3s1q3', null, ['class' => 'form-control'])  }}
							   
							</div>

							<div class="panel-body">
								<h4>application</h4>
							  {{ Form::text('u2m3s1q4', null, ['class' => 'form-control'])  }}
							   
							</div>

							<div class="panel-body">
								<h4>tolerance</h4>
							  {{ Form::text('u2m3s1q5', null, ['class' => 'form-control'])  }}
							   
							</div>
							{{-- {!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}
							{!! Form::close() !!} --}}
							</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Task 2</h3>
						</div>
						<div class="panel-body">
							{{-- {!! Form::open(['action' => 'Admin\Int120u2m3s1Controller@store', 'method' => 'POST']) !!}
							<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}"> --}}
							<h5>Give 5 examples of each of the following:-</h5>
							<ul>
								<li>1. Two nouns making a single compound noun</li>
								<li>eg football; [foot+ball] </li>
								<li>a) {{ Form::text('u2m3s1q6', null, ['class' => 'form-control']) }}</li>
								<li>b) {{ Form::text('u2m3s1q7', null, ['class' => 'form-control']) }}</li>
								<li>c) {{ Form::text('u2m3s1q8', null, ['class' => 'form-control']) }}</li>
								<li>d) {{ Form::text('u2m3s1q9', null, ['class' => 'form-control']) }}</li>
								<li>e) {{ Form::text('u2m3s1q10', null, ['class' => 'form-control']) }}</li>
							</ul>
						</div>
						<div class="panel-body">
							
							<ul>
								<li>2. A verb and a noun making a <span class="underline">common</span> collocation</li>
								<li>eg take a break</li>
								<li>a) {{ Form::text('u2m3s1q11', null, ['class' => 'form-control']) }}</li>
								<li>b) {{ Form::text('u2m3s1q12', null, ['class' => 'form-control']) }}</li>
								<li>c) {{ Form::text('u2m3s1q13', null, ['class' => 'form-control']) }}</li>
								<li>d) {{ Form::text('u2m3s1q14', null, ['class' => 'form-control']) }}</li>
								<li>e) {{ Form::text('u2m3s1q15', null, ['class' => 'form-control']) }}</li>
							</ul>
						</div>
						<br>
					{{-- 	{!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}
							{!! Form::close() !!} --}}
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Task 3</h3>
						</div>
						{{-- {!! Form::open(['action' => 'Admin\Int120u2m3s1Controller@store', 'method' => 'POST']) !!}
							<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}"> --}}
						<div class="panel-body">
							<p>Form 1 word from each of the following prefixes. Use free-standing words to which prefixes can be added. eg un - (un)friendly</p>
							<ul>
								
								<li>1. auto {{ Form::text('u2m3s1q16', null, ['class' => 'form-control']) }}</li>
								<li>2.con {{ Form::text('u2m3s1q17', null, ['class' => 'form-control']) }}</li>
								<li>3. pre {{ Form::text('u2m3s1q18', null, ['class' => 'form-control']) }}</li>
								<li>4. sub {{ Form::text('u2m3s1q19', null, ['class' => 'form-control']) }}</li>
								<li>5.anti {{ Form::text('u2m3s1q20', null, ['class' => 'form-control']) }}</li>
							</ul>
						</div>

						<div class="panel-body">
							<p>Form 1 word from each of the following suffixes, ie add the suffixes to already existing words. eg ness - sad(ness)</p>
							<ul>
								
								<li>1. -able {{ Form::text('u2m3s1q21', null, ['class' => 'form-control']) }}</li>
								<li>2. -less {{ Form::text('u2m3s1q22', null, ['class' => 'form-control']) }}</li>
								<li>3. -er {{ Form::text('u2m3s1q23', null, ['class' => 'form-control']) }}</li>
								<li>4. -ise {{ Form::text('u2m3s1q24', null, ['class' => 'form-control']) }}</li>
								<li>5. -sion {{ Form::text('u2m3s1q25', null, ['class' => 'form-control']) }}</li>
							</ul>
						</div>
						{{-- {!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}
							{!! Form::close() !!} --}}

					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Task 4</h3>
						</div>
						<div class="panel-body">
							{{-- {!! Form::open(['action' => 'Admin\Int120u2m3s1Controller@store', 'method' => 'POST']) !!}
							<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}"> --}}
							<p>Which of the following words would you not teach beginners and why?</p>
							<ul>
								<li>orange {{ Form::text('u2m3s1q26', null, ['class' => 'form-control']) }}</li>
								<li>chum {{ Form::text('u2m3s1q27', null, ['class' => 'form-control']) }}</li>
								<li>student {{ Form::text('u2m3s1q28', null, ['class' => 'form-control']) }}</li>
								<li>welder {{ Form::text('u2m3s1q29', null, ['class' => 'form-control']) }}</li>
								<li>omelette {{ Form::text('u2m3s1q30', null, ['class' => 'form-control']) }}</li>
								<li>black {{ Form::text('u2m3s1q31', null, ['class' => 'form-control']) }}</li>
								<li>chauffer {{ Form::text('u2m3s1q32', null, ['class' => 'form-control']) }}</li>
								<li>hungry {{ Form::text('u2m3s1q33', null, ['class' => 'form-control']) }}</li>
								<li>telephone {{ Form::text('u2m3s1q34', null, ['class' => 'form-control']) }}</li>
								<li>car {{ Form::text('u2m3s1q35', null, ['class' => 'form-control']) }}</li>
								<li>angry {{ Form::text('u2m3s1q36', null, ['class' => 'form-control']) }}</li>

							</ul>
							{{-- {!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}
							{!! Form::close() !!}
							 --}}
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Task 5</h3>
						</div>
						<div class="panel-body">
							<p>How many different lexical choices do we have to say that someone <strong>loses their job?</strong> Select 5 <strong><span class="underline">common</span></strong> ones, describe the situations/contexts in which they would be appropriate and give example sentences for each.</p>
							<p>eg for different lexical choices to say that someone has died:
							'Pass away' - this is a polite and respectful way to refer to someone's death, which can be used in conversation with the person's relatives or in official announcements. 'Jonathan passed away on Friday and is mourned by his wife and two children.'<br>
							'Kick the bucket' – this is a very informal expression that can be used between friends; a lighthearted way to refer to death which may cause offence if used inappropriately. 'Have you heard that Old Joe kicked the bucket last week?' and so on</p>
							<p>Think about the relationship between the speaker and the addressee, as well as the language context (where and when the expression might be used). </p>

						</div>
						<div class="panel-body">
						{{ Form::text('u2m3s1q37', null, ['class' => 'form-control'])  }}
							   </div>
					</div>

					{!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}
							{!! Form::close() !!}
							

				</div>
			</div>
		</div>
	</div>

</div>{{-- end nav sections --}}


@endsection