@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('content')


<div class="row">
	<div class="youtubeVid mediumScreen" >

		<iframe width="640" height="360" src="https://www.youtube.com/embed/pjbcU0Z1Kik" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		
	</div>
	<br>

{{-- 	<div class="youtubeVid mobileScreen" >

		<iframe width="320" height="180" src="https://www.youtube.com/embed/pjbcU0Z1Kik" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		
	</div> --}}
</div>





<div class="row">
	<div class="col-md-12"><h4>INTESOL WORLDWIDE</h4>
              <br>                              
<h4>Level 3 120 hour Certificate in TEFL</h4>
</div>
	<div class="col-md-12">
		<h4>METHOD</h4>
		<p>
			We use a carefully structured approach to learning, and we help you all the way. Each unit/module of the course is illustrated by practical methods and strategies for use in the EFL classroom. There is a balance between theory and practice, our aim is to show you how to become a confident, well-resourced, professional teacher, whatever your background.
		</p>
		</div>
		<div class="col-md-12">
			<h4>ASSESSMENT</h4>
			<p>Each unit/module is marked by your trainer, who will contact you by email to let you know your grade so you know how you are progressing.</p>
		</div>
		<div class="col-md-12">
			<h4>GRADES</h4>
			<p>The grades are awarded as follows: A1 - outstanding work; A2 - excellent; B1 - very good; B2 - good; C - satisfactory; U - ungraded (re-submit) If your work is between 2 grades the mark may appear as C/B2 or A2/A1. </p>
		</div>
		<div class="col-md-12">
			<h4>CERTIFICATE</h4>
			<p>The award of the certificate for these courses is strictly for those who successfully complete all units/modules.</p>
		</div>
		<div class="col-md-12">
			<h4>
COURSE DESCRIPTION</h4>
			<p>The course consists of self-check exercises and trainer-assessed tasks. You have a better chance of achieving good grades if you read each module thoroughly and complete the self assessments before you begin the tasks.   <br>
Though we expect that you may get your ideas for materials and lessons from commercially produced resources, it is expected that ALL work will be original. <br>
The self-check exercises are provided to give you practice and to ensure you have understood the modules before you complete the tasks to be assessed. It is our experience that good marks are not achieved by students who do not work through the course in this way.</p>
		</div>
		<div class="col-md-12">
			<h4>HOW THE COURSE WORKS</h4>
			<p>When you have completed the grammar tasks contained in Unit 2, Module 1, email them to your trainer (clearly labelled as described above). The module will then be marked and assessed and your assessment grade and any necessary comments/guidance will be emailed to you. Your continuous assessment marks will be logged here, to go towards your final mark. When you have completed all units/modules and received your grades, please complete and return the ‘Completion Form which you will have downloaded with the course so that we know to prepare your final grade and certificate.  </p>
		</div>
		<div class="col-md-12">
			
			<p>Please submit one module at a time so that we can monitor your progress and give guidance.
DO NOT submit more than one module without first checking with your trainer or unless you are contacted and told you may do so.</p>
			<br>
			<h4>IMPORTANT NOTE</h4>
			<p>Only the exercises which are marked TASK are to be submitted for assessment. Other exercises are for YOU to check your own understanding, and must not be submitted for marking.</p>
		</div>

	
</div>
@endsection

