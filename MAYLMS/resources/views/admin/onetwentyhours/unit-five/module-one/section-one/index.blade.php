@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('content')
<div class="panel panel-default">
    <div class="panel-body">Unit 5 Module 1 - TEYL</div>
  </div>
<div class="panel panel-group">
<div class="panel-heading"><h3>Unit 5 | Teaching Young Learners</h3></div>
<div class="panel-heading"><h4>Module 1 | Grammar</h4></div>
	<div class="panel-body" id="exTab1">
		<ul  class="nav nav-pills">
        <li class="active"><a  href="#1a" data-toggle="tab" style="background-color:#fff;">Info</a></li>
        <li><a href="#2a" data-toggle="tab" style="background-color:#fff;">Who are the young learners?</a></li>
        <li><a href="#3a" data-toggle="tab" style="background-color:#fff;" class="orangeText">Self Check</a></li>
        <li><a href="#4a" data-toggle="tab" style="background-color:#fff;">Classroom Management</a></li>
        <li><a href="#5a" data-toggle="tab" style="background-color:#fff;"> How to teach young learners</a></li>
        <li><a href="#6a" data-toggle="tab" style="background-color:#fff;" class="orangeText"> Self Check Answer Key</a></li>
    </ul>
	</div>
</div>
    <div class="tab-content clearfix">
        <div class="tab-pane" id="1a">
        	<div class="#">
        		<div class="row">
        			<div class="col-md-12">
        				<div class="panel panel-default">
        				<div class="panel-heading">
        				<h3>Info</h3></div>
        				<div class="panel-body">
        					<p>THIS IS A SELF-STUDY UNIT - DO NOT SEND IN FOR ASSESSMENT</p>
							<ul>
								<li>At the end of this module you will:-</li>
								<li>a) analyse the differences between how adults and how children learn</li>
								<li>b) recognize the differences among the features of different stages of development of young learners</li>
								<li>c) use the classroom management strategies and teaching techniques and activities to make learning happen in your YL classroom</li>
							</ul>
						</div>
						</div>
        			</div>
        		</div>
        	</div>
        </div>
        <div class="tab-pane" id="2a">
        	<div class="#">
        		<div class="row">
        			<div class="col-md-12">
        				<div class="panel panel-default">
        					<div class="panel-heading"><h4>Who are the young learners?</h4></div>
        					<div class="panel-body">
        						<p>We describe as ‘young learners’ children from pre-primary and primary school age and adolescents. Teaching English to young learners is a huge part of the TEFL profession due to a variety of reasons:</p>
        						<ul class="cleanList">
        							<li>a) English has developed into an international language and more and more parents all over the world see in their children’s learning of English better life opportunities for the future;</li>
        							<li>b) There are many multilingual countries and English is a medium of instruction, as children are taught not only English but other subjects in English as preparation for examinations (for secondary schools or universities); eg the International Baccalaureate.</li>
        							<li>c) In many mono-lingual countries we can notice an ever increasing number of bilingual schools where the instruction is carried out in both the students’ native tongue and a foreign language, especially English.</li>
        						</ul>
        						<br>
        						<p>
        							The age the children start learning English varies a lot. The starting age varies according to the countries’ educational systems and the ambitions of the children’s parents. In many countries of the world the obligatory starting age for learning English in state schools is seven or eight, but parents often request the private schools to organize lessons for learners of even younger ages. Nowadays, it is very usual to three or for year olds running in the hallways of private schools, singing and doing drama in English.
        						</p>
        						<p>The researchers have not agreed yet on the optimal age for learning a foreign language: while some argue that the sooner the learner starts the better, others are of the opinion that the disadvantages outweigh the benefits.  Moreover, learners who start later soon catch up. However, many institutions, both public and private, will continue to provide English classes, often just for a few hours a week.  Such classes demand a methodology that meets the special characteristics and needs of young learners.</p>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
        <div class="tab-pane" id="3a">
        	<div class="#">
        		<div class="row">
        			<div class="col-md-12">
        				<div class="panel panel-default">
        					<div class="panel-heading"><h4>What are the differences between children and adults in language learning?</h4></div>
        				</div>
        				<div class="panel panel-default">
        					<div class="panel-heading"><h4>Self Check 1</h4></div>
        					<div class="panel-body"><p>
        						Read the statements in the box below and note down whether you agree or disagree with them. Make a note of any comments or thoughts about these assumptions. 
        					</p>
        					<ul>
        						<li>Younger children learn languages better than the older ones; children learn better than adults.</li>
        						<li>Foreign language learning in school should be started at as early an age as possible.</li>
        						<li>Children and adults learn languages basically in the same way.</li>
        						<li>Adults have a longer concentration span than children.</li>
        						<li>It is easier to interest and motivate children than adults.</li>
        					</ul>
        					<br>
        					<p>
        						Now read the Self-check key at the end of this unit and compare it with your own answers.
        					</p></div>
        				</div>
        				<div class="panel panel-default">
        					<div class="panel-heading"><h4>What do we need to know about our young learners to be able to teach them efficiently?</h4></div>
        					<div class="panel-body">Besides the few characteristics of the young learners discussed in the task above, we need more information about the students we are going to teach. Our classroom management strategies, content of our lessons, the activities we choose to present, the new material to be learnt and practised depend on our thorough knowledge of our learners. The success of our lessons can be measured by the amount of learning that has taken place. Our students’ and our own enjoyment in the process are directly connected with this knowledge.
</div>
        				</div>
        				<div class="panel panel-default">
        					<div class="panel-heading"><h4>Self Check 2</h4></div>
        					<div class="panel-body"><p>
        						What exactly do we need to know about the children and where can we get the information from?
<br>
Compare your answer with the suggested answer at the end of this unit.
        					</p>
        					<br>
        				{!! Form::open(['action' => 'Admin\Int120u5m1s1Controller@store', 'method' => 'POST']) !!}
						<br>
						<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">
						<div class="form-group">
							{!! Form::label('u5m1s1q1', 'What exactly do we need to know about the children and where can we get the information from?') !!}
							<p>Compare your answer with the suggested answer at the end of this unit.</p>
							{!! Form::text('u5m1s1q1', null, ['class' => 'form-control'])!!}
						</div>
        				</div>
        				</div>
        				<div class="panel panel-default">
        					<div class="panel-heading"><h4>Self Check 3</h4></div>
        					<div class="panel-body"><p>
        						The grid below contains the developmental features of each age group of young learners. Read the features for each age group and then think about what type of teaching techniques and activities are suitable for each age group, what topics would be appropriate and some classroom management tips.</p>
        						<br>
        						<table class="table table-bordered">
								  <thead>
								    <tr>
								      <th scope="col">Age group</th>
								      <th scope="col">Emotional</th>
								      <th scope="col">Cognitive</th>
								      <th scope="col">Social</th>
								    </tr>
								  </thead>
								  <tbody>
								    <tr>
								      <th scope="row">Early Elementary (Age 5 to 8)</th>
								      <td>- ego-centric / self-centred / wrapped up in themselves <br>
										  - need and seek approval from adults <br> 
										  - like to play games, but can't accept losing; emphasis needs to be placed on success and cooperative games where everyone can win; failure should be minimized <br>
										  - adult help is needed in learning to cope with failures and problems 
									  </td>
								      <td>- move into a state of industry; more interested in doing things than in the end product; beginning projects is more important than finishing them <br>
										- thinking is concrete; learn through senses by touching, seeing, smelling, tasting, and hearing rather than by thinking alone; verbal instruction should be accompanied by demonstration <br>
										- learning to sort things into categories; collecting things is important and fun 
                                      </td>
								      <td>- can depend on other adults such as teachers or youth group leaders in addition to parents <br>
										- learning to be friends with others; younger boys and girls enjoy playing together, but by the end of the stage, boys and girls will separate; fighting occurs but does not have lasting effects <br>
										- peers become important; want to impress peers more than parents; small groups are effective. <br>
									  </td>
								    </tr>
								    <tr>
								      <th scope="row">Middle School (9 to 11) </th>
								      <td>- need to feel accepted and worthwhile; successes should be emphasized and failures kept in perspective as learning opportunities <br>
										- performance should be compared with past personal performance rather than with the performance of other youth 
										</td>
								      <td>- still think concretely, but begin to think more logically; new ideas are best understood when related to previous experiences <br>
											- think of things as absolutes, black or white, right or wrong 
											</td>
								      <td>- joining clubs becomes important; will form clubs with a group of others similar to themselves <br>
										- begin to identify with peers, although they still need and want guidance from adults <br>
										- have difficulty understanding another person's thinking, but are beginning to discover the benefit of making others happy <br>
										- satisfaction in completing projects comes more from pleasing the adults in their lives than from the value of the activity itself <br>
										- toward the end of this period, are ready to start taking responsibility for their actions <br>
										- divide themselves into sex-segregated groups 
										</td>
								    </tr>
								    <tr>
								      <th scope="row">Early Adolescence (Age 11 to 13) </th>
								      <td>- Begin to demonstrate Kohlberg's post-conventional moral thinking 
											- During puberty, emotions begin the roller coaster ride which characterizes adolescence. 
											- Changes in hormones and changes in thinking contribute to mood swings. 
											- Begin to test values 
											- Have a weak sense of individual identity. Feel challenges to personal self-concepts. 
											- Feel the need to be part of something important. 
										</td>
								      <td>- Move from concrete to abstract thinking, but still tend to think in all-or-nothing terms. <br>
											- Demonstrate formal operational thinking. <br>
											- Speak in longer sentences, use principles of subordination, understand multiple levels of meaning, and increase vocabulary. <br>
											- Will intensely explore subjects of interest. Often reject solutions offered by adults in favour of finding their own solutions. <br>
											- Justice and equality become regarded important issues. 
										</td>
								      <td>- Move away from dependence on parents toward eventual independence. <br>
											- Dependence on opinions of adults shifts to dependence on opinions of peers. <br>
											- Enjoy participating in activities away from home. 
									  </td>
								    </tr>
								    <tr>
								      <th scope="row">Middle Adolescence (Age 14 to 16)  </th>
								      <td>
											- Actively involved in search for independence and personal identity, although neither goal is completely achieved during this age period. <br>
											- Achieving satisfactory adjustment to sexuality and defining career goals are important. <br>
											- Seek emotional autonomy from parents. <br>
											- Learning to cooperate with each other as adults do. <br>
											- Learning to interact with the opposite sex may preoccupy middle adolescents. <br>
											- Unsettled emotions may cause teens to be stormy or withdrawn at times. <br>
											- Take pride in responsibility and respect from others. 
										</td>
								      <td>
								      	-Continue to gain meta-cognitive abilities and improve study skills. Write longer, more complex sentences. Can adapt language to different contexts. Use teen slang. <br>
										- Mastering abstract thinking. May imagine things that never were in a way that challenges, and sometimes threatens, adults who work with them. <br>
										- Egocentric. Believe in imaginary audience and personal fable. <br>
										- Have difficulty understanding compromise; may label adult efforts to cope with inconsistencies as "hypocrisy". <br>
										- Explore and prepare for future careers and roles in life. <br>
										- Set goals based on feelings of personal needs and priorities. Goals set by others are likely to be rejected. 
										</td>
								      <td>
								      	- Generally self-centred, but capable of understanding what other people are feelings. <br>
										- Relationship skills are well developed. Friendships formed at this stage are often sincere and long-lasting. <br>
										- Recreation moves away from the large group and more away from the family. Dating increases and moves from group dates to double dates to couple-only dating. <br>
										- Acceptance by members of the opposite sex is now of high importance. <br>
										- May begin sexual relationships. <br>
										- Want to belong to groups, but he recognized as unique individuals within the groups. 
									  </td>
								    </tr>
								    <tr>
								      <th scope="row">Late Adolescence (Age 17 to 19) </th>
								      <td>
								      	- Independence and identity formation are achieved. <br>
										- Feel they have reached the stage of full maturity and expect to be treated as adults. <br>
										- Leave home for education, employment, and establishing own households, separate from parents. <br>
										- Clubs, meetings, rituals, uniforms, and traditions have lost much of their appeal for late adolescents. 
										</td>
								      <td>
								      	- Metacognitive abilities and study skills continue to improve with instruction and practice. <br>
											- Plans for the future are very important and influence in which activities late adolescents choose to participate. <br>
											- Can determine their own schedules. <br>
											- Only general directions are needed when they are assigned familiar tasks. 
										</td>
								      <td>
								      	- Become preoccupied with the need for intimacy. Some will marry at this age. <br>
										- Likely to be sexually active. <br>
										- Employment and education fill the need for social relationships which were earlier filled by club and group activities. <br>
										- Control their own activities. 
									  </td>
								    </tr>
								  </tbody>
								</table>
        					</div>
        				</div>
        				<div class="panel panel-default">
        					<div class="panel-heading"><h4>Write your own answers and then read on. </h4></div>
        					<div class="panel-body">
							<p>Age Group: Early Elementary (Age 5 to 8)</p>
						<div class="form-group">
							{!! Form::label('u5m1s1q2', 'Topics') !!}
							{!! Form::text('u5m1s1q2', null, ['class' => 'form-control'])!!}
						</div>
						<div class="form-group">
							{!! Form::label('u5m1s1q3', 'Techniques and types of activities') !!}
							{!! Form::text('u5m1s1q3', null, ['class' => 'form-control'])!!}
						</div>
						<div class="form-group">
							{!! Form::label('u5m1s1q4', 'Classroom management tips') !!}
							{!! Form::text('u5m1s1q4', null, ['class' => 'form-control'])!!}
						</div>
						<br>
						<p>Age Group: Middle School (Age 9 to 11) </p>
						<div class="form-group">
							{!! Form::label('u5m1s1q5', 'Topics') !!}
							{!! Form::text('u5m1s1q5', null, ['class' => 'form-control'])!!}
						</div>
						<div class="form-group">
							{!! Form::label('u5m1s1q6', 'Techniques and types of activities') !!}
							{!! Form::text('u5m1s1q6', null, ['class' => 'form-control'])!!}
						</div>
						<div class="form-group">
							{!! Form::label('u5m1s1q7', 'Classroom management tips') !!}
							{!! Form::text('u5m1s1q7', null, ['class' => 'form-control'])!!}
						</div>
						<br>
						<p>Age Group: Early Adolescence (Age 11 to 13) </p>
						<div class="form-group">
							{!! Form::label('u5m1s1q8', 'Topics') !!}
							{!! Form::text('u5m1s1q8', null, ['class' => 'form-control'])!!}
						</div>
						<div class="form-group">
							{!! Form::label('u5m1s1q9', 'Techniques and types of activities') !!}
							{!! Form::text('u5m1s1q9', null, ['class' => 'form-control'])!!}
						</div>
						<div class="form-group">
							{!! Form::label('u5m1s1q10', 'Classroom management tips') !!}
							{!! Form::text('u5m1s1q10', null, ['class' => 'form-control'])!!}
						</div>
						<br>
						<p>Age Group: Middle Adolescence (Age 14 to 16)  </p>
						<div class="form-group">
							{!! Form::label('u5m1s1q11', 'Topics') !!}
							{!! Form::text('u5m1s1q11', null, ['class' => 'form-control'])!!}
						</div>
						<div class="form-group">
							{!! Form::label('u5m1s1q12', 'Techniques and types of activities') !!}
							{!! Form::text('u5m1s1q12', null, ['class' => 'form-control'])!!}
						</div>
						<div class="form-group">
							{!! Form::label('u5m1s1q13', 'Classroom management tips') !!}
							{!! Form::text('u5m1s1q13', null, ['class' => 'form-control'])!!}
						</div>
						<br>
						<p>Age Group: Late Adolescence (Age 17 to 19)  </p>
						<div class="form-group">
							{!! Form::label('u5m1s1q14', 'Topics') !!}
							{!! Form::text('u5m1s1q14', null, ['class' => 'form-control'])!!}
						</div>
						<div class="form-group">
							{!! Form::label('u5m1s1q15', 'Techniques and types of activities') !!}
							{!! Form::text('u5m1s1q15', null, ['class' => 'form-control'])!!}
						</div>
						<div class="form-group">
							{!! Form::label('u5m1s1q16', 'Classroom management tips') !!}
							{!! Form::text('u5m1s1q16', null, ['class' => 'form-control'])!!}
						</div> </p>
						{!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}
						{!! Form::close() !!}
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
        <div class="tab-pane" id="4a">
        	<div class="#">
        		<div class="row">
        			<div class="col-md-12">
        				<div class="panel panel-default">
        					<div class="panel-heading"><h4>Classroom Management</h4></div>
        					<div class="panel-body">
        						<p>In this section the focus will be mainly on the young learners’ motivation and discipline, because these are the main areas that differentiate adult classes from young learners’ ones.</p>
        						<p>A) Young Learners’ motivation</p>
								<p>Initially, the young learners come to the English class already motivated intrinsically. The curiosity about the new subject and the new teacher, the simple fact that they are dreaming of sounding like their favourite cartoon or film hero makes them eager to learn. They also come to class with some English already acquired from informal exposure to it, but they are proud to show the knowledge they have. This enthusiasm is what we rely on when we start an English course with young learners and we would all like to keep the smile on their faces and their eagerness to show what they have learnt in our classes. Unfortunately, this high level of motivation tends to decrease as they advance in their study of English if we, the teachers, try too hard “to teach” them, to control their learning in an authoritarian manner. </p>
        					</div>
        				</div>
        				<div class="panel panel-default">
        					<div class="panel-heading"><h4>Self Check</h4></div>
        					<div class="panel-body">
        						<p>Here are a few statements collected from some teachers who have some experience in teaching young learners. Read them and see if you agree with them or not.</p>
        						<br>
        						<ol>
        							<li>I have a routine in the classroom and I do not change it because the students need to know exactly what to expect.</li>
        							<li>I always give them small tasks that can be corrected immediately. In this way I have full control on what they learn. They are too small to be given “larger tasks”.</li>
        							<li>The young students all need to be given the same task that at the end can be corrected with the whole class. Only in this way can they and I know who has done the work correctly and who has not.</li>
        							<li>Never give them options. This might disturb the class and it is almost impossible to correct at the end.</li>
        							<li>Never involve the young learners in making decisions about what they will do in the next lesson, or the time they need to do a task, or how to set the homework.</li>
        							<li>It is good to know what students think about the lessons and what they need to do more work on, but it is very difficult to cater for all individual needs and interests; so, it is better that I decide what needs to be done in terms of what needs to be done in the classroom.</li>
        							<li>A system of rewards should be set. This encourages the students to learn better. Competition is what most of them like.</li>
        							<li>Be optimistic. Even the weaker students can learn something.</li>
        						</ol>
        						<br>
        						<p>Now go to the Self-check key and read the comments.</p>
        					</div>
        				</div>
        				<div class="panel panel-default">
        					<div class="panel-heading">B) Dealing with discipline problems</div>
        					<div class="panel-body">
        						<p><b>A teacher’s voice:</b><i>Sometimes I envy some of my colleagues who have a charismatic authority, if I can say so. They never seem to have problems with discipline. Whenever I go to observe their classes, all the students are at their desks, doing what they are told to do. There is no noise from the beginning to end. I my classes, I spend most of my time to have their attention. When they do a group activity, they start well, and after a while some of them become disruptive and ‘infect’ everybody else with their misbehaviour. I often have to raise my voice to call for silence. It works for a short while, but then they start again. I very rarely finish what I have planned to do with my students in class because of lack of discipline.</i></p>
        					</div>
        				</div>
        				<div class="panel panel-default">
        					<div class="panel-heading"><h4>Self Check</h4></div>
        					<div class="panel-body"><p>
        						Why do you think this teacher has discipline problems? Could you think of any reasons and give him/her some pieces of advice?
									(There is no key for this Self-check. You will find your answers in the text below.)</p>
									<p><b>Now read on.</b></p>
									<br>
									<p>Most teachers of young learners complain about the lack of discipline in their classrooms. We all know that effective learning can happen only in a disciplined classroom. What does a disciplined classroom look like? 
									In a disciplined class: 
									</p>
									<br>
									<ul>
										<li>learning is taking place. All students, either working individually or in pairs, or in groups or with the whole class, know what they have to do and they do the task(s) which are relevant to them and whose point is clear.</li>
										<li>There is a time for activities done in silence and a time for more noisy activities. Distinction has to be made between chaotic noise and constructive noise.</li>
										<li>The teacher is in control. This does not mean that the teacher has to be standing in front of the class at all times giving orders. Even if you “hand over the control’ for a while, delegating the responsibility for some group activities to some students, you can take it back as soon as the activity does not go as desired; you took the decision to hand the initiative over to them, you can take it back.</li>
										<li>There is cooperation between the teacher and the students. This cooperation is based on mutual respect and on a code of behaviour that has to be mutually agreed on.</li>
										<li>The students are motivated. It is easier to manage the students who are motivated, so it is the teacher’s responsibility to engage students in activities that are motivating.</li>
										<li>The lesson runs smoothly, but not necessarily according to the plan. It is very important to have a plan and to know exactly what you are doing and when. It is important to have all the materials ready at hand and also have some contingency plan. This shows the learners that you are prepared and in control so they will trust you. It happens sometimes that you can’t follow the lesson plan and you find yourself improvising. This is not a problem. At least the students will see that you care for their needs and you are not too rigid.</li>
										<li>It also helps that the students know the objectives of the lesson, or at least the aims of the activities, mainly the ones that you think might not be extremely enjoyable. They need to know why they are doing what you asked them to do, when this is not obvious. You might find yourself in the position of having to make some compromises sometimes, ie the students want to do something else (eg to talk about another topic). It is ok as long as they promise you to do something that you know they need but they will not find so enjoyable (eg a fill-in- the-gaps grammar exercise.) </li>
										<li>It is true that some teachers have charisma which gives an air of authority, and they find it easier to control their classes. The majority of teachers do not possess this natural authority, but they can have equally disciplined classrooms; they just have to work harder.</li>
									</ul>
									<br>
									<p>Here are some tips to maintain discipline in the classroom:</p>
									<ul>
										<li>Start by being firm with the students; you can relax later.</li>
										<li>Involve students in making a code of behaviour in the classroom and also let them decide on sanctions in case this code in broken. Display it on the classroom wall. BE CONSISTENT in applying them.</li>
										<li>Get silence before you start speaking to the whole class.</li>
										<li>Learn and use the students’ names.</li>
										<li>Prepare the lessons thoroughly and have a logical and firm structure.</li>
										<li>Be prepared to deal with the unexpected.</li>
										<li>Be mobile; walk around the class.</li>
										<li>Change the students around.</li>
										<li>Start the lesson with a ‘bang” and try to sustain their interest and curiosity.</li>
										<li>Speak clearly at all times; mainly when giving instructions.</li>
										<li>Check instructions and/or demonstrate activities. Have all your students’ attention.</li>
										<li>Have extra materials prepared for the students who work fast.</li>
										<li>Vary the pace of the activities and teaching techniques.</li>
										<li>Choose topics and tasks that will activate the students. Cooperate with them in this respect.</li>
										<li>Make the work appropriate to the students’ age, ability, cultural background.</li>
										<li>Anticipate discipline problems and act quickly.</li>
										<li>Never reprimand a student in front of the class. Have a private talk with him/her trying to get to the reason of his/her disruptive behaviour.</li>
										<li>Avoid confrontations. Never get angry in front of the students and shout at them.</li>
										<li>Avoid confrontations.</li>
										<li>Show your students that you care by being supportive and encouraging.</li>
										<li>Use praise only when the students deserve it.</li>
										<li>Don’t use threats. If you do, then be ready to put them into practice immediately.</li>
										<li>No matter the age of the students, DO NOT PATRONIZE them. Treat them with respect.</li>
										<li>Use humour constructively. Do not use irony and sarcasm.</li>
										<li>Be warm and friendly to your students.</li>
									</ul>
									<p>Adapted from Ur (1996:263)</p>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading"><h4><b>In conclusion, to ensure discipline in your classroom, you will have to:</b></h4></div>
							<div class="panel-body"><ul>
								<li>Be <b>INSISTENT</b> </li>
								<li>Be <b>CONSISTENT</b> </li>
								<li>Be <b>PERSISTENT</b> </li>
								<li>But most of all be <b>FAIR</b></li>
							</ul></div>
							<div class="panel-body"><h4><b>What to teach?</b></h4>
							<p>On a general English course for adults we teach language and skills: Grammar (structures and functions), vocabulary, pronunciation, and the language skills (listening, speaking, reading and writing). We usually have a balance of skills and all language components are considered as equally important by the students and teachers alike. Naturally, the course components depend on the language needs of the group as a whole.</p>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading"><h4>Self Check</h4></div>
							<div class="panel-body"><p>
								Here is a chart that compares younger learners (pre-school and primary school learners) and older learners (11 years olds +). Study it and then decide which of the language skills and language components should be emphasized in teaching English to these two categories of students.
							</p></div>
							<div class="row">
								<div class="col-md-6">
									<h4>Younger learners</h4>
									<ul>
										<li>These children are learning how to behave in school, classroom routines and learning in community</li>
										<li>They can understand meaningful messages but cannot analyse language yet.</li>
										<li>They are not aware about the learning processes; also, they are not fully aware about themselves</li>
										<li>They have no or very limited reading and writing skills in their first language (which in most cases do not use the Latin alphabet)</li>
										<li>They are more concerned about themselves than others.</li>
										<li>Their knowledge of the world is limited.</li>
										<li>They enjoy fantasy, imagination and movement. </li>
									</ul>
								</div>
								<div class="col-md-6">
									<h4>Older learners</h4>
									<ul>
										<li>These children are already used to school routines which they do not question.</li>
										<li>They start to show a growing interest in language as an abstract system; they are moving from concrete to abstract thinking.</li>
										<li>They are more aware about themselves and the way they learn. They are becoming more and more independent.</li>
										<li>They already have well developed skills as readers and writers in their own language.</li>
										<li>They show interest in the others’ viewpoints and usually compare and contrast them with theirs</li>
										<li>They have a growing awareness about and interest in the world around.</li>
										<li>Real life issues are more interesting to them than fantasy. </li>
									</ul>
									<br>
									<p>Prepare answers and then compare with the suggested answers in the key.</p>
								</div>
							</div>
						</div>
        			</div>
        		</div>
        	</div>
        </div>
        <div class="tab-pane" id="5a">
        	<div class="#">
        		<div class="row">
        			<div class="col-md-12">
        				<div class="panel panel-default">
        					<div class="panel-heading"><h4>How to teach young learners. Some techniques and activities.</h4></div>
        				</div>
        				<div class="panel panel-default">
        					<div class="panel-heading"><h4>A) Listening</h4></div>
        					<div class="panel-body">
        						<p>
        							5-8 year olds: </p>
        						<ul>
        							<li>Children’s songs with movements. </li>
        							<li>TPR-type of activities (listen and do).</li>
        							<li>Games such as ‘Simon says…’. </li>
        							<li>Arranging pictures of a story while listening to it.</li>
        						</ul>
        					</div>
        					<div class="panel-body">
        						<p>9-11 year olds: </p>
        						<ul>
        							<li>Quizzes - Listen to the question and choose the right answer.</li>
        							<li>Matching picture with story from a choice of slightly similar pictures. </li>
        							<li>Drawing dictation.</li>
        						</ul>
        					</div>
        					<div class="panel-body">
        						<p>11-13 year olds:</p>
        						<ul>
        							<li> 
									Most of the activities listed above can be used with this age group as well, but this time you can add a bit of reading as well (eg Listen to the story and put the sentences in chronological order.)</li>
        						</ul>
        					</div>
        					<div class="panel-body">
        						<p>14-16 year olds: </p>
        						<li>Listening to songs and filling in the gaps or arranging song lyrics in order</li>
        					</div>
        					<div class="panel-body">
        						<p>17-19 year olds:</p>
        						<li>Any type of listening activities will work. What you need to take care of is to have relevant topics for their age.</li>
        					</div>
        				</div>
        				<div class="panel panel-default">
        					<div class="panel-heading"><h4>B) Speaking</h4></div>
        					<div class="panel-body">
        						<p>5-8 year olds: </p>
        						<ul>
        							<li>Playing with sounds and words (Odd one out).</li>
        							<li>Short rhymes. </li>
        							<li>Talking about their families, their likes and dislikes, daily activities. </li>
        							<li>Jazz chants with short dialogues (Carolyn Graham’s Jazz Chants for Children is a good book to practise speaking and structures in meaningful chunks)</li>
        							<li>songs, poems, rhymes and stories. (Mother Goose Jazz Chants, Singing, Chanting and Telling Tales and Jazz Chants Fairy Tales by Carolyn Graham) </li>
        							<li>Cutting and colouring animals, things, scenes from stories and speaking at the same time. </li>
        							<li>Info gap exercises such as: ‘What colour is the tail of your tiger?’ ‘Orange. What colour is the tail of your tiger?’ ‘Pink.’</li>
        							<li>Rhyming Drama (Carolyn Graham’s Jazz Chants Fairy Tales can be staged)</li>
        						</ul>
        					</div>
        					<div class="panel-body">
        						<p>9-11 year olds </p>
        						<ul>
        							<li>Quizzes (making their own); finding differences between two pictures (info gap); </li>
        							<li>speaking board games - eg in each square you write something they have to talk about for 30 seconds or a minute (my room, my pet; my favourite food; my friend)</li>
        							<li>drama, role-plays</li>
        						</ul>
        					</div>
        					<div class="panel-body">
        						<p>11-13 year olds</p>
        						<ul>
        							<li>problem solving activities and games; </li>
        							<li>board games with situations (“What would you do if you could understand the language of animals?”), when each of the players lands on a square with a situation they need to talk about it; the others will have to decide if they accept the answer or not. They can write the situations on the squares themselves.</li>
        							<li>Project presentations</li>
        							<li>Role-plays and drama; simulations</li>
        						</ul>
        					</div>
        					<div class="panel-body">
        						<p>14-16 year olds</p>
        						<ul>
        							<li>drama, role-plays and simulations</li>
        							<li>problem-solving activities</li>
        							<li>information gap activities on different topics/li>
        							<li>presentations of personal or group projects</li>
        						</ul>
        					</div>
        					<div class="panel-body">
        						<p>17-19 year olds</p>
        						<ul>
        							<li>role-plays, simulations</li>
        							<li>speeches and presentations</li>
        							<li>problem-solving; negotiation games.</li>
        						</ul>
        					</div>
        				</div>
        				<div class="panel panel-default">
        					<div class="panel-heading">C) Reading</div>
        					<div class="panel-body">
        						<p>5-8 year olds</p>
        						<li>words, short sentences; matching word with pictures.</li>
        					</div>
        					<div class="panel-body">
        						<p>9-11 year olds</p>
        						<li>fables, very short stories, cartoons, poems, simplified texts from classical literary works or written for classroom use (Penguin Graded Readers at http://www.penguinreaders.com/)</li>
        					</div>
        					<div class="panel-body">
        						<p>11-13 year olds</p>
        						<ul>
        							<li>Same graded readers as above on topics of interest to them.</li>
        							<li>Manuals to follow instructions on how to make things.</li>
        							<li>Short stories; anecdotes with a moral at the end.</li>
        							<li>Myths and legends; science fiction.</li>
        						</ul>
        					</div>
        					<div class="panel-body">
        						<p>14-16 year olds</p>
        						<ul>
        							<li>Youth magazines, true stories in the news, short stories, interviews with film or music stars; guide books. It is important to know what their interests are so that you can choose really interesting texts. Students can bring their own texts into the classroom, but they will have to show them to you a day in advance so that you can prepare a lesson around it.</li>
        							<li>Prediction tasks, jigsaw reading, </li>
        						</ul>
        					</div>
        					<div class="panel-body">
        						<p>17-19 year olds</p>
        						<ul>
        							<li>Newspaper articles, classifieds, literature … everything that is of interest to them.</li>
        							<li>Any activity that will allow them to get the most out of the texts and react to them in a personal way will work. So do not stop at factual questions, true/false statements, multiple choice exercises, scanning; start from here and move on to more personalized activities in the post-reading stage.</li>
        						</ul>
        					</div>
        				</div>
        				<div class="panel panel-default">
        					<div class="panel-heading"><h4>Writing</h4></div>
        					<div class="panel-body">
        						<p>5-8 year olds (if you introduce writing at this age): </p>
        						<ul>
        							<li>probably simple games such as ‘hangman’ or exercises that require to fill in one letter of a word.</li>
        							<li>Playing with letters: eg which words begin with a ‘b’? or Point at the letter ‘a’ or hold up the card with ‘c’</li>
        							<li>copying words from lists to match pictures;</li>
        							<p>(You need to do lots of exercises of handwriting with the children whose mother tongue is written in a different alphabet.)</p>
        						</ul>
        					</div>
        					<div class="panel-body">
        						<p>9-11 year olds</p>
        						<ul>
        							<li>Same as above if you introduce writing in English at this age.</li>
        							<li>Short messages, postcards, cards for different occasions. (They will enjoy making the cards as well.)</li>
        							<li>Short descriptions of favourite heroes</li>
        							<li>Speech bubbles in cartoons</li>
        						</ul>
        					</div>
        					<div class="panel-body">
        						<p>11-13 year olds</p>
        						<ul>
        							<li>Letters; e-mail messages; </li>
        							<li>Projects</li>
        						</ul>
        					</div>
        					<div class="panel-body">
        						<p>14-16 year olds</p>
        						<ul>
        							<li>Projects</li>
        							<li>Letters formal/informal; book/film/TV programme reviews</li>
        							<li>Group writing as technique  </li>
        						</ul>
        					</div>
        					<div class="panel-body">
        						<p>17-19 year olds</p>
        						<ul>
        							<li>Reflective essays</li>
        							<li>Creative writing</li>
        							<li>Functional writing (letters formal/informal; reports; reviews)</li>
        							<li>Articles</li>
        							<li>Speeches (text)</li>
        							<li>Projects</li>
        						</ul>
        					</div>
        				</div>
        				<div class="panel panel-default">
        					<div class="panel-heading">E) Vocabulary</div>
        					<div class="panel-body">
        						<p>5-8 year olds: </p>
        						<ul>
        							<li>Use realia and pictures. Visuals are extremely important for this age for both presentation and practice. Teach the vocabulary connected with what is familiar to the students in their environment (family, animals, food, things in the house and in the classroom, daily activities, etc). They learn fast by using all their senses. TPR is again the best approach for practising vocabulary through movement.</li>
        						</ul>
        					</div>
        					<div class="panel-body">
        						<p>9-11 year olds: </p>
        						<ul>
        							<li>Flash cards, word cards, pictures, matching exercises. </li>
        							<li>Games, such as Word Snap and Pelmanism, and other memory games; scrabble in a simplified form; word searches; simple crosswords with pictures instead of definitions.</li>
        							<li>Categorizing vocabulary items (eg furniture in the bedroom, living room, etc); labelling pictures. </li>
        							<li>They can start a vocabulary notebook where they can use drawings or stickers instead of definitions.</li>
        						</ul>
        					</div>
        					<div class="panel-body">
        						<p>11-13 year olds: </p>
        						<ul>
        							<li>Same as above but the topics change. </li>
        							<li>Word definitions can be used with learners of this age group. </li>
        							<li>Word battleships, crosswords, mind maps to organize vocabulary connected with different topics. </li>
        							<li>They can be encouraged to have a vocabulary book where they will write the words they want to remember with their definitions and drawings; also mind maps can be drawn in this notebook.  </li>
        						</ul>
        					</div>
        					<div class="panel-body">
        						<p>14-16 year olds</p>
        						<ul>
        							<li>Besides games such as crosswords, word searches, battleships, scrabble, word dominos for phrasal verbs or compound nouns, etc, etc, inferring meaning from texts is a useful activity. (eg intensive reading tasks such as ‘Read the text again and find the word that means the same as ‘fantastic’.)</li>
        							<li>Encourage the use of a monolingual dictionary and do activities based on the use of these dictionaries.</li>
        						</ul>
        					</div>
        					<div class="panel-body">
        						<p>17-19 year olds</p>
        						<ul>
        							<li>At this age students already have their own system of learning vocabulary. They have become more or less independent. They will always tell you what they need in terms of vocabulary or check the use of newly acquired words with you. They will challenge you in this respect.</li>
        							<li>Thesaurus dictionaries; synonyms and antonyms, idioms; register and style. </li>
        							<li>Word games such as more complicated crosswords are enjoyable and fun.</li>
        						</ul>
        					</div>
        				</div>
        				<div class="panel panel-default">
        					<div class="panel-heading">F) Grammar</div>
        					<div class="panel-body">
        						<p>
									5-8 year olds</p>
        						<ul>
        							<li>Young children pick up language in chunks and are unable to analyze language from a grammatical perspective. For example children will be able to understand the idea of the past tense once they have had stories told/read to them that use narrative past tenses. Grammar will be picked up rather than learnt.  Focus should be on ensuring that meaning is always paramount. Also, grammatical structures have to be presented in a context (story, poem, song, using the situation in the classroom or students’ experience) and practised through speaking by trial and error. Error correction should be very subtle, continuous and consistent. (eg “Miss T, yesterday I speaked English at home.” “Oh, you spoke English at home. That’s so nice. Who did you speak English to?” “I speaked with my mum.” “Great, you spoke with your mum. What about your brother?” “Yes, I spoke to him, too.”</li>
        							<li>Grammar chants will help internalisation of structures and their pronunciation (eg Carolyn Graham’s Jazz chants for Children and Grammar Chants)</li>
        						</ul>
        					</div>
        					<div class="panel-body">
        						<p>9-11 year olds</p>
        						<ul>
        							<li>Basically the routines are the same as above, but this time you can switch gradually to some language observation during the presentation stage. eg if you write on the board sentences with verbs in the past tense taken from a story you have just told them, you can encourage them to notice the pattern. (“What do some of the words naming actions have in common?”)</li>
        						</ul>
        					</div>
        					<div class="panel-body">
        						<p>11-13 year olds</p>
        						<ul>
        							<li>At this age they start to enjoy noticing language. Also some grammatical metalanguage can be used with them, such as ‘nouns’, ‘verbs’, ‘adjectives’, etc, but only if you think this will help or if it is imposed by the decision-making bodies in education. This issue is very culturally sensitive.</li>
        						</ul>
        					</div>
        					<div class="panel-body">
        						<p>14-16 year olds</p>
        						<ul>
        							<li>Most of these young learners start learning for examinations (national and international examinations, such as Cambridge ESOL exams). Grammar being one of the papers for examinations, you will need to teach the students more formal grammar. The basic structures have already been learned, most grammatical metalanguage is familiar, so the more complicated grammatical issues can be taught inductively or deductively.</li>
        							<li>Grammar games can be played. </li>
        							<li>Role-plays and other production activities are used to practice the structures in meaningful contexts.</li>
        						</ul>
        					</div>
        					<div class="panel-body">
        						<p>17-19 year olds</p>
        						<ul>
        							<li>This is definitely the age to teach for examinations. They can learn independently, they have the grammatical metalanguage that will help them use grammar reference books on their own. It should not be a problem for the students to pass these examinations if they test their communicative competence, such as Cambridge ESOL examinations. All they need to be taught is how to deal with time constraints and to be familiar with the test items. Practice using examination-type tasks helps a lot.</li>
        						</ul>
        					</div>
        					<div class="panel-body"><p>Now go back to the grid on page 5 and compare what you have already written with what you have learned so far. Complete the grid if you need to. There are some suggested answers in the Self-check key at the end of this unit.</p></div>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
        <div class="tab-pane" id="6a">
        	<div class="#">
        		<div class="row">
        			<div class="col-md-12">
        				<div class="panel panel-default">
        					<div class="panel-heading">Self Check Answer Key</div>
        					<div class="panel-body">
        						<p>SELF-CHECK 1 p.1 - Comments</p>
        						<p>1. Younger children learn languages better than older ones; children learn better than adults.
									This has become a myth based on the empirical observations of different people watching young children picking up the foreign language very quickly either through immersion into the foreign language, or by having been transplanted into a foreign language environment or by watching hours of English cartoon programmes on TV when parents are too busy to organize their children’s time. They definitely learn pronunciation better, but not necessarily the other components of the language. At the same time, their critical self is not so developed; ie they do not feel so embarrassed when making mistakes as adults usually do. There are studies that reached the conclusion that the older the child, the more effectively she/he learns a foreign language. In this respect, maybe teenagers make the best learners.
									Young children definitely do not learn better in formal classroom settings. Their cognitive skills and self-discipline are not developed enough to make the most of the teacher-filtered input; they rely more on acquisition than on conscious learning.</p>
									<p>2. Foreign language learning in school should be started at as early an age as possible. <br>
									This statement is directly connected with the previous one. As younger children do not necessarily learn better in a formal classroom environment for the reasons already mentioned above, it seems that language learning in school does not need to be started too early. Some say that the age of twelve would be ideal, others ten. It is also true that an early start to language learning, reinforced as the child grows older will lead to better long-term results. So, if there is time and if there are enough teachers in a school, you can start teaching young children at as early an age as possible.</p>
									<p>3. Children and adults learn languages basically in the same way. <br>
									This might be true only in an immersion situation when people of all ages can acquire the foreign/second language for survival. The differences become obvious in formal courses. Adults have a number of learning skills and strategies already developed and their capacity of understanding concepts and logical thinking is greater. Moreover, adults tend to be more disciplined and are more cooperative in the classroom. Adults are more patient, not so competitive, they are aware of their learning needs and no matter what type of motivation they have, they can set their own learning objectives and pursue them. They know why they are in a classroom, while most children have no choice in whether they want to be taught, where or how they are taught.</p>
									<p>4. Adults have a longer concentration span than children. <br>
									The difference here between children and adults is that children will always spend hours in activities that really interest them, but they will never have the adults’ patience to carry on activities for which they have no immediate intrinsic motivation. One major implication for teaching is that the teacher needs to choose (only) activities that the young learners will enjoy.</p>
									<p> 5. It is easier to interest and motivate children than adults. <br>
									This is partly true. The children’s motivation fluctuates rapidly. If the activities are apparently pointless to them or monotonous or boring, they lose their motivation quickly and become disruptive. By selecting interesting activities you can raise the children’s motivation more easily than that of the older learners. The older learners are more tolerant with apparently pointless activities; they can wait to see where the activity is leading them to. Children’s motivation varies more easily depending on the influences of the immediate surroundings (the teacher, the peers, the materials, other distractions); that of the older children tends to be more stable.</p>
									<p>SELF-CHECK 2 p.2 - Suggested answers 
									The characteristics of each age from reference books on child psychology; 
									Knowledge about how children of different ages learn from books on pedagogy on young learners; 
									Periodic contacts and discussions with the children’s parents to find out as much as you can about the children’s personalities and interests; 
									The other teachers of the class can provide a lot of information that you need about each child in the class and the class as a whole.</p>
        					</div>
        					<div class="panel-body">
        						<h4>Some practical ideas for sustaining motivation</h4>
        						<p>1 Experiment, take risks.  Do not be afraid of breaking the routine of the classroom. You need to find a balance between established routine of the classroom and introducing variety. Vary the kinds of things you do in the classroom to see what different students respond to best. For example, try short stories, films, classroom drama, songs, projects, grammar exercises, dictations, etc.</p>
        						<p>2 Choose ‘larger’ tasks.  If you read the features for each age group, you will see that, in one way or another, each age group likes projects, larger tasks for different reasons. Chose tasks that give students more ‘psychological space’ to plan their own work, set their own pace, make their own decisions about how and what they do. For example, process writing and simulations. Just do not forget to set rules that are accepted by everyone and monitor closely</p>
        						<p>3 Choose open-ended tasks.  The production tasks should leave room for individuals to express themselves or to contribute to a group task. As these tasks are part of a process, each child will have the chance to participate and contribute along the process in a personal way. The production task, such as making posters, writing poems, creating designs and describing them, etc where there is “no right answer” will give the children the feeling that they are valued as individuals, and the quality of their performance is judged in relation to their previous performance and not by comparison to their peers’ performance. </p>
        						<p>4. Provide choice.  Children learn effectively, on the principle “I like it, and I’ll do it. I don’t like it, but I’ll do it reluctantly or not at all.” If children are involved in deciding what to do, they are usually more committed to it and there is no excuse for them not to do it. As a teacher, you are in control. If you give them options, such as ‘You can choose exercise 3, 5 or 9. Or if you’d like to do something else, ask me first’ - you are still in control but the student will feel that his/her preferences are important to you, so you will gain more respect</p>
        						<p>5. Involve students in classroom decision-making.  Children have always enjoyed ‘playing the adults - the ones that make decisions’. You still make the decision about what your students will learn but other decisions, such as when homework is set, how long they will spend on a particular task, what they will do next lesson, and so on can be shared with them without any risk to the course as a whole.</p>
        						<p>6. Find out what students think.  Of course the final decision is yours, but you need to know what they think they need as well. In this way, you can make better informed choices about future lessons in terms of content and activities. Find out if students think they need more practice, if they have suggestions of their own, if they find things easy or difficult, boring or interesting.  You could place a ‘suggestion box’ in your class, or write an open-ended letter that students could complete with their ideas, or devise short questionnaires.</p>
        						<p>7. Think about how you give feedback and what you give feedback on. Feedback needs to be done at the end of each activity, and it has to be constructive. If you see any signs of failure in some students, try to identify aspects that you can praise and encourage and for the areas they did not meet the criteria of performance, explain to them, in concrete terms, what they could do to improve it next time.</p>
								<p>Another aspect here is the reward system that some teachers use. The students need to be trained to accept success and just enjoy it for its own sake. They need to be able to see when they were successful or not. That is what the feedback is for. The extrinsic reward as marks or smiley faces will only turn the intrinsic motivation into an extrinsic one: the race to get the rewards. The ones who take them more often will continue taking them, because they feel extremely confident. Success breeds success. The ones who did not get two or three in a row, will soon become demotivated, their self-confidence will diminish gradually, and after a while it is very hard for those students to keep up. They will also be the ones who will give up, will not participate in the lesson or become disruptive.</p>
								<p>Gradually, teach students to self-assess. Establish together criteria of performance and write descriptors for each criterion. They can then do self-assessment of their performance (speaking and writing) by measuring their performance against these criteria. They need patient training in using these descriptors of performance but it is worth it in the long run.
								</p>
								<p>8 Communicate a sense of optimism in learning. As a teacher, you have to show the students a belief that everyone can learn.  Encourage students to try, to take risks without fear of losing marks or feeling stupid. Show them how much they have learned (“You see; now you can spell these words correctly. Last week you couldn’t.”) Tell them that it is ok to ask for help, so offer help when they ask for it.</p>
								<br>
								<p>As Andrew Littlejohn put it “ Success comes in ‘cans’ not in ‘can’ts’.”</p>
								<p><i>Adapted after Andrew Littlejohn, 2001</i></p>
        					</div>
        				</div>
        					<div class="panel panel-default">
        						<div class="panel-heading"><h4>Self Check 3</h4></div>
        						<div class="panel-body">
        							<table class="table table-bordered">
										  <thead>
										    <tr>
										      <th scope="col">Age group</th>
										      <th scope="col">Early Elementary (Age 5 to 8)</th>
										      <th scope="col">Middle School (Age 9 to 11)</th>
										      <th scope="col">Early Adolescence (Age 11 to 13)</th>
										      <th scope="col">Middle Adolescence (Age 14 to 16)</th>
										      <th scope="col">Late Adolescence (Age 17 to 19)</th>
										    </tr>
										  </thead>
										  <tbody>
										    <tr>
										      <th scope="row">Topics</th>
										      <td>Family <br>
												Animals, <br>
												Daily activities, <br>
												Holidays (Christmas, Thanksgiving, etc)
												</td>
										      <td>Friends and family  <br>
												The Earth <br>
												School <br>
												Jobs <br>
												Nature
												</td>
										      <td>Heroes  <br>
												Relationships <br>
												Celebrations <br>
												Fashion <br>
												Manners <br>
												Nature and environment
												</td>
										      <td>Relationships <br>
												The world <br>
												Cinema/films <br>
												Music <br>
												Music and film stars <br>
												Young people of other cultures <br>
												Fashion <br>
												Travelling <br>
												Knowing yourself (strengths and weaknesses) <br>
												Environment
												</td>
										      <td>Anything that has to do with life, relationships, work, careers will do <br>
											“Taboo” topics <br>
											</td>
										    </tr>
										    <tr>
										      <th scope="row">Techniques and types of activities</th>
										      <td>Games involving physical movement <br>
												Arts and crafts <br>
												Pictures <br>
												Stories <br>
												Nursery rhymes <br>
												Chants
												</td>
											 <td>Chants <br>
												World knowledge (eg quizzes) <br>
												Mini-projects <br>
												Poster creation <br>
												Games
												</td>
										      <td>Projects and project presentations <br>
												Letter writing/emails
												</td>
										      <td>Drama <br>
												Creative writing  <br>
												Intensive and extensive reading <br>
												Projects
												</td>
										      <td>Projects  <br>
												Creative writing  <br>
												Functional writing  <br>
												Intensive and extensive reading  <br>
												Drama  <br>
												Reflective essays
												</td>
										    </tr>
										    <tr>
										      <th scope="row">Classroom management tips</th>
										      <td colspan="2">Always demonstrate activities  <br>
												Vary the pace frequently <br>
												Activities have to be short (max 10 minutes each) <br>
												Alternate quiet activities with activities that require movement. <br>
												Close monitoring <br>
												Establish classroom behaviour rules
												</td>
										      <td>Close monitoring most of the time.  <br>
												Establish classroom behaviour rules and be firm  <br>
												Vary pace and types of activities.
											 </td>
										      <td>Individual, pair and group work  <br>
												Establish roles in the groups <br>
												Vary the pace of activities <br>
												Very sensitive with the feedback <br>
												Start using descriptors of performance and encourage self-assessment
                                              </td>
										      <td>@se descriptors of performance and encourage self-assessment</td>
										      <td>At this age they know the routines. <br>
												They appreciate the teacher’s input for a short time, but they need to work independently as well.  <br>
												TTT should be very low
												</td>
										    </tr>
										  </tbody>
										</table>
        						</div>
        					</div>
        					<div class="panel panel-default">
        						<div class="panel-heading"><h4>SELF-CHECK 5 p.8 - Suggested answers</h4></div>
        						<div class="panel-body">
								<p>The children will not learn the language for its own sake; they will always want to learn English to be able to communicate, to get something or just for fun. With the exception of teenagers who will analyze the language to discover its subtleties and use it for self-expression, all the other young learners will not go into such depth.</p>
								<p>Vocabulary: The primary school children will need to develop their vocabulary and some functional language.</p>
								<p>Grammar cannot be taught formally and grammatical metalanguage is not an option. As they can’t analyse language yet, grammar is taught along with vocabulary, in chunks of language in meaningful contexts.</p>
								<p>Pronunciation can be left to acquisition most of the time. If the children can’t pronounce some sounds even after lots of exposure to the language, then some pronunciation exercises can help.</p>
								<p>Listening: The younger learners need a lot of listening before they are ready to speak. We can learn a lot about the acquisition of the second/foreign language from the way children learn their mother tongue/first language. TPR works best with these very young students. Listening to songs, short dialogues, watching short videos with the sound on are among the techniques that work with them.</p>
								<p>Speaking:  Young learners want to speak as soon as possible. They are like sponges absorbing everything the teacher says and how he/she says it. The rule here is slowly and steadily through constant revision and recycling. Choral repetition, songs, chants, nursery rhymes, short everyday dialogues, games, everything in a meaningful context for children will make them learn fast.</p>
								<p>Reading and writing are usually introduced later and gradually when the children already have an amount of vocabulary and functional language acquired or learned through listening and speaking. If the children start learning English at an older age, for example after 10 or 11 years of age, then all four skills can be developed more or less at the same time. Still, the principle remains: lots of listening and speaking practice, and only after that reading and writing which has been practised aurally and orally. This is due to the fact that English spelling and pronunciation are different, so children might get confused between the spoken and written word.
								Last but not least, young learners need to be taught how to learn gradually. Teaching learning strategies has to be introduced in a subtle manner through guidance and relevant tasks (eg organize vocabulary in their wordbooks under topics, using stickers or drawing, to which the written words can be added at a later stage.)</p>
								<br>
        					</div>
        				</div>
        				<div class="panel panel-default">
        					<div class="panel-heading"><h4>Bibliography</h4></div>
        					<div class="panel-body">
								<p>Littlejohn, Andrew (2001), Motivation; Where Does It Come from? Where Does It Go? In English Teaching Professional, Issue 19, March 2001</p>
								<p>Ur, Penny (1996) A Course in language Teaching, CUP</p>
								<br><p>Copyright  INTESOL Worldwide 2015</p>
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
        </div>
@endsection