@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('content')


<div class="panel panel-default">
	<div class="panel-body">Unit Four Module Two</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading"><h3>Unit 4 | Lesson Planning</h3></div>
	<div class="panel-body"><h4>Module 2 | Lesson Stages and Plans</h4></div>


	<div class="panel-body" id="exTab1">
		<ul  class="nav nav-pills">

			<li><a  href="#1a" data-toggle="tab" style="background-color:#fff;">Info</a></li>

			<li><a href="#2a" data-toggle="tab" style="background-color:#fff;">Lesson Planning</a></li>
			<li><a href="#3a" data-toggle="tab" style="background-color:#fff;">The Stages Of A Lesson</a></li>
			<li><a href="#4a" data-toggle="tab" style="background-color:#fff;">Building a Lesson Plan</a></li>
			<li><a href="#5a" data-toggle="tab" style="background-color:#fff;">Choosing Objectives</a></li>	
			<li><a href="#6a" data-toggle="tab" style="background-color:#fff;">Needs Analysis</a></li>
			<li><a href="#7a" data-toggle="tab" style="background-color:#fff;">Aims of the Lesson</a></li>
			<li><a href="#8a" data-toggle="tab" style="background-color:#fff;" class="orangeText">Self Check</a></li>
			<li><a href="#9a" data-toggle="tab" style="background-color:#fff;" class="blueText">Task</a></li>
			<li><a href="#10a" data-toggle="tab" style="background-color:#fff;" class="orangeText">Self Check</a></li>
		</ul>

	</div>
</div>

<div class="tab-content clearfix">

	<div class="tab-pane" id="1a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">Info</div>
						<div class="panel-body">
							<h4>At the end of this unit you will:-</h4>
							<ul>
								<li>a) know the importance of dividing an EFL lesson into separate parts</li>
								<li>b) know the role of the teacher at each stage</li>
								<li>c) understand the importance of adequate planning</li>
								<li>d) be able to write a detailed lesson plan</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="2a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Lesson Planning</h4></div>
						<div class="panel-body"><p>
							A lesson is a set length of time during which the student can either be introduced to a new teaching point, ie an aspect of the language that is new to him/her, or improve a skill, ie reading or writing - where new lexical items (vocabulary) may be introduced.</p>

							<p>It is important that what the student is presented with responds appropriately to his/her needs at the time. As the lessons progress the teacher will come to have a clearer idea of the student's level.</p>

							<p>Each lesson has to be planned, written as a document used for guidance, and executed carefully according to the plan. A simple plan and straightforward approach will yield a higher level of success in terms of a more positive response from the students. The latter must find what the teacher does, particularly the explanations, as easy to follow as possible.</p>
							<br>
							<h4>Planning means:</h4>
							<ol>
								<li>being aware of the time you, the teacher, have to deliver your lesson and to  Achieve your aims</li>
								<li>specifying what your aims for the lesson are and how you intend to put over your teaching point(s) in a coherent manner</li>
								<li>dividing your lesson up into stages starting with an introduction and leading to a further exploitation of the teaching point(s). You, as the teacher, should have an idea of how long each stage will last</li>
								<li>being aware of what the students will be doing at each stage (reading, writing, taking part in group work, listening to a tape etc.)</li>
								<li>choosing suitable situations and activities to exploit the teaching point(s);</li>
								<li>being aware what lexical items are likely to be used by the students in these situations.</li>
							</ol>
							<br>
							<p>One teacher may make more use of a textbook than another who might only include textbook material as a last resort. Planning inevitably involves rejection of ideas whatever the source may be as well as acceptance of them.</p>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane" id="3a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"></div>
						<div class="panel-body">
							<h4>The Stages Of A Lesson</h4>
							<p>1) Introduction</p>
							<p>At this first stage of the lesson it is the teacher's function to set the scene and arouse the students' interest. This involves some teacher talk - kept to a minimum throughout the lesson - with the students being invited to make a small contribution, say of vocabulary that may be needed later in the lesson. This small but vital input on the teacher's part should not exceed five minutes, sometimes two minutes will suffice (LTTT - limit teacher talking time). To generate interest whilst (s)he is talking the teacher may make reference to a visual aid(eg a picture, photo or any object brought into the classroom). An approach such as this will ensure the students' interest is captured whereas the "today we are going to talk about …" approach removes the mystery and the element of surprise from any lesson.</p>
							<br>
							<p>2) Presentation</p>
							<p>
							Once interest has been stimulated you can proceed with almost any piece of linguistic material eg a story, a song, a text, an advertisement or a short relevant excerpt from a tape or video. Here you are laying the foundations of the lesson. Students will respond to some form of presentation and cannot be expected to talk unless there is something which has gone on in the classroom that has given them the stimulation to make them speak.</p>
							<br>
							<p>3) Exploitation</p>
							<p>This is the stage where the student gets the opportunity to use the new aspect of language in a suitable context. It is important not to let the student loose straightaway with the language 'in private'. The practice they are offered should be controlled and undertaken 'in public' before their peers.  For example, the students might be asked to repeat an aspect of new language chorally or individually. Teacher-led drills are appropriate at this point where the students can apply the rules contained in an example to further questions.</p>

							<p>As the lesson progresses the teacher's guidance diminishes and he/she assumes more the role of a monitor of pair and group work. At the end of this a representative of the pair or group can at the teacher's request feed back to the rest of the class the results of their discussion.</p>

							<p>Before the lesson the teacher should fully familiarise him (her) self with the plan and once the lesson has started the plan should serve as an occasional point of reference.</p>

							<p>It is prudent to ensure you have enough material and something 'up your sleeve' to conclude the lesson in case you complete what you have planned sooner than expected.  A short activity such as a song or a game reflecting what has been learned may be an appropriate way to end.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="4a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>BUILDING A LESSON PLAN</h4></div>
						<div class="panel-body">
							<p>A successful lesson is dependent on good planning. The easiest way to plan a lesson is to first identify your objectives, ie what you want to teach, then plan your lesson around the warm-up, presentation, practice and production (exploitation) stages of a lesson. </p>
							<p>Here is a guide to a basic lesson plan:</p>
							<br>
							<p>WARM-UP: <br>
							<p><b>What?</b> This is the first part of the lesson where you say hello, return homework and perhaps review previous lessons and generate interest. A warm-up should be a fun and easy activity.</p>
							<p><b>Why?</b>
							</p>
								<ul>
									<li>relaxes students</li>
									<li>generates interest and focus</li>
									<li>can act as a lead-in to the session’s main theme</li>
									<li>gets students talking in English</li>
								</ul>
							<br>
							<p>Points to consider:</p>
							<p>The warm-up should be fairly easy so that students feel relaxed and confident using English. At this stage you can introduce some of the lesson’s vocabulary and start to get students to think about the topic.</p>
							<br>
							<p>OBJECTIVE</p>
							<p><b>What?</b> Introduce the day’s topic and structures on the board, and if possible tie them in to what has been studied in previous lessons.</p>
							<p><b>Why?</b></p>
							<ul>
								<li>students can see what they are going to learn – gives them a sense of purpose</li>
								<li>increases student motivation</li>
								<li>ensures that you do not get distracted from meeting the objectives</li>
							</ul>
							<br>
							<p>Points to consider: <br>
							It may help you with pacing and timing the lesson.
							</p>
						</div>
						<div class="panel-body">
							<p>PRESENTATION</p>
							<p><b>What?</b> This is the most teacher-centred part of the lesson. The teacher presents the students with the necessary language and structures for the lesson. It is important at this stage to elicit previous knowledge. At this stage chorusing the structures can be used.</p>
							<p><b>Why?</b> - it provides students with the necessary tools to complete the later communicative, fluency-based activities.</p>
							<p>Points to consider: <br>
							Although this part of the lesson is teacher-centred it is important to elicit from the students what they already know. This is important because students can only take in so much new information in one lesson; by finding out how much the students already know you will be able to judge the right amount for that lesson.</p>
						</div>
						<div class="panel-body">
							<p>CONTROLLED PRACTICE</p>
							<p><b>What?</b> This stage gives students the opportunity to practice what has just been presented. It allows students to practice and get accustomed to the language before trying to use it in a context or situation. The teacher should actively monitor and correct.</p>
							<p><b>Why?</b></p>
							<ul>
								<li>practice and reinforce the key language points</li>
								<li>teachers can correct any problems</li>
								<li>teachers can check students’ understanding</li>
							</ul>
							<br>
							<p>Points to consider: <br>
							Students should practice manipulating the language that has just been presented. The exercises should focus on the language points and not introduce any other information (eg role-play exercises). The students need to cement the language so that they can use it competently later.
							</p>
						</div>
						<div class="panel-body">
							<p>FREER PRACTICE</p>
							<p><b>What? </b>Now that the students feel more comfortable with the language they should be given the opportunity to put it into a context. However, it is unrealistic to ask the students to concentrate on both the new language and to have to think of a situation or information for a role-play. So give students a standard situation, with all the appropriate information for them to work with.</p>
							<p><b>Why?</b></p>
							<ul>
								<li>it provides extra practice in using target language</li>
								<li>it is a transitional phase leading to freer and personalised use of the target language in the production stage</li>
							</ul>
							<br>
							<p>Points to consider: <br>
							At this stage the students need the opportunity to practice the language in a natural context. However, they are not yet confident enough to think of the context and use the language freely. Therefore the teacher should provide the discussion questions or role-play scenario or whatever information the students need to practice the target language.</p>
						</div>
						<div class="panel-body">
							<p>PRODUCTION (FREE PRACTICE)</p>
							<p><b>What?</b> Students are now offered the opportunity to use the target language in a free and personalised way. The language is internalised and students are able to produce and use it appropriately. The focus at this stage of the lesson is fluency and mimicking, as far as possible, authentic language outside the classroom. The teacher acts as facilitator and monitor, and should delay feedback until after the activity.</p>
							<p><b>Why?</b></p>
							<ul>
								<li>it ties the lesson together</li>
								<li>it personalises the target language</li>
								<li>it enables students to interact naturally</li>
								<li>it builds confidence</li>
							</ul>
							<br>
							<p>Points to consider: <br>
							At this point you should only be giving students ideas to work with, and allowing them to use the language as they feel appropriate. Also you can encourage the introduction of other language that the students know. The aim is to let the students use the language in as natural and as fluent a way as possible within the classroom.</p>
						</div>
						<div class="panel-body">
							<p>FOLLOW-UP</p>
							<p><b>What?</b> Students are now brought back together as a group. It provides the opportunity for feedback and reflection and encouragement.</p>
							<p><b>Why?</b></p>
							<ul>
								<li>it helps students remember the objective of the activity</li>
								<li>it brings the students back together as a class</li>
								<li>it provides a sense of achievement</li>
							</ul>
							<br>
							<p>Points to consider: <br>
							This is preparation to finish the lesson. Students need the opportunity to feed back and ask any questions.</p>
						</div>
						<div class="panel-body">
							<p>WRAP-UP</p>
							<p><b>What?</b> This brings the lesson to a close. Provide a quick recap of the objectives, target structures and vocabulary studied that day. Tie in the lesson with previous or later lessons. Summarise the objectives of the lesson and what the students have learnt. It is time to set homework and talk about the next session.</p>
							<p><b>Why?</b></p>
							<ul>
								<li>it brings the session to a close</li>
								<li>it lets students see what they have achieved and gives them a sense of achievement</li>
								<li>it prepares the students for the next lesson</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="5a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Choosing Objectives</h4></div>
						<div class="panel-body"><p>
							It is essential to have clear objectives, and to be clear about not only what you are teaching but also why you are teaching it. Clear objectives provide a clear framework for the teacher to select and evaluate the potential value of activities. Objectives should be written on the board and shared with students so that they can see what they have learnt by the end of a lesson. <br>
							Your objectives for a lesson can focus on different areas, for example:
						</p>
						<br>
						<ul>
							<li>Grammar and function <br>
  							eg giving advice  - ‘If I were you.’</li><br>
							<li>Correction <br>
							eg a specific focus such as definite and indefinite articles, combined with general consistent correction</li><br>
							<li>Discussion Skills <br>
  							eg agreeing and disagreeing</li><br>
							<li>Vocabulary <br>
								Introduced so that students become familiar with and are able to use the words</li><br>
							<li> Idioms <br>
  								Introduced so that students become familiar with and are able to use them</li><br>
							<li>Specific reading / listening /writing skills <br>
  							Introduced to develop specific skills eg predictive skills, listening / reading for  
  							gist / writing stories focussing on the correct use of past simple and past continuous</li>
						</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	


	<div class="tab-pane" id="6a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Needs Analysis</h4></div>
						<div class="panel-body">
							<p>The following should be considered:</p>
							<p>Why is the language needed? <br>
 								- for work, for study, for an examination, for travel, for promotion etc.</p>
 							<br>
 							<p>
 								How will the language be used? <br>
								- Medium: speaking, writing, reading <br>
								- Channel: eg telephone, face to face <br>
								- Types of text or discourse: eg academic texts, lectures, informal  
								  conversations, technical manuals etc 

 							</p>
 							<br>
 							<p>
 								What will the content be? <br>
								- Subjects: general, specific eg law, medicine, business etc <br>
								- Level: secondary school, postgraduate, layman, technician etc <br>
								- tailored to students’ interests

 							</p>
 							<br>
 							<p>Who will the learner use the language with? <br>
								- native or non-native speakers <br>
								- level of knowledge of the receiver eg professional, student, layman <br>
								- relationship eg colleague, superior, teacher, relatives
								</p>
							<br>
							<p>
								Where will the language be used? <br>
								- physical setting eg office, hotel, school etc <br>
								- human context eg alone, meetings, on the telephone, presentations <br>
								- context eg in own country, abroad etc
							</p>
							<br>
							<p>
								When will the language be used? <br>
								- frequently, seldom <br>
								- concurrently with the course or subsequently

							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="7a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>AIMS OF THE LESSON</h4></div>
						<div class="panel-body"><p>
							Defining the aims: You need to be clear with the aims; a lesson can be deemed a success if the students have learnt what you wanted them to learn.
						</p>
						<p>
							Your aims for a lesson may be to introduce a grammatical point, to develop reading skills or raise awareness of aspects of phonology etc.
						</p>
						<p>Do not be over ambitious and introduce too many aims for one lesson; students need time to digest and be able to use new language correctly.</p>
						<p>Do not overestimate or underestimate your students; you must gauge and aim the lesson at the right level for your class.</p>
						<p>Whatever you are teaching (eg a grammar structure, a set of vocabulary or reading skills etc), it is essential to have a theme for the lesson; a lesson with a theme is a lot more digestible than one without. You should consider what interests your students and what theme would help them to learn the material more effectively. Another thing to consider is function, eg complaining - this gives students a reason for studying.</p>
						<p>Examples of lesson aims:</p>
						<p>By the end of the lesson the students will be able to: <br>
							<ul>
								<li>talk about their daily routines</li>
								<li>ask others about their daily routines</li>
								<li>use adverbs of frequency (ie sometimes, usually, often, occasionally, never)</li>
							</ul>

						</p>
						<p>
							By the end of the lesson the students will be able to: <br>
							<ul>
								<li>understand idioms related to work</li>
								<li>use work related idioms appropriately in conversation</li>
							</ul>
						</p>
						<p>By the end of the lesson the students will be able to: <br>
							<ul>
								<li>Write a simple text about their experiences using appropriate linking devices (ie and, but, so)</li>
								<li>Use appropriate linking devices within a simple written text to indicate the chronological order of events (ie first, then, after, later)</li>
							</ul></p>
					</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>HOW TO ORGANISE THE STUDENTS</h4></div>
						<div class="panel-body">
							<p>It is important when planning a lesson to consider how you are going to organize the students to maximize the effectiveness of the lesson.
							First we can consider Student Talk Time (S.T.T.), and then Teacher Talk Time (T.T.T.). Of course the aim of any lesson is to maximize S.T.T., however, during the presentation stage of the lesson, for example, there is going to be more T.T.T. than during the production stage.</p>
							<p>
							We also need to consider the role of the students and the teacher during each activity, eg the teacher’s role during an activity might be observation of individuals, groups or pairs, or more active presentation or feedback to the class. Remember to vary the dynamics of the group to encourage the participation of all students.</p>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>POTENTIAL PROBLEMS</h4></div>
						<div class="panel-body"><p>
							However experienced a teacher you may be, at times difficulties will arise. If these difficulties have been considered before the lesson and contingency plans drawn up, you will be in a better position to deal effectively with the problems.</p>

							<p>The easiest way to predict potential problems is to look at the lesson from the students’ point of view; this is where it is beneficial to have a good understanding of your students and their backgrounds.
							There are, of course a whole host of potential problems but, with a little preparation, many of them can be overcome.</p>

							<p>With regards to comprehension difficulties, the answer is to have concept checking questions at hand. Think of questions that you can ask to check that students understand what you are teaching.</p>

							<p>Similarly, if you see a problem with phonology, have a few words written down to give students further practice.</p>

							<p>Apart from difficulties with concept (meaning), structure (the grammar) and phonology (pronunciation), you may experience related to cultural differences. For example in some cultures students may not be used to working with members of the opposite sex, or they may not be used to having to express opposing opinions etc. When handled gently and carefully these problems can be overcome; it is important to explain how your classes work from the start.</p>

							<p>Students not showing interest: this can happen tom even the most experienced and inspiring teacher; it doesn’t necessarily reflect on the lesson being taught. Consider possible extenuating circumstances, eg hung over students, personal problems etc. It is essential therefore to always have standby material which can be used if the main material is not going well. Always be ready to listen to your students and be flexible; it is better to leave an activity (with the option of returning to it at a later date) than to continue with an activity that is clearly not working.</p>

							<p>sStudents asking questions that you don’t know the answer to: If students have questions that relate to the content of the lesson, hopefully you will have identified them as potential problems and will therefore have ready answers. However, other questions unrelated to the lesson may arise. When this occurs and you do not know the answer, remember that everyone is human. You may be able to deflect a problem by telling students that you will deal with it later, hence giving yourself the opportunity to prepare your answer. Of course it is better to admit to students that you don’t know an answer rather than to try to adlib and waffle your way through it, which will only confuse students more.

						</p></div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>TIME MANAGEMENT</h4></div>
						<div class="panel-body">The timing of your lessons is an area which should be built into your lesson plan, to guide you during your lesson, and to avoid running out of materials, or conversely being unable to finish what you have planned due to lack of time. Write estimated timings for each activity in your lesson plan. Timing is something you will find easier as you get to know your students, and also as you gain experience.</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>REFLECTIVE PRACTICE</h4></div>
						<div class="panel-body">
						<p>
							To be an effective teacher it is important that we honestly review and evaluate our teaching. We should consider:- was the lesson effective?
						</p>
						<ul>
							<li>were the aims met?</li>
							<li>were the students able to master and produce the language?</li>
							<li>did the students enjoy the lesson?</li>
							<li>how could the lesson have been improved?</li>
							<li>was there enough S.T.T.?</li>
						</ul>
						<br>
						<p>
							Also, do not be afraid to ask for the students’ honest feedback on activities and lessons - this feedback can be invaluable. This can be done in the format of an anonymous questionnaire.
						</p>
					</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h4>EXAMPLE LESSON PLAN 1 – Group/Class Teaching</h4></div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-3"><h4>Level:</h4></div>
								<div class="col-md-9"><p>Elementary - Adults</p></div>
							</div>
							br
							<div class="row">
								<div class="col-md-3"><h4>Aims:</h4></div>
								<div class="col-md-9"><p>To practise the present progressive in its various contexts.</p></div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-3">Assumptions:</div>
								<div class="col-md-9">Students may be aware of this tense's basic use, ie to
describe actions going on at the moment of speaking, but are unaware of its uses in other contexts.</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-3">Anticipated problems:</div>
								<div class="col-md-9">Students may want to use will + verb when referring to the near future.</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-3">STAGES</div>
								<div class="col-md-3">WHAT TO DO</div>
								<div class="col-md-3">STUDENT ACTIVITY</div>
								<div class="col-md-3">TEACHER ACTIVITY</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-3">Introduction:</div>
								<div class="col-md-3">Questions about sport/spare time in the present simple show picture of football team you support. Talk about plans for Sat afternoon. I'm travelling to watch - / playing at -</div>
								<div class="col-md-3">Active revision of present simple</div>
								<div class="col-md-3">Focis of Student's attention</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-3">Presentation</div>
								<div class="col-md-3">Show another picture connected with sport (to link with intro) Ask and answer own question "What's going on here? "Well .....   "What's he/she wearing?  Write present participles on board with ref to doubling of consonants (eg hitting) and dropping of 'e' before ....ing</div>
								<div class="col-md-3">Passive</div>
								<div class="col-md-3">Focus of students' attention</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-3">Practice (1)</div>
								<div class="col-md-3">Students given picture(s) with 2 or more people in them performing various actions. Ask what's going on. Write unfamiliar verbs on board (public practice).</div>
								<div class="col-md-3">Active - response to teacher's questions</div>
								<div class="col-md-3">Interaction - with students</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-3">Practice (2)</div>
								<div class="col-md-3">They are given a timetable with different fixed engagements. All (3 or 4) would like to go out for a meal sometime. <br> Negotiation: I can’t come on Tuesday evening. I’m seeing my old school friend (pre-arranged)
								</div>
								<div class="col-md-3">Active - ask and answer in groups</div>
								<div class="col-md-3">Passive – monitor by walking around. Feedback: spokesperson could comment on how difficult it was or otherwise to arrange the meeting.</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-3">Practice (3)</div>
								<div class="col-md-3">Writing: you're planning a holiday in the UK. Write a letter to your penfriend who lives in Manchester informing him/her of your plans for your holiday. Give details of your plans to visit the city of . . . (date). Begin letter Dear . . .   End letter Kindest regards . . .</div>
								<div class="col-md-3">Active - working on his/ her own</div>
								<div class="col-md-3">Passive - available if required</div>
							</div>	
							<br>
							<div class="row">
								<div class="col-md-3">Production</div>
								<div class="col-md-3">Time left? Think of your regrettable habits . . . and put them down on paper, eg I'm always locking myself out of the house.</div>
								<div class="col-md-3">Active - working on his/ her own</div>
								<div class="col-md-3">Passive - monitor feedback at the end</div>
							</div>					
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="tab-pane" id="8a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Self Check</h4></div>
						<div class="panel-body"><p>
							{!! Form::open(['action' => 'Admin\Int120u4m2s1t1Controller@store', 'method' => 'POST']) !!}
						
						<br>
						<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">
				

				
						<div class="form-group">
							{!! Form::label('u4m2s1q1', 'Why is it important to plan a lesson') !!}
							{!! Form::text('u4m2s1q1', null, ['class' => 'form-control'])!!}
						
							
						</div>

					
						<div class="form-group">
							{!! Form::label('u4m2s1q2', 'Write a lesson plan for a group of beginners on the use of the prepositions on/in/at. Refer to the grammar unit. Make sure you PRESENT the language point; then you get the students to PRACTISE (maybe in more than one activity); then allow the students to PRODUCE the language.') !!}
							{!! Form::text('u4m2s1q2', null, ['class' => 'form-control'])!!}
						
							
						</div>

						{!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}

						{!! Form::close() !!}
					
						</p></div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane" id="9a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>EXAMPLE LESSON PLAN 2 – One to One Teaching</h4></div>
						<div class="panel-body">
							<p>This is the scenario. A student has decided that he/she would like to attend a British university. Their standard of English is quite good but in order to get it up to university entrance standard they have decided to supplement school / college lessons with a weekly one-to-one lesson. This is likely to be a fairly long term arrangement so you will get to know each other very well. As the selection of, and application to, a university can be a lengthy procedure, you have decided to concentrate on this in the early stages.</p>
						</div>
					</div>
					<div class="panel panel-default">
						
						<div class="panel-body">
							<div class="row">
								<div class="col-md-3">Level:</div>
								<div class="col-md-9">Upper Intermediate.</div>
							</div>
							<div class="row">
								<div class="col-md-3">Lesson Duration:</div>
								<div class="col-md-9">2 hours</div>
							</div>
							<div class="row">
								<div class="col-md-3">Aim:</div>
								<div class="col-md-9">To guide student through a typical university prospectus.</div>
							</div>
							<div class="row">
								<div class="col-md-3">Object:	</div>
								<div class="col-md-9">To enable the student to make an informed choice
                               	when applying for university entrance.</div>
							</div>
							<div class="row">
								<div class="col-md-3">Assumptions:</div>
								<div class="col-md-9">Student has chosen his/her subject and has a long listof suitable universities.</div>
							</div>
							<div class="row">
								<div class="col-md-3">Expected problems:</div>
								<div class="col-md-9">Student is unfamiliar with collating information from a range of prospectuses and handbooks. </div>
							</div>
							<div class="row">
								<div class="col-md-3">Materials:</div>
								<div class="col-md-9">Two or three prospectuses. <br>
									Atlas/maps. <br>
			    					Tourist brochures. <br>
			    					Grid.
			    				</div>
							</div>
							<div class="row">
								<div class="col-md-3">Warm-up:(10 mins)</div>
								<div class="col-md-9">General chat about everyday matters.  Teacher 
		   						speaking more freely than he/she would in a class 
                               	situation. This helps to build social skills and the  
                               	vocabulary used when making small-talk.</div>
							</div>
							<div class="row">
								<div class="col-md-3">Introduction (20 mins)</div>
								<div class="col-md-9">Discuss with student what he/she might be looking 
								for in a British university (other than a suitable course). 
							 	Allow student to lead the discussion as far as
							 	possible.  
							 	[Teacher to make notes and also have a list of suitable 
							 	ideas to prompt student if necessary].
							 	<br>
							 Look at geographical situation of the universities under    
							consideration. Look through tourist information.</div>
							</div>
							<div class="row">
								<div class="col-md-3">Presentation:(15 mins)</div>
								<div class="col-md-9">Produce two or three prospectuses. Show how they all 
			differ in layout. Begin to look at sections which address 
 	the particular concerns expressed in the Introduction.
 	[Teacher's note: unlike a class situation where you 
 	have prepared a lesson on a specific structure with  
 	pre-prepared materials, in this instance to a large  
 	extent you are letting the student dictate the material to  
                             	be covered. Therefore it is important that you have
familiarised yourself with all those aspects which you                                      envisage the student will want to discuss. Naturally of                                     particular concern will be information for international                                     students].	</div>
							</div>
							<div class="row">
								<div class="col-md-3">Practice:(10 - 15 mins)</div>
								<div class="col-md-9">Give student photocopied pages of information for 
									International Students to read through and ask him/her 
							         		to highlight anything he/she doesn't understand or 
							items of particular interest.

							[Teacher's note: try to anticipate problem areas and 
							prepare remedial materials].</div>
							</div>
							<div class="row">
								<div class="col-md-3">Teacher: </div>
								<div class="col-md-9">Whilst student is reading, put the kettle on and make a drink.</div>
							</div>
							<div class="row">
								<div class="col-md-3">B R E A K:(10 mins) </div>
								<div class="col-md-9"> Coffee and a chat. Once again let the student lead the 
	way unless you have something specific you wish to discuss but keep it light. Possibly look more closely at the tourist material to give the student an idea of what is available in the vicinity of the university</div>
							</div>
							<div class="row">
								<div class="col-md-3">Practice: (20 mins) </div>
								<div class="col-md-9">Go through the reading material and test
           		understanding. At this stage you can address any  
                           		problems encountered</div>
							</div>
							<div class="row">
								<div class="col-md-3">Production: (20 mins)</div>
								<div class="col-md-9">Introduce the idea of formulating a chart to enable
	       		the student to compare the pros and cons of several 
		        		universities. </div>
							</div>

							<p>Using the pre-prepared grid, extract information to complete a comparison table. Get the student to suggest suitable column headings.</p>


						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">Suggestion for comparison table</div>
						<div class="panel-body">
							<table class="table">
							  <thead>
							    <tr>
							      <th scope="col">#</th>
							      <th scope="col">Situation: City/ Town/ Rural</th>
							      <th scope="col">No of Foreign Students</th>
							      <th scope="col">Type(s) of accomodation</th>
							      <th scope="col">Emglish language lessons/ assistance</th>
							    </tr>
							  </thead>
							  <tbody>
							    <tr>
							      <th scope="row">1</th>
							      <td>Manchester</td>
							      <td></td>
							      <td></td>
							      <td></td>
							    </tr>
							    <tr>
							      <th scope="row">2</th>
							      <td>York</td>
							      <td></td>
							      <td></td>
							      <td></td>
							    </tr>
							    <tr>
							      <th scope="row">3</th>
							      <td>East Anglia</td>
							      <td></td>
							      <td></td>
							      <td></td>
							    </tr>
							  </tbody>
							</table>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-3">Homework:</div>
								<div class="col-md-9">Read through other prospectuses and create your own comparison grid.</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="tab-pane" id="10a">
		<div class="#">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Self Check 3</h4></div>
						<div class="panel-body">
							{!! Form::open(['action' => 'Admin\Int120u4m2s1t2Controller@store', 'method' => 'POST']) !!}
						
						<br>
						<input id="user_id" name="user_id" type="hidden" value="{{Auth::user()->id}}">
				

				
						<div class="form-group">
							{!! Form::label('u4m2s1q3', '1. Do all language teachers present a language point in the same way?') !!}
							{!! Form::text('u4m2s1q3', null, ['class' => 'form-control'])!!}
						
							
						</div>

				
						<div class="form-group">
							{!! Form::label('u4m2s1q4', '2. Must the textbook be used?') !!}
							{!! Form::text('u4m2s1q4', null, ['class' => 'form-control'])!!}
						</div>

						<div class="form-group">
							{!! Form::label('u4m2s1q5', '3. By the end of your lesson what must you have achieved?') !!}
							{!! Form::text('u4m2s1q5', null, ['class' => 'form-control'])!!}	
						</div>

						<div class="form-group">
							{!! Form::label('u4m2s1q6', '4. During which stage of the lesson does the teacher set the scene?') !!}
							{!! Form::text('u4m2s1q6', null, ['class' => 'form-control'])!!}	
						</div>

						<div class="form-group">
							{!! Form::label('u4m2s1q7', '5. What serves as an occasional point of reference during the lesson?') !!}
							{!! Form::text('u4m2s1q7', null, ['class' => 'form-control'])!!}	
						</div>

						<div class="form-group">
							{!! Form::label('u4m2s1q8', '6. List 3 appropriate ways to end a lesson.') !!}
							{!! Form::text('u4m2s1q8', null, ['class' => 'form-control'])!!}
							{!! Form::text('u4m2s1q9', null, ['class' => 'form-control'])!!}	
							{!! Form::text('u4m2s1q10', null, ['class' => 'form-control'])!!}	
						</div>

						{!! Form::submit('Save Answers', ['class' => 'btn btn-info form-control'])!!}

						{!! Form::close() !!}
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>






</div>
@endsection