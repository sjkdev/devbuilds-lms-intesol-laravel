@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')

	
		<div class="div">
			<div class="row">
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">Welcome {{ Auth::user()->name }}</div>
						<div class="panel-body">User ID | {{ Auth::user()->id }}</div>
						<div class="panel-body">User ID | {{ Auth::user()->id }}</div>
						
					</div>
				</div>

				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">Roles</div>
						<div class="panel-body">{{ Auth::user()->role }}</div>
					</div>
				</div>
			</div>

		{{-- 	<div class="datadump">
				$trainees
			</div> --}}

			<div class="row">
				<div class="col-md-2">
					<div class="panel panel-default">
						<div class="panel-heading">Your trainee ID</div>
						 @foreach ($users as $user)
						 <ul>
						 	<li>{{ $user->id }}</li>
						 </ul>
    
    					@endforeach
						{{-- @foreach($trainees as $trainee)
							<ul>
								<li>{{ $trainee->name }}</li>
								<li>{{ $trainee->level }}</li>
								<li>{{ $trainee->tasksDone }}</li>
							</ul>
						@endforeach --}}
						<div class="panel-body"></div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="panel panel-default">
						<div class="panel-heading">User List</div>
						 @foreach ($users as $user)
						 <ul>
						 	<li>{{ $user->name }}</li>
						 </ul>
    
    					@endforeach
						
						<div class="panel-body"></div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="panel panel-default">
						<div class="panel-heading">Student Details</div>
						<div class="panel-body">
							
							@foreach($trainees as $trainee)

							@foreach($users as $user)
							{{-- @if($user->isTrainee) --}}
							<ul>
								{{-- <li>User ID | {{ $trainee->user_id->name() }}</li> --}}
								{{-- <li>User ID | {{ $trainee->user_id->pluck->name }}</li> --}}
								<li>Role | {{ $user->role_name}}</li>
								<li>Name | {{ $user->name}}</li>
								<br>
								<li>User ID | {{ $trainee->user_id }}</li>
								<li>Course ID | {{ $trainee->course_id }}</li>
								<li>Status | {{ $trainee->course_student_status }}</li>
								<hr>
							</ul>
							{{-- @endif --}}
						@endforeach
						@endforeach
						</div>
					</div>
				</div>
				<div class="col-md-4 col-md-offset-6">
					<div class="panel panel-default">
						<div class="panel-heading">Messages</div>
						<div class="panel-body"><a href="{{ route('admin.messenger.index') }}">Inbox</a>



						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-4 col-md-offset-6">
					<div class="panel panel-default">
						<div class="panel-heading">Notifications</div>
						<div class="panel-body"></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-4 col-md-offset-6">
					<div class="panel panel-default">
						<div class="panel-heading">Responses</div>
						<div class="panel-body"></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">Course List</div>
						<div class="panel-body">{{ Auth::user()->course }}</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">Notifications</div>
					<div class="panel-body">{{ Auth::user()->notifcations }}</div>
					</div>
				</div>
			</div>
		</div>


		{{--         <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($users) > 0 ? 'datatable' : '' }} @can('user_delete') dt-select @endcan">
                <thead>
                    <tr>
                        @can('user_delete')
                            <th style="text-align:center;"><input type="checkbox" id="select-all" /></th>
                        @endcan

                        <th>@lang('global.users.fields.name')</th>
                        <th>@lang('global.users.fields.email')</th>
                        <th>@lang('global.users.fields.role')</th>
                                                <th>&nbsp;</th>

                    </tr>
                </thead>
                
                <tbody>
                    @if (count($users) > 0)
                        @foreach ($users as $user)
                            <tr data-entry-id="{{ $user->id }}">
                                @can('user_delete')
                                    <td></td>
                                @endcan

                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                              
                                                                <td>
                                    @can('user_view')
                                    <a href="{{ route('admin.users.show',[$user->id]) }}" class="btn btn-xs btn-primary">@lang('global.app_view')</a>
                                    @endcan
                                    @can('user_edit')
                                    <a href="{{ route('admin.users.edit',[$user->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                    @endcan
                                    @can('user_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.users.destroy', $user->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
 --}}
		 

	

@endsection