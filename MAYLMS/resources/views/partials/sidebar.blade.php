@inject('request', 'Illuminate\Http\Request')
<style>
  li a{
    color:#028482!important;
  }
</style>

<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">

            @can('admin_meta_access')
            <li><a>
                <i class="fa fa-address-book"></i>
            Admin Menu</a></li>
            <li class="{{ $request->segment(1) == 'home' ? 'active' : '' }}">
                <a href="{{ url('/') }}">
                    <i class="fa fa-wrench"></i>
                    <span class="title">@lang('global.app_dashboard')</span>
                </a>
            </li>
            @endcan


            {{-- trainer menu panel --}}
            @can('trainer_meta_access')
            <li><a>
                <i class="fa fa-address-book"></i>
            Trainer Menu</a></li>
            
            <li class="{{ $request->segment(1) == 'home' ? 'active' : '' }}">
                <a href="{{ url('/') }}">
                    <i class="fa fa-wrench"></i>
                    <span class="title">@lang('global.app_dashboard')</span>
                </a>
            </li>
            @endcan

            @can('120hr_trainee_meta_access')
            <li><a>
                <i class="fa fa-address-book"></i>
            120 HR Trainee Menu</a></li>



            <li class="{{ $request->segment(1) == 'home' ? 'active' : '' }}">
                <a href="{{ url('/') }}">
                    <i class="fa fa-wrench"></i>
                    <span class="title">@lang('global.app_dashboard')</span>
                </a>
            </li>
            @endcan


           

            @can('trainee_meta_access_nil')
            <li><a>
                <i class="fa fa-address-book"></i>
            Trainee Menu</a></li>



            <li class="{{ $request->segment(1) == 'home' ? 'active' : '' }}">
                <a href="{{ url('/') }}">
                    <i class="fa fa-wrench"></i>
                    <span class="title">@lang('global.app_dashboard')</span>
                </a>
            </li>
            @endcan

             @can('190hr_trainee_meta_access')
            <li><a>
                <i class="fa fa-address-book"></i>
            190 HR Trainee Menu</a></li>



            <li class="{{ $request->segment(1) == 'home' ? 'active' : '' }}">
                <a href="{{ url('/') }}">
                    <i class="fa fa-wrench"></i>
                    <span class="title">@lang('global.app_dashboard')</span>
                </a>
            </li>
            @endcan


             @can( 'trainer_meta_access')
            <li class="{{ $request->segment(2) == 'trainerhome' ? 'active' : '' }}">
                <a href="{{ route('admin.trainerhome.index') }}">
                    <i class="fa fa-gears"></i>
                    <span class="title">@lang('global.trainerhome.title')</span>
                </a>
            </li>
            @endcan

            


            @can('trainer_meta_access')

            <li class="{{ $request->segment(2) == 'onetwenty' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.index') }}"> 
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.onetwentyhours.title')
                    </span>
                </a>
            </li>
            @endcan


            @can('trainer_meta_access')
            <li class="{{ $request->segment(2) == 'oneninety' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.oneninetyhours.index') }}">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.oneninetyhours.title')
                    </span>
                </a>
            </li>
            @endcan

           

            @can( 'trainer_meta_access_nil')
            <li class="{{ $request->segment(2) == 'traineelist' ? 'active' : '' }}">
                <a href="{{ route('admin.traineelist.index') }}">
                    <i class="fa fa-list"></i>
                    <span class="title">@lang('global.traineelist.title')</span>
                </a>
            </li>
            @endcan

             @can( 'trainer_meta_access')
            <li class="{{ $request->segment(2) == 'courses' ? 'active' : '' }}">
                <a href="{{ route('admin.courses.index') }}">
                    <i class="fa fa-list"></i>
                    <span class="title">Trainees</span>
                </a>
            </li>
            @endcan



            @can( 'trainer_meta_access')
            <li class="{{ $request->segment(2) == 'notifications' ? 'active' : '' }}">
                <a href="{{ route('admin.notifications.index') }}">
                    <i class="fa fa-list"></i>
                    <span class="title">@lang('global.notifications.title')</span>
                </a>
            </li>
            @endcan


            @can( 'trainer_meta_access')
            <li class="{{ $request->segment(2) == 'canned' ? 'active' : '' }}">
                <a href="{{ route('admin.canned.index') }}">
                    <i class="fa fa-list"></i>
                    <span class="title">@lang('global.canned.title')</span>
                </a>
            </li>
            @endcan

            {{-- trainee menu panel --}}


            @can('trainee_meta_access_nil')

            <li class="{{ $request->segment(2) == 'onetwenty' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.index') }}">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.onetwentyhours.title')
                    </span>
                </a>
            </li>
            @endcan

            @can('trainee_meta_access_nil')
            <li class="treeview">
               <li class="{{ $request->segment(2) == 'unit-one' ? 'active active-sub' : '' }}">
                <a href="{{ url('admin.onetwentyhours.unit-one.index') }}">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.unit-one.title')
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">

                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                </ul>
            </li>
            @endcan


            @can('trainee_meta_access_nil')
            <li class="treeview">
               <li class="{{ $request->segment(2) == 'unit-two' ? 'active active-sub' : '' }}">
                <a href="{{ url('admin.onetwentyhours.unit-two.index') }}">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.unit-two.title')
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">

                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                </ul>
            </li>
            @endcan


            @can('trainee_meta_access_nil')
            <li class="treeview">
               <li class="{{ $request->segment(2) == 'unit-three' ? 'active active-sub' : '' }}">
                <a href="{{ url('admin.onetwentyhours.unit-three.index') }}">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.unit-three.title')
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">

                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                </ul>
            </li>
            @endcan


            @can('trainee_meta_access_nil')
            <li class="treeview">
               <li class="{{ $request->segment(2) == 'unit-four' ? 'active active-sub' : '' }}">
                <a href="{{ url('admin.onetwentyhours.unit-four.index') }}">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.unit-four.title')
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">

                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                    <li><a href="">module#</a></li>
                </ul>
            </li>
            @endcan

            @can('trainer_meta_access')
            @php ($unread = App\MessengerTopic::countUnread())
            <li class="{{ $request->segment(2) == 'messenger' ? 'active' : '' }} {{ ($unread > 0 ? 'unread' : '') }}">
                <a href="{{ route('admin.messenger.index') }}">
                    <i class="fa fa-envelope"></i>

                    <span>Messages</span>
                    @if($unread > 0)
                        {{ ($unread > 0 ? '('.$unread.')' : '') }}
                    @endif
                </a>
            </li>
            <style>
                .page-sidebar-menu .unread * {
                    font-weight:bold !important;
                }
            </style>
            @endcan

            {{-- 120 hr trainee admin panel --}}


            @can('120hr_trainee_meta_access')

            <li class="{{ $request->segment(2) == 'onetwentyhours' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.index') }}">
                    <i class="fa fa-gears"></i>
                    <span class="title">
                        @lang('global.onetwentyhours.title')
                    </span>
                </a>
            </li>
            @endcan

             @can('120hr_trainee_meta_access')

            <li class="{{ $request->segment(2) == 'onetwentyhourshome' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.home.index') }}">
                    <i class="fa fa-gears"></i>
                    <span class="title">
                        Control Panel
                    </span>
                </a>
            </li>
            @endcan

         {{--    @can('120hr_trainee_meta_access')

            <li class="{{ $request->segment(2) == 'onetwentyhourshome' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.home.index') }}">
                    <i class="fa fa-gears"></i>
                    <span class="title">
                        Control Panel
                    </span>
                </a>
            </li>
            @endcan --}}

            @can('120hr_trainee_meta_access')
            <li class="treeview">
               <li class="{{ $request->segment(2) == 'unit-one' ? 'active active-sub' : '' }}">
                <a href="{{ url('admin.onetwentyhours.unit-one.index') }}">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.unit-one.title')
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">

                    <li class="{{ $request->segment(2) == 'U1M1S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-one.module-one.section-one.index') }}">module</a></li>
                 
                </ul>
            </li>
            @endcan


            @can('120hr_trainee_meta_access')
            <li class="treeview">
               <li class="{{ $request->segment(2) == 'unit-two' ? 'active active-sub' : '' }}">
                <a href="{{ url('admin.onetwentyhours.unit-two.index') }}">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.unit-two.title')
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">

                    <li class="{{ $request->segment(2) == 'U2M1S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-two.module-one.section-one.index') }}">Module 1</a></li>
                {{-- <li class="{{ $request->segment(2) == 'U2M1S2' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-two.module-one.section-two.index') }}">test section</a></li> --}}
                    <li class="{{ $request->segment(2) == 'U2M2S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-two.module-two.section-one.index') }}">Module 2</a></li>
                    <li class="{{ $request->segment(2) == 'U2M3S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-two.module-three.section-one.index') }}">Module 3</a></li>
                    
                </ul>
            </li>
            @endcan


            @can('120hr_trainee_meta_access')
            <li class="treeview">
               <li class="{{ $request->segment(2) == 'unit-three' ? 'active active-sub' : '' }}">
                <a href="{{ url('admin.onetwentyhours.unit-three.index') }}">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.unit-three.title')
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">

                    <li class="{{ $request->segment(2) == 'U3M1S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-three.module-one.section-one.index') }}">Module 1</a></li>
                    <li class="{{ $request->segment(2) == 'U3M2S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-three.module-two.section-one.index') }}">Module 2</a></li>
                    <li class="{{ $request->segment(2) == 'U3M3S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-three.module-three.section-one.index') }}">Module 3</a></li>
                    <li class="{{ $request->segment(2) == 'U3M4S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-three.module-four.section-one.index') }}">Module 4</a></li>
                    <li class="{{ $request->segment(2) == 'U3M5S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-three.module-five.section-one.index') }}">Module 5</a></li>
                    
                </ul>
            </li>
            @endcan


            @can('120hr_trainee_meta_access')
            <li class="treeview">
               <li class="{{ $request->segment(2) == 'unit-four' ? 'active active-sub' : '' }}">
                <a href="{{ url('admin.onetwentyhours.unit-four.index') }}">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.unit-four.title')
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">

                    <li class="{{ $request->segment(2) == 'U4M1S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-four.module-one.section-one.index') }}">Module 1</a></li>
                    <li class="{{ $request->segment(2) == 'U4M2S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-four.module-two.section-one.index') }}">Module 2</a></li>
                    
                </ul>
            </li>
            @endcan

             @can('120hr_trainee_meta_access')
            <li class="treeview">
               <li class="{{ $request->segment(2) == 'unit-five' ? 'active active-sub' : '' }}">
                <a href="{{ url('admin.onetwentyhours.unit-five.index') }}">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.unit-five.title')
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">

                   <li class="{{ $request->segment(2) == 'U5M1S1' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.onetwentyhours.unit-five.module-one.section-one.index') }}">Module 1 - TEYL</a></li>
                    
                    
                </ul>
            </li>
            @endcan

            @can('120hr_trainee_meta_access')
            @php ($unread = App\MessengerTopic::countUnread())
            <li class="{{ $request->segment(2) == 'messenger' ? 'active' : '' }} {{ ($unread > 0 ? 'unread' : '') }}">
                <a href="{{ route('admin.messenger.index') }}">
                    <i class="fa fa-envelope"></i>

                    <span>Messages</span>
                    @if($unread > 0)
                        {{ ($unread > 0 ? '('.$unread.')' : '') }}
                    @endif
                </a>
            </li>
            <style>
                .page-sidebar-menu .unread * {
                    font-weight:bold !important;
                }
            </style>
            @endcan


            {{-- 190 hr trainee admin panel --}}



            @can('190hr_trainee_meta_access')
            <li class="{{ $request->segment(2) == 'oneninetyhours' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.oneninetyhours.index') }}">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.oneninetyhours.title')
                    </span>
                </a>
            </li>
            @endcan

            @can('190hr_trainee_meta_access')
            <li class="treeview">
               <li class="{{ $request->segment(2) == 'oneninetyhours' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.oneninetyhours.onlineteachingpractice.index') }}">
                    <i class="fa fa-envelope"></i>
                    <span class="title">
                        @lang('global.onlineteachingpractice.title')
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">
                    <li class="{{ $request->segment(2) == 'onlineteachingpractice' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.oneninetyhours.onlineteachingpractice.index') }}">Info</a></li>
                    
                </ul>
            </li>
            @endcan
            @can('190hr_trainee_meta_access')
            <li class="treeview">
               <li class="{{ $request->segment(2) == 'unit-one' ? 'active active-sub' : '' }}">
                <a href="{{ url('admin.oneninetyhours.unit-one.index') }}">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.unit-one.title')
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">

                    <li class="{{ $request->segment(2) == 'U1M1S1_L' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.oneninetyhours.unit-one.module-one.section-one.index') }}">Module 1</a>
                    </li>
                </ul>
            </li>
            @endcan


            @can('190hr_trainee_meta_access')
            <li class="treeview">
               <li class="{{ $request->segment(2) == 'unit-two' ? 'active active-sub' : '' }}">
                <a href="{{ url('admin.oneninetyhours.unit-two.index') }}">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.unit-two.title')
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">

                    <li class="{{ $request->segment(2) == 'U2M1S1_L' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.oneninetyhours.unit-two.module-one.section-one.index') }}">Module 1</a>
                    </li>
                    <li class="{{ $request->segment(2) == 'U2M2S1_L' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.oneninetyhours.unit-two.module-two.section-one.index') }}">Module 2</a>
                    </li>
                    <li class="{{ $request->segment(2) == 'U2M3S1_L' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.oneninetyhours.unit-two.module-three.section-one.index') }}">Module 3</a>
                    </li>
                   
                  
                </ul>
            </li>
            @endcan


            @can('190hr_trainee_meta_access')
            <li class="treeview">
               <li class="{{ $request->segment(2) == 'unit-three' ? 'active active-sub' : '' }}">
                <a href="{{ url('admin.oneninetyhours.unit-three.index') }}">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.unit-three.title')
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">

                    <li class="{{ $request->segment(2) == 'U3M1S1_L' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.oneninetyhours.unit-three.module-one.section-one.index') }}">Module 1</a>
                    </li>
                    <li class="{{ $request->segment(2) == 'U3M2S1_L' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.oneninetyhours.unit-three.module-two.section-one.index') }}">Module 2</a>
                    </li>                    
                    <li class="{{ $request->segment(2) == 'U3M3S1_L' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.oneninetyhours.unit-three.module-three.section-one.index') }}">Module 3</a>
                    </li>   
                    <li class="{{ $request->segment(2) == 'U3M4S1_L' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.oneninetyhours.unit-three.module-four.section-one.index') }}">Module 4</a>
                    </li>   
                    <li class="{{ $request->segment(2) == 'U3M5S1_L' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.oneninetyhours.unit-three.module-five.section-one.index') }}">Module 5</a>
                    </li>  
                  
                </ul>
            </li>
            @endcan


            @can('190hr_trainee_meta_access')
            <li class="treeview">
               <li class="{{ $request->segment(2) == 'unit-four' ? 'active active-sub' : '' }}">
                <a href="{{ url('admin.oneninetyhours.unit-four.index') }}">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        @lang('global.unit-four.title')
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">

                    <li class="{{ $request->segment(2) == 'U4M1S1_L' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.oneninetyhours.unit-four.module-one.section-one.index') }}">Module 1</a>
                    </li>
                    <li class="{{ $request->segment(2) == 'U4M2S1_L' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.oneninetyhours.unit-four.module-two.section-one.index') }}">Module 2</a>
                    </li>
                </ul>
            </li>
            @endcan

            @can('190hr_trainee_meta_access')
            @php ($unread = App\MessengerTopic::countUnread())
            <li class="{{ $request->segment(2) == 'messenger' ? 'active' : '' }} {{ ($unread > 0 ? 'unread' : '') }}">
                <a href="{{ route('admin.messenger.index') }}">
                    <i class="fa fa-envelope"></i>

                    <span>Messages</span>
                    @if($unread > 0)
                        {{ ($unread > 0 ? '('.$unread.')' : '') }}
                    @endif
                </a>
            </li>
            <style>
                .page-sidebar-menu .unread * {
                    font-weight:bold !important;
                }
            </style>
            @endcan


        @can('admin_meta_access')
        
        <li class="{{ $request->segment(2) == 'onetwentyhours' ? 'active active-sub' : '' }}">
            <a href="{{ route('admin.onetwentyhours.index') }}">
                <i class="fa fa-book"></i>
                <span class="title">
                    @lang('global.onetwentyhours.title')
                </span>
            </a>
        </li>
        @endcan
        
        
        @can('admin_meta_access')
        <li class="{{ $request->segment(2) == 'oneninetyhours' ? 'active active-sub' : '' }}">
            <a href="{{ route('admin.oneninetyhours.index') }}">
                <i class="fa fa-book"></i>
                <span class="title">
                    @lang('global.oneninetyhours.title')
                </span>
            </a>
        </li>
        @endcan
        
        
        @can('admin_meta_access_nil')
        <li class="{{ $request->segment(2) == 'createcourse' ? 'active active-sub' : '' }}">
            <a href="{{ route('admin.createcourse.index') }}">
                <i class="fa fa-gears"></i>
                <span class="title">
                    @lang('global.createcourse.title')
                </span>
            </a>
        </li>
        @endcan
        
        
        @can('admin_meta_access')
        <li class="treeview">
            <a href="#">
                <i class="fa fa-users"></i>
                <span class="title">@lang('global.user-management.title')</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">

                @can('permission_access')
                <li class="{{ $request->segment(2) == 'permissions' ? 'active active-sub' : '' }}">
                    <a href="{{ route('admin.permissions.index') }}">
                        <i class="fa fa-briefcase"></i>
                        <span class="title">
                            @lang('global.permissions.title')
                        </span>
                    </a>
                </li>
                @endcan


                @can('role_access')
                <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                    <a href="{{ route('admin.roles.index') }}">
                        <i class="fa fa-briefcase"></i>
                        <span class="title">
                            @lang('global.roles.title')
                        </span>
                    </a>
                </li>
                @endcan


                @can('user_access')
                <li class="{{ $request->segment(2) == 'users' ? 'active active-sub' : '' }}">
                    <a href="{{ route('admin.users.index') }}">
                        <i class="fa fa-user"></i>
                        <span class="title">
                            @lang('global.users.title')
                        </span>
                    </a>
                </li>
                @endcan


            </ul>
        </li>
        @endcan

        @can('admin_meta_access')
        <li class="{{ $request->segment(2) == 'traineelist' ? 'active active-sub' : '' }}">
            <a href="{{ route('admin.traineelist.index') }}">
                <i class="fa fa-user"></i>
                <span class="title">
                    @lang('global.traineelist.title')
                </span>
            </a>
        </li>
        @endcan

        @can('admin_meta_access')
        <li class="{{ $request->segment(2) == 'trainerlist' ? 'active active-sub' : '' }}">
            <a href="{{ route('admin.trainerlist.index') }}">
                <i class="fa fa-user"></i>
                <span class="title">
                    @lang('global.trainerlist.title')
                </span>
            </a>
        </li>
        @endcan


        @can('admin_meta_access')
        <li class="{{ $request->segment(2) == 'courses' ? 'active' : '' }}">
            <a href="{{ route('admin.courses.index')}}">
                <i class="fa fa-gears"></i>
                <span class="title">@lang('global.courses.title')</span>
            </a>
        </li>
        @endcan

        
        @can('admin_meta_access')
        <li class="{{ $request->segment(2) == 'lessons' ? 'active' : '' }}">
            <a href="{{ route('admin.lessons.index') }}">
                <i class="fa fa-gears"></i>
                <span class="title">@lang('global.lessons.title')</span>
            </a>
        </li>
        @endcan

        
        @can('admin_meta_access')
        <li class="{{ $request->segment(2) == 'questions' ? 'active' : '' }}">
            <a href="{{ route('admin.questions.index') }}">
                <i class="fa fa-question"></i>
                <span class="title">@lang('global.questions.title')</span>
            </a>
        </li>
        @endcan

        
        @can('admin_meta_access')
        <li class="{{ $request->segment(2) == 'questions_options' ? 'active' : '' }}">
            <a href="{{ route('admin.questions_options.index') }}">
                <i class="fa fa-gears"></i>
                <span class="title">@lang('global.questions-options.title')</span>
            </a>
        </li>
        @endcan

        
        @can('admin_meta_access')
        <li class="{{ $request->segment(2) == 'tests' ? 'active' : '' }}">
            <a href="{{ route('admin.tests.index') }}">
                <i class="fa fa-gears"></i>
                <span class="title">@lang('global.tests.title')</span>
            </a>
        </li>
        @endcan

        {{-- appts --}}
        @can('admin_meta_access')
            <li class="{{ $request->segment(2) == 'appointments' ? 'active' : '' }}">
                <a href="{{ route('admin.appointments.index') }}">
                    <i class="fa fa-calendar"></i>
                    <span class="title">@lang('global.appointments.title')</span>
                </a>
            </li>
        @endcan



        {{-- message system --}}
        @can('admin_meta_access')
        @php ($unread = App\MessengerTopic::countUnread())
        <li class="{{ $request->segment(2) == 'messenger' ? 'active' : '' }} {{ ($unread > 0 ? 'unread' : '') }}">
            <a href="{{ route('admin.messenger.index') }}">
                <i class="fa fa-envelope"></i>

                <span>Messages</span>
                @if($unread > 0)
                    {{ ($unread > 0 ? '('.$unread.')' : '') }}
                @endif
            </a>
        </li>
        <style>
            .page-sidebar-menu .unread * {
                font-weight:bold !important;
            }
        </style>
        @endcan

      {{--   <li class="{{ $request->segment(2) == 'devtest' ? 'active' : '' }}">
            <a href="{{ url('admin.devtest.index') }}">
                <i class="fa fa-book"></i>
                <span class="title">@lang('global.devtests.title')</span>
            </a>
        </li> --}}
        
        
        <li class="{{ $request->segment(1) == 'change_password' ? 'active' : '' }}">
            <a href="{{ route('auth.change_password') }}">
                <i class="fa fa-key"></i>
                <span class="title">Change password</span>
            </a>
        </li>



        
        <li>
            <a href="#logout" onclick="$('#logout').submit();">
                <i class="fa fa-arrow-left"></i>
                <span class="title">@lang('global.app_logout')</span>
            </a>
        </li>
    </ul>
</section>
</aside>
{!! Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
<button type="submit">@lang('global.logout')</button>
{!! Form::close() !!}


