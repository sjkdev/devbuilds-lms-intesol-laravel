<?php

// dev test routes

//--------- / dev test routes
Route::get('/admin/devtest', 'Admin\DevtestController@index')->name('admin.devtest.index');
Route::resource('/admin/devtest', 'Admin\DevtestController');

 
Route::get('/', 'HomeController@index');
Route::get('/test','test\TestController@getTest');





// messenger routes without middleware


    Route::get('/admin/messenger/inbox', 'Admin\MessengerController@inbox')->name('admin.messenger.inbox');
    Route::get('/admin/messenger/reply', 'Admin\MessengerController@reply')->name('admin.messenger.reply');
    Route::put('/admin/messenger/show', 'Admin\MessengerController@update')->name('admin.messenger.show');
    Route::get('/admin/messenger/create', 'Admin\MessengerController@create')->name('admin.messenger.create');
    Route::get('/admin/messenger/outbox', 'Admin\MessengerController@outbox')->name('admin.messenger.outbox');
    Route::resource('messenger', 'Admin\MessengerController');

// -----------users  ----------- \\
Route::get('admin/traineelist', 'Admin\TraineeListController@traineeUser')->name('admin.traineelist.index');
Route::get('admin/traineelist', 'Admin\TraineeListController@traineeUser')->name('admin.traineelist.mass_destroy');

//  ----------   120 hour course routes ---------- \\


// 120 Hour Course
// home page
Route::get('/admin/onetwentyhours', 'Admin\OnetwentyhourhomepageController@index')->name('admin.onetwentyhours.index');
Route::get('/admin/onetwentyhours/home', 'Admin\OnetwentyhourhomepageController@indexHome')->name('admin.onetwentyhours.home.index');


Route::get('/admin/onetwentyhours/home', 'Admin\OnetwentyhourhomepageController@indexHome')->name('admin.onetwentyhours.home.index');

// 120 Hour Course
// unit one module one


Route::get('/admin/onetwentyhours/unit-one/module-one/section-one', 'Admin\Int120u1m1s1Controller@index')->name('admin.onetwentyhours.unit-one.module-one.section-one.index');

Route::post('/admin/onetwentyhours/unit-one/module-one/section-one', 'Admin\Int120u1m1s1Controller@store')->name('admin.onetwentyhours.unit-one.module-one.section-one.index');

Route::post('/admin/onetwentyhours/unit-one/module-one/section-one/t1', 'Admin\Int120u1m1s1t1Controller@store');
Route::post('/admin/onetwentyhours/unit-one/module-one/section-one/t2', 'Admin\Int120u1m1s1t2Controller@store');
Route::post('/admin/onetwentyhours/unit-one/module-one/section-one/t3', 'Admin\Int120u1m1s1t3Controller@store');
Route::post('/admin/onetwentyhours/unit-one/module-one/section-one/t4', 'Admin\Int120u1m1s1t4Controller@store');



// 120 Hour Course
// unit two module one
Route::get('/admin/onetwentyhours/unit-two/module-one/section-one', 'Admin\Model120hrUnitTwoModuleOneSectionOneController@index')->name('admin.onetwentyhours.unit-two.module-one.section-one.index');

Route::post('/admin/onetwentyhours/unit-two/module-one/section-one', 'Admin\Int120u2m1s1Controller@store@show'); 


Route::post('/admin/onetwentyhours/unit-two/module-one/section-one/t1', 'Admin\Int120u2m1s1t1Controller@store');
Route::post('/admin/onetwentyhours/unit-two/module-one/section-one/t2', 'Admin\Int120u2m1s1t2Controller@store');
Route::post('/admin/onetwentyhours/unit-two/module-one/section-one/t3', 'Admin\Int120u2m1s1t3Controller@store');
Route::post('/admin/onetwentyhours/unit-two/module-one/section-one/t4', 'Admin\Int120u2m1s1t4Controller@store');
Route::post('/admin/onetwentyhours/unit-two/module-one/section-one/t5', 'Admin\Int120u2m1s1t5Controller@store');
Route::post('/admin/onetwentyhours/unit-two/module-one/section-one/t6', 'Admin\Int120u2m1s1t6Controller@store');







//120 unit two module section two test page
Route::get('/admin/onetwentyhours/unit-two/module-one/section-two', 'Admin\Model120hrUnitTwoModuleOneSectionOneController@indexTwo')->name('admin.onetwentyhours.unit-two.module-one.section-two.index');

Route::post('/admin/onetwentyhours/unit-two/module-two/section-one', 'Admin\Int120u2m2s1Controller@store'); 


// 120 Hour Course
// unit two module two
Route::get('/admin/onetwentyhours/unit-two/module-two/section-one', 'Admin\Model120hrUnitTwoModuleTwoSectionOneController@index')->name('admin.onetwentyhours.unit-two.module-two.section-one.index');

Route::post('/admin/onetwentyhours/unit-two/module-two/section-one', 'Admin\Int120u2m2s1Controller@store@show'); 


Route::post('/admin/onetwentyhours/unit-two/module-two/section-one/t1', 'Admin\Int120u2m2s1t1Controller@store');
Route::post('/admin/onetwentyhours/unit-two/module-two/section-one/t2', 'Admin\Int120u2m2s1t2Controller@store');
Route::post('/admin/onetwentyhours/unit-two/module-two/section-one/t3', 'Admin\Int120u2m2s1t3Controller@store');
Route::post('/admin/onetwentyhours/unit-two/module-two/section-one/t4', 'Admin\Int120u2m2s1t4Controller@store');


// 120 Hour Course
// unit two module three
Route::get('/admin/onetwentyhours/unit-two/module-three/section-one', 'Admin\Model120hrUnitTwoModuleThreeSectionOneController@index')->name('admin.onetwentyhours.unit-two.module-three.section-one.index');

Route::post('/admin/onetwentyhours/unit-two/module-three/section-one', 'Admin\Int120u2m3s1Controller@store'); 

// Route::post('/admin/onetwentyhours/unit-two/module-three/section-one/t1', 'Admin\Int120u2m3s1t1Controller@store');


// 120 Hour Course
// unit three module one
Route::get('/admin/onetwentyhours/unit-three/module-one/section-one', 'Admin\Int120u3m1s1Controller@index')->name('admin.onetwentyhours.unit-three.module-one.section-one.index');


Route::post('/admin/onetwentyhours/unit-three/module-one/section-one', 'Admin\Int120u3m1s1Controller@store'); 

Route::post('/admin/onetwentyhours/unit-three/module-one/section-one/t1', 'Admin\Int120u3m1s1t1Controller@store'); 
Route::post('/admin/onetwentyhours/unit-three/module-one/section-one/t2', 'Admin\Int120u3m1s1t2Controller@store'); 



// 120 Hour Course
// unit three module two
Route::get('/admin/onetwentyhours/unit-three/module-two/section-one', 'Admin\Model120hrUnitThreeModuleTwoSectionOneController@index')->name('admin.onetwentyhours.unit-three.module-two.section-one.index');

Route::post('/admin/onetwentyhours/unit-three/module-two/section-one', 'Admin\Int120u3m2s1Controller@store'); 
Route::post('/admin/onetwentyhours/unit-three/module-two/section-one/t1', 'Admin\Int120u3m2s1t1Controller@store'); 
Route::post('/admin/onetwentyhours/unit-three/module-two/section-one/t2', 'Admin\Int120u3m2s1t2Controller@store'); 
Route::post('/admin/onetwentyhours/unit-three/module-two/section-one/t3', 'Admin\Int120u3m2s1t3Controller@store'); 

// 120 Hour Course
// unit three module three
Route::get('/admin/onetwentyhours/unit-three/module-three/section-one', 'Admin\Model120hrUnitThreeModuleThreeSectionOneController@index')->name('admin.onetwentyhours.unit-three.module-three.section-one.index');

Route::post('/admin/onetwentyhours/unit-three/module-three/section-one', 'Admin\Int120u3m3s1Controller@store');
Route::post('/admin/onetwentyhours/unit-three/module-three/section-one/t1', 'Admin\Int120u3m3s1t1Controller@store'); 
Route::post('/admin/onetwentyhours/unit-three/module-three/section-one/t2', 'Admin\Int120u3m3s1t2Controller@store'); 
Route::post('/admin/onetwentyhours/unit-three/module-three/section-one/t3', 'Admin\Int120u3m3s1t3Controller@store');
Route::post('/admin/onetwentyhours/unit-three/module-three/section-one/t4', 'Admin\Int120u3m3s1t4Controller@store'); 

// 120 Hour Course
// unit three module four
Route::get('/admin/onetwentyhours/unit-three/module-four/section-one', 'Admin\Model120hrUnitThreeModuleFourSectionOneController@index')->name('admin.onetwentyhours.unit-three.module-four.section-one.index');

// 120 Hour Course
// unit three module five
Route::get('/admin/onetwentyhours/unit-three/module-five/section-one', 'Admin\Model120hrUnitThreeModuleFiveSectionOneController@index')->name('admin.onetwentyhours.unit-three.module-five.section-one.index');

Route::post('/admin/onetwentyhours/unit-three/module-five/section-one', 'Admin\Int120u3m5s1Controller@store');

// 120 Hour Course
// unit four module one
Route::get('/admin/onetwentyhours/unit-four/module-one/section-one', 'Admin\Model120hrUnitFourModuleOneSectionOneController@index')->name('admin.onetwentyhours.unit-four.module-one.section-one.index');

Route::post('/admin/onetwentyhours/unit-four/module-one/section-one', 'Admin\Int120u4m1s1Controller@store'); 

// 120 Hour Course
// unit four module two
Route::get('/admin/onetwentyhours/unit-four/module-two/section-one', 'Admin\Model120hrUnitFourModuleTwoSectionOneController@index')->name('admin.onetwentyhours.unit-four.module-two.section-one.index');
Route::post('/admin/onetwentyhours/unit-four/module-two/section-one', 'Admin\Int120u4m2s1Controller@store'); 

Route::post('/admin/onetwentyhours/unit-four/module-two/section-one/t1', 'Admin\Int120u4m2s1t1Controller@store'); 
Route::post('/admin/onetwentyhours/unit-four/module-two/section-one/t2', 'Admin\Int120u4m2s1t2Controller@store');




// 120 Hour Course
// unit five module one
Route::get('/admin/onetwentyhours/unit-five/module-one/section-one', 'Admin\Model120hrUnitFiveModuleOneSectionOneController@index')->name('admin.onetwentyhours.unit-five.module-one.section-one.index');

Route::post('/admin/onetwentyhours/unit-five/module-one/section-one', 'Admin\Int120u5m1s1Controller@store'); 



// // ----------  190 hour course routes  ---------- \\
 

// 190 hour course
// homepage
Route::get('/admin/oneninetyhours', 'Admin\OneninetyhourhomepageController@index')->name('admin.oneninetyhours.index');


// 190 Hour Course
// unit one module one
Route::get('/admin/oneninetyhours/unit-one/module-one/section-one', 'Admin\Model190hrUnitOneModuleOneSectionOneController@index')->name('admin.oneninetyhours.unit-one.module-one.section-one.index');


// 190 Hour Course
// unit two module one
Route::get('/admin/oneninetyhours/unit-two/module-one/section-one', 'Admin\Model190hrUnitTwoModuleOneSectionOneController@index')->name('admin.oneninetyhours.unit-two.module-one.section-one.index');

// 190 Hour Course
// unit two module two
Route::get('/admin/oneninetyhours/unit-two/module-two/section-one', 'Admin\Model190hrUnitTwoModuleTwoSectionOneController@index')->name('admin.oneninetyhours.unit-two.module-two.section-one.index');


// 190 Hour Course
// unit two module three
Route::get('/admin/oneninetyhours/unit-two/module-three/section-one', 'Admin\Model190hrUnitTwoModuleThreeSectionOneController@index')->name('admin.oneninetyhours.unit-two.module-three.section-one.index');


// 190 Hour Course
// unit three module one
Route::get('/admin/oneninetyhours/unit-three/module-one/section-one', 'Admin\Model190hrUnitThreeModuleOneSectionOneController@index')->name('admin.oneninetyhours.unit-three.module-one.section-one.index');

// 190 Hour Course
// unit three module two
Route::get('/admin/oneninetyhours/unit-three/module-two/section-one', 'Admin\Model190hrUnitThreeModuleTwoSectionOneController@index')->name('admin.oneninetyhours.unit-three.module-two.section-one.index');

// 190 Hour Course
// unit three module three
Route::get('/admin/oneninetyhours/unit-three/module-three/section-one', 'Admin\Model190hrUnitThreeModuleThreeSectionOneController@index')->name('admin.oneninetyhours.unit-three.module-three.section-one.index');

// 190 Hour Course
// unit three module four
Route::get('/admin/oneninetyhours/unit-three/module-four/section-one', 'Admin\Model190hrUnitThreeModuleFourSectionOneController@index')->name('admin.oneninetyhours.unit-three.module-four.section-one.index');

// 190 Hour Course
// unit three module five
Route::get('/admin/oneninetyhours/unit-three/module-five/section-one', 'Admin\Model190hrUnitThreeModuleFiveSectionOneController@index')->name('admin.oneninetyhours.unit-three.module-five.section-one.index');

// 190 Hour Course
// unit four module one
Route::get('/admin/oneninetyhours/unit-four/module-one/section-one', 'Admin\Model190hrUnitFourModuleOneSectionOneController@index')->name('admin.oneninetyhours.unit-four.module-one.section-one.index');

// 190 Hour Course
// unit four module two
Route::get('/admin/oneninetyhours/unit-four/module-two/section-one', 'Admin\Model190hrUnitFourModuleTwoSectionOneController@index')->name('admin.oneninetyhours.unit-four.module-two.section-one.index');

// 190 Hour Course
// online teaching practice module
Route::get('/admin/oneninetyhours/onlineteachingpractice', 'Admin\Model190hrOnlineTeachingPracticeController@index')->name('admin.oneninetyhours.onlineteachingpractice.index');



// Trainee routes
// trainee list
Route::get('/admin/traineelist', 'Admin\TraineeListController@index')->name('admin.traineelist.index');
Route::get('/admin/traineelist', 'Admin\TraineeListController@isTrainee')->name('admin.traineelist.index');

// Trainee routes
// trainee list
Route::get('/admin/traineelist', 'Admin\UsersController@indexTrainee')->name('admin.traineelist.index');

// Trainer routes
// trainer list
Route::get('/admin/trainerlist', 'Admin\TrainerListController@index')->name('admin.trainerlist.index');
Route::get('/admin/trainerlist', 'Admin\UsersController@indexTrainer')->name('admin.trainerlist.index');
// Route::get('admin/trainerlist', 'Admin\TrainerListController@create')->name('admin.trainerlist.create');

// Notifications route routes
// notifications 
Route::get('/admin/notifications', 'Admin\NotificationsController@index')->name('admin.notifications.index');
Route::post('/admin/notifications', 'Admin\NotificationsController@store')->name('admin.notifications.index');

// Canned routes
// Canned
Route::get('/admin/canned', 'Admin\CannedController@index')->name('admin.canned.index');


// trainer home route
Route::get('/admin/trainerhome', 'Admin\TrainerHomeController@index')->name('admin.trainerhome.index');
// Route::resource('/admin/trainerhome', 'Admin\TrainerHomeController');
// Create course routes
// create course
Route::get('/admin/createcourse', 'Admin\CreateCourseController@index')->name('admin.createcourse.index');

Route::get('/admin/createcourses', 'Admin\UsersController@userList');

// Create course routes
// create course
Route::get('/admin/courses', 'Admin\CoursesController@show@index')->name('admin.courses.index');

// messages routes
// create course
Route::get('/admin/messages', 'Admin\MessagesController@show@index')->name('admin.messages.index');

// online teaching practice routes
// `OTP
Route::get('/admin/onlineteachingpractice', 'Admin\Model190hrOnlineTeachingPracticeController@show@index')->name('admin.onlineteachingpractice.index');


// ------------------------------------------------------------------------- \\
//     messages route 
// ------------------------------------\\

// admin messages
Route::get('/messages', 'MessagesController@index')->name('messages');



// Route::get('/messages', 'MessagesController@index')->name('messages');

Route::group(['prefix' => 'messages'], function () {
    Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
    Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
    Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
});


// ----------------------------------------------------------------------------- \\

Route::get('course/{slug}', ['uses' => 'CoursesController@show', 'as' => 'courses.show']);
Route::post('course/payment', ['uses' => 'CoursesController@payment', 'as' => 'courses.payment']);
Route::post('course/{course_id}/rating', ['uses' => 'CoursesController@rating', 'as' => 'courses.rating']);

Route::get('lesson/{course_id}/{slug}', ['uses' => 'LessonsController@show', 'as' => 'lessons.show']);
Route::post('lesson/{slug}/test', ['uses' => 'LessonsController@test', 'as' => 'lessons.test']);

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->post('logout', 'Auth\LoginController@logout')->name('auth.logout');

// Registration Routes...
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('auth.register');
$this->post('register', 'Auth\RegisterController@register')->name('auth.register');

// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
$this->patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

Route::group(['middleware' => ['admin'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/home', 'Admin\DashboardController@index');
    // permission
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::post('permissions_mass_destroy', ['uses' => 'Admin\PermissionsController@massDestroy', 'as' => 'permissions.mass_destroy']);
    // roles
    Route::resource('roles', 'Admin\RolesController');
    Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
    Route::resource('users', 'Admin\UsersController');
    Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);
    //courses
    Route::resource('courses', 'Admin\CoursesController');
    Route::post('courses_mass_destroy', ['uses' => 'Admin\CoursesController@massDestroy', 'as' => 'courses.mass_destroy']);
    Route::post('courses_restore/{id}', ['uses' => 'Admin\CoursesController@restore', 'as' => 'courses.restore']);
    Route::delete('courses_perma_del/{id}', ['uses' => 'Admin\CoursesController@perma_del', 'as' => 'courses.perma_del']);

    // create courses
    Route::resource('createcourses', 'Admin\CreateCourseController');
    Route::post('createcourses_mass_destroy', ['uses' => 'Admin\CreateCourseController@massDestroy', 'as' => 'createcourses.mass_destroy']);
    Route::post('createcourses_restore/{id}', ['uses' => 'Admin\CreateCourseController@restore', 'as' => 'createcourses.restore']);
    Route::delete('courses_perma_del/{id}', ['uses' => 'Admin\CreateCourseController@perma_del', 'as' => 'createcourses.perma_del']);

    Route::get('createcourses', 'Admin\UsersController@userList');

    // lessons
    Route::resource('lessons', 'Admin\LessonsController');
    Route::post('lessons_mass_destroy', ['uses' => 'Admin\LessonsController@massDestroy', 'as' => 'lessons.mass_destroy']);
    Route::post('lessons_restore/{id}', ['uses' => 'Admin\LessonsController@restore', 'as' => 'lessons.restore']);
    Route::delete('lessons_perma_del/{id}', ['uses' => 'Admin\LessonsController@perma_del', 'as' => 'lessons.perma_del']);
    // questions
    Route::resource('questions', 'Admin\QuestionsController');
    Route::post('questions_mass_destroy', ['uses' => 'Admin\QuestionsController@massDestroy', 'as' => 'questions.mass_destroy']);
    Route::post('questions_restore/{id}', ['uses' => 'Admin\QuestionsController@restore', 'as' => 'questions.restore']);
    Route::delete('questions_perma_del/{id}', ['uses' => 'Admin\QuestionsController@perma_del', 'as' => 'questions.perma_del']);
    // questions options
    Route::resource('questions_options', 'Admin\QuestionsOptionsController');
    Route::post('questions_options_mass_destroy', ['uses' => 'Admin\QuestionsOptionsController@massDestroy', 'as' => 'questions_options.mass_destroy']);
    Route::post('questions_options_restore/{id}', ['uses' => 'Admin\QuestionsOptionsController@restore', 'as' => 'questions_options.restore']);
    Route::delete('questions_options_perma_del/{id}', ['uses' => 'Admin\QuestionsOptionsController@perma_del', 'as' => 'questions_options.perma_del']);
    // messages
    Route::resource('messages', 'Admin\MessagesController');
    Route::post('messages_mass_destroy', ['uses' => 'Admin\MessagesController@massDestroy', 'as' => 'messages.mass_destroy']);
    Route::post('messages_restore/{id}', ['uses' => 'Admin\MessagesController@restore', 'as' => 'messages.restore']);
    Route::delete('messages_perma_del/{id}', ['uses' => 'Admin\MessagesController@perma_del', 'as' => 'messages.perma_del']);
    // tests
    Route::resource('tests', 'Admin\TestsController');
    Route::post('tests_mass_destroy', ['uses' => 'Admin\TestsController@massDestroy', 'as' => 'tests.mass_destroy']);
    Route::post('tests_restore/{id}', ['uses' => 'Admin\TestsController@restore', 'as' => 'tests.restore']);
    Route::delete('tests_perma_del/{id}', ['uses' => 'Admin\TestsController@perma_del', 'as' => 'tests.perma_del']);
    // spatie media uploader
    Route::post('/spatie/media/upload', 'Admin\SpatieMediaController@create')->name('media.upload');
    Route::post('/spatie/media/remove', 'Admin\SpatieMediaController@destroy')->name('media.remove');
    // messenger section 
 Route::model('messenger', 'App\MessengerTopic');
    Route::get('messenger/inbox', 'Admin\MessengerController@inbox')->name('messenger.inbox');
    Route::get('messenger/outbox', 'Admin\MessengerController@outbox')->name('messenger.outbox');
    Route::resource('messenger', 'Admin\MessengerController');
    // appointments
    Route::resource('appointments', 'Admin\AppointmentsController');
    Route::post('appointments_mass_destroy', ['uses' => 'Admin\AppointmentsController@massDestroy', 'as' => 'appointments.mass_destroy']);
});
